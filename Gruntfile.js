var clc = require('cli-color');
var error = clc.red.bold;
var warn = clc.yellow;
var notice = clc.cyan;
var os = require('os');
var weinreAddr = '';
/*if(os.hostname() == 'DEV-HDMSVPN' || os.hostname() == 'GHQ-PC07') {
  weinreAddr = '<script src="http://dev-hdmsvpn.nightlife.com.au:9999/target/target-script-min.js#anonymous"></script>';
} */
var reloadAddr = '';


module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    clean: {
      files: ['build'],
			options: { force : true},
      source: ['sourcePackage.zip', 'sourcePackage']
    },
    uglify: {
      files: {
        'build/assets/js/<%= pkg.name %>.js': ['src/**/*.js', '!src/assets/js/lib/**/*.js']
      },
      banner: 'var win=window,doc=document,$win=$(win),$doc=$(doc);\n',
      development: {
        options: {
          banner: '<%= uglify.banner %>',
          mangle: false,
          beautify: true,
          compress: {
            drop_debugger: false
          }
        },
        files: '<%= uglify.files %>'
      },
      production: {
        options: {
          banner: '<%= uglify.banner %>',
          mangle: true,
          compress: {
            drop_debugger: true
          }
        },
        files: '<%= uglify.files %>'
      }
    },
    jshint: {
      files: ['Gruntfile.js', 'src/**/*.js', '!src/assets/js/lib/**/*.js'],
      options: {
        bitwise: false,
        //camelcase: true, //disabled due to json packet using _'s
        curly: true,
        //indent: 2, //disabled as it expects the end brace to be indented :(
        latedef: true,
        newcap: true,
        noarg: true,
        noempty: true,
        nonew: true,
        smarttabs: true,
        quotmark: 'single',
        trailing: false,
        maxdepth: 4,
        maxcomplexity: 12,
        maxlen: 160
      },
      development: {
        files: {
          src: '<%= jshint.files %>'
        },
        options:{
          debug: true
        }
      },
      production: {
        files: {
          src: '<%= jshint.files %>'
        }
      }
    },

    recess: {
      options: {
        compile: true
      },
      development: {
        src: ['src/assets/css/nightlife.less'],
        dest: 'build/assets/css/nightlife.css'
      },
      production: {
        options: {
          compress: true
        },
        src: ['src/assets/css/nightlife.less'],
        dest: 'build/assets/css/nightlife.css'
      }
    },
    concat: {
      // Combine all templates together
      build: {
        files: {
          'build/views/templates.html': 'src/views/**/*.html',
          'build/assets/js/<%= pkg.name %>.libs.js': [
          'src/assets/js/lib/loglevel.js',
          'src/assets/js/lib/jquery-1.10.2.min.js',
          'src/assets/js/lib/knockout-2.3.0.js',
          'src/assets/js/lib/jquery-ui-1.10.4.custom.min.js',
          'src/assets/js/lib/jquery.ui.touch-punch.min.js',
          'src/assets/js/lib/bootstrap/transition.js',
          'src/assets/js/lib/bootstrap/alert.js',
          'src/assets/js/lib/bootstrap/button.js',
          'src/assets/js/lib/bootstrap/carousel.js',
          'src/assets/js/lib/bootstrap/collapse.js',
          'src/assets/js/lib/bootstrap/dropdown.js',
          'src/assets/js/lib/bootstrap/modal.js',
          'src/assets/js/lib/bootstrap/tooltip.js',
          'src/assets/js/lib/bootstrap/popover.js',
          'src/assets/js/lib/bootstrap/scrollspy.js',
					'src/assets/js/lib/bootstrap/typeahead.js',
          'src/assets/js/lib/bootstrap/tab.js',
          'src/assets/js/lib/bootstrap/affix.js',
          'src/assets/js/lib/sockjs-0.3.min.js',
          'src/assets/js/lib/jquery.scrollTo-min.js',
          'src/assets/js/lib/jquery.knob.js',
          'src/assets/js/lib/globalize/globalize.js',
          'src/assets/js/lib/globalize/cultures/globalize.culture.en-AU.js',
          'src/assets/js/lib/globalize/cultures/globalize.culture.en-US.js',
					'src/assets/js/lib/knockout-bootstrap.min.js',
					'src/assets/js/lib/hammer.min.js',
					'src/assets/js/lib/knockouch.min.js',
          'src/assets/js/lib/bootstrap-select.min.js',
					'src/assets/js/lib/keyboard.js',
          'src/assets/js/lib/jquery.loadmask.min.js',
          'src/assets/js/lib/moment.min.js',
          //'src/assets/js/lib/jquery.debounce.min.js'
          'src/assets/js/lib/bootstrap-switch.min.js',
          'src/assets/js/lib/jquery.highlight.js',
          'src/assets/js/lib/selectize.min.js',
          'src/assets/js/lib/resize.js',
          'src/assets/js/lib/knockout.mapping-latest.js',
          'src/assets/js/lib/modernizr.custom.js',
          'src/assets/js/lib/classie.js',
          'src/assets/js/lib/notificationFx.js',
          'src/assets/js/lib/spotify-web-api.js',
          'src/assets/js/lib/farbtastic.js'
          ]
        }
      }
    },
    replace: {
      build: {
        options: {
          variables: {
            'weinerRef': weinreAddr,
            'reloadRef' : reloadAddr
          }
        },
        copy: {
          build: {
            files: [
            {
              expand: true,
              cwd: 'src/assets/',
              src: ['img/**', 'font/**'],
              dest: 'build/assets/',
              filter: 'isFile'
            }, // Copy image assets

            {
              src: 'src/web.config',
              dest: 'build/web.config',
              filter: 'isFile'
            } // Web config
            ]
          },
          source: {
            files: [
            {
              expand: true,
              cwd: 'src/',
              src: ['**/*'],
              dest: 'sourcePackage/src'
            },

            {
              expand: true,
              src: ['Gruntfile.js', 'package.json', 'README.md'],
              dest: 'sourcePackage'
            }
            ]
          }
        },
        files: [
        {
          expand: true,
          cwd: 'src/',
          src: ['index.html'],
          dest: 'build/',
          filter: 'isFile'
        } // Copy views
        ]
      }
    },
    copy: {
      build: {
        files: [
        {
          expand: true,
          cwd: 'src/',
          src: ['list.html'],
          dest: 'build/',
          filter: 'isFile'
        }, // Copy list prototype

        {
          expand: true,
          cwd: 'src/assets/',
          src: ['img/**', 'font/**'],
          dest: 'build/assets/',
          filter: 'isFile'
        }, // Copy image assets

        {
          expand: true,
          cwd: 'src/assets/',
          src: ['toi/**'],
          dest: 'build/assets/',
          filter: 'isFile'
        }, // Copy toi assets
        {
          expand: true,
          cwd: 'src/', 
          src: ['assets/css/**', '!assets/css/**/*.less'],
          dest: 'build/',
          filter: 'isFile'
        } // Copy css assets
        ]
      },
      source: {
        files: [
        {
          expand: true,
          cwd: 'src/',
          src: ['**/*'],
          dest: 'sourcePackage/src'
        },

        {
          expand: true,
          src: ['Gruntfile.js', 'package.json', 'README.md'],
          dest: 'sourcePackage'
        }
        ]
      }
    },

    // Generate an HTML5 application cache manifest file
    manifest: {
      generate: {
        options: {
          basePath: 'build',
          cache: [],
          //              network: ['http://*', 'https://*'],
          //              fallback: ['/ /offline.html'],
          exclude: [],
          preferOnline: true,
          verbose: false,
          timestamp: true,
          hash: false,
          master: ['index.html']
        },
        src: [
        'views/*.html',
        'assets/**/*.*'
        ],
        dest: 'build/manifest.appcache'
      }
    },
    watch: {
      src: {
        files: ['<%= jshint.files %>', 'src/**/*.less', 'src/*.html', 'src/views/**'],
        tasks: ['default']
      },
      options: {
          livereload: true
      }
    },
    zip: {
      'sourcePackage.zip': ['sourcePackage/**/*']
    }
  });

  // Load the various plugins.
  grunt.loadNpmTasks('grunt-replace');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-recess');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-replace');
  grunt.loadNpmTasks('grunt-manifest');
  grunt.loadNpmTasks('grunt-zip');
  grunt.loadNpmTasks('grunt-contrib-clean');

  // Default task(s).
  grunt.registerTask('default', [
    'build-dev',
    'replace:build',
    'copy:build']);

  grunt.registerTask('remote', [
    'build-dev',
    'setLocalIP',
    'replace:remote',
    'copy:build']);

  grunt.registerTask('production', [
    'build-prod',
    'replace:build',
    'copy:build']);

  grunt.registerTask('source', [
    'clean:source',
    'copy:source',
    'zip']);

  // Simplified Build Step - DEV
  grunt.registerTask('build-dev', [
    'clean:files',
    'jshint:development',
    'uglify:development',
    'recess:development',
    'concat'
    ]);

  // Simplified Build Step - PROD
  grunt.registerTask('build-prod', [
    'clean:files',
    'jshint:production',
    'uglify:production',
    'recess:production',
    'concat'
    ]);


  grunt.registerTask('setLocalIP', function() {
    var ipAddress = '127.0.0.1';

    // Find the IP address of the building server
    console.log(warn('Finding IP address'));
    var ifaces = require('os').networkInterfaces();
    var found = false;

    for (var dev in ifaces) {
      for (var i in ifaces[dev]) {
        var details = ifaces[dev][i];
        if (!found && details.family == 'IPv4' && !details.internal) {
          ipAddress = details.address;
          found = true;
        }
      }
    }

    console.log('Using IP: ' + notice(ipAddress) + '\n');
    grunt.config(['replace', 'remote', 'options', 'variables', 'ipAddress'], ipAddress);
  });

  grunt.registerTask('watchRemote', function() {
    grunt.config(['watch', 'src', 'tasks'], ['remote']);

    grunt.task.run('watch');
  });

  /*grunt.registerTask('lessToJS', function() {
    var inStr = grunt.file.read('src/assets/css/colours.less');
    var outStr = 'window.colours = { ' + inStr.replace('@','').replace(';',',');
    grunt.file.write('src/app/data/Colours.js',outStr);
    console.log('big dirty bean bags!');
  });*/
};
