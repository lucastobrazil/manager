var port = 9090;


process.argv.forEach(function (val, index, array) {
	 if(val == '-p' && !isNaN(process.argv[index+1])) {
		console.log('port is ' + process.argv[index+1]);
		port = process.argv[index+1];
	 }
});

var http = require("http");
var connect = require('connect');

var app = connect()

app.use(connect.static(__dirname));
app.use(function(req, res){
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);  
    res.end();  
});



var server = http.createServer(app);

server.listen(port, function () {

console.log('server is listening on port ' + port);
});