
/*
 * Maintain cached value of the window height
 */
var winHeight;
var setWinHeight = function() {
  winHeight = $win.height();
};

var winWidth;
var setWinWidth = function() {
  winWidth = $win.width();
};

var isMiniLayout = ko.observable();
var checkMiniLayout = function(){
  if (winWidth <= 1024){
    isMiniLayout(true);
  }else{
    isMiniLayout(false);
  }
};

var isMiniAd = ko.observable();
var checkMiniAd = function(){
  if (winWidth < winHeight){
    isMiniAd(true);
  }else{
    isMiniAd(false);
  }
};

$doc.ready(function() {
  setWinHeight();
  setWinWidth();
  setTimeout(checkMiniLayout(), 3000);
  setTimeout(checkMiniAd(), 3000);

  $win.resize(function() {
    setWinHeight();
    setWinWidth();
    setTimeout(checkMiniLayout(), 3000);
    setTimeout(checkMiniAd(), 3000);
  });
});

var utils = {
  scrollToTop: function() {
    $('#body-content .tab-content > .tab-pane.active').scrollTop(0);
  },

  /**********************************************
  * Converts the string to an SEO safe name
  * Replace ‘ ‘ with ‘-‘
  * Replace ‘_’ with ‘-‘
  * Replace ‘&’ with ‘and’
  * Remove all non-alphanumeric characters
  * Lower case
  **********************************************/
  toSeo: function(s) {
    var regex = new RegExp('[^a-zA-Z0-9\\-]+', 'gi');
    return s.replace(/_| /g, '-').replace(/&amp;|&/gi, 'and').replace(regex, '').replace(/-+/g, '-').toLowerCase();
  },

  collapseIcon: function(e) {
    if($($($(e)).parent().attr('data-target')).hasClass('collapse')) {
      return 'icon-chevron-up';
    }
    return 'icon-chevron-down';
  },

  isInputDirSupported : function() {
    var tmpInput = document.createElement('input');
    if ('webkitdirectory' in tmpInput ||
         'mozdirectory' in tmpInput ||
         'odirectory' in tmpInput   ||
         'msdirectory' in tmpInput  ||
         'directory' in tmpInput) {
      return true;
    }

    return false;
  },

  getUUID : function() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });
  }

//  Moment : moment

};


/*
 * Object.keys doesn't exist in older browsers
 */
Object.keys = Object.keys || (function() {
  var hasOwnProperty = Object.prototype.hasOwnProperty;
  var hasDontEnumBug = !{
    toString: null
  }.propertyIsEnumerable('toString');
  var DontEnums = [
          'toString',
          'toLocaleString',
          'valueOf',
          'hasOwnProperty',
          'isPrototypeOf',
          'propertyIsEnumerable',
          'constructor'];
  var DontEnumsLength = DontEnums.length;
  return function(o) {
    if (typeof o != 'object' && typeof o != 'function' || o === null) {
      throw new TypeError('Object.keys called on a non-object');
    }

    var result = [];
    for (var name in o) {
      if (hasOwnProperty.call(o, name)) {
        result.push(name);
      }
    }

    if (hasDontEnumBug) {
      for (var i = 0; i < DontEnumsLength; i++) {
        if (hasOwnProperty.call(o, DontEnums[i])) {
          result.push(DontEnums[i]);
        }
      }
    }
    return result;
  };
})();


/* Detect if div in viewport */
(function($){
        $.fn.inViewAdvertising = function(partial,hidden){

                    var $t                                = $(this).eq(0),
                    t                                = $t.get(0),
                    $w                                = $(window),
                    viewTop                   = $(this).scrollParent().offset().top,
                    viewBottom                = viewTop + $(this).scrollParent().height(); //- 36,//$('footer').height(), //fudge factor
                    _top                        = $t.offset().top,
                    _bottom                        = _top + $t.height(),
                    compareTop                = partial === true ? _bottom : _top,
                    compareBottom        = partial === true ? _top : _bottom,
                    clientSize                = hidden === true ? t.offsetWidth * t.offsetHeight : true;
                return !!clientSize && ((compareBottom <= viewBottom) && (compareTop >= viewTop));
    };
})(jQuery);

(function($){
        $.fn.inViewPlaylist = function(partial,hidden){

                    var $t                                = $(this).eq(0),
                    t                                = $t.get(0),
                    $w                                = $(window),
                    viewTop                   = $(this).scrollParent().offset().top,
                    viewBottom                = viewTop + $('#body-wrapper').height() - 36,
                    _top                        = $t.offset().top,
                    _bottom                        = _top + $t.height(),
                    compareTop                = partial === true ? _bottom : _top,
                    compareBottom        = partial === true ? _top : _bottom,
                    clientSize                = hidden === true ? t.offsetWidth * t.offsetHeight : true;
                return !!clientSize && ((compareBottom <= viewBottom) && (compareTop >= viewTop));
    };
})(jQuery);
/*
(function($){
        $.fn.aboveOrBelow = function(partial,hidden){

                    var $t                                = $(this).eq(0),
                    t                                = $t.get(0),
                    $w                                = $(window),
                    viewTop                        = $w.scrollTop() + $('header').height(),
                    viewBottom                = viewTop + $('#body-wrapper').height() - 36, //fudge factor
                    _top                        = $t.offset().top,
                    _bottom                        = _top + $t.height(),
                    compareTop                = partial === true ? _bottom : _top,
                    compareBottom        = partial === true ? _top : _bottom,
                    clientSize                = hidden === true ? t.offsetWidth * t.offsetHeight : true;

                return !!clientSize && ((compareBottom <= viewBottom) && (compareTop >= viewTop));
    };
})(jQuery);*/

/* LOL WUT?
$(document).click(function(event) {

  console.log(event.which);
  if(event.which === 1) {
    event.preventDefault();
  }
});
*/
/* override bootstraps typeahead behaviour */
var newRender = function(items) {
     var that = this;

     items = $(items).map(function (i, item) {
       i = $(that.options.item).attr('data-value', item);
       i.find('a').html(that.highlighter(item));
       return i[0];
     });

     this.$menu.html(items);
     return this;
};
$.fn.typeahead.Constructor.prototype.render = newRender;

$.fn.typeahead.Constructor.prototype.select = function() {
    console.log('OVERRIDETHEMAINFRAME');
    var val = this.$menu.find('.active').attr('data-value');
    if (val) {
      this.$element
        .val(this.updater(val))
        .change();
    }
    return this.hide();
};
