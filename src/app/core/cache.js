win.appCacheReady = true;

// If the cache is updated - force a reload to get the new version
if (win.applicationCache) {
  if(applicationCache.status === applicationCache.DOWNLOADING){
    win.appCacheReady = false;
  }

  applicationCache.addEventListener('downloading', function(){
    win.appCacheReady = false;
  });

  applicationCache.addEventListener('updateready', function() {
    applicationCache.swapCache();
    win.location.reload();
  });

  if(applicationCache.status === applicationCache.UPDATEREADY){
    applicationCache.swapCache();
    win.location.reload();
  }
}