/*
 * Ensure logging is supported
 */
if (log) {
  if (console && console.groupCollapsed === undefined) {
    if (console.group) {
      console.groupCollapsed = console.group;
    } else {
      console.groupCollapsed = function() {
        };
      console.groupEnd = function() {
        };
    }
  }

  log.event = function(event, status, level) {
    if (level === undefined) {
      level = 'debug';
    }

    log[level].call(console, 'Event', {
      Event: event,
      status: status
    });
  };
  log.group = function(title, messages, level) {
    if (level === undefined) {
      level = 'debug';
    }

    if (log.currentLevel <= log.levels[level.toUpperCase()]) {
      console.groupCollapsed(title);
      messages.forEach(function(msg) {
        log[level].call(console, msg);
      });
      console.groupEnd();
    }
  };
  log.setLevel(localStorage['settings.logLevel'] || 'SILENT');
}