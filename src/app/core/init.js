win.controls = {};
win.controllers = {};

$doc.ready(function(){

  win.NL=new NightlifeViewModel();

  // $doc.trigger('nightlife-pre-ready'); // Ebrahim M: Added to get the spotify token

  // Load the templates
  $('#templates').load('views/templates.html', function() {

    controls.navigation.init();

    // Kick off the app
    NL.init();

    // Bind with knockout
    ko.applyBindings(NL);

    $('#body-wrapper').show();

    // Let the rest of the app know it's ok to bind up
    $doc.trigger('nightlife-ready');

  // do something…
  });
        //bind modals to context
    // init any third party libs
});
