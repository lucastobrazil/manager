/* all custom bindings used for the app */

ko.bindingHandlers.numericValue = {
    init : function(element, valueAccessor, allBindingsAccessor) {
        var underlyingObservable = valueAccessor();
        var interceptor = ko.dependentObservable({
            read: underlyingObservable,
            write: function(value) {
                if (!isNaN(value)) {
                    underlyingObservable(parseFloat(value));
                }
            }
        });
        ko.bindingHandlers.value.init(element, function() { return interceptor; }, allBindingsAccessor);
    },
    update : ko.bindingHandlers.value.update
};

ko.bindingHandlers.logger = {
    update: function(element, valueAccessor, allBindings) {
        //store a counter with this element
        var count = ko.utils.domData.get(element, '_ko_logger') || 0,
            data = ko.toJS(valueAccessor() || allBindings());

        ko.utils.domData.set(element, '_ko_logger', ++count);

        if (console && console.log) {
            console.log(count, element, data);
        }
    }
};

/**
 * Knockout binding handler for bootstrapSwitch indicating the status
 * of the switch (on/off): https://github.com/nostalgiaz/bootstrap-switch
 */
ko.bindingHandlers.bootstrapSwitchOn = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        $elem = $(element);
        //$(element).bootstrapSwitch();
        $(element).bootstrapSwitch('setState', ko.utils.unwrapObservable(valueAccessor())); // Set intial state
        $elem.on('switch-change', function (e, data) {
            valueAccessor()(data.value);
        }); // Update the model when changed.
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var vStatus = $(element).bootstrapSwitch('state');
        var vmStatus = ko.utils.unwrapObservable(valueAccessor());
        if (vStatus != vmStatus) {
            $(element).bootstrapSwitch('setState', vmStatus);
        }
    }
};
ko.bindingHandlers.tooltip = {
  init: function (element, valueAccessor, allBindingsAccessor) {
      $(element).tooltip();
      if ('ontouchstart' in window) {
        $('*[data-original-title]').tooltip('disable');
      }
  }
};

ko.bindingHandlers.slider = {
  init: function (element, valueAccessor, allBindingsAccessor) {
    var options = allBindingsAccessor().sliderOptions || {};
    $(element).slider(options);
    ko.utils.registerEventHandler(element, 'slidechange', function (event, ui) {
        var observable = valueAccessor();
        observable(ui.value);
    });
    ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
        $(element).slider('destroy');
    });
    ko.utils.registerEventHandler(element, 'slide', function (event, ui) {
        var observable = valueAccessor();
        observable(ui.value);
    });
  },
  update: function (element, valueAccessor) {
    var value = ko.utils.unwrapObservable(valueAccessor());
    if (isNaN(value)) { value = 0; }
    $(element).slider('value', value);
  }
};


ko.bindingHandlers.farbtastic = {
    init: function(element, valueAccessor, allBindingsAccessor) {
        console.log('value now is =' + valueAccessor());
        var options =  allBindingsAccessor().farbtasticOptions || {};
        
        // $(element).farbtastic(options.targetInput);
        $(element).farbtastic(function(){
            
            var newColor = $.farbtastic($(element)).color;
        
            // $('.toi-textline').css('color', newColor);
            // $('.fontchange').css('color', newColor);
            // $('.dropdown-list li').css('color', newColor);

            // tbProps.textColour = newColor.substr(1);
            // console.log(newColor);
            // NL.advertising.newTOIAsset.textColour(newColor.substr(1));
            var observable = valueAccessor();
            observable(newColor.substr(1));
        });
    }
};


/* replace at index and notify */
ko.observableArray.fn.replaceIndex = function(index, newValue) {
    this.valueWillMutate();
    this()[index] = newValue;
    this.valueHasMutated();
};

ko.bindingHandlers.enableUI = {
    /*init: function(element, valueAccessor) {
        var value = ko.utils.unwrap(valueAccessor()); //unwrapObservable() ?
        if(value) {
            $(element).addClass('ui-state-disabled')
        } else {
            $(element).removeClass('ui-state-disabled')
        }
    },*/
    update: function(element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor()); //unwrapObservable() ?
        if(!value) {
            $(element).addClass('ui-state-disabled');
            $(element).prop('disabled', true);
        } else {
            $(element).removeClass('ui-state-disabled');
            $(element).prop('disabled', false);
        }
    }

};

ko.bindingHandlers.disableClick = {
    init: function(element, valueAccessor) {

        $(element).click(function(evt) {
            //alert('test before');
        });

        $(element).click(function(evt) {
            if (valueAccessor()) {
                evt.preventDefault();
                evt.stopImmediatePropagation();
            }
        });

        //begin of 'hack' to move our 'disable' event handler to to of the stack
        var events = $._data(element, 'events');
        console.log(events);
        var handlers = events.click;

        if (handlers.length == 1) {
            return;
        }

        handlers.splice(0, 0, handlers.pop());
        //end of 'hack' to move our 'disable' event handler to to of the stack


        $(element).click(function(evt) {
            //alert('test after');
        });
    },

    update: function(element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        ko.bindingHandlers.css.update(element, function() {
            return {
                disabled_anchor: value
            };
        });
    }
};

ko.bindingHandlers.datepicker = {
    init: function(element, valueAccessor, allBindingsAccessor) {
        //initialize datepicker with some optional options
        var options = allBindingsAccessor().datepickerOptions || {},
            $el = $(element);

        $el.datepicker(options);

        //handle the field changing
        ko.utils.registerEventHandler(element, 'change', function () {
            var observable = valueAccessor();
            observable($el.datepicker('getDate'));
        });

        //handle disposal (if KO removes by the template binding)
        ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
            $el.datepicker('destroy');
        });

    },
    update: function(element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor()),
            $el = $(element);
        //handle date data coming via json from Microsoft
        if (String(value).indexOf('/Date(') === 0) {
            value = new Date(parseInt(value.replace(/\/Date\((.*?)\)\//gi, '$1'),10));
        }

        var current = $el.datepicker('getDate');

        if (value - current !== 0) {
            $el.datepicker('setDate', value);
        }
    }
};


ko.bindingHandlers.files = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel) {
        var allBindings = allBindingsAccessor();
        var loadedCallback, progressCallback, errorCallback, fileFilter, readAs, maxFileSize;
        var filesBinding = allBindings.files;

        if (typeof(ko.unwrap(filesBinding)) == 'function')
        { loadedCallback = ko.unwrap(filesBinding); }
        else
        {
            loadedCallback = ko.unwrap(filesBinding.onLoaded);
            progressCallback = ko.unwrap(filesBinding.onProgress);
            errorCallback = ko.unwrap(filesBinding.onError);
            fileFilter = ko.unwrap(filesBinding.fileFilter);
            maxFileSize = ko.unwrap(filesBinding.maxFileSize);
            readAs = ko.unwrap(filesBinding.readAs);
        }

        if(typeof(loadedCallback) != 'function')
        { return; }

        var readFile = function(file) {
            var reader = new FileReader();
            reader.onload = function(fileLoadedEvent) {
                loadedCallback(file, fileLoadedEvent.target.result);
            };

            if(typeof(progressCallback) == 'function')
            {
                reader.onprogress = function(fileProgressEvent) {
                    progressCallback(file, fileProgressEvent.loaded, fileProgressEvent.total);
                };
            }

            if(typeof(errorCallback) == 'function')
            {
                reader.onerror = function(fileErrorEvent) {
                    errorCallback(file, fileErrorEvent.target.error);
                };
            }

            if(readAs == 'text')
            { reader.readAsText(file); }
            else if(readAs == 'array')
            { reader.readAsArrayBuffer(file); }
            else if(readAs == 'binary')
            { reader.readAsBinaryString(file); }
            else
            { reader.readAsDataURL(file); }
        };

        var handleFileChangedEvent = function(fileChangedEvent) {
            var files = fileChangedEvent.target.files;
            for (var i = 0; i < files.length; i++) {
                f = files[i];
                if (typeof(fileFilter) != 'undefined' && !f.type.match(fileFilter))
                {
                    if(typeof(errorCallback) == 'function')
                    { errorCallback(f, 'File type does not match filter'); }
                    continue;
                }

                if(typeof(maxFileSize) != 'undefined' && f.size >= maxFileSize)
                {
                    if(typeof(errorCallback) == 'function')
                    { errorCallback(f, 'File exceeds file size limit'); }
                    continue;
                }

                readFile(f);
            }
        };

        element.addEventListener('change', handleFileChangedEvent, false);
    }
};

// extends observable objects intelligently
ko.utils.extendObservable = function ( target, source ) {
    var prop, srcVal, tgtProp, srcProp,
        isObservable = false;

    for ( prop in source ) {

        if ( !source.hasOwnProperty( prop ) ) {
            continue;
        }

        if ( ko.isWriteableObservable( source[prop] ) ) {
            isObservable = true;
            srcVal = source[prop]();
        } else if ( typeof ( source[prop] ) !== 'function' ) {
            srcVal = source[prop];
        }

        if ( ko.isWriteableObservable( target[prop] ) ) {
            target[prop]( srcVal );
        } else if ( target[prop] === null || target[prop] === undefined ) {

            target[prop] = isObservable ? ko.observable( srcVal ) : srcVal;

        } else if ( typeof ( target[prop] ) !== 'function' ) {
            target[prop] = srcVal;
        }

        isObservable = false;
    }
};

// then finally the clone function
ko.utils.clone = function(obj, emptyObj){
    var json = ko.toJSON(obj);
    var js = JSON.parse(json);

    return ko.utils.extendObservable(emptyObj, js);
};
