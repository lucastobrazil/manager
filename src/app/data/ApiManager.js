function ApiManager(NL) {

  var publicFunctions = {
    send: function(event_category, event, obj, callback) {
      // console.error('#####sending', event_category, event, obj );
      sendMessage(event_category, event, obj, null, callback);
    },
    sendMsg: function(msg, protocol){
      sendRawMessage(msg, protocol);
    },
    close: function(reason){
      closeConnection(reason);
    },
    post: function(event_category, event, obj, callback) {
      sendMessage(event_category, event, obj, 'http', callback);
    },
    connect: function(){
      setupSocket();
    },
    applySettings: function(){
      applyUtopiaSettings();
    },
    socketStatus: ko.observable(SocketReadyStateType.Closed)
  };

  $.extend(this, publicFunctions);

  function onmessage(msg, connectionName){
    //alert('sdfsdf');
    // console.error('11111111 ws', msg);
    if(typeof(msg) === 'object'){
      // Skip events
      if (msg.actor && msg.actor.zone && msg.actor.zone !== settings.zone) {
          log.debug('Inbound message ignored (Zone %d) : %s', msg.actor.zone, msg);
          return false;
      }

      // Check the basic data is there
      if (!(msg.verb && msg.verb.event_category && msg.verb.event)) {
          log.warn('Inbound message invalid', msg);
          return false;
      }


      var eventName = msg.verb.event_category + '|' + msg.verb.event;
      log.debug('Inbound Message - ' + eventName + ' (' + connectionName + ')', msg);

      NL.trigger(eventName, [msg]);      
      if(msg.target.request_id !== undefined) {
        if(typeof(NL.callbacks[msg.target.request_id.sent]) === 'function') { // if callback registed for this msg
          NL.callbacks[msg.target.request_id.sent](msg);
          delete NL.callbacks[msg.target.request_id.sent]; // remove from list of callbacks
        } 
      }
    }else{

      log.debug('Inbound Raw Message (' + connectionName + ')', {message: msg});

      if(msg.indexOf('PONG') === 0){
        NL.trigger('websocket|PONG', msg);
      } 
    }
  }

  function onerror(message, connectionName){
    log.error('Communication Error (%s)', connectionName);

    if(message){
      message = ' - ' + message;
    }

    controls.notification.show('Error Communicating with the server' + message, controls.notification.types.error );
  }

  function onclose(event, reason){
    NL.trigger('websocket|disconnected', [event, reason]);
  }

  var settings = {
    acl: NL.settings.utopia.acl,
    onmessage: onmessage,
    onerror: onerror,
    onclose: onclose
  };


  function applyUtopiaSettings(){    
    $.extend(true, settings, NL.settings.utopia);
  }

  applyUtopiaSettings();

  var socket;
  var http = new HttpClient({
    onsuccess: onmessage,
    onerror: onerror,
    url: settings.apiAddress()
  });



  // Private sending logic
  function sendMessage(event_category, event, obj, protocol, callback) {
    if(protocol !== 'http' && NL.api.socketStatus() !== SocketReadyStateType.Open){
      log.error('Tried to send message without an active connection');
      // controls.notification.show('Not Connected - Unable to send message.', controls.notification.types.error);
      console.log('##### returned ');
      return false;
    }

      var actionWhitelist = settings.acl.actions();
      var allowed = actionWhitelist[event_category] && actionWhitelist[event_category][event];

      // Ebrahim M : little hack for me too !!! (two music zone that the access level for the first one is removed!!!!)
      if(allowed === undefined && event !== 'intro' && event_category !== 'request_user' && event !== 'spotify_filenames'){
        console.log('##### event = ' + event  +  ', event_category = ' + event_category +'allowed = ', allowed );
        return false;
      }

      if ( !allowed && event !== 'intro' && event_category !== 'request_user' && event !== 'spotify_filenames') { // Lawrence O: little hack !!          
          log.warn('Authorisation failed! Category: ' + event_category + ', Event: ' + event, settings);
          controls.notification.show('Insufficient permissions to perform this function!', controls.notification.types.error);
          console.log('##### returned ');
          return false;
      }

      /////////////////////////////////////////////////////////////////////////////////////////////
      // Ebrahim M:
      // I have added this block here because when the music zone is connected directly, 
      // we want to set user name and password to 'guest' in order to get the access level from HDMS
      // (by calling 'request_zone\details') but 'listing' API does not work with guest user 
      // so we call it with the last logged in user.
      if (! (event_category === 'request_systems' && event === 'listing') && 
                                    NL.settings.selectedSystemId() === 'manual') {
        settings.username('guest');
        settings.password('guest');
      }
      else{
          if(NL.settings.utopia.username() === 'guest'){
            NL.settings.utopia.username(NL.settings.lastLoggedUser().user) ;
            NL.settings.utopia.password(NL.settings.lastLoggedUser().password);
          }
      }
      /////////////////////////////////////////////////////////////////////////////////////////////

      var msg = new SocketMessageModel(settings, event_category, event, obj, callback);
      var eventName = event_category + '|' + event;

      var conn = protocol == 'http' ? http : socket;
      log.debug('Outbound Message - ' + eventName + ' (' + conn.connectionName + ')', msg);

      return sendRawMessage(JSON.stringify(msg), protocol, eventName);      
  }

  function sendRawMessage(msg, protocol, eventName){
    var conn = protocol == 'http' ? http : socket;

    if(!conn || !conn.send){
      log.error('Tried to send message without an active connection');
      controls.notification.show('Not Connected - Unable to send message.', controls.notification.types.error);
      console.log('+++++  returned ');
      return false;
    }

    // console.log('--- Raw Message', msg);
    if(!eventName){
      log.debug('Outbound Raw Message (' + conn.connectionName + ')', {message: msg});
    }

    return conn.send(msg);
  }

  // Private close logic
  var closeConnection = function(reason){
    if(socket && socket.destroy){
      socket.destroy(reason);
    }
    socket = undefined;
    NL.api.socketStatus = ko.observable();
  };

  function setupSocket(){
    var self = NL.api;
    if(socket){
      closeConnection();
    }

    settings.name = NL.settings.connectionMethod();
    var sys = NL.settings.selectedSystem();

    if(sys){
      if(settings.name === 'local'){
        settings.address = sys.address;
      } else {
        if( (sys.serverAddress !== undefined) && (sys.serverAddress !== null) ){
          settings.address = sys.serverAddress.replace(/\/websocket$/, '');
          settings.address = settings.address.replace('ws:', 'http:'); 
          settings.address = settings.address.replace('wss:', 'https:'); 
          //console.log('+++ settings.address = ' + settings.address);
        }
      }

      settings.system = sys.system;
      settings.zone = parseInt(sys.zone,10);
    }

    socket = new SocketClient(settings);
    self.socketSettings = settings;
    self.socketStatus = socket.state;

    self.uploadClient = new UploadClient();
  }
}

$doc.one('nightlife-ready', function() {
  NL.api = new ApiManager(NL);

  if(NL.settings.utopia.username() === 'guest'){
    NL.settings.utopia.username(NL.settings.lastLoggedUser().user) ;
    NL.settings.utopia.password(NL.settings.lastLoggedUser().password);
  }

  // Check if we should auto login
  if(NL.settings.autoLogin() && NL.settings.utopia.username() && NL.settings.utopia.password()){
    controllers.request_systems.listing();
  }
});