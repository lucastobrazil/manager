function SocketClient(settings) {
  if (!settings) {
    return;
  }

  log.debug('Creating socket (%s)', settings.name);

  // Private variables
  var ws,
  self = this;

  this.closeReason = '';
    
  try {
    // Ebrahim M: 'settings.address' is set in ApiManager.js 
    ws = new SockJS(settings.address, null, {debug: true, protocols_whitelist: 
                      ['websocket', 'xdr-polling', 'xhr-polling', 'iframe-xhr-polling', 'jsonp-polling']});
  } catch (e) {
    controls.notification.show('Unable to establish connection', controls.notification.types.error);
    NL.settings.systemStatus('Not connected - System Offline');
    log.error('Failed to create socket', e);
    return;
  }

  // Public state descriptor
  this.state = ko.observable(SocketReadyStateType.Connecting);
  this.connectionName = settings.name;

  this.close = function(skipCallback) {

    // Skip the callback
    if (skipCallback) {
      var onclose = settings.onclose;
      // Replace the callback with a function which when run will re-attach the callback.
      // This is done because we don't know when the onclose event will fire only that it will
      // this way we can re-attach the original function without any fuss
      settings.onclose = function() {
        settings.onclose = onclose;
      };
    }

    ws.close();
  };

  // Public facing send function
  this.send = function(event_category, event, obj) {
    return sendMessage(event_category, event, obj);
  };

  // Private send function (this one does the actual work)
  var sendMessage = function(msg) {
    if (self.state() === SocketReadyStateType.Open) {
      return ws.send(msg);
    }
    return false;
  };

  /*
     * Configure the socket
     */
  ws.onopen = function(e) {
    log.info('Socket Connected (%s)', self.connectionName);

    self.state(ws.readyState);

    if (settings.onopen) {
      var result = settings.onopen.call(self, e);
      if (result === false) {
        return false;
      }
    }

    NL.trigger('websocket|connected');
  };

  ws.onerror = function(e) {
    self.state(ws.readyState);

    if (settings.onerror) {
      settings.onerror.call(NL.api, e, self.connectionName);
    }
  };

  ws.onclose = function(e) {
    log.info('Socket Closing (%s)', self.connectionName);
    self.state(this.readyState);
    if (settings.onclose) {
      settings.onclose.call(self, e, self.closeReason);
    }
  };

  ws.onmessage = function(e) {
    var msg = e.data;
    try{
      msg = JSON.parse(e.data);
    }catch(err){}

    settings.onmessage.call(NL.api, msg, self.connectionName);
  };

  this.destroy = function(reason) {
    self.closeReason = reason;
    log.debug('Destroying Socket (%s)', this.connectionName);
    ws.close();
    self.state(SocketReadyStateType.Closing);
    ws = undefined;
    self.state(SocketReadyStateType.Closed);
  };
}