function UploadClient() {
    
    beforeSendHandler = function(e) {
    };
    
    afterSendHandler = function(e) {
    };
    
    errorHandler = function(e) {        
      controls.notifications.show('Error Uploading: ' + xhr.responseJSON.object.error);
    };
    
    completeHandler = function(e) {            
      //console.log(e.object.job_id);
    };


    this.sendFile = function(file, msg, onProgress, cb) {	
      var formData = new FormData();
      formData.append('actor',  JSON.stringify(msg.actor));
      formData.append('verb',   JSON.stringify(msg.verb));
      formData.append('object', JSON.stringify(msg.object));
      formData.append('target', JSON.stringify(msg.target));
      formData.append('upload', file, file.name.toUpperCase());
      log.debug('Upload sent ', formData);

      /*if(NL.settings.connectionMethod() === 'utopia') {
        // Ebrahim M: Replaced with the new line below!
        // url = NL.api.socketSettings.address.substring(0,NL.api.socketSettings.address.indexOf(':9090/hl'))+'/api';
        url = NL.api.socketSettings.address.substring(0,NL.api.socketSettings.address.indexOf('/hl'))+'/api';
      } else {
        url = NL.api.socketSettings.address.substring(0,NL.api.socketSettings.address.indexOf(':9090/hl'))+':9090/api';
      }
      */

      url = NL.api.socketSettings.address.substring(0,NL.api.socketSettings.address.indexOf('/hl'))+'/api';
      console.log('3 ### connectionMethod = ' +  NL.settings.connectionMethod() , url);

      $.ajax({
        url: url,  //Server script to process data
        type: 'POST',
        xhr: function() {  // Custom XMLHttpRequest
            var myXhr = $.ajaxSettings.xhr();
            if(myXhr.upload && (typeof onProgress === typeof Function)){ // Check if upload property exists
                myXhr.upload.addEventListener('progress' , onProgress.bind(null, file.name.toUpperCase()) , false); // For handling the progress of the upload
            }
            return myXhr;
        },
        //Ajax events
        beforeSend: beforeSendHandler,
        success: (typeof(cb.success) === 'function') ? cb.success : completeHandler,
        error:  (typeof(cb.error) === 'function') ? cb.error : errorHandler,
        // Form data
        data: formData,
        //Options to tell jQuery not to process data or worry about content-type.
        cache: false,
        contentType: false,
        processData: false
    });

  };
}

