function HttpClient(settings){
  var self = this;
  this.connectionName = 'http';

  var options = {
    type: 'POST',
    crossDomain: true,
    dataType: 'json',
    url: settings.url
  };

  this.send = function(msg){
    options.data = msg;

    $.ajax(options)
      .done(function(data, textStatus, jqXHR) {
        settings.onsuccess.call(NL.api, data, self.connectionName);
      })
      .fail(function(jqXHR, textStatus, errorThrown){
        // Some errors still contain JSON - handle these as a success
        if(jqXHR && jqXHR.responseJSON && jqXHR.responseJSON.object){
          settings.onsuccess.call(NL.api, jqXHR.responseJSON, self.connectionName);
        }else{
          var reason = '';

          if(jqXHR && jqXHR.status){
            reason = jqXHR.statusText + ' (' + jqXHR.status + ')';
          }

          settings.onerror.call(NL.api, reason, self.connectionName);
        }
      });
  };
}