$.extend(true, controllers, {
  update_system: {
    set_generated_bingo_number: function(e, jsonObj) {
        // console.log('#### set_generated_bingo_number received' , jsonObj);
        
        // Ebrahim M : Recieved Bingo Call
        var bingoHiLightDur = 1000 * 3;
        var nBingoNum = jsonObj.object.number;
        var strBingoCall = jsonObj.object.call;

        if(NL.videoStates !== undefined && NL.videoStates.states().length > 0){
          if(NL.videoStates.states()[0].asset().indexOf('Bingo') >= 0) {
            if(nBingoNum > 0){
              NL.videoStates.states()[0].asset('Bingo - ' + nBingoNum + ' (' + strBingoCall + ')' );
              NL.videoStates.states()[0].lastDrawnNo(nBingoNum); 
              setTimeout(function () {                
                NL.videoStates.states()[0].lastDrawnNo(0);
                NL.videoStates.states()[0].generatedBngNumbers.push(nBingoNum);
              }, bingoHiLightDur);
            }
            else{
              NL.videoStates.states()[0].asset('Bingo - ' + ' (' + strBingoCall + ')' ); 
              NL.videoStates.states()[0].generatedBngNumbers.removeAll();
              NL.videoStates.states()[0].lastDrawnNo(0);
            }

          }
        } 


    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'update_system|set_generated_bingo_number';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.update_system.set_generated_bingo_number(e, jsonObj);
  });
});