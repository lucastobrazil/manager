$.extend(true, controllers, {
  update_system: {
    da_environment: function(e, jsonObj) {  

      ////////////////////////////////////////////////////////////////////////////////////
      // Ebrahim M : Added this part to select the last asset after updating    
      var lastPrioritySelectedAsset = '';
      if(NL.advertising.priority.selectedAssets().length > 0){
        lastPrioritySelectedAsset = NL.advertising.priority.selectedAssets()[0].filename();      
      }
      var lastUserSelectedAsset = '';
      if(NL.advertising.user.selectedAssets().length > 0){
        lastUserSelectedAsset = NL.advertising.user.selectedAssets()[0].filename();
      }
      /////////////////////////////////////////////////////////////////////////////////////

      NL.advertising.user.assets([]);
      NL.advertising.priority.assets([]);
      NL.advertising.galleries.assets([]);      
      var ad, i;
      for(i = 0; i < jsonObj.object.paid.length; i++) {
        ad = new AdvertisementModel(jsonObj.object.paid[i]);
        ad.priority = true;
        ad.index = i;
        ad.viewIndex = i;
        ad.parent = NL.advertising.priority;
        NL.advertising.priority.assets.push(ad);
        if(ad.filename().substr(-3) === 'GAL') {
          NL.advertising.galleries.assets.push(ad);          
        }
        if(ad.filename() === lastPrioritySelectedAsset){
          controls.advertising.selectAsset(ad, {});
        }
      }
      if(jsonObj.object.user) {
        for(i = 0; i < jsonObj.object.user.length; i++) {
          ad = new AdvertisementModel(jsonObj.object.user[i]);
          ad.priority = false;
          ad.index = i;
          ad.viewIndex = i;
          ad.parent = NL.advertising.user;
          NL.advertising.user.assets.push(ad);
          if(ad.filename().substr(-3) === 'GAL') {
            NL.advertising.galleries.assets.push(ad); //@TODO remove testing code;            
          }
          if(ad.filename() === lastUserSelectedAsset){
            controls.advertising.selectAsset(ad, {});
          }
        }
      }
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'update_system|da_environment';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.response_system.da_environment(e, jsonObj, NL.settings.utopia);
  });
});
