$.extend(true, controllers, {
  update_system: {
    set_videostate: function(e, jsonObj, videoStatesModel) {
        controllers.response_system.videostate(e, jsonObj, videoStatesModel);
    }
  }
});



//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'update_system|set_videostate';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.update_system.set_videostate(e, jsonObj, NL.videoStates);
  });
});