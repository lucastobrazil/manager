$.extend(true, controllers, {
  update_system: {
    set_generated_random_number: function(e, jsonObj) {
      console.log('#### set_generated_random_number received' , jsonObj.object);
      if( typeof(NL) !== typeof(undefined) ){
        NL.videoStates.lastDrawnRngNo(0); 
        NL.videoStates.lastDrawnRngNoTemp = jsonObj.object.number;
        controls.video.runRngDemoPattern();
      }
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'update_system|set_generated_random_number';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.update_system.set_generated_random_number(e, jsonObj);
  });

});