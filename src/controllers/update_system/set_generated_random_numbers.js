$.extend(true, controllers, {
  update_system: {
    set_generated_random_numbers: function(e, jsonObj) {
      console.log('#### set_generated_random_numbers received' , jsonObj.object);
      if( typeof(NL) !== typeof(undefined) ){
        controllers.response_system.generated_random_numbers(e, jsonObj);
      }

    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'update_system|set_generated_random_numbers';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.update_system.set_generated_random_numbers(e, jsonObj);
  });

});