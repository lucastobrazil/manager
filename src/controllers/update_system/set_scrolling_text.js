// Fri 25 Sep 15 11:54 AM Ebrahim Mardani:
$.extend(true, controllers, {
  update_system: {
    set_scrolling_text: function(e, jsonObj) {
      if( typeof(NL) !== typeof(undefined) ){
        controllers.response_system.scrolling_text(e, jsonObj);
        if(NL.textScrolling.uploadInProgress() ){
          NL.textScrolling.uploadInProgress(false);
          controls.newNotification.initNotif('Scrolling text in NLMS successfully added/editted.', 4500);
        }
        if(NL.textScrolling.deleteInProgress()){
          NL.textScrolling.deleteInProgress(false);
          controls.newNotification.initNotif('Scrolling text successfully deleted from NLMS.', 4500);
        }
      }      
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'update_system|set_scrolling_text';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.update_system.set_scrolling_text(e, jsonObj);
  });

});