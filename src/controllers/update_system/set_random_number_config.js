$.extend(true, controllers, {
  update_system: {
    set_random_number_config: function(e, jsonObj) {
      console.log('#### set_random_number_config received' , jsonObj.object);
      var objRngTemp = new CRng(jsonObj.object);
      //console.log('>>>>>>>>>>>>>>>> objRngTemp = ', objRngTemp);
      if( typeof(NL) !== typeof(undefined) ){
        NL.videoStates.objRng(objRngTemp);
      }
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'update_system|set_random_number_config';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.update_system.set_random_number_config(e, jsonObj);
  });
});