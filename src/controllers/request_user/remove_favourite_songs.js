$.extend(true, controllers, {
  request_user: {
    remove_favorite_songs: function(listSongs , stateParam) {
      var msg = {    
        object: {
          songs: listSongs,
          list:stateParam
        },    
		    verb: {
		      proxy: true
		    }
      };
      NL.api.send('request_user', 'remove_favourite_songs', msg);
    }
  }
});