$.extend(true, controllers, {
  request_user: {
    add_favorite_songs: function(listSongs,stateParam) {
      var msg = {    
        object: {
          songs: listSongs,
          list:stateParam
        },
        verb: {
          proxy: true
        }
      };
      NL.api.send('request_user', 'add_favourite_songs', msg);
    }
  }
});