$.extend(true, controllers, {
  request_user: {
    forgotten_password: function(uname, cb) {
      var msg = {
        target: {user : uname},
        object: null
      };
      NL.api.post('request_user', 'forgotten_password', msg, cb);
    }
  }
});