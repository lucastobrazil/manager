$.extend(true, controllers, {
  request_user: {
    favorite_songs: function(stateParam) {
      var msg = {        
        object: {         
          list:stateParam
        },       
        verb: {
          proxy: true
        }
      };
      // console.log('favorite_songs called. stateParam = ' + stateParam);
      if(NL.api !== undefined){
        NL.api.send('request_user', 'favourite_songs', msg);
      }
    }
  }
});