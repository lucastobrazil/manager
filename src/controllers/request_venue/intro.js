$.extend(true, controllers, {
  request_venue: {
    intro: function(cb) {        
        var msg = new SocketMessageModel(NL.settings.utopia, 'request_venue', 'set_intro', {});
        //msg.object = {};
        msg.target = {client : NL.settings.selectedSystem().client_code};
      NL.api.post('request_venue', 'intro', msg, cb);
    }
  }
});