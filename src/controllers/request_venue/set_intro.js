$.extend(true, controllers, {
  request_venue: {
    set_intro: function(file, obj, cb) {
      
        var msg = new SocketMessageModel(NL.settings.utopia, 'request_venue', 'set_intro', obj);
        msg.object = obj;
        console.log(msg.object);
        msg.target = {client : NL.settings.selectedSystem().client_code};  
        // Ebrahim M: According to Lawrence O, it is not used.       
        NL.api.uploadClient.sendFile(file, msg, 
            {
                success : cb.bind(undefined, true),                                           
                error : cb.bind(undefined, false)
        }); 
    }
  }   
});