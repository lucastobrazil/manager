// TODO: Implement
$.extend(true, controllers, {
  update_zone: {
    song_play: function(){
      log.warn('update_zone|song_play not implemented');
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function(){
  var eventName = 'update_zone|song_play';
  log.event(eventName, 'binding events');

  NL.bind(eventName, controllers.update_zone.song_play);

});
