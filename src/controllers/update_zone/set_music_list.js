$.extend(true, controllers, {
  update_zone: {
    set_music_list: function(e, jsonObj, model) {

      //console.log('######## jsonObj.object = ' , jsonObj.object);
      if (jsonObj.object && jsonObj.object.list_names) {
        model.loadData(jsonObj.object);
        model.resetLists();
      }
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'update_zone|set_music_list';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.update_zone.set_music_list(e, jsonObj, NL.musicList);
  });
});