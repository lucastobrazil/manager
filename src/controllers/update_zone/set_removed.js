$.extend(true, controllers, {
  update_zone: {
    set_removed: function(e, jsonObj, allSongs) {
      if (jsonObj.object) {
        var removedSong = jsonObj.object;
        var regexFilename = new RegExp('([A-Z]+[0-9]*T)([0-9])$');
        if (regexFilename.test(removedSong.filename)) {
          var parts = regexFilename.exec(removedSong.filename);
          removedSong.filename = parts[1] + '0' + parts[2];
        }
        var song = allSongs[removedSong.filename];
        if (song) {
          song.removed(removedSong.removed);
        }
      }
    }
  }
});


//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'update_zone|set_removed';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.update_zone.set_removed(e, jsonObj, NL.allSongs);
  });
});