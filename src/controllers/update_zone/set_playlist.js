$.extend(true, controllers, {
  update_zone: {
    set_playlist: function(e, jsonObj) {
      var nowPlayingModel = NL.nowPlaying();
      var indexOffset = 1;
      var timeTilPlay = 0;

      if (jsonObj.object !== null && jsonObj.object.playlist !== null) {
        var playlist = jsonObj.object.playlist;
        var songArray = [];
        var songFileNameArr = []; // Ebrahim M: Added to get the Spotify IDs
        if(playlist !== null && playlist !== undefined ){

          for(var i = 0; i < playlist.length && i < NL.settings.playlistHorizon(); i++) {
              var obj = playlist[i];
           
              obj.distance = i + indexOffset + 1;
              obj.context = '#tab-playlist';
              obj.when = timeTilPlay;
              timeTilPlay += +obj.length;
              var song = new SongViewModel(obj, 1);

              songArray.push(song);
              songFileNameArr.push(song.filename);

              var sa = songArray;                        
          }
          NL.playlist.queue(songArray.slice());          
          NL.playlist.current(nowPlayingModel.song());          
          controls.playlist.setBottomSpaceHeight(); // add spacer if required
          //controls.playlist.updateModal();          

          // Ebrahim M: Added to get the Spotify IDs
          if(NL.playlist.current() !== undefined && NL.playlist.current().filename !== undefined){
            songArray.push(NL.playlist.current() );
            songFileNameArr.push(NL.playlist.current().filename);
            controllers.general.getSpotifyIds(songFileNameArr, songArray);
          }          
          controllers.general.getSpotifyIds(songFileNameArr, songArray); // NL.playlist.queue());
          
          ///////////////////////////////////////////////////////////////////////////

        }
      } else {
        NL.playlist.queue([]);
        NL.playlist.current = undefined;
      }
    }
  }
});


//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'update_zone|set_playlist';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.update_zone.set_playlist(e, jsonObj);    
  });



});
