$.extend(true, controllers, {
  update_zone: {
    set_credit_limits: function(e, jsonObj) {
      var bUpdated = false;
      // debugger;
      if (jsonObj.object && jsonObj.object.credits_max ) {
          bUpdated  = true;
          NL.zone.creditsMax(jsonObj.object.credits_max );
      }
      if (jsonObj.object && jsonObj.object.credits_refill !== undefined ) {
          bUpdated  = true;
          NL.zone.creditRefill(jsonObj.object.credits_refill );
      }
      if (jsonObj.object && jsonObj.object.last_reset ) {
          bUpdated  = true;
          NL.zone.creditLastReset(jsonObj.object.last_reset );
      }    

      if (bUpdated && NL.context.modal() === 'zone-controls'){
        controls.newNotification.initNotif('The credit values are now updated.' , 3500 ); 
      }
      /*if (jsonObj.object && jsonObj.object.credits_refill === 0 ) {                    
          NL.zone.freePlayMode(true);
      }
      else{        
        NL.zone.freePlayMode(false);         
      } */
    }
  }
});


//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'update_zone|set_credit_limits';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.update_zone.set_credit_limits(e, jsonObj);
  });
});