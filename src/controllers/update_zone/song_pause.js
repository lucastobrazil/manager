// TODO: Whats the difference between pause and halt?
$.extend(true, controllers, {
  update_zone: {
    song_pause: function(){
      log.warn('update_zone|song_pause not implemented');
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function(){
  var eventName = 'update_zone|song_pause';
  log.event(eventName, 'binding events');

  NL.bind(eventName, controllers.update_zone.song_pause);

});
