$.extend(true, controllers, {
  update_zone: {
    set_volume: function(e, jsonObj, volumeModel) {
      if (jsonObj.object !== null && jsonObj.object.level !== null)
      {
        volumeModel.level(jsonObj.object.level);
      }
    }
  }
});



//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'update_zone|set_volume';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.update_zone.set_volume(e, jsonObj, NL.volume);
  });

});