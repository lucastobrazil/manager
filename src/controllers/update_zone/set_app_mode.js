$.extend(true, controllers, {
  update_zone: {    
    set_app_mode: function(e, jsonObj) {
      // alert('set_app_mode');
      if (jsonObj.object && jsonObj.object.mode) {
          NL.zone.appMode(jsonObj.object.mode);
      }
    }
  }
});

// Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'update_zone|set_app_mode';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.update_zone.set_app_mode(e, jsonObj);
  });
});