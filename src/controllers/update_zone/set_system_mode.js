$.extend(true, controllers, {
  update_zone: {
    set_system_mode: function(e, jsonObj) {      
      if (jsonObj.object) {
        console.log('>>>> set_system_mode is received : ', jsonObj.object);

        var o = jsonObj.object;
        // Ebrahim M : A simple hack for 3.92. It will be changed in 3.93
        if(o.primary === 'dj_manual_auto_play'){
          o.primary = 'dj_manual_cue_points';          
        }
        NL.zone.primary(o.primary); // = new ZoneViewModel(jsonObj.object);        
        NL.zone.pause(o.pause);
        NL.zone.additive(o.additive);
        NL.zone.juke(o.juke);
        if(o.primary === 'karaoke_mc') {
          NL.songSearch.typeFilter(NL.songSearch.filterFieldTypes[2]);  
        } else {
          NL.songSearch.typeFilter(NL.songSearch.filterFieldTypes[0]);  
        }

        if (NL.videoStates.states() !== undefined ){
          switch(o.additive){
            case 'random_number': 
              NL.videoStates.states()[0].mode('R');
              controls.navigation.isRngModalOpen(true);
              if(controls.navigation.isPlaylistRecieved){
                controls.video.showRngModal();
              }

            break;
            
            case 'none':
              controls.modal.hide();
            break;
          }
        }
      }
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'update_zone|set_system_mode';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.update_zone.set_system_mode(e, jsonObj);
  });
});