$.extend(true, controllers, {
  update_zone: {
    set_selection_set: function(e, jsonObj, model) {
      if (jsonObj.object) {
        controllers.response_zone.selection_set(e,jsonObj, model);
      }
    }
  }
});


//Bind event handlers after nightlife is up and running 
$doc.one('nightlife-ready', function() {
  var eventName = 'update_zone|set_selection_set';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.update_zone.set_selection_set(e, jsonObj, NL.zone);
  });
});