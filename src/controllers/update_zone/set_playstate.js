$.extend(true, controllers, {
  update_zone: {
    set_playstate: function(e, jsonObj) {
      if (jsonObj.object && jsonObj.object.playstate) {

        var playstate = jsonObj.object.playstate;
        var oldState = NL.nowPlaying();
        var song = oldState.song();

        // Ebrahim M: Added to get the Spotify IDs 
        var songArray = [];
        var songFileNameArr = []; 
        ////////////////////////////////////////////////////////////////////

        playstate.distance = 1;
        if(playstate.new_song === true) {
            NL.zone.pause('mixing');
        } else {
            NL.zone.pause('');
        }
        if((playstate.new_song === true || ['karaoke_pausing','playing'].indexOf(playstate.status) < 0) &&
           oldState.playing === true) 
        {          
          song.context = '#tab-playlist';
          var modifiedSong = song.justPlayed();
          NL.playlist.appendHistory(modifiedSong);
          songArray.push( modifiedSong);
          songFileNameArr.push(modifiedSong.filename);
        } 

        NL.nowPlaying(new NowPlayingSongViewModel(playstate));
        NL.playlist.current(new SongViewModel(playstate, 0));
        NL.zone.fromPlayState(playstate.status);
        setTimeout(controls.playlist.updateModal, 1000);
        
        // Ebrahim M: Added to get the Spotify IDs 
        songArray.push(NL.playlist.current() );
        songFileNameArr.push(NL.playlist.current().filename);
        controllers.general.getSpotifyIds(songFileNameArr, songArray);
        ////////////////////////////////////////////////////////////////////
      }
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'update_zone|set_playstate';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.update_zone.set_playstate(e, jsonObj);
  });
});