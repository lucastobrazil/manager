$.extend(true, controllers, {
  response_user: {
    info: function(e, jsonObj, model) {

    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_systems|listing';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.response_user.info(e, jsonObj, NL.settings);
  });
});