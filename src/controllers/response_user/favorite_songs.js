$.extend(true, controllers, {
  response_user: {
    favorite_songs: function(e, jsonObj, model) {      
      if(jsonObj.object.list === 'like'){
		    NL.userInfo.likeSongs(jsonObj.object.songs);
      }
      if(jsonObj.object.list === 'peeve'){
         NL.userInfo.dislikeSongs(jsonObj.object.songs);
      }
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_user|favourite_songs';
  log.event(eventName, 'binding events');  
  NL.bind(eventName, function(e, jsonObj) {    
    controllers.response_user.favorite_songs(e, jsonObj, NL.settings);
  });
});