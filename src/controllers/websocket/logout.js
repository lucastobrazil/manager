$.extend(true, controllers, {
  websocket: {
    //Function to fire on web socket connected
    logout: function() {
      if(NL.api){
        log.event('websocket|logout', 'logout');
        if (NL.settings && NL.settings.systems().length) {
          controllers.control.unsubscribe();
        }
        NL.api.isAuthenticated(false);
      }
    }
  }
});