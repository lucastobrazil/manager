$.extend(true, controllers, {
  websocket: {
    //Function to fire on web socket connected
    connected: function() {
      log.event('websocket|connected', 'connected');

      if (NL.settings.selectedSystem()) {
        controllers.control.subscribe(NL);
        

        // Now that we're connected navigate to the original page
        controls.navigation.enableNav();
        controls.settings.connected(NL.settings.selectedSystem());
        controls.navigation.navigateFromHash();
      }
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'websocket|connected';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function() {
    controllers.websocket.connected();
  });
});