$.extend(true, controllers, {
  websocket: {
    //Function to fire on web socket connected
    pong: function(msg) {
      // Strip away "PONG:" and convert to number
      var time = 1 * msg.slice(5);
      var start = new Date(time);
      var end = new Date();
      var diff = end - start;

      controls.diagnostics.endPingTest(diff);
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'websocket|PONG';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, msg) {
    controllers.websocket.pong(msg);
  });
});