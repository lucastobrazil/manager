$.extend(true, controllers, {
  websocket: {
    //Function to fire on web socket connected
    authenticate: function() {
      log.event('websocket|authenticate', 'authenticate');

      controllers.request_systems.listing();
    }
  }
});
