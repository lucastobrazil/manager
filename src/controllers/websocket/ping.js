$.extend(true, controllers, {
  websocket: {
    //Function to fire on web socket connected
    ping: function() {
      NL.api.sendMsg('PING:' + new Date().getTime());
    }
  }
});
