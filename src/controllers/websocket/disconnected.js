$.extend(true, controllers, {
  websocket: {
    //Function to fire on web socket connected
    disconnected: function(closeEvent, reason) {

      log.event('websocket|disconnected', 'Disconnected');
      //console.log('########### disconnected websocket called');

      controls.settings.disconnect(closeEvent, reason);
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'websocket|disconnected';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, closeEvent, reason) {
    controllers.websocket.disconnected(closeEvent, reason);
  });
});