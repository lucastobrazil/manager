$.extend(true, controllers, {
  request_zone: {
    set_music_list: function(list_names, cb) {
      var msg = {
        object: {
          list_names: list_names
        }
      };
      NL.api.send('request_zone', 'set_music_list', msg, cb);
    },

    clear_music_list: function(cb) {
      var msg = {object : {list_names : []}};
      NL.api.send('request_zone', 'set_music_list', msg, cb);
    }
  }
}); 