$.extend(true, controllers, {
  request_zone: {
    set_system_mode: function(mode, cb) {
      var msg = '';  
      // Ebrahim M: Bug is fixed! - 14 Apr 2015    
      if( (mode.juke().mode === 'juke_on')  || ( NL.zone.system_mode.juke() &&  mode.juke().mode === 'juke_off'))  {
        msg = {
          object: {
            primary: mode.primary(),
            additive: mode.additive(),
            juke: mode.juke().mode
          }        
        };
      }
      else{
        msg = {
          object: {
            primary: mode.primary(),
            additive: mode.additive()
          }
        };
      }

      NL.api.send('request_zone', 'set_system_mode', msg, cb);
    }
  }
});