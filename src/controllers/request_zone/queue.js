$.extend(true, controllers, {
  request_zone: {
    queue: function(filename, last, cb) {
      var msg = {
        object: {
          filename: filename     
        },
        verb: {
          end: last
        }
      };
      NL.api.send('request_zone', 'queue', msg, cb);
    }
  }
});