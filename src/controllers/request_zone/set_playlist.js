$.extend(true, controllers, {
    request_zone:{
      set_playlist: function(filenames, cb)
      {
        var msg = {
          object: {
            'songs': filenames,
            'sort_method': 'programmed'
          }
        };
        NL.api.send('request_zone', 'set_playlist', msg, cb);
      }
   }
});