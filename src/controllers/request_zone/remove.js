$.extend(true, controllers, {
  request_zone: {
    remove: function(filename, cb)
    {
      var msg = {
        object: {
          filename: filename
        }
      };

      NL.api.send('request_zone', 'remove', msg, cb);
    }
  }
});