$.extend(true, controllers, {
  request_zone: {
    play: function(filename, when, cb) {
      if (!when) {
        when = 'now';
      }

      var msg = {
        verb: {
          when: when
        },
        object: {
          filename: filename
        }
      };

      NL.api.send('request_zone', 'play', msg, cb);
    }
  }
});