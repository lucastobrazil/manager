$.extend(true, controllers, {
  request_zone: {
    songsearch: function(model) {
      var msg = {
        verb: model.toSongSearchVerbs()
      };

      NL.api.send('request_zone', 'songsearch', msg);
    }
  }
});