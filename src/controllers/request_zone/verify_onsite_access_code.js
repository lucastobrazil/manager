$.extend(true, controllers, {
  request_zone: {
    verify_onsite_access_code: function(accessCode, cb){
      if(typeof(accessCode) !== 'undefined') {
        var msg = {
          actor: {
            onsite_access_code: accessCode
          }
        };

        NL.api.send('request_zone', 'verify_onsite_access_code', msg, cb);
      }
    }
  }
});