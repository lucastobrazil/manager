$.extend(true, controllers, {
  request_zone: {
    set_removed: function(filename, removed, cb)
    {
      var msg = {
        object: {
          filename: filename,
          removed: removed
        }
      };

      NL.api.send('request_zone', 'set_removed', msg, cb);
    }
  }
});