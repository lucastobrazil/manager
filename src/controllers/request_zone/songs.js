$.extend(true, controllers, {
  request_zone: {
    songs: function(model, cb) {   
      var msg = {
        verb: model
      };
      if(NL.api !== undefined){
        NL.api.send('request_zone', 'songs', msg, cb);
      }
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'request_zone|songs';
  log.event(eventName, 'binding events');

});