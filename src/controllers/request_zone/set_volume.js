$.extend(true, controllers, {
  request_zone: {
    set_volume: function(model, cb) {
      var msg = {
        object: {
          level: model.level()
        }
      };

      NL.api.send('request_zone', 'set_volume', msg);
    }
  }
});