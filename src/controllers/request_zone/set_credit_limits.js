$.extend(true, controllers, {  
    request_zone:{
      set_credit_limits: function(maxCredits, creditRefill, cb)
      {
        //alert(maxCredits);
        var msg = {
          object: {
            'credits_max': maxCredits,
            'credits_refill': creditRefill                        
          }
        };
        
        NL.api.send('request_zone', 'set_credit_limits', msg, cb);
      }
   }
});