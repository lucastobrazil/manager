$.extend(true, controllers, {
  request_zone: {

    halt: function(pauseNow, cb){
      var msg = {
        verb: {
          now: pauseNow
        }
      };
      NL.api.send('request_zone', 'halt', msg, cb);
    },

    haltNow: function(cb){
      controllers.request_zone.halt(true, cb);
    },

    haltEnd: function(cb){
      controllers.request_zone.halt(false, cb);
    }
  }
});



