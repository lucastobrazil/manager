$.extend(true, controllers, {  
    request_zone:{
      reset_credits: function(cb)
      {
        var msg = {
          object: {
            'reset_credits': true,
            'request_id': 'reset_credits'                        
          }
        };
        
        NL.api.send('request_zone', 'set_credit_limits', msg, cb);
      }
   }
});