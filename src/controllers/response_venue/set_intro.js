$.extend(true, controllers, {
  response_venue: {
    set_intro: function(e, jsonObj){
      NL.zone.fromAvailModes(jsonObj.object);
        
    }
  }
});

$doc.one('nightlife-ready', function(){
  var eventName = 'response_venue|set_intro';
  NL.bind(eventName, function(e, jsonObj){
    controllers.response_venue.set_intro(e, jsonObj, NL.playlist);
  });
});