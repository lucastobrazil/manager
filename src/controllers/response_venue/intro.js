$.extend(true, controllers, {
  response_venue: {
    intro: function(e, jsonObj){
      NL.venue.appAd(jsonObj.object); 
      //delete(NL.advertising.app);
      // = new AdvertisingAppModel(jsonObj.object);
      var obj = jsonObj.object;
        NL.advertising.app.header(obj.header);
        NL.advertising.app.sub_header(obj.sub_header);
        NL.advertising.app.body_text(obj.body_text);
        NL.advertising.app.imgSrc(obj.image_url);
        NL.advertising.app.filename(''/*obj.filename*/);
        NL.advertising.app.bg_color('#'+obj.bg_color);
    }
  }
});

$doc.one('nightlife-ready', function(){
  var eventName = 'response_venue|intro';
  NL.bind(eventName, function(e, jsonObj){
    controllers.response_venue.intro(e, jsonObj, NL.playlist);
  });
});