$.extend(true, controllers, {
  response_system: {
    lists: function(e, jsonObj, model) {

      if (jsonObj.object && jsonObj.object.lists) {
        var lists = [];
        

       jsonObj.object.lists.forEach(function(obj) {
            var list = new ListViewModel(obj);                        
            lists.push(list);

            // Ebrahim M : Only numbers list name are fixed!
            if(list !== undefined && list.name !== undefined){
              if ( ! isNaN (list.name) ){
                list.name = '_' + list.name;
              }
              list.name = list.name.replace(/\s+/g, '_');                            
              
              lists[list.name] = list;
              lists['slot-' + list.slot] = list;
            }
        });

        // Ebrahim M : Bug is fixed - 15 Apr 2015
        /*jsonObj.object.lists.forEach(function(obj) {
            var list = new ListViewModel(obj);                        
            lists.push(list);


            if(list !== undefined && list.name !== undefined){
              if ( ! isNaN (list.name) ){ // if it is a number then replace it with underscore
                list.name = '_' + list.name;
              }
              list.name = list.name.replace(/\s+/g, '_'); // replace the spaces with underscore                          

            // Ebrahim M : Only numbers list name are fixed!
            if(list !== undefined && list.name !== undefined){
              if ( ! isNaN (list.name) ){
                list.name = '_' + list.name;
              }
              list.name = list.name.replace(/\s+/g, '_');                            
              

              lists[list.name] = list;
              lists['slot-' + list.slot] = list;
            }
          }
        });*/

        model.lists(lists);
        model.listsAll(lists);
        controllers.request_zone.music_list();
      }
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_system|lists';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    log.event(eventName, 'setting lists');
    controllers.response_system.lists(e, jsonObj, NL.musicList);
  });
});
