// Fri 25 Sep 15 11:31 AM Ebrahim Mardani:
$.extend(true, controllers, {
  response_system: {
    scrolling_text: function(e, jsonObj) {
      console.log('*********** scrolling_text received' , jsonObj.object);
      NL.textScrolling.user.texts([]);
      NL.textScrolling.priority.texts([]);

      var txtMsg, i;
      if(jsonObj.object.priority) {
        for(i = 0; i < jsonObj.object.priority.length; i++) {
          txtMsg = new TextModel(jsonObj.object.priority[i]);
          txtMsg.viewIndex = i;
          txtMsg.parent = NL.textScrolling.priority;

          NL.textScrolling.priority.texts.push(txtMsg);
        }
      }
      
      if(jsonObj.object.user) {
        for(i = 0; i < jsonObj.object.user.length; i++) {
          txtMsg = new TextModel(jsonObj.object.user[i]);
          txtMsg.viewIndex = i;
          txtMsg.parent = NL.textScrolling.user;

          NL.textScrolling.user.texts.push(txtMsg);
        }
      }      

    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_system|scrolling_text';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.response_system.scrolling_text(e, jsonObj);
  });
});
