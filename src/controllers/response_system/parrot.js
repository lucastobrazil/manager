$.extend(true, controllers, {
  response_system: {
    parrot: function(e, jsonObj, model) {
      // Strip away "PONG:" and convert to number
      var time = 1 * jsonObj.object.time;
      var start = new Date(time);
      var end = new Date();
      var diff = end - start;
      controls.diagnostics.endParrotTest(diff);
    }
  }
});


//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_system|parrot';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.response_system.parrot(e, jsonObj, NL.musicList);
  });
});
