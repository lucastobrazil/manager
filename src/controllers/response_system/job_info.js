$.extend(true, controllers, {
  response_system: {
    job_info: function(e, jsonObj, model) {
      // Strip away "PONG:" and convert to number
      /*var time = 1 * jsonObj.object.time;
      var start = new Date(time);
      var end = new Date();
      var diff = end - start;
      controls.diagnostics.endParrotTest(diff);*/
      //console.log(jsonObj.object.state);
    }
  }
});


//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_system|job_info';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.response_system.job_info(e, jsonObj, NL.advertising);
  });
});
