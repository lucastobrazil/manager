$.extend(true, controllers, {
  response_system: {
    generated_random_numbers: function(e, jsonObj) {
        console.log('####************** generated_random_numbers received' , jsonObj.object.numbers);
        if( typeof(NL) !== typeof(undefined) ){
          NL.videoStates.objRngNumbers(jsonObj.object.numbers);
        }
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_system|generated_random_numbers';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.response_system.generated_random_numbers(e, jsonObj);
  });
});
