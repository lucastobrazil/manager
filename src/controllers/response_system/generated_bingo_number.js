$.extend(true, controllers, {
  response_system: {
    generated_bingo_number: function(e, jsonObj) {
       // console.log('####************** generated_bingo_number received' , jsonObj);
       var bingoNums = jsonObj.object.drawn_numbers;
        var i = 0;
        if(NL.videoStates !== undefined && NL.videoStates.states().length > 0){
          if(NL.videoStates.states()[0].asset().indexOf('Bingo') >= 0) {
            NL.videoStates.states()[0].generatedBngNumbers.removeAll();
            NL.videoStates.states()[0].lastDrawnNo(0);
            for(i=0; i< bingoNums.length ; i++){
               NL.videoStates.states()[0].generatedBngNumbers.push(bingoNums[i]);
            }
          }
        }

    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_system|generated_bingo_number';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.response_system.generated_bingo_number(e, jsonObj);
  });
});
