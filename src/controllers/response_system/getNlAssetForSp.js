$.extend(true, controllers, {
  response_system: {
    isItDuplicate: function(item){
      var i=0;
      for (i=0 ; i < controls.search.arImportedPlaylists().length ; i++){
        if(item === controls.search.arImportedPlaylists()[i].fileNames){
          return true;
        }
      }
      return false;
    },
    /*removePlaylist: function (aPlaylistIId){
      controls.search.arImportedPlaylists.remove(function(item){ 
        return item.spPlaylistId === aPlaylistIId;
      });
    },
    */
    getNlAssetForSp: function(jsonObj) {
      //console.log('<<<<<<<<<<<<<<<<<<<< jsonObj = ' , jsonObj);
      var nNoOfValidAssets = 0;
      var i = 0;
      var importedFileNames = [];
      for (i = 0; i < jsonObj.object.mapping.length ; i++){
        if(jsonObj.object.mapping[i] !== null){
          if( !controllers.response_system.isItDuplicate(jsonObj.object.mapping[i]) ){
            importedFileNames.push(jsonObj.object.mapping[i]);
            ++nNoOfValidAssets;
          }
        }
      }
      var nIndex = controls.search.arImportedPlaylists().length  - 1;
      if(nIndex >= 0){
        var playListName = controls.search.arImportedPlaylists()[nIndex].spPlaylistName;
        var playListId   = controls.search.arImportedPlaylists()[nIndex].spPlaylistId;
        var owner   = controls.search.arImportedPlaylists()[nIndex].spOwner;
        var fileNamesAtSys   = controls.search.arImportedPlaylists()[nIndex].fileNamesAtSys;
        var atSystem = controls.search.arImportedPlaylists()[nIndex].AtSystem;

        if(playListId === undefined){
          return false;
        }
        controls.song.removePlaylist(playListId);

        controls.search.arImportedPlaylists.push({
                          spPlaylistName:playListName,
                          spPlaylistId:playListId,
                          spOwner:owner,
                          fileNames:importedFileNames// ,
                          //fileNamesAtSys:fileNamesAtSys,
                          //resultCntAtSys: fileNamesAtSys.length,
                          //AtSystem:atSystem
                        });

        var msgTemp = (nNoOfValidAssets > 1)? nNoOfValidAssets + ' songs' : 'No song';
        msgTemp = (nNoOfValidAssets === 1)? 'Just one song': msgTemp;
        controls.newNotification.initNotif(msgTemp + ' of ( ' + playListName +
                ' ) matched to Nightlife songs.' , 8000);
        controls.song.spinBtnImportShow(false);
        if (nNoOfValidAssets > 0){
            controls.navigation.jumpToSearchSpotify();
            setTimeout(function () { 
              NL.songSearch.selectedSpotifyList( {id:playListId, name:playListName, owner:''} );
            }, 1000);
        }
        
                
      }
    }
    
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_songs|spotify_filenames';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.response_system.getNlAssetForSp(jsonObj);
  });
});
