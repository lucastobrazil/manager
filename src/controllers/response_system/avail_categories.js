$.extend(true, controllers, {
  response_system: {
    avail_categories: function(e, jsonObj, model) {      
        model.availableCategories([]);
        jsonObj.object.categories.forEach(function(obj) {
          model.availableCategories.push(obj);
        });
        model.availableCategories.sort(function(left, right) { // sort categories alphamagically
          return left == right ? 0 : (left < right ? -1 : 1);
        });

        // Ebrahim M : Added to make it similar to MMNL mobile app!.
		    // model.availableCategories.unshift('All Songs', 'All Karaoke Songs' , 'All Visual Clips');
    }    
  }
});


//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_system|avail_categories';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    log.event(eventName, 'setting lists');
    controllers.response_system.avail_categories(e, jsonObj, NL.songSearch);
  });
});
