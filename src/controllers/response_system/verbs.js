$.extend(true, controllers, {
  response_system: {
    verbs: function(e, jsonObj, model) {
      if (jsonObj.object && jsonObj.object.events) {
        var events = $.extend({}, jsonObj.object.events);
        var actions = model.acl.actions();
        actions.request_system = events;
        model.acl.actions(actions); // append new actions received

        controllers.request_system.videostate();
        controllers.request_system.lists();
      }
    }
  }
});


//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_system|verbs';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.response_system.verbs(e, jsonObj, NL.settings.utopia);
  });
});
