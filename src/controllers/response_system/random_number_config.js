$.extend(true, controllers, {
  response_system: {
    random_number_config: function(e, jsonObj) {
        console.log('####************** random_number_config received' , jsonObj);
        controllers.update_system.set_random_number_config(e, jsonObj);
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_system|random_number_config';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.response_system.random_number_config(e, jsonObj);
  });
});
