$.extend(true, controllers, {
  response_system: {
    videostate: function(e, jsonObj, model) {
      if (jsonObj.object && jsonObj.object.states) {
        var states = jsonObj.object.states;
        var stateArray = [];

        // console.log('>>>>>>>>>>>####### states = ', states);
        var i = 0;
        $.each(states, function(key, value) {
          if(value) {
            // Ebrahim M : Modified the Video State if Bingo is active in the first zone.
            // If the Bingo is already active we dont overwrite the bingo state due to saving Bingo call.
            if( (i++ === 0) && (model.states().length > 0) && 
              (value.asset.toUpperCase() === 'BINGO') && (model.states()[0].asset().toUpperCase().indexOf('BINGO') >= 0 ) ) {
              stateArray.push( model.states()[0] );
            }
            else{                            
              stateArray.push(new VideoStateViewModel(value));
              if( value.output === 1){                
                controls.navigation.isBingoModalOpen( value.mode === 'B' ); // It is just for the start up 
                
                if( (value.mode === 'B') && controls.navigation.isPlaylistRecieved ){
                  controls.video.showBingoModal();
                }

              }
            }
          }
        });

        model.states(stateArray);
      }
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_system|videostate';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.response_system.videostate(e, jsonObj, NL.videoStates);
  });
});
