$.extend(true, controllers, {
  response_system: {
    da_environment: function(e, jsonObj) {
      controllers.update_system.da_environment(e, jsonObj);
    }
  }
});


//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_system|da_environment';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.response_system.da_environment(e, jsonObj, NL.settings.utopia);
  });
});
