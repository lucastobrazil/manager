$.extend(true, controllers, {
  control: {
    subscribe: function() {

      var msg = {
        target: {},
        object: {
          systems: [{
            system: NL.settings.selectedSystem().system,
            zone: NL.settings.selectedSystem().zone
          }]
        }
      };

      NL.api.send('control', 'subscribe', msg);
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'control|subscribe';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function() {
    controllers.request_system.verbs();
    controllers.request_zone.verbs();
  });

});