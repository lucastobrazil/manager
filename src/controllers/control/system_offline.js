$.extend(true, controllers, {
    control: {
        system_offline: function(e, jsonObj) {
        /* disconnect ws, notify user */
           // console.log('########### system_offline called');
      
            controls.settings.disconnect();
        }
    }
}); 

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'control|system_offline';
  NL.bind(eventName, function(e, jsonObj) {
    controllers.control.system_offline(e, jsonObj);
  });

});