$.extend(true, controllers, {
  control: {
    unsubscribe: function() {

      var msg = {
        object: {
          systems: [{
            system: NL.settings.selectedSystem().system,
            zone: NL.settings.selectedSystem().zone
          }]
        }
      };

      NL.api.send('control', 'unsubscribe', msg);
    }
  }
});