$.extend(true, controllers, {
  response_zone: {
    playlist: function(e, jsonObj, model) {
      controllers.update_zone.set_playlist(e, jsonObj, model);
      
      controls.navigation.isPlaylistRecieved = true;
      // Ebrahim M : 
      if( (controls.navigation.lastNavigatedTab === '#tab-advertising' || 
            controls.navigation.lastNavigatedTab === '#tab-uname' ||
            controls.navigation.lastNavigatedTab === '#tab-hashtag') && 
              controls.navigation.isInstagramFeedModalOpen() ) {
        controls.navigation.jumpToAdvertising();  
      }
      else{
        if( controls.navigation.isBingoModalOpen() ){
          controls.navigation.jumpToBingo();
        }
        else if( controls.navigation.isRngModalOpen() ){
          controls.navigation.jumpToRng(); 
        }
        else if(!controls.settings.suspendedConnect){ // If back button in startup page is not pressed while it was trying to connect ('Suspended!')
          controls.navigation.businessAsUsual(); 
        }
        // controls.settings.suspendedConnect = false;
      }      
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_zone|playlist';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.response_zone.playlist(e, jsonObj, NL);
  });

});