$.extend(true, controllers, {
  response_zone: {
    credit_limits: function(e, jsonObj, model) {
      controllers.update_zone.set_credit_limits(e, jsonObj, model);
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_zone|credit_limits';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.response_zone.credit_limits(e, jsonObj, NL);
  });

});