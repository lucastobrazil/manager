$.extend(true, controllers, {
  response_zone: {
    songsearch: function(e, jsonObj, songSearchModel) {
      if (jsonObj.object && jsonObj.object.songs && jsonObj.object.artists) {
        var songs = jsonObj.object.songs;
        var songArray = [];
        var songFileNameArr = []; // Ebrahim M: Added to get the Spotify IDs
        var artists = jsonObj.object.artists;

        songs.forEach(function(obj) {
          var song = new SongViewModel(obj);
          songArray.push(song);
          songFileNameArr.push(song.filename);
        });

        songSearchModel.songs(songArray);
        songSearchModel.artists(artists);

        // Ebrahim M: Added to get the Spotify IDs
        controllers.general.getSpotifyIds(songFileNameArr, songSearchModel.songs() );
        ////////////////////////////////////////////////////////////////////

//        if (songArray.length) {
 //         controls.search.showSongs();
  //      }
   //     else if (artists.length) {
    //      controls.search.showArtists();
     //   }
      }
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_zone|songsearch';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    log.event(eventName, 'setting songsearch results');
    controllers.response_zone.songsearch(e, jsonObj, NL.songSearch);
  });
});