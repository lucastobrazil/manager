$.extend(true, controllers, {
  response_zone: {
    verify_onsite_access_code: function(e, jsonObj, model) {
      if (jsonObj.object && !isNaN(jsonObj.object.code_expiry)){
        var expiry = jsonObj.object.code_expiry;

        if(expiry){
          controls.notification.show('Code Accepted', controls.notification.types.success);
          model.accessCode.code(controls.settings.accessCode());
          model.accessCode.verified(true);
          controls.modal.hide('onsite-access');
          // set Expiry
          var d = new Date();
          d.setHours(d.getHours() + 12);
          NL.settings.accessCode.expiry(d);
          NL.settings.accessCode.verified(true);

          // If user is verified onsite and RNG window is open I then call 'getDrawnRng()' function
          // to read the settings of RNG
          if(controls.navigation.isRngModalOpen() ){
            controls.video.getDrawnRng();
          }

          
        }else{
          if(controls.settings.accessCode().length > 0) {
            controls.notification.show('Invalid Code', controls.notification.types.error);
            model.accessCode.code(undefined);
            model.accessCode.verified(false);
          }
        }
      }
    }
  }
});



  //Bind event handlers after nightlife is up and running
  $doc.one('nightlife-ready', function() {
    var eventName = 'response_zone|verify_onsite_access_code';
    log.event(eventName, 'binding events');

    NL.bind(eventName, function(e, jsonObj) {
      controllers.response_zone.verify_onsite_access_code(e, jsonObj, NL.settings);
    });
  });