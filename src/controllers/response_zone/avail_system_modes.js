$.extend(true, controllers, {
  response_zone: {
    avail_system_modes: function(e, jsonObj){
      NL.zone.fromAvailModes(jsonObj.object);
		
    }
  }
});

$doc.one('nightlife-ready', function(){
  var eventName = 'response_zone|avail_system_modes';
  NL.bind(eventName, function(e, jsonObj){
    controllers.response_zone.avail_system_modes(e, jsonObj, NL.playlist);
  });
});