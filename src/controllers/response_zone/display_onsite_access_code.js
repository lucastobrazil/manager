$.extend(true, controllers, {
  response_zone: {
    display_onsite_access_code: function() {
      controls.notification.show('On Site Access Code Requested', 'success');
    }
  }
});



//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_zone|display_onsite_access_code';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function() {
    controllers.response_zone.display_onsite_access_code();
  });
});