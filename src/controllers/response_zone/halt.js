$.extend(true, controllers, {
  response_zone: {
    halt: function(e, jsonObj, nightlife) {
      
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_zone|halt';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.response_zone.halt(e, jsonObj, NL);
  });
});