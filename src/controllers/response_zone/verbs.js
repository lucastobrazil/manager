$.extend(true, controllers, {
  response_zone: {
    verbs: function(e, jsonObj, model) {
      if (jsonObj.object && jsonObj.object.events) {
     
        var events = $.extend({}, jsonObj.object.events);     
        var actions = model.acl.actions();

        actions.request_zone = events;
        model.acl.actions(actions); // append new actions received
     
        controllers.request_zone.details();
        controllers.request_zone.volume();
        controllers.request_zone.system_mode();
        controllers.request_zone.playlist();
        controllers.request_zone.play_history();
        controllers.request_zone.playstate();
        controllers.request_zone.avail_system_modes();
        controllers.request_zone.selection_set();
        controllers.request_system.avail_categories();
        controllers.request_system.da_environment();
        controllers.request_system.scrolling_text();
        controllers.request_zone.app_mode();        
        controllers.request_zone.credit_limits();
        controls.search.search();

        if(NL.settings.connectionMethod() === 'utopia') {
          controllers.request_zone.verify_onsite_access_code(NL.settings.accessCode.code());
          controllers.request_venue.intro();          
        }               
      }
    }

    
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_zone|verbs';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.response_zone.verbs(e, jsonObj, NL.settings.utopia);
  });
});
