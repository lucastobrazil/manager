$.extend(true, controllers, {
  response_zone: {
    system_mode: function(e, jsonObj, model) {
      controllers.update_zone.set_system_mode(e, jsonObj, model);
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_zone|system_mode';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.response_zone.system_mode(e, jsonObj, NL);
  });

});