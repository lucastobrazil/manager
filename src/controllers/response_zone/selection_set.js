$.extend(true, controllers, {
  response_zone: {
    selection_set: function(e, jsonObj, model) {

      if (jsonObj.object !== null && jsonObj.object.list_names !== null) {        
        model.selection_set.list_names(jsonObj.object.list_names);        
        model.selection_set.active(jsonObj.object.active);        
        if(model.selection_set.list_names().length > 0 && NL.settings.selectedSystem().selections_only) { // if we have a selection set                  
           //loop of set and populate lists-all
          NL.musicList.listsAll(NL.musicList.findLists(model.selection_set.list_names()));          
        } else {
          NL.musicList.listsAll(NL.musicList.lists());
          // console.log(NL.musicList.listsAll());
        }

      }
    }
  }
});


//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_zone|selection_set';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    log.event(eventName, 'setting songs results');
    controllers.response_zone.selection_set(e, jsonObj, NL.zone);
  });
});
