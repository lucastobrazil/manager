$.extend(true, controllers, {
  response_zone: {
    playstate: function(e, jsonObj, nightlife) {
      if (jsonObj.object && jsonObj.object.playstate) {

        var playstate = jsonObj.object.playstate;
        playstate.distance = 1;
        if(playstate.new_song === true) {
            NL.zone.pause('mixing');
        } else {
            NL.zone.pause('');
        }
        playstate.context = '#tab-playlist';
        NL.nowPlaying(new NowPlayingSongViewModel(playstate));
        NL.playlist.current(new SongViewModel(playstate, 0));
        NL.zone.fromPlayState(playstate.status);

        // Ebrahim M: Added to get the Spotify IDs for the current song
        var songArray = [];
        var songFileNameArr = []; 
        songArray.push(NL.playlist.current() );
        songFileNameArr.push(NL.playlist.current().filename);
        controllers.general.getSpotifyIds(songFileNameArr, songArray);
    
        ////////////////////////////////////////////////////////////////////
      }
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_zone|playstate';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.response_zone.playstate(e, jsonObj, NL);
  });
});