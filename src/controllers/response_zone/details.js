$.extend(true, controllers, {
  response_zone: {
    details: function(e, jsonObj) {
      //console.log('##############>>>>>>>>>>>>>>>>>>>>', jsonObj);
      if(NL.settings.selectedSystemId() === 'manual'){
        NL.settings.selectedSystem().access = jsonObj.object.access;
        NL.settings.selectedSystem().accessLevel(jsonObj.object.access);
      }	
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_zone|details';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.response_zone.details(e, jsonObj);
  });
});