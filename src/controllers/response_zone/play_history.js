$.extend(true, controllers, {
  response_zone: {
    play_history: function(e, jsonObj, playlistModel){

      if (jsonObj.object){
        var history = jsonObj.object.play_history;
        var songArray = [];
        var songFileNameArr = []; // Ebrahim M: Added to get the Spotify IDs

//        if (jsonObj.object && jsonObj.object.current) {
//          //if(nowPlayingModel.filename !== jsonObj.object.current) {            NL.zone.pause('mixing');}
//          jsonObj.object.current.distance = 0;
//          NL.nowPlaying().song(new SongViewModel(jsonObj.object.current, 0));
//         // indexOffset = 1;
//        } 
        var timeSincePlay = 0;
        history.forEach(function(obj, index){
            if(index < NL.settings.playlistHorizon() || NL.settings.playlistHorizon() === 0) { // @TODO cahnge this so not every item is processed
                obj.distance = -1 - index;
                timeSincePlay -= obj.length;
                obj.when = timeSincePlay;
                obj.context = '#tab-playlist';
                var song = new SongViewModel(obj, -1);
                songArray.push(song);

                songFileNameArr.push(song.filename);
            }
        });
 
        songArray.reverse(); 
        songFileNameArr.reverse();

        playlistModel.history(songArray);        
       // $('.tooltip-helper').tooltip();
        controls.playlist.updateModal();
        controllers.general.getSpotifyIds(songFileNameArr, NL.playlist.history());
      }
    }
  }
});

$doc.one('nightlife-ready', function(){
  var eventName = 'response_zone|play_history';
  NL.bind(eventName, function(e, jsonObj){
    controllers.response_zone.play_history(e, jsonObj, NL.playlist);
  });
});