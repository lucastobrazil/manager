$.extend(true, controllers, {
  response_zone: {
    songs: function(e, jsonObj, model) {
      // console.log('##### response_zone.songs is recieved ' , jsonObj);
      if (jsonObj.object !== null && jsonObj.object.songs !== null) {
        model.loadResults(jsonObj.object);
//        controls.search.showSongs();
//        controls.search.expandAdvancedSearch();
      }      
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_zone|songs';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    log.event(eventName, 'setting songs results');
    controllers.response_zone.songs(e, jsonObj, NL.songSearchResults);
  });
});
