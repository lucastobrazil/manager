$.extend(true, controllers, {
  response_zone: {
    music_list: function(e, jsonObj, model) {
      controllers.update_zone.set_music_list(e, jsonObj, model, true);
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_zone|music_list';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.response_zone.music_list(e, jsonObj, NL.musicList);
  });
});