$.extend(true, controllers, {
  response_zone: {
    mix: function(e, jsonObj, nightlife) {
      
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_zone|mix';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.response_zone.mix(e, jsonObj, NL);
  });
});