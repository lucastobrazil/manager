$.extend(true, controllers, {
  request_system: {
    restart: function() {
      var msg = {
        verb: {
          hard: true
        }
      };

      NL.api.send('request_system', 'restart', msg);
    }
  }
});