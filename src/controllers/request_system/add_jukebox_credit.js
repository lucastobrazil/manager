$.extend(true, controllers, {
  request_system: {
    add_jukebox_credit: function(credit) {
      var msg = {
        object: {
          credit: credit
        }
      };

      NL.api.send('request_system', 'add_jukebox_credit', msg);
    }
  }
});