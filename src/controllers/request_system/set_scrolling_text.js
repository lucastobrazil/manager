$.extend(true, controllers, {
  request_system: {
    set_scrolling_text: function(obj, cb) {
      var msg = {
        object: obj
      };
      NL.api.send('request_system', 'set_scrolling_text', msg, cb);      
    }
  }
});