$.extend(true, controllers, {
  request_system: {
    avail_categories: function(e, jsonObj, cb){
		//NL.zone.fromAvailModes(jsonObj.object);
    var msg = {object:{}};
    NL.api.send('request_system', 'avail_categories', msg, cb);
    }
  }
});

$doc.one('nightlife-ready', function(){
  var eventName = 'response_zone|avail_categories';
  NL.bind(eventName, function(e, jsonObj){
    controllers.request_system.avail_categories(e, jsonObj, NL.playlist);
  });
});