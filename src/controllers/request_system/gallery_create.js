$.extend(true, controllers, {
  request_system: {
    gallery_create: function(obj, cb) {
      var msg = {
        object: obj
      };

      NL.api.send('request_system', 'gallery_create', msg, cb);
    }
  }
});