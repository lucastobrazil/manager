$.extend(true, controllers, {
  request_system: {
    set_random_number_config: function(objRngConfig) {
      var msg = {
		object: {
			'display_time': objRngConfig.display_time,
			'excluded_numbers': objRngConfig.excluded_numbers,
			'excluded_ranges': objRngConfig.excluded_ranges,
			'message': objRngConfig.message,
			'range': objRngConfig.range,
			'show_drawn_numbers': objRngConfig.show_drawn_numbers
		}
      };

      NL.api.send( 'request_system', 'set_random_number_config', msg );
    }
  }
});


