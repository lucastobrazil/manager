$.extend(true, controllers, {
  request_system: {
    delete_da_slide: function(filename, priority, cb) {
      var msg = {
        object: {
          filename: filename,
          priority: priority
        }
      };

      NL.api.send('request_system', 'delete_da_slide', msg, cb);
    }
  }
});