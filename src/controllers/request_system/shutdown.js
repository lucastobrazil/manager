$.extend(true, controllers, {
  request_system: {
    shutdown: function() {
      var msg = {
        verb: {
          hard: true
        }
      };

      NL.api.send('request_system', 'shutdown', msg);
    }
  }
});