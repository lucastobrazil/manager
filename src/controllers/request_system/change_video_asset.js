$.extend(true, controllers, {
  request_system: {
    change_video_asset: function(obj) {
      var msg = {
        object: {
          output: obj.output,
          navigate: obj.navigate
        }
      };

      NL.api.send('request_system', 'change_video_asset', msg);
    }
  }
});