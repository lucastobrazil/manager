$.extend(true, controllers, {
  request_system: {
    change_advertising_state: function(obj) {
      var msg = {
        object: {
          output: obj.output,
          pause: obj.pause
        }
      };

      NL.api.send('request_system', 'change_advertising_state', msg);
    }
  }
});