$.extend(true, controllers, {
  request_system: {
    job_info: function(job_id, cb) {
      var msg = {
        object: {
          'job_id': job_id          
        }
      };

      NL.api.send('request_system', 'job_info', msg, cb);
    }
  }
});