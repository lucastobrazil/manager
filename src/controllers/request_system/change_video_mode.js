$.extend(true, controllers, {
  request_system: {
    change_video_mode: function(obj) {
      var msg = {
        object: {
          output: obj.output,
          mode: obj.mode
        }
      };

      NL.api.send('request_system', 'change_video_mode', msg);
    }
  }
});