$.extend(true, controllers, {
  request_system: {
    gallery_delete: function(obj, cb) {
      var msg = {
        object: obj
      };

      NL.api.send('request_system', 'gallery_delete', msg, cb);
    }
  }
});