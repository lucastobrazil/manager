$.extend(true, controllers, {
  request_system: {
    set_jukebox_freeplay: function(toggle, cb) {
      var msg = {
        object: {
          enabled : toggle
        }
      };

      NL.api.send('request_system', 'set_jukebox_freeplay', msg, cb);
    }
  }
});