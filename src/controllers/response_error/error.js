$.extend(true, controllers, {
  response_error: {
    error: function(e, jsonObj) {

      if (jsonObj.object)
      {
        var error = jsonObj.object;
        log.error('An error occurred', error);

        switch(error.code){
          case 'system_disconnected':
            NL.api.close(error);
            break;
          case 'auth_fail':
            controls.settings.requireAuthentication(true);
            break;
          case 'onsite_verification_required':
            controls.settings.requireOnSiteAccess();
            break;
          case 'invalid_onsite_access_code':
            NL.settings.accessCode.code(undefined);
            NL.settings.accessCode.verified(false);
            controls.settings.requireOnSiteAccess();
            break;
          case 'action_disabled':
            controls.notification.show('This action has been disabled by the system configuration.');
            break;
          case 'access_denied' :
            controls.notification.show('Access Denied');
          }
      }
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_error|error';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.response_error.error(e, jsonObj);
  });
});