$.extend(true, controllers, {
  response_systems: {
    listing: function(e, jsonObj, model) {
      if (jsonObj.object && jsonObj.object.systems) {
        var systems = jsonObj.object.systems;

        // console.log('****** Listing ' , systems);
        if(jsonObj.object.user_is_manager !== true) {
          controls.notification.show('Error: User is not a manager');
          return;
        }
        var systemArray = [];

        systems.forEach(function(obj) { 
          systemArray.push(new SystemViewModel(obj));          
        });

        ////////////////////////////////////////////////////////////////////
        // Ebrahim M : Added at 'Tue 26 May 15 5:05 PM Ebrahim Mardani'
        /*systemArray.sort(function(left, right){ return left.client_name == right.client_name ? 
                            ( (left.zone == right.zone ) ? (left.zone_name > right.zone_name ? 1: -1 ) :  
                              (left.zone > right.zone ? 1 : -1) ):
                            ( left.client_name  >  right.client_name ? 1: -1 ); }                                        
                        );*/
        //////////////////////////////////////////////////////////////////////

        // console.log('systemArray =' , systemArray);
        systemArray.push(NL.settings.manualSystem);


        model.systems(systemArray);
        if(NL.settings.accountStatus() !== 'Signed in') {
            controls.settings.signedIn();
        }
        // Check if we should auto connect to the last known system
      }
    }
  }
});

//Bind event handlers after nightlife is up and running
$doc.one('nightlife-ready', function() {
  var eventName = 'response_systems|listing';
  log.event(eventName, 'binding events');

  NL.bind(eventName, function(e, jsonObj) {
    controllers.response_systems.listing(e, jsonObj, NL.settings);
  });
});