/*

highlight v4

Highlights arbitrary terms.

<http://johannburkard.de/blog/programming/javascript/highlight-javascript-text-higlighting-jquery-plugin.html>

MIT license.

Johann Burkard
<http://johannburkard.de>
<mailto:jb@eaio.com>

*/

jQuery.fn.highlight = function(pat) {
 if(this.find('.highlight').length === 0) {
   function innerHighlight(node, pat) {
    var skip = 0;
    if (node.nodeType == 3) {
     var pos = node.data.toUpperCase().indexOf(pat);
     if (pos >= 0) {
      var spannode = document.createElement('span');
      spannode.className = 'highlight';
      var middlebit = node.splitText(pos);
      var endbit = middlebit.splitText(pat.length);
      var middleclone = middlebit.cloneNode(true);
      spannode.appendChild(middleclone);
      middlebit.parentNode.replaceChild(spannode, middlebit);
      skip = 1;
     }
    }
    else if (node.nodeType == 1 && node.childNodes && !/(script|style)/i.test(node.tagName)) {
     for (var i = 0; i < node.childNodes.length; ++i) {
      i += innerHighlight(node.childNodes[i], pat);
     }
    }
    return skip;
   }
   return this.length && pat && pat.length ? this.each(function() {
    innerHighlight(this, pat.toUpperCase());
   }) : this;
  }
};


jQuery.fn.highlightNonExact = function(pat) {
  if(this.find('.highlight').length === 0) {  
    this.each(function() {
      $(this).html( r($(this).html() , pat).replace(/amp;/g, ''));  //@todo hacky fix for unescaped ampersands      
    });
  }
}

function r(str, search) {
                // assumes search.length > 0
                
                var searchChars = search.split('');
                var search = '';
                var replacement = '';
                for(var i=0, cnt=searchChars.length; i<cnt; i++) {
                                var c = searchChars[i];                                
                                search += '(' + c.replace(/([.?*+^$[\]\\(){}|-])/, "\\$1") + ')(.*?)';
                                replacement += '<span class="highlight">$' + (1+i*2) + '</span>$' + (2+i*2);
                }
                return str.replace(new RegExp(search, 'i'), replacement);
}

jQuery.fn.removeHighlight = function() { 
 return this.find("span.highlight").each(function() {
  this.parentNode.firstChild.nodeName;
  with (this.parentNode) {
   replaceChild(this.firstChild, this);
   normalize();
  }
 }).end();
};