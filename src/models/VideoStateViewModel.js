var VideoStateViewModel = function(obj) {
  if (obj === undefined) {
    obj = {};
  }

  var self = this;

  // Actual properties  
  self.generatedBngNumbers = ko.observableArray(); // Ebrahim M: This array is added for storing Bingo numbers
  self.lastDrawnNo = ko.observable(0); // Ebrahim M: This var is added for storing Bingo numbers
  self.asset = ko.observable(obj.asset);
  self.event_timestamp = obj.event_timestamp;
  self.locked = ko.observable(obj.locked);
  self.method = obj.method;
  self.output = obj.output;
  self.mode = ko.observable(obj.mode); // Viedo Mode : That can visual ambient or video , etc
  self.paused = ko.observable(obj.paused);
  self.selectedId = obj.locked ? obj.mode : 'AUTO';

  self.modes = ko.computed(function() {
    return ko.utils.arrayFilter(VideoOutputModes, function(item) {
      return $.inArray(self.output, item.show_on_output) > -1;
    });
  });

  self.lookupMode = function(id) {
    var mode;
    if (VideoOutputModes) {
      mode = ko.utils.arrayFirst(VideoOutputModes, function(item) {
        return id === item.id;
      });
    }
    return mode;
  };
  self.selectedMode = ko.computed(function() {
    return self.lookupMode(self.mode());
  });

  self.selectedModeName = ko.computed(function() {
    var mode = self.lookupMode(self.selectedId);

    if (mode) {
      return mode.name + ' [' + mode.id + ']';
    } else {
      return 'AUTO';
    }
  });

  self.videoOutputMode = ko.computed(function() {
    return self.lookupMode(self.mode());
  });

  ko.computed(function(){
    controls.video.newBingoLook( (NL.zone.version_num() < 3.94) ? false : true  );
    controls.video.newRngLook( (NL.zone.version_num() < 3.94) ? false : true  );    
  });
  
  self.changeMode = function(data) {
    console.log('$$$$$$$$ changeMode called >> ' , data);
    var mode = self.lookupMode(data.id);
    if (mode) { 
      mode = mode.code;
    }
    else {
      mode = 'AUTO';
    }
    if(mode == 'R') {
      NL.zone.system_mode.random(true);
    } else if(mode == 'Bingo') {
      NL.zone.system_mode.bingo(true);      
    } else { 
      NL.zone.additive('none');
      controllers.request_zone.set_system_mode(NL.zone);
      controls.video.changeMode(self.output, mode);
    }
  };

  self.toggleLock = function(data){
    if (self.locked()) {
      controls.video.unlock(self.output);
    }else{
      var mode = self.lookupMode(self.mode());
      controls.video.changeMode(self.output, mode.code);
    }
  },

  self.changeAdvertisingState = function(data, event) {    
    if (data.paused()) {
      controls.video.play(data.output);
    } else {
      controls.video.pause(data.output);
    }
  };

  self.setVideoAsset = function(data, event, navigate) {
    if (navigate === 'next') {
      controls.video.next(data.output);
    } else if (navigate === 'previous') {
      controls.video.previous(data.output);
    }
  };
};

