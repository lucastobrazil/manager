
function SettingsModel(options) {

  var settings = {
    eventNameSpace: '.nightlife',
    connectionMethod: ko.observable('utopia'),
    connectionPolicy: ko.observable('select'), // Ebrahim M : added and can be  'Manual' or 'select'
    utopia: new AuthenticationTicketModel({
      address: 'http://hdms-live.nightlife.com.au/hl',
      apiAddress: 'http://hdms-live.nightlife.com.au/api',
      username: '',
      password: ''
    }),
    logLevel: ko.observable('SILENT'),
    prevUsernames: ko.observableArray(),
    lastLoggedUser:ko.observable(''),
    respTimeout: ko.observable(2000),
    tickInterval : ko.observable(500),
    animationEnabled: ko.observable(true),
    pingInterval: ko.observable(9000),
    playlistHorizon: ko.observable(100),
    reconnectMax: ko.observable(1), // 150 -> 1
    reconnectInterval: ko.observable(5000),
    maxPings : ko.observable(3),
    systems: ko.observableArray(),
    selectedSystemId: ko.observable(),
    guestAccessLevel: ko.observable(), // Ebrahim M: This will be used if the zone is connected directly.
    manualSystem : new ManualIPSystemViewModel(),    
    //zoneIp:ko.observable(''), // Ebrahim M :  Added and update the t_address where the conect button is pressed
    //systemCode: ko.observable(''), // Ebrahim M :  Added and update the t_system where the conect button is pressed
    //zoneNumber: ko.observable(''), // Ebrahim M :  Added and update the t_zone where the conect button is pressed

    /* Access code stuff */
    accessCode: new AccessCodeModel(),

   // maxHistory: 100,
    developer: ko.observable(true),
    culture: ko.observable('en-AU'),
    showImages: ko.observable(true),
    autoListing: ko.observable(true),
    allLists: ko.observable(false),
    autoLogin: ko.observable(true),
    autoConnect: ko.observable(true),
    autoDisconnect: ko.observable(true),
    deviceInfo: ko.observable(),

    //search
    searchPageLimit : ko.observable(200), //
    autoPage : ko.observable(true),

    browser: ko.observable('')
  };

  // Allow overriding of default settings
  if (options) {
    settings = $.extend(settings, options);
  }

  // Attach the settings to self as direct properties
  $.extend(this, settings);

  this.accountStatus = ko.observable('Not signed in');
  this.systemStatus = ko.observable('Disconnected');

  this.init = function() {
    var self = this;

    // Keep the following variables synced with localStorage
    var settingsToSync = [
    {
      name: 'settings.developer',
      obj: self.developer
    },
    {
      name: 'settings.accessCode.code',
      obj: self.accessCode.code
    },
    {
      name: 'settings.accessCode.expiry',
      obj: self.accessCode.expiry
    },
    {
      name: 'settings.accessCode.lastVerified',
      obj: self.accessCode.lastVerified
    },
    {
      name: 'settings.accessCode.lengthValid',
      obj: self.accessCode.lengthValid
    },
    {
      name: 'settings.utopia.address',
      obj: self.utopia.address
    },

    {
      name: 'settings.utopia.apiAddress',
      obj: self.utopia.apiAddress
    },

    {
      name: 'settings.utopia.username',
      obj: self.utopia.username
    },
    {
      name: 'settings.prevUsernames',
      obj: self.prevUsernames
    },
    {
      name: 'settings.utopia.password',
      obj: self.utopia.password
    },

    {
      name: 'settings.connectionMethod',
      obj: self.connectionMethod
    },
    {
      name: 'settings.connectionPolicy',
      obj: self.connectionPolicy
    },
    {
      name: 'settings.logLevel',
      obj: self.logLevel,
      callback: log.setLevel
    },
    {
        name: 'settings.autoListing',
        obj: self.autoListing
    },
    {
        name: 'settings.autoDisconnect',
        obj:    self.autoDisconnect
    },
    {
      name: 'settings.reconnectInterval',
      obj: self.reconnectInterval
    },
    {
        name: 'settings.maxPings',
        obj: self.maxPings
    },
    {
      name: 'settings.respTimeout',
      obj: self.respTimeout
    },
    {
      name: 'settings.pingInterval',
      obj: self.pingInterval
    },
    {
      name: 'settings.selectedSystemId',
      obj: self.selectedSystemId
    },

    {
      name: 'settings.culture',
      obj: self.culture
    },
    {
      name: 'settings.developer.allLists',
      obj: self.allLists
    },
    {
      name: 'settings.developer.playlistHorizon',
      obj: self.playlistHorizon
    },
    {
      name: 'settings.developer.searchPageLimit',
      obj: self.searchPageLimit
    },
    {
      name: 'settings.developer.autoPage',
      obj: self.autoPage
    },
    {
      name: 'settings.utopia.acl.tokens',
      obj: self.utopia.acl.tokens
    },
    {
      name: 'settings.developer.showImages',
      obj: self.showImages
    },
    {
      name: 'settings.developer.tickInterval',
      obj: self.tickInterval
    },
    {
      name: 'settings.developer.animationEnabled',
      obj: self.animationEnabled
    },
    {
      name: 'settings.developer.autoLogin',
      obj: self.autoLogin
    },
    {
      name: 'settings.developer.autoConnect',
      obj: self.autoConnect
    },
    {
      name: 'settings.manualSystem.address',
      obj: self.manualSystem.t_address
    },
    {
      name: 'settings.manualSystem.system',
      obj: self.manualSystem.t_system
    },
    {
      name: 'settings.manualSystem.zone',
      obj: self.manualSystem.t_zone
    },
    {
      name: 'instagram.access_token',
      obj: NL.instagram.access_token
    },
    {
      name: 'controls.navigation.pinStatus',
      obj : controls.navigation.isPinned
    },
    {
      name: 'controls.navigation.drawerMenuShown',
      obj : controls.navigation.menuShown
    },
    {
      name: 'controls.navigation.lastNavigatedTab',
      obj : NL.context.tab      
    },
    {
      name: 'controls.navigation.isInstagramFeedModalOpen',
      obj : controls.navigation.isInstagramFeedModalOpen
    },
    {
      name: 'NL.spotify.userName',
      obj : NL.spotify.userName
    },
    {
      name: 'NL.spotify.access_token',
      obj : NL.spotify.access_token
    },
    {
      name: 'NL.spotify.refresh_token',
      obj : NL.spotify.refresh_token
    },
    {
      name: 'controls.search.arImportedPlaylists',
      obj : controls.search.arImportedPlaylists
    },
    {
      name: 'controls.search.arImportedResultlists',
      obj :controls.search.arImportedResultlists
    },
    {
      name: 'NL.spotify.spotifyUsers',
      obj : NL.spotify.spotifyUsers      
    },
    {
      name: 'settings.lastLoggedUser',
      obj: settings.lastLoggedUser
    },
    {
      name: 'controls.spotify.spAllGuestUsers',
      obj: controls.spotify.spAllGuestUsers
    }    
    ];

    // Loop through em
    $(settingsToSync).each(function(i, item) {
      if (!item.name || !item.obj) {
        return;
      }
      // Update settings based on local storage
      if (localStorage[item.name]) {
        var storedValue = localStorage[item.name];

        // console.log('!!!!!!!!!!!!' , localStorage[item.name]  + ' - ' + localStorage[item.obj]);
        if (storedValue !== 'undefined') {
          var value = ko.toJS(storedValue);

          // Extra step to parse objects
          if(value.indexOf && (value.indexOf('{') === 0 || value.indexOf('[') === 0)){
            value = JSON.parse(value);
          }
//          console.log('!!! ***** ' , value);
          // handle ACL date specifically          
          if(item.name === 'settings.accessCode.expiry') {
            value = new Date(Date.parse(value.replace(/"/g,'')));
          }

          if(item.name === 'controls.navigation.lastNavigatedTab'){
            controls.navigation.lastNavigatedTab = value;
          }

          if (value) {
            if(value === 'true'){
              value = true;
            }else if(value === 'false'){
              value = false;
            }
            item.obj(value);
          }

        }
      }

      // Watch for changes and update local storage as needed
      item.obj.subscribe(function(newValue) {        
        // console.log('>>>>>>>>>>>> arImportedPlaylists() written = ', controls.search.arImportedPlaylists());
        // console.log('subscription fired', newValue, typeof newValue);
        // window.testVal = window.testVal || [];
        // window.testVal.push(newValue);
        if (typeof(newValue) === 'undefined') {
          localStorage.removeItem(item.name);
        }
        else if (typeof(newValue) === 'object' || typeof(newValue) === 'array' ){          
          localStorage[item.name] = ko.toJSON(newValue);
        }
        else {
          localStorage[item.name] = newValue;
        }

        // Run any callback functions
        if (item.callback) {
          item.callback.call(this, newValue);
        }

      });
    });

    self.listsClients = function(systems) { // returns each unique group for a list of lists      
        var listClients = [];
        systems.forEach(function(system, index){          
          if(systems !== undefined && (system.client_name !== undefined) && (system.zone_name !== undefined)) {                        
            // console.log('I am here!' , system);
            var criteria = controls.settings.searchCriteria().toUpperCase();
            var clientName = system.client_name.toUpperCase();
            var zoneName = system.zone_name.toUpperCase();
            if( (clientName.indexOf(criteria) >= 0) || (zoneName.indexOf(criteria) >= 0) ) {              
              //if(system.active || controls.settings.showOfflineZone() ){
              // if showOfflineZone is false it filters the offline zones :) 
                if( (listClients.indexOf(system.client_name) < 0 ) ) {            
                    listClients.push(system.client_name);
                }
              //}
            }
          }
        });
        return listClients;   
    };

    self.clientZones = function(client) { // returns each unique group for a list of lists             
        var listZones = [];
        NL.settings.systems().forEach(function(system, index){
          var clientName = system.client_name;
//          console.log('*** : SettingsModel.js: '  + clientName + '- local Active: ' + system.localActive + '- Detect:'  + system.isDetecting(), system);
          if( (clientName === client) && ( system.localActive || system.isDetecting() ||  (NL.settings.connectionMethod() !== 'local'))  ) {
          //if( clientName === client) {
            listZones.push(system);
          }
        });
        return listZones;   
    };

    self.setSelectedSystemId = function(data) {
      if (data) {
        self.selectedSystemId(data.id);
      }
    };

    self.selectedSystem = ko.computed(function() {
      var system;
      if (self.systems()) {
        system = ko.utils.arrayFirst(self.systems(), function(item) {
          return self.selectedSystemId() === item.id;
        });
      }
      if(system !== null) {
        var selOnly = system.selections_only !== undefined ? system.selections_only : false;
        NL.musicList.selectionsOnly(selOnly);
        if(system.id === 'manual') { 
          NL.settings.connectionMethod('local');
        }
      }
      return system;
    });

    // Ebrahim M: Whenever the selectedSystem changes this subscribtion will be called as well!
    self.selectedSystem.subscribe(function(value) {
      // console.log(value);
      if(value){
        NL.settings.manualSystem.t_address(value.address);
        NL.settings.manualSystem.t_system( value.system );
        NL.settings.manualSystem.t_zone( value.zone );
        // NL.settings.manualSystem.access = 'view_only';

      } 
                 
      // NL.settings.manualSystem = new ManualIPSystemViewModel(value);
    });

    if (controls.navigation.queryString.allLists) {
      var allLists = controls.navigation.queryString.allLists;
      self.allLists(allLists === 'true');
    }

    if (controls.navigation.queryString.developer) {
      var dev = controls.navigation.queryString.developer;
      var setting = false;
      if(dev === 'true'){
        setting = true;
      }
      self.developer(setting);
    }

    if(settings.utopia.acl.tokens()[settings.selectedSystemId()]){
      settings.utopia.siteToken(settings.utopia.acl.tokens()[settings.selectedSystemId()]);
    }

    settings.utopia.siteToken.subscribe(function(newValue){
      var tokens = self.utopia.acl.tokens();
      tokens[self.selectedSystemId()] = newValue;
      self.utopia.acl.tokens.valueHasMutated();
    });
    controls.settings.accessCode(self.accessCode.code());
    self.deviceInfo(navigator.appVersion);
    if(self.deviceInfo().indexOf('MSIE') !== -1) {
        self.browser('IE');
        self.autoListing(false);
    } else {
        self.browser('Webkit');
    }

  };
}