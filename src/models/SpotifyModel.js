var SpotifyModel = function() {
    var self = this;

    self.isSingedIn = ko.observable(false);
    self.userName = ko.observable('Not signed in');
    self.displayUserName = null;
    self.spotifyCode = '';
    self.access_token = ko.observable('');
    self.refresh_token = ko.observable('');
    self.clientID = '078b9c942df947fdb580fea155a999d3';
    self.clientSecret = '7706ed6aaf2a429e925d9d3339cca5c1';

    // self.urlSpotifyInServerTokens = 'http://dev-hdmsvpn.nightlife.com.au/papi/spotify/get_token';
    // self.urlSpotifyInServerRefresh ='http://dev-hdmsvpn.nightlife.com.au/papi/spotify/refresh_token';
    self.urlSpotifyInServerTokens = 'http://live.nightlife.com.au/papi/spotify/get_token';
    self.urlSpotifyInServerRefresh ='http://live.nightlife.com.au/papi/spotify/refresh_token';

    // self.urlSpotifyInServerTokens = 'http://ebrahim.new-devintranet.nightlife.com.au/apps/SpotifyMmnl/SpGetTokens.php';
    // self.urlSpotifyInServerRefresh ='http://ebrahim.new-devintranet.nightlife.com.au/apps/SpotifyMmnl/SpRefreshToken.php';
    self.spotifyScope = 'user-read-private%20user-read-email%20user-library-modify%20playlist-read-private' +
           '%20playlist-read-private%20playlist-modify-public%20playlist-modify-private%20playlist-read-collaborative';

    if(window.location.href.indexOf('127.0.0.1') > -1) {
        self.redirect_uri = 'http://127.0.0.1:9093/build/';
    } else if(window.location.href.indexOf('dev-hdmsvpn') > -1) {
        self.redirect_uri = 'http://dev-hdmsvpn.nightlife.com.au/manager/';
    } else if(window.location.href.indexOf('hdms-live.nightlife.com.au') > -1) {
        self.redirect_uri = 'http://hdms-live.nightlife.com.au/manager/';
    } else if(window.location.href.indexOf('live.nightlife.com.au') > -1) {
        self.redirect_uri = 'http://live.nightlife.com.au/manager/';
    }

    self.authURL = 'https://accounts.spotify.com/authorize/?client_id=' + self.clientID + '&response_type=code' +
                   '&redirect_uri='+ self.redirect_uri + '&scope=' + self.spotifyScope + '&show_dialog=true&state=102938';
    self.pApi = new SpotifyWebApi({clientId: self.clientID, clientSecret: self.clientSecret });

    self.spotifyExpPlaylists = ko.observableArray();
    self.selectedExpPlaylist = ko.observable('');

    self.selectedImpPlaylist = ko.observable('');

    // For the Signed In + Guests users
    self.spotifyPlaylists = ko.observableArray();
    self.spotifyUsers = ko.observableArray();
    self.spotifyOwners = ko.observableArray();
    /////////////////////////

    self.getSpotifyPlaylists = function(aGuestUser, aGuestDisplayName, cb) {
        self.getAShotOfUserPlaylists(aGuestUser , 0 , 50, [], function (aSuccess, aTotalPlaylists){
            if (aSuccess === 'success'){
                if( aGuestUser === self.userName() ){
                    self.spotifyExpPlaylists.removeAll();
                }

                var arGuestPlaylist = [];
                var arGuestOwners = [];
                for (i = 0; i < aTotalPlaylists.length ; i++){
                    arGuestPlaylist.push( { name: aTotalPlaylists[i].name,
                                               id: aTotalPlaylists[i].id,
                                               owner: aTotalPlaylists[i].owner.id,
                                               collaborative:aTotalPlaylists[i].collaborative,
                                               tracksNumber: aTotalPlaylists[i].tracks.total} );

                    if( arGuestOwners.indexOf(aTotalPlaylists[i].owner.id.toUpperCase() ) < 0 ){
                        arGuestOwners.push(aTotalPlaylists[i].owner.id.toUpperCase() );
                    }

                    if(self.findOwnerIdx(aTotalPlaylists[i].owner.id) < 0) { // It does not exist in the array 
                        self.spotifyOwners.push({ownerId: aTotalPlaylists[i].owner.id,
                                                displayName: ''});
                    }
                    
                    if(self.userName() === aTotalPlaylists[i].owner.id){
                        self.spotifyExpPlaylists.push({
                                                    name: aTotalPlaylists[i].name,
                                                    id: aTotalPlaylists[i].id,
                                                    owner: aTotalPlaylists[i].owner.id});
                    }                    
                }

                if(self.spotifyExpPlaylists().length > 0){
                    self.selectedExpPlaylist(self.spotifyExpPlaylists()[0].id);
                    self.sortExpPlaylists();
                }
                self.removeUserPlaylists(aGuestUser);

                // It is the signed in user so we push it on top of the array
                if(aGuestUser === self.userName() ){  
                    self.spotifyPlaylists.unshift({  guestUser:aGuestUser, 
                                                  guestDisplayName:aGuestDisplayName, 
                                                  guestPlaylists:arGuestPlaylist, 
                                                  guestUserOwners: arGuestOwners});
                }
                else{
                    self.spotifyPlaylists.push({  guestUser:aGuestUser, 
                                                  guestDisplayName:aGuestDisplayName, 
                                                  guestPlaylists:arGuestPlaylist, 
                                                  guestUserOwners: arGuestOwners});

                    ///////////////////////////////////////////////////////////////////////////////
                    // Ebrahim M: pushed them for the dropdown list in 'Lists/Spotify Users Lists' tab page.
                    if( !controls.spotify.isGuestUserAdded(aGuestUser) ){
                        var tempDisplayName = (aGuestDisplayName === undefined || aGuestDisplayName === null)? 
                                                        '': ( ' (' + aGuestDisplayName + ')' );
                        controls.spotify.spAllGuestUsers.push({
                                            id:aGuestUser, 
                                            name: ( aGuestUser + tempDisplayName ) 
                                            });
                    }
                    if(controls.spotify.spSelectedGuestUser() === undefined){
                        controls.spotify.spSelectedGuestUser(aGuestUser);
                    }
                    ////////////////////////////////////////////////////////////////////////////////                
                }

                if(self.spotifyUsers().indexOf(aGuestUser) < 0){ // It does not exist in the array 
                    self.spotifyUsers.push( aGuestUser );
                }                

                cb('success', aTotalPlaylists.length);
            }
            else{
                cb('error', -1);
            }
        });
    };

    self.removeUserPlaylists = function (aSpotifyUser){
      self.spotifyPlaylists.remove(function(item){ 
        return item.guestUser === aSpotifyUser;
      });
    };

    self.sortExpPlaylists = function(){
        self.spotifyExpPlaylists.sort(function(left, right){
            leftUpper = left.name.toUpperCase();
            rightUpper = right.name.toUpperCase();
            return (leftUpper > rightUpper)? 1:-1;
        });
    }; 
    
    self.getAShotOfUserPlaylists = function(aUserName , aOffset , aLimit, aTotalPlaylists, cb){
        self.pApi.getUserPlaylists(aUserName , {offset:aOffset, limit:aLimit} , function(param, data) {
            if (data !== null && data !== undefined ){
                if( data.items.length > 0 ){
                    aTotalPlaylists = aTotalPlaylists.concat(data.items);
                }
                if(data.items.length === aLimit){
                    self.getAShotOfUserPlaylists(aUserName, (aOffset+50), aLimit, aTotalPlaylists , cb);
                }
                else{
                    cb('success',aTotalPlaylists);
                }
            }
            else{
             cb('error' , null);   
            }
        });
    };

    self.getPlaylistTracks = function( aOwnerUserName, aPlaylistId, cb) {
        if(aPlaylistId !== '' && aOwnerUserName !== ''){
           self.getAShotOfPlaylistTracks(aOwnerUserName, aPlaylistId, 0, 100, [], function(aTotalTracks){
                cb(aTotalTracks , aPlaylistId);
           });
        }
        else{
            cb(null);
        }
    };    
    
    self.getAShotOfPlaylistTracks = function(aOwnerUName, aPlalistId , aOffset ,aLimit , aTotalTracks, cb){
        self.pApi.getPlaylistTracks(aOwnerUName, aPlalistId , {offset:aOffset, limit:aLimit},function(param, data) 
        {            
            if( (data !== undefined) && (data !== null) && (data.items.length > 0) ){
                aTotalTracks = aTotalTracks.concat(data.items);
            }
            
            if( (data !== undefined) && (data !== null) && (data.items.length === aLimit) ){
                self.getAShotOfPlaylistTracks(aOwnerUName, aPlalistId , (aOffset+aLimit), aLimit, aTotalTracks , cb);
            }
            else{
                cb(aTotalTracks);
            }
        });
    };

    self.removeTracksFromSpPlaylist = function(playlistId, trackIds, cb) {
        var arSpotifyTracks = [];
        var i = 0;
        for( i = 0; i < trackIds.length ; i++){
            arSpotifyTracks.push('spotify:track:' + trackIds[i]);    
        }
        
        self.pApi.removeTracksFromPlaylist(self.userName(), playlistId , arSpotifyTracks , function(param , data) 
        {
            if(data !== null && data !== undefined){
                cb('success');
            }
            else{
                cb('error');
            }
        });
    };

    self.addTracksToSpPlaylist = function(trackIds, playlistName , cb) {
        var playListId = '';
        var arSpotifyTracks = [];
        var i = 0;
        for( i = 0; i < trackIds.length ; i++){
            arSpotifyTracks.push('spotify:track:' + trackIds[i]);    
        }

        for (i = 0; i< NL.spotify.spotifyExpPlaylists().length; i++){
          if(playlistName === NL.spotify.spotifyExpPlaylists()[i].name){
            playListId = NL.spotify.spotifyExpPlaylists()[i].id;
            break;
          }
        }
        
        self.addLimitTracksToSpPlaylist(playListId, arSpotifyTracks, 0 , cb);
    };

    self.addLimitTracksToSpPlaylist = function (playListId, arSpotifyTracks,StartIdx, cb){
        if(StartIdx >= arSpotifyTracks.length){
            cb('success');
            return true;
        }
        var arSpTracks = [];
        var i = 0;
        for (i = StartIdx ; (i < arSpotifyTracks.length) && (arSpTracks.length < 20 ); i++){
            arSpTracks.push(arSpotifyTracks[i]);
        }
        self.pApi.addTracksToPlaylist(self.userName(), playListId , arSpTracks , function(param, data) 
        {
            if(data !== null && data !== undefined){
                self.addLimitTracksToSpPlaylist(playListId, arSpotifyTracks, (StartIdx + 20) , cb);
            }
            else{
                cb('error');
            }
        });
    };

    self.createSpPlaylist = function(playlistName, cb) {
        if(playlistName === '' || playlistName === undefined){
            cb('error' , {});
            return false;
        }       

        self.pApi.createPlaylist(self.userName(), {name: playlistName}, function(param, data) 
        {               
            if(data !== null && data !== undefined){
                self.spotifyExpPlaylists.push({name: playlistName , id: data.id , owner:self.userName()});
                // self.spotifyImpPlaylists.push({name: playlistName , id: data.id , owner:self.userName()});
                cb('success' , data);
            }
            else{
                cb('error');
            }
        });
    };

    self.updateAllGuestsPlaylists = function(aIndex){          
        console.log('>>> called updateAllGuestsPlaylists for ' + aIndex);
        if(aIndex < NL.spotify.spotifyUsers().length){
            self.isSpUserValid(NL.spotify.spotifyUsers()[aIndex], function (status,ownerId ,displayName){
                if(status === 'valid'){
                    self.getSpotifyPlaylists( NL.spotify.spotifyUsers()[aIndex], displayName, function(){
                        var spinTemp = ( (aIndex + 1) < NL.spotify.spotifyUsers().length );
                        controls.spotify.spinAddGuestUser(spinTemp);
                        
                        self.updateAllGuestsPlaylists(aIndex + 1);
                    });
                }
                else{
                    controls.spotify.spinAddGuestUser(false);
                }
            });
        }
    };

    self.getLoggedUser = function(playlistId, cb){
       if(self.access_token() !== ''){
            self.pApi.setAccessToken( self.access_token() );
            self.pApi.getMe({} ,function( param, data) {
                if(data !== null){                    
                    self.userName(data.id);
                    self.displayUserName = data.display_name;
                    // self.getUserPlaylists(function (){}); // We have the token so we get the Spotify playlists
                    self.getSpotifyPlaylists(self.userName(), data.display_name,  function (){});
                    self.updateAllGuestsPlaylists(0);
                }
            });
        }
    };

    // Ebrahim M: I move the last track (recently added track) by reordering the playlist 
    // (by calling 'reorderTracksInPlaylist' API) :
    // For the detail of the API (rangeStart, insertBefore and etc) refer to https://developer.spotify.com/web-api/reorder-playlists-tracks/
    self.moveLastTrackToTop = function(playlistId){
        NL.spotify.getPlaylistTracks( self.userName , playlistId , function(aItems, aPlayListId){
            var i = 0; 
            var arSpotifyTracks = [];

            if (data === null){
                return false;
            }
            for(i = 0; i < aItems.length ; i++ ){
              arSpotifyTracks.push(aItems[i].track.id);          
            }
            if(arSpotifyTracks.length > 0){
                var rangeStart  = arSpotifyTracks.length - 1;
                var insertBefore = 0;
                var options = {};
                self.pApi.reorderTracksInPlaylist( self.userName(), aPlayListId, rangeStart, insertBefore, options, 
                    function(param , data){} );
            }
        });
    };

    self.isTrackDuplicated = function(playlistId, songTitle, songArtist, cb){
        NL.spotify.getPlaylistTracks( self.userName , playlistId ,  function(aItems, aPlayListId){
            var i = 0;             
            if (aItems === null){
                cb(false);
                return false;
            }
            for(i = 0; i < aItems.length ; i++ ){
                if( ( aItems[i].track.name.toUpperCase() === songTitle.toUpperCase() ) && 
                    ( aItems[i].track.artists[0].name.toUpperCase() === songArtist.toUpperCase() ) ){
                    cb(true);
                    return true;
                } 
            }
            cb(false);
        });
    };
    
    self.isSpUserValid = function(aSpUserId,cb){
        self.pApi.getUser(aSpUserId, {} , function(param, data){
            // console.log('>>>>>>>>>>>>>>>>>>> Spotify User data', data);
            if(data === null || data === undefined){
                cb('invalid', aSpUserId , null);
            }
            else{
                cb('valid' , aSpUserId, data.display_name);
            }
        });            
    };

    self.findOwnerIdx = function(aOwnerId){
        for(var i = 0 ; i < self.spotifyOwners().length ; i++){
            if(self.spotifyOwners()[i].ownerId === aOwnerId){
                return(i);
            }
        }
        return(-1);
    };

    ko.computed(function() {
        if( ( self.userName() !== undefined ) && (self.userName() !== '') && (self.userName() !== 'Not signed in') ) {
            self.isSingedIn(true);            
        }
        else{
            self.isSingedIn(false);
        }
    }); 
    ko.computed(function(){
        var index = self.spotifyOwners().length - 1;
        if(index > 0){
            self.isSpUserValid(self.spotifyOwners()[index].ownerId , function (status, ownerId, displayName){
                if(status === 'valid') {                    
                    var idx = self.findOwnerIdx(ownerId);
                   // console.log( '@@@@@@@@@@@@@@@' + ownerId + ' >> ' + displayName + ' IDX = ' + idx);
                    if(idx >= 0){
                        self.spotifyOwners()[idx].displayName = displayName;
                    }
                }
            });            
        }
    }); 
};