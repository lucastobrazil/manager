var SocketReadyStateType = {
  Connecting: 0, // A value of 0 indicates that the connection has not yet been established.
  Open: 1, //A value of 1 indicates that the connection is established and communication is possible.
  Closing: 2, //A value of 2 indicates that the connection is going through the closing handshake.
  Closed: 3, //A value of 3 indicates that the connection has been closed or could not be opened.
  Default: 4 //A value of 4 indicates that the connection has not been initialised.
};