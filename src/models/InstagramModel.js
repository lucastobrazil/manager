var InstagramModel = function() {
    var self = this;
    self.images = ko.observableArray(); 
    self.tag = ko.observable('');
    self.sourceMode = ko.observable('uname');
    self.advancedSettingsShown = ko.observable(false);   
    self.displayTime = ko.observable(20);
    self.limitNumber = ko.observable(10);
    self.uname = ko.observable('');    
    self.apiHost  = 'https://api.instagram.com';      
    self.access_token = ko.observable('');
    self.username = ko.observable('');
    var thisYear = new Date().getFullYear();
    //self.redirect_uri = window.location.href +'/'; 
    //self.apiHost  = 'https://api.instagram.com'; 
    //self.redirect_uri = 'http://goo.gl/sj9YUl';
    self.tag = ko.observable('');
    self.start_date = ko.observable(new Date(thisYear, 0, 1));
    self.end_date = ko.observable(new Date(thisYear, 11, 31)); // months are zero based lol
    self.start_day = ko.observable();
    self.end_day = ko.observable();
    self.start_time = ko.observable();
    self.end_time = ko.observable();
    self.num_secs = ko.observable(30);
    self.plays_per_hour = ko.observable(99);
    self.squish = ko.observable(false);
    self.priority = ko.observable(false);
    self.width = ko.observable();
    self.height = ko.observable();

    if(window.location.href.indexOf('127.0.0.1') > -1) {
        self.clientID = '242e10521c594f3f83767c6666589683';
        self.redirect_uri = 'http://127.0.0.1:9093/build/'; 

    } else if(window.location.href.indexOf('dev-hdmsvpn') > -1) {
        self.clientID = '6d0fee503f6c4672a8c23b3f56c63171';  
        self.redirect_uri = 'http://dev-hdmsvpn.nightlife.com.au/manager/'; 

    } else if(window.location.href.indexOf('hdms-live.nightlife.com.au') > -1) { 
        self.redirect_uri = 'http://hdms-live.nightlife.com.au/manager/';
        self.clientID = '7124aa44ec60468b9cc77eac0406a0f2';

    } else if(window.location.href.indexOf('live.nightlife.com.au') > -1) { 
        self.redirect_uri = 'http://live.nightlife.com.au/manager/';
        self.clientID = '4474696e9a1249f2b8297e9d52838267';
    } 

    self.authURL = self.apiHost+'/oauth/authorize/?client_id='+self.clientID +'&redirect_uri='+
																	self.redirect_uri+'&response_type=token';    
    self.displayTime.subscribe(function(newValue) {
        if(self.displayTime() < 7){
            self.displayTime(7);
            controls.notification.show('Display Time must be greater than 7 seconds', controls.notification.types.error);
        }
    });

    self._url = ko.computed({
        read: function() {            
            var strExtraUrl = '';        
            // Ebrahim M: Check if NL is initalized!
           /* if(typeof(NL) !== typeof(undefined) ){
                if( NL.instagram.displayTime() !== 20 || NL.instagram.limitNumber() !== 10){
                    strExtraUrl = '&delay=' + NL.instagram.displayTime() + '&limit=' + NL.instagram.limitNumber();
                }
            } */
            
            if(self.displayTime() !== 20 || self.limitNumber() !== 10){
                strExtraUrl = '&delay=' + self.displayTime() + '&limit=' + self.limitNumber();
            }
            if(self.sourceMode() === 'uname'){                
                return 'http://www.nightlife.com.au/instagram/?user='+
                    self.uname().trim() + '&access_token=' + self.access_token() +  strExtraUrl;
            }else{
                return 'http://www.nightlife.com.au/instagram/?tag='+
                    self.tag().trim() + '&access_token=' + self.access_token() + strExtraUrl ;
            }             
        },
        write: function(value) {
            
        }
    });    
};

