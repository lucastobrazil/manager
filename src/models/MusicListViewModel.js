var MusicListViewModel = function() {
  var self = this;

  self.filtered_artists = ko.observable(0);
  self.filtered_songs = ko.observable(0);
  self.num_songs = ko.observable(0);
  self.selection_method = ko.observable();
  self.source = ko.observable();

  self.lists = ko.observableArray();
  self.listsAll = ko.observableArray();
  self.loadedLists = ko.observableArray();
  self.bIsAnyUserlistLoaded = ko.observable(true);

  self.selectedLists = ko.computed(function() {
    var lists = self.lists();
    return ko.utils.arrayFilter(lists, function(list) {
      if(list !== undefined) {
        return list.selected();
      }
    }).sort(self.compareLists);
  });

  self.activeTab = ko.observable('lists-all-panel');
  self.excludedUserLists = ['TAGGED', 'DELETED', 'HIDDEN', 'CULL'];

  self.selectionsOnly = ko.observable(false);

  self.loadData = function(data) {
    var self = this;

    var listNames = data.list_names;
    self.loadedLists(self.findLists(listNames));
    self.filtered_artists(data.filtered_artists);
    self.filtered_songs(data.filtered_songs);
    self.num_songs(data.num_songs);
    self.selection_method(data.selection_method);

    self.source(data.source);
  };

  self.filterLists = function(showUserList, filterParam) {
    var lists = self.lists();
    switch (filterParam){
      case 'songs' :
        return ko.utils.arrayFilter(lists, function(list) {
          if(list) {
            var group = list.group.toUpperCase();
            list.selected(false);
            return ( (group === 'UNKNOWN' || group === 'USER LIST') === showUserList );  
          }            
        });
    case 'visuals' : // Ebrahim M: Added to filter Visuals
        return ko.utils.arrayFilter(lists, function(list) {
          if(list) {
            var group = list.group.toUpperCase();
            list.selected(false);            
            return ( group === 'AMBIENT' && showUserList ); 
          }            
        });    
    case 'karaoke' : // Ebrahim M: Added to filter Karaokes
        return ko.utils.arrayFilter(lists, function(list) {
          if(list) {
            var group = list.group.toUpperCase();
            list.selected(false);            
            return ( (group === 'KARAOKE') ); 
          }            
        });
    }    
  };

  self.findLists = function(listNames) {
    var lists = self.lists();
    var result = [];

    listNames.forEach(function(listName, index) {      
      if ( ! isNaN (listName) ){
        listName = '_' + listName;
      }
      listName = listName.replace(/\s+/g, '_');

      var list = lists[listName];
      if (list) {
        result.push(list);
      }
    });

    return result;
  };

  self.findListsBySlot = function(listSlots) {
    var lists = self.lists();
    var result = [];

    listSlots.forEach(function(slot, index) {
      var list = lists['slot-' + slot];
      if (list) {
        result.push(list);
      }
    });

    return result.sort(self.compareLists);
  };

  self.userLists = function(lists) {
    var ret = ko.utils.arrayFilter(lists, self.filterUser);    

    // Ebrahim M : added to set the bIsAnyUserlistLoaded() flag!
    var i = 0;
    for(i= 0 ; i < ret.length ; i++){      
      if( (ret[i].style === 'user-list')  && ret[i].isLoaded() ){
          self.bIsAnyUserlistLoaded(true);
          break;
      }
    }
    if( i >= ret.length) {
      self.bIsAnyUserlistLoaded(false);
    }

    return ret.sort(self.compareLists);
  };

  self.ambientLists = function(lists) {

    var ret = ko.utils.arrayFilter(lists, self.filterAmbient);    
    return ret.sort(self.compareRawLists);
  };

  self.otherLists = function(lists) {

    var ret = ko.utils.arrayFilter(lists, self.filterOther);    
    return ret.sort(self.compareRawLists);
  };

  self.loadLists = ko.computed(function() {
    var filterType = (typeof(NL) !== typeof(undefined) )  ? NL.songSearch.typeFilter().value : 'songs';
    return self.filterLists(false, filterType).sort(self.compareLists);
  });

  self.loadUserLists = ko.computed(function() {
    var filterType = (typeof(NL) !== typeof(undefined) )  ? NL.songSearch.typeFilter().value : 'songs';
    var result = self.filterLists(true , filterType).sort(self.compareLists);
    return result;
  });

  self.resetLists = function() {
    var lists = self.lists();
    lists.forEach(function(list, index) {
      if (list.selected()) {
        list.selected(false);
      }
    });
  };

  self.filterUser = function(list) {
        if(list === undefined) {
          return false;
        }
        if(NL && NL.settings.allLists()){
          return true;
        }
        var group = list.group.toUpperCase();
        var type = list.type.toUpperCase();
        var schedulable = list.schedulable.toUpperCase();
        return type === 'A' && self.excludedUserLists.indexOf(list.name) < 0;
  };

  self.filterOther = function(list) {   
    if(list === undefined) {
      return false;
    }
    if(list.group.toUpperCase() === 'AMBIENT') {
      return false;
    }
    if(list.schedulable.toUpperCase() === 'N' && list.type !== 'A') {
      return true;
    }
    if(list.group.toUpperCase() === 'NEGATIVE') {
      return true;
    }
    if(list.type === 'A' && self.excludedUserLists.indexOf(list.name) >= 0) {
      return true;
    }
    return false;
    
  };

  self.filterAmbient = function(list) { 
    if(list === undefined) {
      return false;
    }
    if(list.group.toUpperCase() === 'AMBIENT') {
      return true;
    }
    return false;    
  };

  self.filterAll = function(list) {
      var group = list.group.toUpperCase();
      var type = list.type.toUpperCase();
      var schedulable = list.schedulable.toUpperCase();      
      return group !== 'NEGATIVE' && schedulable !== 'N';      
  };

  self.filterGroup = function(lists, group) {
    return ko.utils.arrayFilter(lists, function(list) {
      if(list !== undefined) {
          return ( ( group.toUpperCase() === list.group.toUpperCase() ) && (list.schedulable !== 'N' ||  list.group === 'AMBIENT') );
      }
    }).sort(self.compareLists);
  };

  self.filterSpGuestGroup = function(guestPlaylist, guestOwner) {
    var retArr = [];
    for(var i=0 ; i< guestPlaylist.length ; i++){
      if(guestPlaylist[i].owner.toUpperCase() === guestOwner.toUpperCase()){
        retArr.push({ name:guestPlaylist[i].name.toUpperCase(), 
                      id:guestPlaylist[i].id,
                      collaborative:guestPlaylist[i].collaborative, 
                      tracksNumber: guestPlaylist[i].tracksNumber,
                      owner: guestPlaylist[i].owner});
      }
    }   
    self.sort(retArr);
    return retArr;
  };

  self.sort = function(arrInput) {
    arrInput.sort(function(left, right) {
      return left.name.toUpperCase() < right.name.toUpperCase() ? -1 : 1;
    });
  };

  // Active lists are the selectable lists shown to the user
  // we don't show negative on non schedulable lists
  // group !== 'Negative' && schedulable !== 'N'
  self.activeLists = ko.computed(function() {
    var result = [];
    result = self.loadUserLists();
    result.push.apply(result,self.loadLists());
    result.sort(self.compareRawLists);

	  // Ebrahim M : Added to make it similar to MMNL mobile app!.
	  result.unshift( 
          {displayName:'All Songs (Everything available here and now)' , slot:'All Songs' }, 
					{displayName:'All Karaoke Songs (Karaoke songs available here and now)' , slot:'All Karaoke Songs'} , 
					{displayName:'All Visual Clips (Visual Clips available here and now)',  slot:'All Visual Clips'}, 
					{displayName:'Favourites (All your chosen songs)' , slot:'Favourites'},
          {displayName:'My Removed Songs (All removed songs from Music Zone)' , slot:'My Removed Songs'});
    return result;
  });

  self.selectedSongLists = function() { // returns all lists a current song is in
    if(NL.context.selectedSong()) {
        var result = self.findListsBySlot(NL.context.selectedSong().list_slots);
        return result;
    }
    return [];
  };

  self.compareLists = function(a, b) {
    var groupA = a.group.toUpperCase();
    var groupB = b.group.toUpperCase();

    if (groupA === 'USER LIST' && groupB !== 'USER LIST') {
      return -1;
    } else if (groupA !== 'USER LIST' && groupB === 'USER LIST') {
      return 1;
    } else {
      return a.group + a.name > b.group + b.name ? 1 : a.group + a.name < b.group + b.group ? -1 : 0;
    }
  };
  
  // sort lists alphabetically ignoring group names 
  self.compareRawLists = function(a,b) {
    if(a.name > b.name) {return 1;}
    if(a.name < b.name) {return -1;}
    return 0;
  };

  self.listsGroups = function(lists) { // returns each unique group for a list of lists      
      var listGroups = [];
      lists.forEach(function(list, index){
        if(lists !== undefined) {
         // console.log('!!!!!  list.group = '  + list.group + ' - list.schedulable  = '+ list.schedulable );
          if(listGroups.indexOf(list.group) < 0 && list.group !== 'User List' &&
             list.group !== 'NEGATIVE' && list.schedulable !== 'N' && list.group !== 'AMBIENT' ) {            
              listGroups.push(list.group);
          }
        }
      });
      // console.log('********************' , listGroups);
      listGroups.sort(function (a,b) {        
          return (a > b); // sort ascending
      });
      return listGroups;   
  };
};

$doc.on('nightlife-ready', function() {

 $('#list-tabs').find('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    NL.musicList.activeTab($(e.target).prop('href').split('#').pop()); // track current tab
  });
 
});