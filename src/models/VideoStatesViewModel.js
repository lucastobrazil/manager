// Ebrahim M: added this class to support 'Random Number'
var CRng = function(obj) {
  if (obj === undefined) {
    obj = {};
  }
  var self = this;
  self.msgTitle = ko.observable(obj.message);
  self.incRangeFrom = ko.observable(obj.range.from);
  self.incRangeTo = ko.observable(obj.range.to);
  self.displayTime = ko.observable(obj.display_time);
  self.excNumbers = ko.observableArray(obj.excluded_numbers);
  self.excRanges = ko.observableArray(obj.excluded_ranges);
  self.showDrawnNumbers = ko.observable(obj.show_drawn_numbers);
  if( self.showDrawnNumbers() ){
      controls.video.displayDrawnNum(true);
  }
  else{
      controls.video.displayDrawnNum(false);
  }

  self.sendRngSettings = function(){
    self.displayTime.notifySubscribers();
  };

  ko.computed(function() {
    controls.video.getExcRanges(self.excRanges);
  });

  ko.computed(function(){        
    var objRngTemp = {
      'display_time': self.displayTime(),
      'excluded_numbers': self.excNumbers(),
      'excluded_ranges': self.excRanges(),
      'message': self.msgTitle(),
      'range': { from: self.incRangeFrom(), to: self.incRangeTo() },
      'show_drawn_numbers': self.showDrawnNumbers()
    };
    controllers.request_system.set_random_number_config(objRngTemp);
  });

  ko.computed(function(){
    self.showDrawnNumbers(controls.video.displayDrawnNum());
  });
};

var VideoStatesViewModel = function() {
  var self = this;
  self.states = ko.observableArray();

  // Random Generator
  self.objRng = ko.observable();
  self.objRngNumbers = ko.observableArray();
  self.lastDrawnRngNo = ko.observable(-1);
  self.lastDrawnRngNoTemp = 0;
  ////////////////////////////////////////

  // Function to sort the array based on output number
  self.sort = function() {
    self.states.sort(function(left, right) {
      return left.output < right.output ? -1 : 1;
    });
  };

};

ko.bindingHandlers.YesNoList = {
    init: function(element, valueAccessor, allBindingsAccessor) {
        var target = valueAccessor();
        var options = function() { return [ 'Yes', 'No']; };
        ko.bindingHandlers.options.update(element, options, allBindingsAccessor);

        var observable = valueAccessor(),
            interceptor = ko.computed({
                read: function() {
                    var result = '';
                    if(observable() === true){
                        result = 'Yes';
                    } else if(observable() === false){
                        result = 'No';
                    }
                    return result;
                },
                write: function(newValue) {
                    var result = null;
                    if(newValue === 'Yes'){
                        result = true;
                    } else if(newValue === 'No'){
                        result = false;
                    }
                    observable(result);
                }                   
            });

        ko.applyBindingsToNode(element, { value: interceptor });
    }
};
