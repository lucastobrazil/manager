var SongSearchResultsViewModel = function() {
	var self = this;
	self.count = ko.observable(-1); //preserve vir

	self.moreResults = ko.observable(false); // true if there are more results to show for the current query
	self.lessResults = ko.observable(false); // true if results available to be prepended
	self.songs = ko.observableArray();
	self.jumpsOnPage = ko.observableArray(); //@TODO: remove if no longer used
	self.lockScroll = false;
	/*self.minYear = ko.observable(false);
	self.maxYear = ko.observable(false);
	self.minAlpha = ko.observable(false);
	self.maxAlpha = ko.observable(false);*/

	self.minKey = ko.observable('A');
	self.maxKey = ko.observable('Z');
	self.pageBarShown = ko.observable(false); // @TODO: remove if no longer used

	self.loadResults = function(object) { // @todo refactor		
		self.moreResults(true);
		self.lessResults(true);
		self.jumpsOnPage([]);
		self.count(object.count);
		// console.debug('loading results' , object);
		
		// Ebrahim M : These two lines are added to implment Exact and then Loose search (if there is no exact search)
		if(!NL.songSearch.adjacent()){ 
			// If it came here it means that it is the second round of the seach (it is loose search) so we set it to true
			NL.songSearch.isLooseSearchDone(true);
			NL.songSearch.bSearchInProgress(false);
		}

		if(object.count === 0) {
			self.moreResults(false);
			self.lessResults(false);

			// Ebrahim M : These two lines are added to implment Exact and then Loose search (if there is no exact search)
			if(NL.songSearch.adjacent()){
				NL.songSearch.adjacent(false);
			}
			else{
				////////////////////////////////////////////////////////////////////////////////////////////////////
				// Note: Mon 17 Aug 15 3:09 PM Ebrahim Mardani:
				// This if case is added because of a special case where in dropdown list in 'Search/Spotify LISTS'
				// is selected and there is no stored local data for this playlist and we have to call Spotify APIs.
				if( (NL.songSearch.activeTab() === 'search-spotify')  && 
						NL.songSearch.callRefresh  &&
						(NL.songSearch.selectedSpotifyList() !== undefined ) ){
					NL.songSearch.callRefresh = false;
					controls.lists.importSpPlaylist(NL.songSearch.selectedSpotifyList().id,
													NL.songSearch.selectedSpotifyList().name,
													NL.songSearch.selectedSpotifyList().owner );
				}
				////////////////////////////////////////////////////////////////////////////////////////////////////
			}

			// self.songs([]); // Thu 9 Jul 15 3:50 PM Ebrahim Mardani: This one is recently added.
			return;
		}

		NL.songSearch.bSearchInProgress(false);
		// console.debug(1);
		self.minKey(object.min);
		self.maxKey(object.max);
		// console.debug(2);
		NL.songSearch.setKeys();

		var jumpsArray = []; // @TODO remove
		var searchResults = object.songs;
		var songArray = [];
		var songFileNameArr = []; // Ebrahim M: Added to get the Spotify IDs
		var newSongFileNameArr = [];  // Ebrahim M: Added

		if(object.start !== undefined) {
			NL.songSearch.startIndex(object.start);// most likely not needed?
			if(object.start === 0) {
				self.lessResults(false);
			}
		} 
		var dist = 0;
		for(var i = 0; i < searchResults.length; i++) {
			var obj = searchResults[i];		
			var song = new SongViewModel(obj);

			song.distance = dist+NL.songSearch.startIndex()+1;
			song.context = '#tab-search';			
			songArray.push(song);
	        songFileNameArr.push(song.filename);

			dist++;
		}
		NL.songSearch.startIndex(object.start);   
		if(self.count() <= (NL.songSearch.limit()-0)+NL.songSearch.startIndex()) { 
			self.moreResults(false);
		}
		self.pageBarShown(NL.songSearchResults.count() > NL.songSearch.limit()); //@TODO Remove
		
		// console.debug('Saving results', songArray);

		self.songs(songArray); 
		self.jumpsOnPage(jumpsArray); //@TODO remove
		self.highlight();
		if(self.lessResults()) {
			controls.search.scrollToTop();
		}

		// Ebrahim M : Added because of Spotify Lists
		// console.log('###### songFileNameArr = ', songFileNameArr);
		newSongFileNameArr = self.checkIsItSpotifySearch(songFileNameArr, object.start);		

		if(newSongFileNameArr.length === 0){
			newSongFileNameArr = songFileNameArr;
		}

        // Ebrahim M: Added to get the Spotify IDs
		 controllers.general.getSpotifyIds(newSongFileNameArr, self.songs() ); 
		////////////////////////////////////////////////////////////////////
    };
    self.checkIsItSpotifySearch = function(arSongFileName , aStartIndex){
		var FileNameArr = [];  
		if (NL.songSearch.activeTab() === 'search-spotify' ){
			self.fillImportedPlaylists(arSongFileName, aStartIndex);
		}

		if( (self.songs().length > 0) && 
			(NL.songSearch.primarySort().value === '') && 
			(NL.songSearch.activeTab() === 'search-spotify' ) ){
				FileNameArr = self.reorderResult(arSongFileName);
		}

		return FileNameArr;
    };
    self.areTwoArraysEqual = function(Arr1,Arr2) {
		if (Arr1.length !== Arr2.length) {
			return false;
		}
		for (var i = 0; i < Arr2.length; i++) {
			if (Arr1[i].compare) {
				if (!Arr1[i].compare(Arr2[i])) {
					return false;
				}
			}
			if (Arr1[i] !== Arr2[i]) {
				return false;
			}
		}
		return true;
	};
	self.fillImportedPlaylists = function(arAvailableSongs, aStartIndex){
		if(NL.songSearch.selectedSpotifyList() === undefined){
			return false;
		}

		var selectedSpId = NL.songSearch.selectedSpotifyList().id;
		var idx = 0;
		for(idx = 0 ; idx < controls.search.arImportedPlaylists().length ; idx++){
			if(controls.search.arImportedPlaylists()[idx].spPlaylistId === selectedSpId){
				break;
			}
		}
		if(idx < controls.search.arImportedPlaylists().length){
			var playListName = controls.search.arImportedPlaylists()[idx].spPlaylistName;
			var playListId   = controls.search.arImportedPlaylists()[idx].spPlaylistId;
			var playlistOwner = controls.search.arImportedPlaylists()[idx].spOwner;
			var matchedFileNames = controls.search.arImportedPlaylists()[idx].fileNames;

			if(aStartIndex === 0){ // It's a new search result.
				controls.search.removeResultList(playListId);
		        controls.search.arImportedResultlists.push({ spPlaylistId:playListId,
                                              spOwner: playlistOwner,
                                              fileNamesAtSys:arAvailableSongs,
                                              resultCntAtSys: NL.songSearchResults.count(),
                                              AtSystem:NL.settings.selectedSystem().system
                                              });

				/*if(! self.areTwoArraysEqual(controls.search.arImportedPlaylists()[idx].fileNamesAtSys , arAvailableSongs) ){
					controls.song.removePlaylist(playListId);
					controls.search.arImportedPlaylists.push({
										spPlaylistName:playListName,
										spPlaylistId:playListId,
										spOwner:playlistOwner,
										fileNames:matchedFileNames,
										fileNamesAtSys:arAvailableSongs,
										resultCntAtSys:NL.songSearchResults.count(),
										AtSystem:NL.settings.selectedSystem().system
									});
				}*/
			}
		}
	};

	// Ebrahim M : This function is developed to reoder the 'My Spotify Tracks' based on the Spotify playlist order.
	self.reorderResult = function(pPatternArr){
		if(NL.songSearch.selectedSpotifyList() === undefined){
			return false;
		}
		
		var i = 0;
		var j = 0;
		var newSortedSongArray = [];
		var newSortedPatternArray = [];
		var selectedSpId = NL.songSearch.selectedSpotifyList().id;

		var idx = 0;
		for(idx = 0 ; idx < controls.search.arImportedPlaylists().length ; idx++){
		  if(controls.search.arImportedPlaylists()[idx].spPlaylistId === selectedSpId){
		    break;
		  }
		}
		if(idx < controls.search.arImportedPlaylists().length){
			for(i = 0; i < controls.search.arImportedPlaylists()[idx].fileNames.length ; i++){
				// var foundIn = $.inArray(pPatternArr, controls.search.arImportedPlaylists()[idx].fileNames[i]);
				var foundIn = pPatternArr.indexOf(controls.search.arImportedPlaylists()[idx].fileNames[i]); 
				if(foundIn >= 0 ){
					self.songs()[foundIn].distance = ++j;
					newSortedSongArray.push(self.songs()[foundIn]);
					newSortedPatternArray.push(pPatternArr[foundIn]);
				}
			}
			if(newSortedSongArray.length > 0){
				self.songs.removeAll();
				self.songs(newSortedSongArray);
			}
		}
		return newSortedPatternArray;
	};

	self.toJumpPoint = function(song) { // @TODO remove	
		var c = song[NL.songSearch.primarySort()][0].toLowerCase();	
		if(NL.songSearch.primarySort() === 'released') {
			return ''+new Date(song.released_sortkey).getFullYear();
		}
		else {
			if(c.search(/[^A-Za-z\s]/) != -1) { //if non alpha-numeric
				c = '#'; //convert to hash
			}
		}
		return c;
	};


	self.scrollBarStart = function() {
		return NL.songSearch.startIndex()/self.count()*100;
	};
	self.scrollBarWidth = function() {
		var setEnd = Math.min(NL.songSearch.limit()+NL.songSearch.startIndex(),self.count());
		var r = setEnd - NL.songSearch.startIndex();
		return self.count() * 100; // to percentage
		//return NL.songSearch.startIndex()/100;
	};
	self.unlockScroll = function() {
		self.lockScroll = false;
	};
	self.highlight = function() {
		var pattern = '';
		var target;
		if(NL.songSearch.activeTab() === 'search-title') {
			pattern = NL.songSearch.searchTitle().replace(/\s{2,}/g, ' ').trim();			
			target = $('#search-results-body').find('.title');
			
		} else if (NL.songSearch.activeTab() === 'search-artist') {
			pattern = NL.songSearch.searchArtist().replace(/\s{2,}/g, ' ').trim();
			target = $('#search-results-body').find('.artist');
		} else {
			return;
		}
		if(NL.songSearch.adjacent() === false && target !== undefined) {			
			target.highlightNonExact(pattern);
		} else {
			if(target !== undefined) {target.highlight(pattern);}
		}
	};

	self.spacerHeight = ko.computed(function() {		
		
		if(self.count() != -1) {
			if(self.songs().length < Math.round($('#tab-search').outerHeight() / $('.song-info').outerHeight() + 1)) {
			    //console.log('SPACE!');
				//return  ((NL.songSearch.limit() - NL.songSearchResults.songs().length) * $('.song-info').height())+'px';				
				return '100%';
			}
		}
		return '0px';
	});
 
};

$doc.on('nightlife-ready', function() {
	$('#search-results-body').on('scroll',function() {
		var offset = $('.search-spinner').height();
		if(! NL.songSearchResults.lockScroll) {
			if($('#search-results-body').scrollTop() >= $('#search-results-body').prop('scrollHeight') - $('#search-results-body').height() -offset) {      
				if($('#search-results-spinner-bottom').css('display') == 'none') {return;}
				NL.songSearchResults.lockScroll = true;
				controls.search.appendMore();
			} else if($('#search-results-body').scrollTop() <= 5) {
				if($('#search-results-spinner-top').css('display') == 'none') {return;}
				NL.songSearchResults.lockScroll = true;
				controls.search.prependMore();
			}
		} 
	});
});