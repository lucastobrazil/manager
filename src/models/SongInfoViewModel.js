
var SongInfoViewModel = function(song, list, index) {
  $.extend(this, song);

  if(list){
    this.list = list;
  }

  if(index !== undefined){
    this.index = index;
  }

  this.prev = undefined;
  this.next = undefined;
};