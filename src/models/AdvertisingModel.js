/* Advertising state
*/

var AdvertisingFileModel = function(file, cb) {
  var self = this;
  self.filename = file.name;
  self.size = file.size;
  self.uploaded = ko.observable(false);

  self.loadProgress = ko.observable(0);
  self.uploadingProgress = ko.observable(0); // Ebrahim M : Added for showing the progress bar

  self.file = file;
  self.img = new Image();
  self.img.cacheSrc = ko.observable();

  // Ebrahim M : divided the file to two parts 'known Extensions' and 'Unknown Extensions'
  if(self.filename.substr(-3).toLowerCase() === 'jpg' || self.filename.substr(-4).toLowerCase() === 'jpeg' ||
      self.filename.substr(-3).toLowerCase() === 'png' || self.filename.substr(-3).toLowerCase() === 'bmp' ||
      self.filename.substr(-3).toLowerCase() === 'gif') { // Ebrahim M: known file extensions
    var fr = new FileReader();
    fr.onprogress = function(event) {
      self.loadProgress(parseInt(event.loaded/event.total * 100, 10));
    };
    fr.onload = function() { // file is loaded

        self.img.onload = function() {
            self.width = self.img.width;
            self.height = self.img.height;
            //self.img = img;
            cb(self); // constructor finished, fire away capt'n
        };
        self.img.cacheSrc(fr.result);
    };
    fr.readAsDataURL(file);
  }
  else{ // Ebrahim M: unknown file extensions
    self.loadProgress(100);
    self.img.cacheSrc('unknown');
  }
};

var AdvertisingAppModel = function(obj) {
  var self = this;
    self.header = ko.observable('');
    self.sub_header = ko.observable('');
    self.body_text = ko.observable('');
    self.imgSrc = ko.observable('');
    self.filename = ko.observable('');
    self.bg_color = ko.observable('#000000');

  self.loadLogo = function(file, data) {
    self.filename(file.name);
    self.imgSrc(data);
  };
};

var AdvertisingChannelModel = function(name) {
  var self = this;
  self.assets = ko.observableArray([]);
  self.sidebarVisible = ko.observable(false);
  self.firstAssetSelected = ko.observable(0); //index
  self.lastAssetSelected = ko.observable(0); //model
  self.lastSelectedWasCtrl = ko.observable(false); //index
  self.assetListConfig = {
    canMultiSelect : ko.observable(false), // Lucas : For multiple selection change to true!
    canSearchFilter : ko.observable(true),
    canEditInPlace : ko.observable(false),
    canTogglePlayers : ko.observable(true),
    canToggleSidebar : ko.observable(false)
  };
  self.name = name;
  self.videoModeId = name === 'user' ? 'UA' : 'PA';
  self.currentModel = ko.observable(); //when user selects one ad - set by controls.advertising.showAssetSidebar

  self.currentEdittingModel  =  ko.observable(); // Added by Ebrahim M
  self.DAListEditMode = ko.observable(false); //allows user to 'edit in place'
  self.canReOrderAssets = ko.observable(false);
  self.isItHyperlink = ko.observable(true); // Added by Ebrahim M : for the hyper link in the side bar window.

  self.selectedAssets = ko.computed(function(){
    return self.assets().filter(function(e){
      if(e.selected()){
        // console.log('##############' , e.filename() );
        var filenameTemp = e.friendlyName();
        if( filenameTemp.substr(-3).toLowerCase() === 'jpg' || filenameTemp.substr(-4).toLowerCase() === 'jpeg' ||
            filenameTemp.substr(-3).toLowerCase() === 'mpg' || filenameTemp.substr(-3).toLowerCase() === 'mp4' ||
            filenameTemp.substr(-3).toLowerCase() === 'swf' || filenameTemp.substr(-3).toLowerCase() === 'gal') {
            self.isItHyperlink(false);
        }
        else{
          self.isItHyperlink(false);
          if(e.filename().indexOf('http://') >= 0 ){
            self.isItHyperlink(true);
          }
        }

        return true;
      }
    });
  });

  self.searchStr = ko.observable('');

  //filter the items using the filter text
   this.searchAssets = ko.computed(function() {
        return self.assets().filter(function(e) {
           return e.filename().toUpperCase().indexOf(self.searchStr().toUpperCase()) >= 0 && e.visible();
        });
    }, this);

   self.TOIwizardpage = ko.observable(1);

     // TODO: Create parameters so this function can be more global
    // self.clearSearchFilter = function(){
    //   self.searchStr('');
    // };
};

var AdvertisingEnvironmentModel = function() {

  var self = this;
  self.activeTab = ko.observable('user');
  self.playersVisible = ko.observable(false);
  self.assetListAbilities = ko.observableArray();

  self.uploadInProgress = ko.observable(false);
  self.deleteInProgress = ko.observable(false);

  // self.assetListConfig = {
  //     canMultiSelect : ko.observable(false),
  //     canSearchFilter : ko.observable(true),
  //     canEditInPlace : ko.observable(false),
  //     canTogglePlayers : ko.observable(true),
  //     canToggleSidebar : ko.observable(false)
  // };

  // self.DAListEditMode = ko.observable(false); //allows user to 'edit in place'
  // self.canReOrderAssets = ko.observable(false);
  //self.sidebarVisible = ko.observable(false);
  //self.firstAssetSelected = ko.observable(0); //index
  //self.lastSelectedWasCtrl = ko.observable(false); //index
  self.dateFormat = 'D dd M';
  self.fileSrc = '';
  self.priority = new AdvertisingChannelModel('priority');
  self.user = new AdvertisingChannelModel('user');
  self.galleries = new AdvertisingChannelModel();
    /*user : new AdvertisingChannelModel(),
    priority : new AdvertisingChannelModel()
  };*/
  self.app = new AdvertisingAppModel();

  var thisYear = new Date().getFullYear();
  self.jobs = {};
  self.times = ['All Day'];
  for(var h = 0; h < 24; h++) {
    for(var m = 0; m < 60; m += 15) {
      var nh = h.toString().length == 1 ? '0'+h : h;
      var nm = m.toString().length == 1 ? '0'+m : m;
      self.times.push(nh + ':' + nm);
    }
  }
  self.days = [
    {'label' : 'All Week', 'value' : 'All'},
    {'label' : 'Monday', 'value' : 'Mon'},
    {'label' : 'Tuesday', 'value' : 'Tue'},
    {'label' : 'Wednesday', 'value' : 'Wed'},
    {'label' : 'Thursday', 'value' : 'Thu'},
    {'label' : 'Friday', 'value' : 'Fri'},
    {'label' : 'Saturday', 'value' : 'Sat'},
    {'label' : 'Sunday', 'value' :'Sun'}
  ];

  self.allDate = [
    {'label' : 'All Year', 'value' : 'allYear'},
    {'label' : 'Specific Date', 'value' : 'specific'}
  ];

  self.newFileAsset = {
    selDate: ko.observable('allYear'),
    file : ko.observable(),
    filename : ko.observable(),
    start_date : ko.observable(new Date()),
    end_date : ko.observable(new Date(thisYear, 11, 31)), // months are zero based lol
    start_day : ko.observable(),
    end_day : ko.observable(),
    start_time : ko.observable(),
    end_time : ko.observable(),
    num_secs : ko.observable(5),
    plays_per_hour : ko.observable(99),
    squish : ko.observable(false),
    priority : ko.observable(false),
    width : ko.observable(),
    height : ko.observable(),
    isItJpgFile : ko.observable(false) // // Ebrahim M : added to show/hide 'fill screen' field in the advertising popup wnd
  };


  self.newWebPageAsset = {
     selDate: ko.observable('allYear'),
    _url : ko.observable(''),
    start_date : ko.observable(new Date()),
    end_date : ko.observable(new Date(thisYear, 11, 31)), // months are zero based lol
    start_day : ko.observable(),
    end_day : ko.observable(),
    start_time : ko.observable(),
    end_time : ko.observable(),
    num_secs : ko.observable(5),
    plays_per_hour : ko.observable(99),
    squish : ko.observable(false),
    priority : ko.observable(false),
    width : ko.observable(),
    height : ko.observable()
  };
  self.newWebPageAsset.url = ko.computed({
      read: self.newWebPageAsset._url,
      write: function(value) {
        self.newWebPageAsset._url(value.replace('http://','').replace('HTTP://',''));
      }
  });
  self.toiImageProcessing = ko.observable(false);
  self.toiUploadDialogVisible = ko.observable(true);
  self.fontListDropDownOpen = ko.observable(false);
  self.textColorPickerVisible = ko.observable(false);
  self.bgColorPickerVisible = ko.observable(false);

  // These need to be EXACTLY the same as the filenames of fonts in assets/toi/font-files/
  self.fontFamilies = ko.observableArray([
    {name: 'amaticsc'}, 
    {name: 'architectsdaughter'}, 
    {name: 'bangers'}, 
    {name: 'berkshireswash'}, 
    {name: 'changaone'}, 
    {name: 'chewy'}, 
    {name: 'dancingscript'}, 
    {name: 'frederickathegreat'}, 
    {name: 'fredokaone'}, 
    {name: 'frijole'}, 
    {name: 'gloriahallelujah'}, 
    {name: 'lilitaone'}, 
    {name: 'limelight'}, 
    {name: 'lobstertwo'}, 
    {name: 'marckscript'}, 
    {name: 'monda'}, 
    {name: 'montserrat'}, 
    {name: 'niconne'}, 
    {name: 'open_sanslight'}, 
    {name: 'pacifico'}, 
    {name: 'playball'}, 
    {name: 'rochester'}, 
    {name: 'specialelite'}
  ]);

  self.newTOIAsset = {
    bgImage: ko.observable(), // need to make this the right form of data 
    bgImageFilename: ko.observable(), 
    bgColor: ko.observable('000000'),
    textSize: ko.observable(1.5), 
    textColour: ko.observable('ffffff'), 
    fontFamily: ko.observable('montserrat'),
    bold: ko.observable(false), 
    shadowed: ko.observable(false), 
    borderguides: ko.observable(false), 
    verticalOffset: ko.observable(0),
    verticalOffsetPercent: ko.observable(0),
    verticalSpacing: ko.observable(10),
    verticalSpacingPercent: ko.observable(10),
    bgOpacity: ko.observable(50), //0 to 100
    tbArray: ko.observableArray([ // Array to hold up to 6 text boxes
      {content: ''} 
    ]),
    previewContainerHeight: ko.observable(360),
    previewContainerWidth: ko.observable(640),

    copy_bg_img: ko.observable('true'),
    upload_img: ko.observable('true'),
    // file : ko.observable(), 
    filename : ko.observable(''),

    start_date : ko.observable(new Date(thisYear, 0, 1)),
    end_date : ko.observable(new Date(thisYear, 11, 31)), // months are zero based lol
    start_day : ko.observable(),
    end_day : ko.observable(),
    start_time : ko.observable(),
    end_time : ko.observable(),
    num_secs : ko.observable(5),
    plays_per_hour : ko.observable(99),
    squish : ko.observable(false),
    priority : ko.observable(false),
    width : ko.observable(),
    height : ko.observable(),
    isItJpgFile : ko.observable(false) // // Ebrahim M : added to show/hide 'fill screen' field in the advertising popup wnd
  };

  self.newFileList = ko.observableArray();

  self.newGallery = {
    selDate: ko.observable('allYear'),
    filename : ko.observable(),
    start_date : ko.observable(new Date()),
    end_date : ko.observable(new Date(thisYear, 11, 31)), // months are zero based
    start_day : ko.observable(),
    end_day : ko.observable(),
    start_time : ko.observable(),
    end_time : ko.observable(),
    num_secs : ko.observable(30),
    plays_per_hour : ko.observable(99)
  };

  self.filterTypes = [
    {value : 'All', name : 'All'},
    {value : 'Active', name : 'Active'},
    {value : 'Inactive', name : 'Inactive'}
  ];

  self.filter = ko.observable(self.filterTypes[0]); // Ebrahim M : 1 -> 0

 self.isBetween = function (val, start, end) {
    if(start < end) {
      return(val >= start && val <= end);
    } else if(start > end) {
      return (val <= end || val >= start);
    } else if(start == end) {
      return val == start || val == end;
    }
    return false;
  };

  self.isActiveDay = function(startDay, endDay) {
    if(startDay == 'All Week' || startDay == 'All') { return true;}
    var days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
    var n_today = new Date().getDay();
    var n_start = days.indexOf(startDay);
    var n_end = days.indexOf(endDay);

    return self.isBetween(n_today, n_start, n_end);
  };

  self.isActiveTime = function(startTime, endTime) {
    if(startTime == 'All Day') { return true;}
    var d = new Date();
    var d_start = new Date();
    d_start.setHours(startTime.split(':')[0]);
    d_start.setMinutes(startTime.split(':')[1]);

    var d_end = new Date();
    d_end.setHours(endTime.split(':')[0]);
    d_end.setMinutes(endTime.split(':')[1]);

    return self.isBetween(d, d_start, d_end);
  };

  self.isActiveDate = function(startDate, endDate) {
    var d = new Date();
    var d_start = startDate.setHours('00');
    var d_end = endDate.setHours('23');
    return self.isBetween(d, startDate, endDate);
  };

  self.setActiveTab = function(tab) {
        self.activeTab(tab);

    };
};

var PreviewFile = function(obj){
  var self = this;

};

/* Model for multiple assets */
var MultiFileDetailsModel = function(obj) {
    var self = this;

};

/* Model for a single asset */
var AdvertisementModel = function(obj) {
  var self = this;
  self.selDate = ko.observable('specific');

  for(var key in obj) {
    self[key] = ko.observable(obj[key]);
  }
  var thisYear = new Date().getFullYear();
  if(typeof(obj.start_date) === 'string') {
    if(obj.start_date === '01/01' && obj.end_date === '31/12'){
      self.selDate = ko.observable('allYear');
    }

    var splitDate = self.start_date().split('/');
    self.start_date(new Date(thisYear, splitDate[1]-1, splitDate[0]));
    splitDate = self.end_date().split('/');
    self.end_date(new Date(thisYear, splitDate[1]-1, splitDate[0]));
  }
  if(self.start_day() == 'All') {
    self.start_day('All Week');
    self.end_day('');
  }
  if(self.start_time() == '00:00' && self.end_time() == '23:59') {
    self.start_time('All Day');
    self.end_time('');
  }
  if(self.filename().substr(-3).toUpperCase() === 'GAL') {
    self.gallery = true;
  } else {
    self.gallery = false;
  }

self.friendlyName = ko.observable(self.filename());
/*********************************/
/***** PROCESS FILENAMES *********/
/*********************************/
  if(self.filename().substr(0,37) === 'http://www.nightlife.com.au/instagram'){

    var s = self.filename();
    uname = s.match(/user=([^&]+)/) !== null ? s.match(/user=([^&]+)/)[1] : undefined;
    tag = s.match(/tag=([^&]+)/) !== null ? s.match(/tag=([^&]+)/)[1] : undefined;
    delay = s.match(/delay=([^&]+)/) !== null ? s.match(/delay=([^&]+)/)[1] : undefined;

    if(uname){
      self.friendlyName('Instagram @' + uname);
    }

    if(tag){
      self.friendlyName('Instagram #' + tag);
    }
  }
  self.players = ko.observableArray([]);
  self.selected = ko.observable(false);
  self.index = ko.observable();
  self.viewIndex = ko.observable(0);
  self.playing = ko.computed(function() {
    self.players([]);
    var isPlaying = false;
    for(var i = 0; i < NL.videoStates.states().length; i++) {
      if(NL.videoStates.states()[i].asset().substr(8).toUpperCase() === (self.filename().toUpperCase()) ||
        NL.videoStates.states()[i].asset().toUpperCase() === (self.filename().toUpperCase()))
      {
        self.players.push(NL.videoStates.states()[i].output);
        isPlaying = true;
      }
    }
    return isPlaying;
  });

  self.active = ko.computed(function(newval) {
    /*console.log('active changing',
      self.filename(),
      self.
    )*/
    if(self.plays_per_hour() === 0 || self.num_secs() === 0) {
      return false;
    }
    return NL.advertising.isActiveDay(self.start_day(), self.end_day()) &&
      NL.advertising.isActiveTime(self.start_time(), self.end_time()) &&
      NL.advertising.isActiveDate(self.start_date(), self.end_date());
  });

  self.visible = ko.computed(function() {
    switch(NL.advertising.filter().name) {
      case 'Active' :
        return self.active();
      case 'Inactive' :
        return !self.active();
      case 'All' :
        return true;
    }
  });


  self.src = ko.computed(function() {
    if(self.filename().substr(-3).toLowerCase() === 'swf') {
      return 'assets/img/flash-icon.svg';
    }
    if(['jpg','png'].indexOf(self.filename().substr(-3).toLowerCase()) >= 0) {
        return 'assets/img/jpg-icon.svg';
    }
    if(self.filename().substr(-4).toLowerCase() === 'html'){
      return 'assets/img/html-icon.svg';
    }
    if(self.filename().substr(0,38).toLowerCase() ==='http://www.nightlife.com.au/instagram/'){
     return 'assets/img/instagram-icon.svg';
    }else if(self.filename().substr(0,4).toLowerCase() === 'http'){
      return 'assets/img/html-icon.svg';
    }
    if(self.filename().substr(-3).toLowerCase() === 'mp4' || self.filename().substr(-3).toLowerCase() === 'mpg')  {
      return 'assets/img/mp4-icon.svg';
    }
    if(self.filename().substr(-3).toLowerCase() === 'gal')  {
      return 'assets/img/gallery-icon.svg';
    } else {
      return 'assets/img/unknown-icon.svg';
    }
  });

  self.isItaJpgFile = ko.computed(function() {
    if(self.filename().substr(-3).toLowerCase() === 'jpg' || self.filename().substr(-4).toLowerCase() === 'jpeg') {
      return true;
    }
    return false;
  });

};

$doc.on('nightlife-ready', function() {

 $('.advertising-tabs').find('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

    $('.advertising-tabs').find('.advertising-tab').removeClass('callout-bottom'); //add callout
    $(e.target).addClass('callout-bottom');
  });

    // var tabHeight = $('.advertising-panel').outerHeight();
    // var panelHeaderHeight = $('.advertising-panel-header').outerHeight();
    // var rowHeaderHeight = $('.advertising-table > .row-header').outerHeight();
    // console.log('tab height =  ' + tabHeight);
    // console.log('panelHeaderHeight =  ' + panelHeaderHeight);
    // console.log('rowHeaderHeight =  ' + rowHeaderHeight);
    // $('#advertising-table-body').height(tabHeight - panelHeaderHeight - rowHeaderHeight);
});
