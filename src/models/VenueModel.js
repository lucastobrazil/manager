var VenueModel = function(obj) {
    if(obj) {
        this.appAd = ko.observable({
                bg_color: obj.bg_color,
                body_text: obj.body_text,
                header: obj.header,
                image_url: obj.image_url,
                last_modified: obj.last_modified,
                page_url: obj.page_url,
                sub_header: obj.sub_header
        });
    } else {
        this.appAd = ko.observable({
                bg_color: '',
                body_text: '',
                header: '',
                image_url: '',
                last_modified: '',
                page_url: '',
                sub_header: ''
        });
    }
};