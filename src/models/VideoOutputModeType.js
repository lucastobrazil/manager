var VideoOutputModeType = {
  VideoClips: 'V', // Only available on video output 1.
  OutPut1: '1', // Only available on video output 2.
  UserAdvertising: 'UA', // Uset Advertising.
  VideoInput3: '3', // Video Input 3.
  VideoInput4: '4', // Video Input 4.
  PriorityAdvertising: 'PA', // Priority Advertising.
  AmbientVisuals: 'AM', // Ambient Visuals.
  RandomNumber: 'R' // Random Number GEnerator
};
var VideoOutputModes = [
{
  id: 'V',
  code: 'Video',
  name: 'Video Clips',
  show_on_output: [1],
  can_pause: false,
  can_cycle: false
},
{
  id: '1',
  code: 'Video',
  name: 'Output 1',
  show_on_output: [2],
  can_pause: false,
  can_cycle: false
},
{
  id: 'UA',
  code: 'UA',
  name: 'User Promos',
  show_on_output: [1, 2],
  can_pause: true,
  can_cycle: true
},
{
  id: '3',
  code: 'EXT1',
  name: 'Video Input 3',
  show_on_output: [1, 2],
  can_pause: false,
  can_cycle: false
},
{
  id: '4',
  code: 'EXT2',
  name: 'Video Input 4',
  show_on_output: [1, 2],
  can_pause: false,
  can_cycle: false
},
{
  id: 'PA',
  code: 'PA',
  name: 'Priority Promos',
  show_on_output: [1, 2],
  can_pause: true,
  can_cycle: true
},
{
  id: 'AM',
  code: 'Ambient',
  name: 'Ambient Visuals',
  show_on_output: [1, 2],
  can_pause: false,
  can_cycle: true
},
{
  id: 'R',
  code: 'R',
  name: 'Random Number',
  show_on_output: [1],
  can_pause: false,
  can_cycle: false
},
{
  id: 'B',
  code: 'Bingo',
  name: 'Bingo',
  show_on_output: [1],
  can_pause: false,
  can_cycle: false
}
];