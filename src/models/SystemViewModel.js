var SystemViewModel = function(obj) {
  if (obj === undefined) {
    obj = {};    
  }

  var self = this;

  self.access = obj.access;
  self.active = obj.active;  // Ebrahim M: This variable shows the active status of a zone in Web mode
  self.localActive = obj.localActive;  // Ebrahim M: This variable shows the active status of a zone in Local mode
  self.version = obj.version;
  self.client_name = obj.client_name;
  self.client_code = obj.client_code;
  self.da_access = obj.da_access;
  self.local_ip = obj.local_ip;
  self.local_port = 9090;
  self.system = obj.system;
  self.zone = obj.zone;
  self.zone_name = obj.zone_name;    
  self.id = (self.client_name + '.' + self.system + '.' + self.zone_name).replace(' ', '-');
  self.address = self.local_ip && self.local_port ? self.address = 'http://' + self.local_ip + ':' + self.local_port + '/hl' : '';

  // Sent through e-mail by Xing L at '19 May 2015'
  // For direct connects, the address won’t change (continue using the local_ip / local_port as before).
  // For server connects, the request_systems/listing API call will return a new parameter for each zone, named “ws_url”
  // This is the raw websocket URL, which the native apps should connect to for that zone.  Note that 
  // if the systems/listing request was made over HTTPS, the protocol for ws_url will be wss:// instead of ws://
  // As for the webapp, since it connects via SockJS, Ebi will need to amend the URL for it to work properly.  
  // My suggestion is as follows: if ws_url ends in “/websocket”, strip it off and send the resulting URL to SockJS, eg:
  // ws_url = ws_url.replace(/\/websocket$/, '');
  self.serverAddress = obj.ws_url;  // Ebrahim M : Added because of removing port from url

  self.selections_only = obj.selections_only ? obj.selections_only : false;
  self.getInfo = function() {
      return 'System - ' + self.system + ' Zone - ' + self.zone + '\n' + 'IP - ' + self.local_ip + ' Port - ' + self.local_port;
  };
  self.isDetecting = ko.observable(false);  
  self.accessLevel = ko.observable(''); // Ebrahim M : Added because we need a observable 'access' variable 
  //self.OnlineNotConnectable = ko.observable(false); // Ebrahim M :

  // Lawrence - call a function for a CSS class name   
  /*self.icon = ko.computed(function() {
    if(NL.selectedSystemID == self.id) {
      if(system status is connected)
        return 'tick'
      else 
        return 'cross'
    }
  })*/
};

// Attached using prototype to minimise overhead
ko.utils.extend(SystemViewModel.prototype, {
  displayName: function() {
    var status =  this.active ? '[Online '  : '[Offline ';
    status += this.version ? this.version.substr(0,4) +']' : ']';
    if (NL.settings.developer()) {
      return this.client_name + ' - ' + this.zone_name ;
      // return this.client_name + ' - ' + this.system + ' - ' + this.zone_name ;// + ' ' + status;
    } else {
      return this.client_name + ' - ' + this.zone_name ;// + ' ' + status;
    }
  },
  zoneStatus : function(){
    var status =  this.active ? 'Online '  : 'Offline ';
    status += this.version ? this.version.substr(0,4) : '';

    return status;
  },  
  accessLevelName: function() {
    //console.log('********************' , this.access);
    this.accessLevel(this.access);

    var words = this.access.split('_');
    words.forEach(function(word, i) {
      words[i] = word[0].toUpperCase() + word.slice(1);
    });
    return words.join(' ');
  }
});

var ManualIPSystemViewModel = function(){ //system) {
  var self = this;
  self.access = 'full_control';
  self.accessLevel = ko.observable('full_control'); 
  self.da_access = 'priority';
  self.active = true;
  self.system = ko.observable;
  self.version = '3.90';//null;
  self.id = 'manual';
  self.zone_name = '';
  self.local_ip = '';
  self.accessLevelName = function() { return '';};
  self.getInfo = function() { return '';};  
  self.address = '';
  self.displayName = ko.observable('');
  self.isDetecting = ko.observable(false);
   /*ko.computed(function() { 
    alert('Direct Connection: ' + self.local_ip);
    return 'Direct Connection: ' + self.local_ip;
  });*/
  self.zoneStatus = function() {return '';};
  self.zone = 1;

  self.t_address = ko.observable();
  self.t_system = ko.observable();
  self.t_zone = ko.observable();

  self.zoneIp = ko.observable(''); // Ebrahim M :  Added and update the t_address where the conect button is pressed
  self.systemCode = ko.observable(''); // Ebrahim M :  Added and update the t_system where the conect button is pressed
  self.zoneNumber = ko.observable(''); // Ebrahim M :  Added and update the t_zone where the conect button is pressed

  /* glue */
  self.t_address.subscribe(function(newValue) {   
    var newValueTemp = newValue;    
    var index1 = newValueTemp.indexOf('//');
    if(index1 >= 0){
        var index2 = newValueTemp.indexOf(':9090');
        if((index2 - index1 - 2) > 0 ){
          newValue = newValueTemp.substr( (index1 + 2), (index2 - index1 - 2));
        }
    }  
    self.address = 'http://' + newValue + ':9090/hl';
    self.local_ip  = newValue;

    self.displayName('Direct Connection: ' + self.local_ip   ) ;     
    // NL.settings.zoneIp(newValue);
    self.zoneIp(newValue);
  });

  self.t_system.subscribe(function(newValue) {
    self.system = newValue;    
    // NL.settings.systemCode(newValue);    
    self.systemCode(newValue);    
  });

  self.t_zone.subscribe(function(newValue) {
    self.zone = newValue;
    // NL.settings.zoneNumber (self.zone);        
    self.zoneNumber (self.zone);        
  });
};
