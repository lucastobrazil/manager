/*
 * ViewModel for a single song. This will be reused in several places including in lists
 */
var SongViewModel = function(song, playState) {

  var self = this;
  song = song || {};

  if (playState !== undefined) {
    this.playState = playState;
  }

  this.artist = song.artist;
  this.title = song.title;
  this.context = song.context !== undefined ? song.context : '';
  this.releasedMonth = '';
  this.releasedYear = '';
  if (this.regexReleaseDate.test(song.released)) {
    var date = this.regexReleaseDate.exec(song.released);
    this.releasedMonth = date[1];
    this.releasedYear = date[2];
  }
  this.added_by = song.added_by;
  this.category = song.category;
  this.length = song.length;
  this.released = song.released;
  this.removed = ko.observable(song.removed || false);
  this.bpm = song.bpm;
  this.filename = song.filename;
  this.list_slots = song.list_slots || [];
  this.load_method = song.load_method || '';
  this.video = song.video;
  this.when = song.when ? song.when : 0;
  this.distance = song.distance === undefined ? null : song.distance;
  this.id_spotify = song.id_spotify === undefined ? '' : song.id_spotify;

  this.preview_url = ko.observable('');
  this.favorite = song.favorite; // Added by Ebrahim M

  // Cache the song
  if (this.filename !== undefined) {
    NL.allSongs[this.filename] = this;
  }

  this.time_loaded = song.time_loaded;// ? moment(song.time_loaded) : '';
  this.time_played = song.time_played;// ? moment(song.time_played) : '';

  this.cssClass = this.playStateStyle();
  this.cssClass += (this.cssClass ? ' ' : '' ) + this.loadMethodStyle();
};

// Attached using prototype to minimise overhead
ko.utils.extend(SongViewModel.prototype, {
  regexFilename: new RegExp('([A-Z]+[0-9]*T)([0-9])$'),
  regexReleaseDate: new RegExp('([a-zA-Z]*)([0-9]{2})$'),

  lists: function() {
    return NL.musicList.findListsBySlot(this.list_slots);
  },

  formatedLength : function() {
      var min = Math.floor(this.length / 60);
      var sec = this.length - min * 60;
      if (min < 10) {min = '0' + min;}
      if (sec < 10) {sec = '0' + sec;}
      return min + ':' + sec;
  },
  formatedWhen : function() {
      var mins = 0;
      if(this.distance === 1) { // handle current song
         return 'Playing Now';
      } else if(this.when >= 0) {
        mins = Math.floor((this.when + NL.nowPlaying().remaining() / 1000) / 60);
        if(mins === 0) {
            mins = '< 1 ';
        }
        return 'Playing in about ' + mins + ' minutes';
      } else if (this.distance < 0) {
          var now = new Date();//new utils.Moment();
          /*if(this.time_played.dayOfYear() === now.dayOfYear()) { //if today
            return 'Played Today at ' + this.time_played.format('hh:mm A');
          } else {
            return 'Played ' + this.time_played.format('ddd') + ' at ' + this.time_played.format('hh:mm A');
          }*/
      } else { //
          return '';
      }
  },
  playStateStyle: function() {
    var result = '';
    switch (this.playState) {
      case -1:
        result = 'history';
        break;
      case 0:
        result = 'current';
        break;
      case 1:
        result = 'future';
        break;
    }

    return result;
  },

  loadMethodStyle: function() {
    var result = '';

    switch (this.load_method) {
        case '$':
          result = 'load-method-paid';
          break;
        case '#':
          result = 'load-method-teaser';
          break;
        case 'P':
          result = 'load-method-programmed';
          break;
        case 'R':
          result = 'load-method-random';
          break;
        case 'UL':
        case 'U':
          result = 'load-method-user';
          break;
        case 'UM':
          result = 'load-method-user-manager';
          break;
        case 'N':
          result = 'load-method-nightlife';
          break;
        case 'DJ':
          result = 'load-method-dj';
          break;
        case 'UP':
          result = 'load-method-crowddj';
          break;
    }
    return result;
  },

  loadMethodText: function() {
    console.log('<<<<<<<<<>>>>>>>>> loadMethodText = ' + this.load_method);
    switch (this.load_method) {
      case '$':
        result = 'Jukebox Paid';
        break;
      case '#':
        result = 'Jukebox Teaser';
        break;
      case 'P':
        result = 'Programmed';
        break;
      case 'R':
        result = 'load-method-random';
        break;
      case 'U':
        result = 'User';
        break;
      case 'UM':
        result = this.added_by !== undefined ?
          this.added_by.split('.')[0]+' '+this.added_by.split('.')[1] :
          'Manager App';
        break;
      case 'UP':
        result = 'crowdDJ';
        break;
      case 'N':
        result = 'Nightlife List';
        break;
      case 'DJ':
        result = 'DJ';
        break;
      default :
        result = '';
        break;
    }
    return result;
  },

  justPlayed : function() {
    this.playState = -1;
    this.cssClass = this.playStateStyle();
    this.cssClass += (this.cssClass ? ' ' : '' ) + this.loadMethodStyle();
    this.distance = this.distance === 1 ? -1 : this.distance - 1;
    return this;
  },

  total : function() {
    if(this.context == '#tab-search') {
      return NL.songSearchResults.count();
    } else {
      return NL.playlist.queue().length;
    }
  }
});
