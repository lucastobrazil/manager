var ZoneViewModel = function(obj) {
  var self = this;  
  if (obj === undefined) {
    obj = {};
  }  
  self.juke = ko.observable(obj.juke);
  self.additive = ko.observable(obj.additive);  
  self.primary = ko.observable(obj.primary);
  self.pause = ko.observable(obj.pause);  
  self.appMode = ko.observable(obj.appMode);
  self.creditsMax = ko.observable(obj.credits_max);
  self.creditRefill = ko.observable(obj.credits_refill);
  self.creditLastReset = ko.observable(obj.last_reset);  
  // self.freePlayMode = ko.observable(true);

  self.availModes = undefined;
  self.selection_set = { 
    list_names : ko.observable([]),
    active : ko.observable(false)
  };
  self.version = ko.observable('');
  self.version_num = ko.computed(function() {
    if(self.version() === '') {
      return 0;
    }    
    return parseFloat(self.version().substr(0,4));
  });

  self.system_mode = { // Scheduler , Kareoke , Bingo , RNG
    scheduler : ko.computed({
      read: function() {
        return self.primary() === 'scheduler_on';
      },
      write: function(checked) {
        
        if(checked === true) {
          self.primary('scheduler_on');
        } else {
          self.primary('scheduler_off');
        }
        controllers.request_zone.set_system_mode(self);
      }
    }).extend({throttle: 400}),
    karaoke : ko.computed({
      read: function() {        
        return self.primary() === 'karaoke_mc';
      },
      write: function(checked) {
        if(checked === true) {
          self.primary('karaoke_mc');
        } else {
          self.primary('scheduler_on');         
        }
        controllers.request_zone.set_system_mode(self);
      }
    }).extend({throttle: 400}),
    djManual : ko.computed({
      read: function() {        
        return self.primary() === 'dj_manual_cue_points'; //// 
      },
      write: function(checked) {
        if(checked === true) {
          self.primary('dj_manual'); //// 
        } else {
          self.primary('scheduler_on');   /////////      
        }
        controllers.request_zone.set_system_mode(self);
      }
    }).extend({throttle: 400}),
    random : ko.computed({      
      read: function() {
        return self.additive() === 'random_number';
      },
      write: function(checked) {
        if(checked === true) {
          self.additive('random_number');          
          /*controls.modes.callback = function() {
            setTimeout(controllers.request_system.generate_random_number, 800); // Ebrahim M : 500 -> 800
          };*/
        } else {     
            controls.modes.callback = undefined;
            self.additive('none');
        }       
        controllers.request_zone.set_system_mode(self, controls.modes.callback);         
        controls.modes.callback = function(){return;};
      }
    }).extend({throttle: 400}),
    bingo : ko.computed({
      read: function() {
        return self.additive() === 'bingo';
      },
      write: function(checked) {        
       // self.additive('none');
       //controllers.request_zone.set_system_mode(self);
        if(checked === true) {
          self.additive('bingo');
          controls.modes.callback = function() {
            setTimeout(controllers.request_system.display_bingo_screen, 800); // Ebrahim M : 500 -> 800
          };
        } else { 
            controls.modes.callback = undefined;
            self.additive('none');
      //    }
        }        
        controllers.request_zone.set_system_mode(self,controls.modes.callback);
         controls.modes.callback = function(){return;};
      }
    }).extend({throttle: 400}),
    juke : ko.computed({
      read: function() {
        if(self.juke()) {
          return self.juke().mode !== 'juke_off';
        } else {
          return false;
        }
      },      
      write: function(checked) {                      
        if(checked === true) {          
          self.juke().mode = 'juke_on';
        } else {             
            self.juke().mode = 'juke_off';      
        }        
        controllers.request_zone.set_system_mode(self,controls.modes.callback);
         controls.modes.callback = function(){return;};
      }
    }).extend({throttle: 400}),
    crowdDJ : ko.computed({
      read: function() {        
        // console.log('************ crowdDJ = ' , self.appMode());
        if( (self.appMode() == 'credits') || (self.appMode() == 'requests') ) {
          return true;
        }
        else {
          return false;        
        }
      }/*,       
      write: function(checked) {
        alert('write');
        if(checked === true) {          
          alert('credits');
          self.appMode('credits');
        } else {             
            self.appMode('off');
        }        
        controllers.request_zone.set_app_mode(self.appMode() );        
      }*/
      // The write section is not needed since the appMode variable will be updated by updateZone message 
    }).extend({throttle: 400}),
    crowdDJFreePlay : ko.computed({
      read: function() {
        if(self.creditRefill() > 0) {          
          return false;
        }
        else {          
          return true;        
        }        
      }
      /*,      
      write: function(checked) {  
        alert(checked);
        self.freePlayMode(checked);
        // controllers.request_zone.set_app_mode(self.creditsMax() , 0);        
      } 
      */     
      // The write section is not needed since the appMode variable will be updated by updateZone message
    }).extend({throttle: 400}),
    crowdDJMaxCredits : ko.computed({
      read: function() {
        var nMaxCredit = self.creditsMax();
        if(nMaxCredit < 1){
          nMaxCredit = 0;
        }
        return (nMaxCredit);
      },
      write: function(MaxCredits) {
        self.creditsMax(MaxCredits);
      }
    }).extend({throttle: 400}) ,
    crowdDJRefreshTime : ko.computed({
      read: function() {
          var RefreshTime = self.creditRefill() / 60.0;
          if(RefreshTime < 1){
            RefreshTime = 0;
          }
          return ( RefreshTime );
          //return ( RefreshTime.toFixed(1) );
      },
      write: function(RefreshTime) {
        self.creditRefill(RefreshTime * 60); // Min -> Sec
      }
    }).extend({throttle: 400})
  };

  self.pauseMsgs = {
      '' : 'PLAYING',
      'paused' : 'PAUSED',
      'pausing' : 'SCHEDULED PAUSE AT END OF SONG',
      'mixing' : 'MIXING'
  },

  self.primaryMsgs = {
     'dj_manual_cue_points' : 'DJ',
     'dj_automix' : 'SCHEDULER OFF',
     'scheduler_off' : 'SCHEDULER OFF',
     'karaoke_mc' : 'KARAOKE'
  };

  self.additiveMsgs = {
    'bingo' : 'BINGO',
    'random_number' : 'RANDOM NUMBER GENERATOR'
  };
  
  self.jukeMsgs = function() {
    if(self.juke()) {
      if(self.juke().connected) {
        if(self.juke().free_play) {
          return 'JUKEBOX IN FREE PLAY';
        } else {
          return 'JUKEBOX RUNNING';
        }
      }
    }
    return '';
  };

  self.fromPlayState = function(status) {
      switch(status) {
          case 'halting' : self.pause('pausing'); break;
          case 'halted' : self.pause('paused'); break;
          case 'karaoke_paused' : self.pause('paused');break;
          case 'playing' : self.pause(''); break;
          default : self.pause(''); break;
      }
  };
  self.fromAvailModes = function(obj) {
    self.availModes = obj;
  };


  self.statusMsg = ko.computed(function() {
        var status = self.pauseMsgs[self.pause()] ? self.pauseMsgs[self.pause()] : '';
        var juke = self.jukeMsgs();
        if(self.primaryMsgs[self.primary()]) {
          status += ' [' + self.primaryMsgs[self.primary()];
          if(self.additiveMsgs[self.additive()]) {
            status += ' + ' + self.additiveMsgs[self.additive()];
          }
          if(juke !== '') {
            status += ' + ' + self.jukeMsgs();  
          }          
          status += ']';
        } else {
          if(self.additiveMsgs[self.additive()]) {
            status += ' [' + self.additiveMsgs[self.additive()];
            if(juke !== '') {
              status += ' + ' + self.jukeMsgs();
            }
            status += ']';
          } else {
            status += ' ' + self.jukeMsgs();
          }
        }
        

        return status;
    }
  );
  self.cssClass= ko.computed(function() {
    switch(self.pause()) {
        case 'pausing' : return 'badge-warning'; 
        case 'paused' : return 'badge-error'; 
        case '' : return 'badge-success'; 
        case 'mixing' : return 'badge-info'; 
        default : return 'badge-success';
    }
    return 'badge-info';
  });

};

ko.utils.extend(ZoneViewModel.prototype, {

});

ko.bindingHandlers.numeric = {
    init: function (element, valueAccessor) {
        $(element).on('keydown', function (event) {
            // Allow: backspace, delete, tab, escape, and enter
            if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
                // Allow: Ctrl+A
                (event.keyCode == 65 && event.ctrlKey === true) ||
                // Allow: . ,
                (event.keyCode == 188 || event.keyCode == 190 || event.keyCode == 110) ||
                // Allow: home, end, left, right
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            else {
                // Ensure that it is a number and stop the keypress
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                    event.preventDefault();
                }
            }
        });
    }
};

