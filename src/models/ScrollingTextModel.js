// Mon 28 Sep 15 10:32 AM Ebrahim Mardani: Created
var ScrollingChannelModel = function(name) {
  var self = this;
  self.name = name;

  self.texts = ko.observableArray([]);

  self.sidebarMsgVisible = ko.observable(false);  
  // self.firstTextMsgSelected = ko.observable(0); //index
  self.lastTextMsgSelected = ko.observable(0); //model
  // self.lastSelectedWasCtrl = ko.observable(false); //index

  self.currentModel = ko.observable(); //when user selects one ad - set by controls.advertising.showAssetSidebar
  self.curMsgEditModel = ko.observable(); 

  self.selectedTextMsg = ko.computed(function(){
    return self.texts().filter(function(e){

      if(e.selected()){
        return true;
      }
    });
  });

  this.getAssets = ko.computed(function() {
        return self.texts().filter(function(e) {
           return true;
        });
  }, this);
};

var ScrollingTextModel = function() {
  var self = this;
  var thisYear = new Date().getFullYear();

  self.scrollingTextVisible = ko.observable(false);
  self.priority = new ScrollingChannelModel('priority');
  self.user = new ScrollingChannelModel('user');  
  self.uploadInProgress = ko.observable(false);
  self.deleteInProgress = ko.observable(false);

  self.times = ['All Day'];  
  for(var h = 0; h < 24; h++) {
    for(var m = 0; m < 60; m += 15) {
      var nh = h.toString().length == 1 ? '0'+h : h;
      var nm = m.toString().length == 1 ? '0'+m : m;
      self.times.push(nh + ':' + nm);
    }
  }
  self.days = [
    {'label' : 'All Week', 'value' : 'All'},
    {'label' : 'Monday', 'value' : 'Mon'},
    {'label' : 'Tuesday', 'value' : 'Tue'},
    {'label' : 'Wednesday', 'value' : 'Wed'},
    {'label' : 'Thursday', 'value' : 'Thu'},
    {'label' : 'Friday', 'value' : 'Fri'},
    {'label' : 'Saturday', 'value' : 'Sat'},
    {'label' : 'Sunday', 'value' :'Sun'}
  ];

  self.allFreq = [
    {'label' : 'Off', 'value' : 'off'},
    {'label' : 'Once per song', 'value' : 'once'},
    {'label' : 'Twice per song', 'value' : 'twice'},
    {'label' : 'Three times per song', 'value' : 'thrice'},
    {'label' : 'Four times per song', 'value' : 'four'},
    {'label' : 'Five times per song', 'value' : 'five'},
    {'label' : 'To end of song', 'value' : 'end'},
    {'label' : 'This message only', 'value' :'permanent'}
  ];

  self.allSpeed = [
    {'label' : 'Fast', 'value' : 'fast'},
    {'label' : 'Medium', 'value' : 'medium'},
    {'label' : 'Slow', 'value' : 'slow'},
    {'label' : 'Static', 'value' : 'static'}
  ];

  self.allDate = [
    {'label' : 'All Year', 'value' : 'allYear'},
    {'label' : 'Specific Date', 'value' : 'specific'}
  ];

  self.newMsgText = {
    selDate: ko.observable('allYear'),
    text:  ko.observable(''),
    slot:  ko.observable(1),
    frequency :ko.observable('once'),
    speed:ko.observable('slow'),
    start_date : ko.observable(new Date()),
    end_date : ko.observable(new Date(thisYear, 11, 31)), // months are zero based lol
    start_day : ko.observable(),
    end_day : ko.observable(),
    start_time : ko.observable(),
    end_time : ko.observable(),
    priority : ko.observable(false)
  };
};

/* Model for a single text message */
var TextModel = function(obj) {
  var self = this;
 // self.index = 0;
  self.selDate = ko.observable('specific');
  self.viewIndex = 0;
  self.selected = ko.observable(false);
  for(var key in obj) {
    self[key] = ko.observable(obj[key]);
  }
  var thisYear = new Date().getFullYear();
  if(typeof(obj.start_date) === 'string') {
    if(obj.start_date === '01/01' && obj.end_date === '31/12'){
      self.selDate = ko.observable('allYear');
    }
    var splitDate = self.start_date().split('/');
    self.start_date(new Date(thisYear, splitDate[1]-1, splitDate[0]));
    splitDate = self.end_date().split('/');
    self.end_date(new Date(thisYear, splitDate[1]-1, splitDate[0]));
  }
  if(self.start_day() == 'All') {
    self.start_day('All Week');
    self.end_day('');
  }
  if(self.start_time() == '00:00' && self.end_time() == '23:59') {
    self.start_time('All Day');
    self.end_time('');
  }
};