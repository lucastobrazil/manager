var NightlifeViewModel = function(){
  //this is the master view model which we attach all the other view models to
    var self = this;
  //$.extend(this, {    
    self.settings= new SettingsModel(),
    self.volume= new VolumeViewModel(),
    self.nowPlaying= ko.observable(new NowPlayingSongViewModel()),
    self.songSearch= new SongSearchViewModel(this.settings),
    self.songSearchResults= new SongSearchResultsViewModel(),
    self.videoStates= new VideoStatesViewModel(),
    self.musicList= new MusicListViewModel(),
    self.playlist= new PlaylistViewModel(),
    self.zone = new ZoneViewModel(),
    self.advertising = new AdvertisingEnvironmentModel(),
    self.textScrolling = new ScrollingTextModel(),
    self.instagram = new InstagramModel(),
    self.userInfo = new UserInfoModel(), // Ebrahim M : Added
    self.spotify = new SpotifyModel(), // Ebrahim M : Added
    self.context = { 
           tab : ko.observable('settings'), 
           modal : ko.observable(''),
           selectedSong : ko.observable('')
    },
    self.allSongs = {}, // Keep a ref to all the songs returned from the services
    self.venue = new VenueModel(),

    self.init = function() {
      this.settings.init();
      log.info('Nightlife - ready');
    },
    //Centralised location for binding document events 
    self.bind = function(event, callback) {
      var evt = event.replace(/ /g, this.settings.eventNameSpace + ' ') + this.settings.eventNameSpace;
      $doc.on(evt, callback);
    },
    //Centralised location for firing document events
    self.trigger = function(event, data) {
      var evt = event.replace(/ /g, this.settings.eventNameSpace + ' ') + this.settings.eventNameSpace;
      $doc.trigger(evt, data);
    },
    self.callbacks =  {};

  //});
};