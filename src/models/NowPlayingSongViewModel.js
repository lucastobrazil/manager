var NowPlayingSongViewModel = function(obj) {
  var self = this;

  if (obj === undefined) {
    obj = {};
  }

  this.cue_method = obj.cue_method;
  this.cue_type = obj.cue_type;
  this.event_timestamp = obj.event_timestamp;
  this.filename = obj.filename;
  this.first_cued = obj.first_cued;
  this.player = obj.player;
  this.playing = obj.playing;
  this.remaining = ko.observable(obj.remaining * 1000);
  this.elapsed = ko.observable(obj.elapsed * 1000);
  this.status = obj.status;
  this.id_spotify = obj.id_spotify; // HOLD
  
  this.song = ko.observable(new SongViewModel(obj, 0));
  this.song.distance = 1;

  //TODO: The below is a temporary workaround for a bug in the api that returns the filename incorrectly. Remove this code:
  var regexFilename = new RegExp('([A-Z]+[0-9]*T)([0-9])$');
  if (regexFilename.test(this.filename)) {
    var parts = regexFilename.exec(this.filename);
    this.filename = parts[1] + '0' + parts[2];
  }

  this.progress = function() {
    return parseInt(self.elapsed() / (self.elapsed() + (self.remaining())) * 100, 0);
  };

  this.pad = function(n) {
    return n < 10 ? '0' + n : n;
  };
  this.trim = function(n) {
    return n.substring(0, 2);
  };

  this.minSec = function(raw) //TODO: move this out somewhere more logical
  {
    if (raw <= 0) {
      return '00:00:00';
    }

    var d = new Date(raw);

    var min = d.getMinutes();
    var sec = d.getSeconds();
    
    var milli = d.getMilliseconds()%25;

    if (isNaN(min) || isNaN(sec) || isNaN(milli))
    {
      return ''; // no time available, show nothing instead of NaN
    }
    return (this.pad(min) + ':' + this.pad(sec) + ':' + this.pad(milli));
  };

  this.elapsedMinSec = ko.computed(function() {
    return this.minSec(this.elapsed());
  }, this);
  this.remainingMinSec = ko.computed(function() {
    if (this.remaining()) {
      return '-' + this.minSec(this.remaining());
    } else {
      return '';
    }
  }, this);

  this.tock = new Date(); // Used to calculate actual time diff
  this.tick = function() {

    if (this.remaining() <= 0) // No time left - no need to keep goi ng
    {
    //  controls.notification.show('mixing',controls.notification.types.success);
      NL.zone.pause('mixing');
      return;
    }
    // Calculated using dates because we can't rely on setInterval to fire on time, every time
    var diff = new Date() - this.tock;
    this.tock = new Date();

    var el = this.elapsed() + diff;
    var rem = this.remaining() - diff;
    this.elapsed(el);
    this.remaining(rem);
//    if(this.remaining() === 0) {
//        controls.notification.show('mixing',controls.notification.types.success);
//    }
    if (rem <= 0) //hack for testing
    {
      this.remaining(0);
      this.elapsed(0);
    }
  };

  this.placeholder = function(obj, e) {
    self.song().placeholder(obj, e);
  };
};