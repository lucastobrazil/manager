/* Manage code and expirary for onsite access verification
*/
var AccessCodeModel = function() {

  var self = this;

  self.code = ko.observable(undefined);
  self.expiry = ko.observable();
  self.verified = ko.observable(false);
  self.lengthValidHours = ko.observable();
  self.lengthValid = ko.computed(function() {
      return self.lengthValidHours();
  });
  //self.checkInterval = 3600000; // 1 hr
  self.lastVerified = ko.observable();
  //self.verified = ko.observable(false);
  self.timeout = undefined;
/*
  self.setACTimeout = function() {
    console.log('setting oav interval:');
  };*/

};