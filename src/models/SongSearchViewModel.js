var SongSearchViewModel = function(settingsModel) {
  var self = this;

  self.selectedListSlot = ko.observable();
  self.selectedSpotifyList = ko.observable(); // Ebrahim M: Defined this var for 'SPOTIFY LISTS/Dropdown list'
  self.callRefresh = false;
  self.query = ko.observable();
  self.search = ko.observable();
  self.adjacent = ko.observable(true);
  self.bSearchInProgress = ko.observable(false); // Ebrahim M : Defined this var to show the spinning icon when search is in progress


  // Ebrahim M : 'isLooseSearchDone' var is added to implement Exact and then Loose search (if there is no exact search)
  self.isLooseSearchDone = ko.observable(true);
  self.searchArtist = ko.observable('');
  self.searchTitle = ko.observable('');

  self.searchYear = ko.observable('');
  self.searchYearRange = ko.computed(function() {
    if(self.searchYear() !== undefined){
      if ( ( self.searchYear() === 'All Karaoke Songs') ||
           (self.searchYear() === 'All Visual Clips')  ||
                     (self.searchYear() === 'All Songs') ){
        return self.searchYear();
      }
      else{
        if(self.searchYear() === 'Pre 1920') {
          return {start: '' + 1600, end: 1919+''};
        }
        if(self.searchYear().length > 4) {
          var y = parseInt(self.searchYear().substring(0,4),10); //omg
          return {start: ''+y, end: (y+9)+''};
        } else {
          return {
            start: ''+self.searchYear(),
            end: ''+self.searchYear()};
        }
      }
    }
  });

  self.searchYears = ko.observableArray();


  self.startIndex = ko.observable(0);
  self.limit = settingsModel.searchPageLimit;//ko.observable(100);
  self.offset = ko.observable('All');


  self.activeTab = ko.observable('search-title');

  self.availableCategories = ko.observableArray();
  self.searchCategories = ko.observableArray();
  self.searchCategory = ko.observable();

  self.sortFieldTypes = [
   {value : 'title', name : 'SONG TITLE', dir : 'asc'},
   {value : 'artist', name : 'ARTIST NAME', dir : 'asc'},
   {value : 'category', name : 'GENRE', dir : 'asc'},
   {value : 'released', name : 'YEAR', dir : 'desc'},
   {value: 'bpm', name : 'BPM', dir : 'desc'},
   {value : '', name : 'SPOTIFY', dir : ''} // Ebrahim M: This option will be added only if 'SPOTIFY LISTS' is selected.
  ];

  self.primarySort = ko.observable(self.sortFieldTypes[0]);
  self.secondarySort = ko.computed(function() {
    switch(self.primarySort().value) {
      case 'title' :
        return self.sortFieldTypes[3];

      case 'artist' :
        return self.sortFieldTypes[3];

      case 'category' :
        return self.sortFieldTypes[3];

      case 'released' :
        return self.sortFieldTypes[0];

      default :
        return self.sortFieldTypes[0];
    }
  });

  self.searchTypeSortDefault = {
    '#search-artist' : self.sortFieldTypes[1],
    '#search-title' : self.sortFieldTypes[0],
    '#search-lists' : self.sortFieldTypes[3],
    '#search-year' : self.sortFieldTypes[3],
    '#search-category' : self.sortFieldTypes[3],
    '#search-spotify' : self.sortFieldTypes[3]
  };

  self.sortDir = ko.computed(function() {
    if(self.primarySort().value === 'released') { return 'desc';}
    else {return 'asc';}
  });

  self.filterFieldTypes = [
    {value : 'songs', name : 'SONGS'},
    {value : 'karaoke', name : 'KARAOKE'},
    {value : 'visuals', name : 'VISUALS'}
  ];

  self.typeFilter = ko.observable(self.filterFieldTypes[0]);

  self.categoryWhitelist = ko.computed(function() {
    // self.searchCategory(null);
    var whiteList = ['All Songs', 'All Karaoke Songs', 'All Visual Clips'];
    switch(self.typeFilter().value) {
        case 'songs' :
          whiteList = self.availableCategories().filter(function(el, index){
              return (( ['VISUALS','KARAOKE','PROMO'].indexOf(el) < 0)  && (el !== undefined));
          });

          // Ebrahim M : Added to make it similar to MMNL mobile app!.
          whiteList.unshift('All Songs', 'All Karaoke Songs' , 'All Visual Clips');
          self.searchCategories(whiteList);

          // Ebrahim M: I added this timeout to let filter selectable in 'Lists' tab page
          setTimeout(function () {
                if(self.selectedListSlot() === 'All Visual Clips' || self.selectedListSlot() === 'All Karaoke Songs'){
                  self.selectedListSlot('All Songs');
                }
          }, 1500);
          /////////////////////////////////////////////////////////////////////

          return whiteList;

        case 'visuals' :
          if(NL.songSearch.activeTab() === 'search-category'){
            self.searchCategories(whiteList);
          }
          // Ebrahim M: I added this line to let filter selectable in 'Lists' tab page
          // self.selectedListSlot('All Visual Clips');
          return 'VISUALS';

        case 'karaoke' :
          if(NL.songSearch.activeTab() === 'search-category'){
            self.searchCategories(whiteList);
          }

          // Ebrahim M: I added this line to let filter selectable in 'Lists' tab page

          return 'KARAOKE';
    }
  });

  self.alphabet = ['#','a','b','c','d','e','f','g','h',
          'i','j','k','l','m','n','o','p','q','r','s',
          't','u','v','w','x','y','z'];

  self.keys = ko.observableArray([]);
  self.setKeys = function() {
      var i;
      var ret;

      switch(self.primarySort().value) {
        case 'artist' :
        case 'title' :
          ret = self.alphabet;
          break;
        case 'released' :
          var years = [];
          var sr = NL.songSearchResults;
          var maxYear = new Date(NL.songSearchResults.maxKey()).getFullYear();
          var minYear = new Date(NL.songSearchResults.minKey()).getFullYear();
          for(i = maxYear; i >= minYear; --i) {
            years.push(''+i);
          }
          ret = years;
          break;
        case 'category' :
          ret = self.availableCategories();
          break;
        case 'bpm' :
          var maxBPM = parseInt(NL.songSearchResults.maxKey(),10);
          var minBPM = parseInt(NL.songSearchResults.minKey(),10);
          var bpms = [];
          for(var j = minBPM; j <= maxBPM; j+=10) {
            bpms.push(j);
          }
          ret = bpms;
          break;
      }
      self.keys(ret);
  };

  searchTitleHelper = function(paramFilter){
    delete paramFilter.filter_songs; // Ebahim M: Added

    paramFilter.search_title = self.searchTitle();
    if(self.adjacent() &&
       self.searchTitle().trim()[0] !== undefined &&
       NL.songSearch.primarySort().value === 'title')
    {
      self.offset(self.searchTitle().trim()[0]); //@TODO: necessary?
    } else {
      self.offset('');
    }
  };

  searchArtistHelper = function(paramFilter){
    delete paramFilter.filter_songs; // Ebahim M: Added
    paramFilter.search_artist = self.searchArtist();
    if(self.adjacent() &&
       self.searchArtist().trim()[0] &&
       NL.songSearch.primarySort().value === 'artist')
    {
        self.offset(self.searchArtist().trim()[0]);
    } else {
      self.offset('');
    }
  };
  searchNlListHelper = function(paramFilter){
    if ( ( self.selectedListSlot() === 'All Karaoke Songs')  ||
             (self.selectedListSlot() === 'All Visual Clips')  ||
                      (self.selectedListSlot() === 'All Songs') ){
      paramFilter.filter_category = searchSpecialHelper(self.selectedListSlot());
      delete paramFilter.filter_songs;
      delete paramFilter.filter_music_list; // delete 'filter_music_list' item from the object

    } else if( self.selectedListSlot() === 'Favourites' ) {
      paramFilter.filter_songs = NL.userInfo.likeSongs();
      paramFilter.filter_category = searchSpecialHelper('All Songs');
    } else if( self.selectedListSlot() === 'My Removed Songs' ) {
      paramFilter.filter_songs = NL.userInfo.dislikeSongs();
      paramFilter.filter_category = searchSpecialHelper('All Songs');
    }
    else{
      delete paramFilter.filter_songs; // Ebahim M: Added
      paramFilter.filter_music_list = self.selectedListSlot();
    }
  };
  searchSpListExtraHelper = function(paramFilter, aPlaylistId){
    var i = 0;
    var selectedSpId = aPlaylistId;

    for(i = 0 ; i < controls.search.arImportedPlaylists().length ; i++){
      if(controls.search.arImportedPlaylists()[i].spPlaylistId === selectedSpId){
        break;
      }
    }
    if( i < controls.search.arImportedPlaylists().length){
      paramFilter.filter_songs = controls.search.arImportedPlaylists()[i].fileNames;
      paramFilter.filter_category = searchSpecialHelper('All Songs');
    }
    else { // There is no stored data for this playlist locally so we have to call Spotify APIs.
      // So we set callRefresh flag to true.
      paramFilter.filter_songs = '[\'\']';
      paramFilter.filter_category = searchSpecialHelper('All Songs');
      self.callRefresh = true;
    }
  };
  searchSpListHelper= function(paramFilter){
    // console.log('<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>@@@@@@@@@@@@@@@@@@@@', paramFilter);
    if(self.selectedSpotifyList() === undefined ) {
      // Ebrahim M: I have added this for for the very first time that the user selects the Spotify tab page
      // So I tried to load the first play list of the Spotify logged in user.

      
      /* This code is replaced with the new one below.
        if(NL.spotify.userName() !== 'Not signed in'){
        var i = 0;
        var selectedSpId ='';
        var selectedSpName ='';
        for( i = 0; i < NL.spotify.spotifyPlaylists().length ; i++){
          if(NL.spotify.spotifyPlaylists()[i].guestUser === NL.spotify.userName()){
            selectedSpId = NL.spotify.spotifyPlaylists()[i].guestPlaylists[0].id;
            selectedSpName = NL.spotify.spotifyPlaylists()[i].guestPlaylists[0].name;
            break;
          }
        }
        self.selectedSpotifyList({ id:selectedSpId,
                                        name:selectedSpName,
                                        owner: NL.spotify.userName()
                                      });
        }
        else{        
          paramFilter.filter_songs = '[\'\']';
          paramFilter.filter_category = searchSpecialHelper('All Songs');        
        } */
      if(controls.search.arImportedPlaylists().length > 0 ) {
        var i = 0;
        var selectedSpId = '';
        var selectedSpName = '';
        var selectedSpOwner = '';
        selectedSpId = controls.search.arImportedPlaylists()[0].spPlaylistId;
        selectedSpName = controls.search.arImportedPlaylists()[0].spPlaylistName;
        selectedSpOwner = controls.search.arImportedPlaylists()[0].spOwner;
        self.selectedSpotifyList({ id:selectedSpId,
                                   name:selectedSpName,
                                   owner: selectedSpOwner
                                  });
      }
      else {
        paramFilter.filter_songs = '[\'\']';
        paramFilter.filter_category = searchSpecialHelper('All Songs');
      }
    }
    else{
      searchSpListExtraHelper(paramFilter, NL.songSearch.selectedSpotifyList().id);
    }
  };

  searchYearHelper = function(paramFilter){
    delete paramFilter.filter_songs; // Ebahim M: Added
    //delete paramFilter.filter_music_list;
    delete paramFilter.search_released; // Ebahim M: Added
    if ( self.searchYearRange() === 'All Karaoke Songs'){
      self.typeFilter(self.filterFieldTypes[1]);
      paramFilter.filter_category = searchSpecialHelper(self.searchYearRange());
    } else if(self.searchYearRange() === 'All Visual Clips'){
      self.typeFilter(self.filterFieldTypes[2]);
      paramFilter.filter_category = searchSpecialHelper(self.searchYearRange());
    }else if(self.searchYearRange() === 'All Songs'){
      self.typeFilter(self.filterFieldTypes[0]);
      paramFilter.filter_category = searchSpecialHelper(self.searchYearRange());
    }
    else{
      paramFilter.search_released = self.searchYearRange();
      //  $('#txtSearchYear').focus(); // wut?
      //paramFilter.sort = [{ key : self.primarySort().value, dir : self.sortDir() }];
    }
  };
  searchCategoryHelper = function(paramFilter) {
    // self.primarySort(self.sortFieldTypes[2]);
    delete paramFilter.filter_songs; // Ebahim M: Added
    if ( self.searchCategory() === 'All Karaoke Songs') {
      self.typeFilter(self.filterFieldTypes[1]);
      paramFilter.filter_category = searchSpecialHelper(self.searchCategory());
    } else if(self.searchCategory() === 'All Visual Clips')  {
      self.typeFilter(self.filterFieldTypes[2]);
      paramFilter.filter_category = searchSpecialHelper(self.searchCategory());
    }else if (self.searchCategory() === 'All Songs' ){
      self.typeFilter(self.filterFieldTypes[0]);
      paramFilter.filter_category = searchSpecialHelper(self.searchCategory());
    }
    else{
      if(self.searchCategory() !== undefined){
        paramFilter.filter_category = self.searchCategory();
      }
    }
    //paramFilter.sort = [{ key : self.primarySort().value, dir : self.sortDir() }];
  };
  searchSpecialHelper = function(param) {
    var retVal = '';
    if ( param === 'All Karaoke Songs') {
        self.typeFilter(self.filterFieldTypes[1]);
        retVal = 'KARAOKE';
    } else if (param  === 'All Visual Clips') {
      self.typeFilter(self.filterFieldTypes[2]);
      retVal = 'VISUALS';
    } else if (param === 'All Songs') {
      self.typeFilter(self.filterFieldTypes[0]);
      // it returns all categories except: 'VISUALS','KARAOKE','PROMO'
      retVal = self.availableCategories().map(function(el, index){
                  if(['VISUALS','KARAOKE','PROMO'].indexOf(el) < 0) {
                    return el;
                  }
                });
    }
    return retVal;
  };

  // Ebrahim M : I've added this computed function to set 'sort by: Spotify' as the default sort where
  // 'SPOTIFY LISTS' tab page is selected in 'List' tab page.
  ko.computed (function (){

    if(self.activeTab() === 'search-spotify'){
      setTimeout(function () {
          self.primarySort(self.sortFieldTypes[5]);
      }, 1000);
    }
  });
  ///////////////////////////////////////////////////////////////////

  self.searchQuery = ko.computed(function() {
      var sort = {};

      if(self.primarySort().value !== ''){
        sort = [{ key : self.primarySort().value, dir : self.sortDir() }];
        sort.push({key : self.secondarySort().value, dir : self.secondarySort().dir});
      }

      var filter = { search_title:'', search_artist:'', filter_music_list:'' , filter_songs:{} ,limit:{}, sort:sort};
      filter.filter_category = self.categoryWhitelist();

      switch(self.activeTab()) {
        case 'search-title' :
          searchTitleHelper(filter);
          break;
        case 'search-artist' :
          searchArtistHelper(filter);
          break;
        case 'search-lists' :
         // console.log('####### self.typeFilter = ', self.typeFilter());
          searchNlListHelper(filter);
          break;
        case 'search-year' :
          searchYearHelper(filter);
          break;
        case 'search-category' :
          searchCategoryHelper(filter);
          break;
        case 'search-spotify' :
          searchSpListHelper(filter);
          break;
      }

      filter.adjacent = self.adjacent();
      return filter;
  });


  self.limitObj = ko.computed(function(){
    var offset = ['All','#'].indexOf(self.offset()) >= 0 ? '' : ''+self.offset();
    /*if(self.adjacent() && ['search-title','search-artist'].indexOf(self.activeTab()) >= 0) {
      if(self.activeTab() === 'search-title') {
        offset = self.searchTitle()[0];
      } else if(self.activeTab() === 'search-artist') {
        offset = self.searchArtist()[0];
      }
    }*/
    return {start : self.startIndex(),
            length : self.limit(),
            offset : offset
    };
  });

  self.filterThrottled = ko.computed(self.searchQuery)
                                .extend({ throttle: 800 });
  self.filterThrottled.subscribe(function(){
    if(self.primarySort().value == 'released') {
      NL.playlist.showGenre(false);
    } else if (self.primarySort().value == 'category') {
      NL.playlist.showGenre(true);
    }

    // Ebrahim M: I have just removed the next line because it search page sometimes it was stuck and did not work.
    // but I am not quite sure about that.
    NL.songSearch.bSearchInProgress(true);
    // Ebrahim M : These two lines are added to implment Exact and then Loose search (if there is no exact search)
    if( NL.songSearch.isLooseSearchDone() ){
      // If it came here, it means that the previous search had been a loose search so it is set to exact again!
      NL.songSearch.adjacent(true);
      NL.songSearch.isLooseSearchDone(false);
    }
    controls.search.newSearch();

  });

  self.isValid = ko.computed(function() {
    return true;
  });
};

$doc.on('nightlife-ready', function() {
  NL.songSearch.limit = NL.settings.searchPageLimit;
  var model = NL.songSearch;

 $('.search-tabs').find('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    NL.songSearch.activeTab($(e.target).prop('href').split('#').pop()); // track current tab
    // $('.search-tabs').find('.search-tab').removeClass('callout-bottom'); //add callout
    // $(e.target).addClass('callout-bottom');

    model.primarySort(model.searchTypeSortDefault[e.target.hash]); // set sort
  });

  $('.search-tabs').find('a[data-toggle="tab"]:first').tab('show');
  NL.songSearch.searchYears.push('All Songs');
  NL.songSearch.searchYears.push('All Karaoke Songs');
  NL.songSearch.searchYears.push('All Visual Clips');
  for(i = (new Date().getFullYear()); i >= 1920; i = Math.round((i-10)/10)*10) {
    var y = {end : (i-1).toString(), start : ''+ Math.round((i)/10)*10};
    y.text = y.start+'s';
    if(y.start < 2020) {
      NL.songSearch.searchYears.push(y.text);
    }
  }
  NL.songSearch.searchYears.push('Pre 1920');

  $('#txtSongArtist').on('keyup', function(){
    NL.songSearch.searchTitle(NL.songSearch.searchArtist());
  });

  $('#txtSongTitle').on('keyup', function(){
    NL.songSearch.searchArtist(NL.songSearch.searchTitle());
  });

});
