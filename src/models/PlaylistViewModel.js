
var PlaylistViewModel = function(){
  var self = this;

  this.active = {
    filename: ko.observable(),
    distance: ko.observable()
  };

  this.history = ko.observableArray();
  this.current = ko.observable();
  this.queue = ko.observableArray();

  this.showGenre = ko.observable(false);
  this.toggleGenre = function(d, e) {
    e.stopPropagation();
    NL.playlist.showGenre(!NL.playlist.showGenre());
    //$('.tooltip-helper').tooltip();
  };

  this.songs = ko.computed(function() {
    var theList = self.history().slice(0);  // Copy the history array
    var future = self.queue();

    if(self.current()){
      theList.push(self.current());
      if( (NL.context.selectedSong() !== null) && (NL.context.selectedSong() !== undefined ) ){
        if(NL.context.selectedSong().artist === undefined || 
              NL.context.selectedSong().title === undefined ) { // assign as selected song if none selected
            NL.context.selectedSong(NL.playlist.current());
        }
      }

      /*if(future.length && future[0].filename === this.current().filename){
        future.shift();
      }*/

    }

    //    controls.playlist.monitor(future);
    theList = theList.concat(future);

    return theList;
  }, this);


  // When we have enough data - fire up the playlist control
  this.songsSubscription = this.songs.subscribe(function(newValue){
    if(self.current() && self.history().length > 0 && newValue.length > 1){
      controls.playlist.init();
      this.dispose();
    }
  });

  this.appendHistory = function(song) {            
    var arr = self.history();
      for (var i=0; i < arr.length; i++) {
        arr[i].distance -= 1;
      }    
    self.history.push(song);
    self.history.shift();

  };

};

