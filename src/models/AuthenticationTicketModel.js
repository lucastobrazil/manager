var AuthenticationTicketModel = function(obj) {
  if (obj === undefined) {
    obj = {};
  }

  var self = this;

  self.address = ko.observable(obj.address);
  self.apiAddress = ko.observable(obj.apiAddress);
  self.password = ko.observable(obj.password);
  self.username = ko.observable(obj.username);
  self.siteToken = ko.observable();
  self.acl = {
    tokens: ko.observable({}),
    actions: ko.observable({
      control: {
        subscribe: {},
        unsubscribe: {}
      },
      request_system: {
        verbs: {}
      },
      request_systems: {
        listing: {}
      },
      request_zone: {
        verbs: {}
      },
      request_user: {
        forgotten_password: {}
      }
    })
  };

  // Ebrahim M: Added 'ignoreOnsiteAccess' parameter due to detect if it need onsite access for the verb or not.
  self.canIUse = function(event_category, event, negate, ignoreOnsiteAccess) { // negate is a dirty hack to invert return value

    return ko.computed(function() {            
      var ret=false;            
      if(NL.settings.utopia.acl.actions()[event_category][event]) {      
        if(NL.settings.utopia.acl.actions()[event_category][event].restricted) { // restricted verb
          ret = true;
          if(!ignoreOnsiteAccess){
            ret = NL.settings.accessCode.verified()=== true ? true : false; // can acces if onsite
          }          
        } else if (NL.settings.utopia.acl.actions()[event_category][event].disabled === true) {
          ret = false;
        } else {
          ret = true; // unrestricted
        }
      } else {
        ret = false; //verb not found */
      }
      return (negate === false || negate === undefined) ? ret : !ret;
    });    
  };

  self.ready = function(){
    return self.address() && self.username() && self.password();
  };
};
