var SocketMessageModel = function(settings, event_category, event, obj, callback) {
  var template = {
    published: {
      time: new Date().toJSON()
    },
    actor: {
      api_version: 3.92,
      type: 'client',
      auth_user:  settings.username(),
      auth_pass:  settings.password(),
      agent:{type: 'consumer' , name: 'MMNL Web App', platform:'browser' , version: '2.23'},
      request_id: {
        sent: event_category +'|'+event+'-'+utils.getUUID()
      }
    },
    object: {},
    target: {
      system: settings.system === undefined ? '' : settings.system.toUpperCase(),
      zone: settings.zone
    }
  };

  var msg = $.extend(true, template, obj);
  msg.verb = $.extend(true, msg.verb, {
    event_category: event_category,
    event: event
  });
  if(typeof(callback) === 'function') { // if callback supplied, register it for later use
    NL.callbacks[template.actor.request_id.sent] = callback;
  }

  // Attach the access code (but only if it doesn't already exist)
  if(NL.settings.accessCode.code() && !msg.actor.hasOwnProperty('onsite_access_code')){
    msg.actor.onsite_access_code = NL.settings.accessCode.code();
  }

  return msg;
};