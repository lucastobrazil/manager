var ListViewModel = function(obj) {
  if (obj === undefined) {
    obj = {};
  }

  var self = this;

  self.name = obj.name;
  self.nameOriginal = obj.name;       // Ebrahim M: Added because name may contains 'only numbers ''
  self.description = obj.description;
  self.group = obj.group.toUpperCase() === 'UNKNOWN' ? 'User List' : obj.group.toUpperCase();
  self.num_songs = obj.num_songs;
  self.schedulable = obj.schedulable.toUpperCase();
  self.slot = obj.slot;
  self.type = obj.type;

  self.selected = ko.observable(obj.selected);

  self.displayName = self.name + ' (' + self.description + ')';
  self.style = utils.toSeo(self.group);

  self.selectList = function(data) {
    if (self.canSchedule()) {
      data.selected(!data.selected());
    } 
  };

  self.canSchedule = ko.computed(function() {   
    
    /* This block commented due to Paul D's suggestion (for the selection set only).
    if(NL.settings.selectedSystem()) { 
      if(NL.settings.selectedSystem().selections_only) { // cant load list when selections set enabled
       // if(NL.zone.selection_set.list_names.indexOf(self.name))
        return false;
      }
    }
    */

    if (self.group === 'NEGATIVE' || self.group === 'AMBIENT') {
      return false;
    }

    if (self.type === 'A' && ['TAGGED', 'DELETED', 'HIDDEN', 'CULL'].indexOf(self.name) < 0) {
      return true; // schedulable user list.
    }
    
    if (self.type !== 'A' && self.schedulable !== 'N') {
      return true;
    }
    return false;

  });

  self.isLoaded = ko.computed(function() {
    return NL.musicList.loadedLists().indexOf(self) >= 0;
  });
};

$doc.on('nightlife-ready', function() {
  $('.collapse').on('hide.bs.collapse', function (e) {
    var t=$('*[data-target="#'+e.currentTarget.id+'"][data-toggle=collapse]');
    t.find('i').removeClass('icon-chevron-up');
    t.find('i').addClass('icon-chevron-down');
  });
  $('.collapse').on('show.bs.collapse', function (e) {
    var t=$('*[data-target="#'+e.currentTarget.id+'"][data-toggle=collapse]');
    t.find('i').removeClass('icon-chevron-down');
    t.find('i').addClass('icon-chevron-up');
  });
});