
var VolumeViewModel = function() {
	var self = this;
	this.level =  ko.observable(0);
	this.sliderLevel = ko.observable(0);
	this.level.subscribe(function() {
		self.sliderLevel(self.level());
	});
};
   