$.extend(true, controls, {
    settings: {
        showSystems: ko.observable(false),
        targetSystemId: ko.observable(''),
        username: ko.observable(''),
        password: ko.observable(''),
        accessCode: ko.observable(''),
        bWaitInProgress: ko.observable(false),
        bDeliberateDelay : ko.observable(false), // Ebrahim M: Added to keep the spinner alive while there is a deliberate delay!!
        lmShown: ko.observable(true),         
        bIsRestart: ko.observable(false),         
        reconnectCount: 0,
        enSystems: true,
        signOutIsPressed :false, // Ebrahim M: Added to detect if sing out is pressed or the system is shut down accidently!
        searchBoxIsActive: ko.observable(false),
        searchCriteria: ko.observable(''),
        signoutSelected: false, // Ebrahim M : Added this variable to detect the 'Sign out' option from the drawrer menu bar        
        showOfflineZone: ko.observable(false),
        discoverBaseIp: ko.observable(''), // 192.168.4
        discoverBaseIpTemp: '', // 192.168.4
        discoverCancelPressed: false,
        discoverRefreshRunning: ko.observable(false),
        suspendedConnect:false , // Ebrahim M : Added this variable to detect if the zone is in progress of connecting (I call it 'Connection suspended').
                                 // This var will be set if back button in startup page is pressed and it is 'Connection suspended'.        
        discoveredZones: ko.observableArray(),
        listOfLocalClient: ko.observableArray(),        
        nominatedZoneForConnect:'', // Ebrhaim M : A temp var to double check that nominated zone for connecting is similar to connected zone. 
        hideLM: function(){
            controls.settings.lmShown(false);
            // console.log('************** lmShown = ' + controls.settings.lmShown());
        },
        useLocal: function(settingsModel) {            
            settingsModel.connectionMethod('local');
        },
        useUtopia: function(settingsModel) {
            settingsModel.connectionMethod('utopia'); 
        },
        toggleConnectionMethod: function (settingsModel){
            if(settingsModel.connectionMethod() === 'utopia'){
                settingsModel.connectionMethod('local');
            }
            else{                
                settingsModel.connectionMethod('utopia');
            }
            controls.settings.refreshSystems();
        },        
        toggleConnectionPolicy: function (param){                        
            if(param !== NL.settings.connectionPolicy() ){
                NL.settings.connectionPolicy(param);
                controls.settings.searchCriteria('');                
            }
            //if(NL.settings.connectionPolicy() === 'select'){
            //    controls.settings.refreshSystems();
            //}
        },
        toggleFilterOffline: function(){
            var self = controls.settings;
            self.showOfflineZone(!self.showOfflineZone());

            /*if(self.showOfflineZone()){
                self.listOfLocalClient.removeAll();
                for(i=0 ; (NL.settings.systems) && i < (NL.settings.systems().length - 1) ; i++){                    
                    if (NL.settings.systems()[i].system){                    
                        // controls.diagnostics.testWebSocketLocal(NL.settings.systems()[i], callback);                        
                        controls.diagnostics.testWebSocketLocal(NL.settings.systems()[i]);
                    }
                }                                                        
                self.listOfLocalClient(NL.settings.systems());
            }*/

            controls.settings.refreshSystems();
        },
        connect: function() {          
            var self = controls.settings;

            // Try to retrieve the desired system           
            var targetSys = ko.utils.arrayFirst(NL.settings.systems(), function(item) {
                return self.targetSystemId() === item.id;
            });
            
            // Check if it's valid
            if (!self.targetSystemId() || !targetSys) {
                setTimeout(function () { 
                    if(self.nominatedZoneForConnect === self.targetSystemId()) {
                        self.bWaitInProgress(false);
                    }
                }, 1000);                          

                // If we are in the 'log out' page we simply stay there with no message
                if(NL.settings.accountStatus() !== 'Logged out'){ 
                    controls.notification.show('No system selected', controls.notification.types.error);
                }
                return false;
            }
            // console.log(targetSys);
            if((!targetSys.version ) || (targetSys.version.substr(0,3) !== '3.9') ) {                
                setTimeout(function () {        
                    if(self.nominatedZoneForConnect === self.targetSystemId()) {
                        self.bWaitInProgress(false);
                    }
                }, 1000);          

                controls.notification.show(self.targetSystemId() + ': Invalid System Version', controls.notification.types.error);
                return false;
            }
         
            // Save and connect
            NL.settings.setSelectedSystemId({
                id: self.targetSystemId()
            });

            // Update the API with the new settings and connect Select Zone
            NL.api.applySettings();
            NL.api.connect();            
        },
        directConnect: function(data, event) {             
            var self = controls.settings;
            if(self.bWaitInProgress()){
                return;            
            }
            self.suspendedConnect = false;
            self.bWaitInProgress(true);
            self.lmShown(true); // Ebrahim M: Added to make direct connect similar to 
            self.nominatedZoneForConnect =  NL.settings.manualSystem.systemCode();            

            NL.settings.manualSystem.t_address(NL.settings.manualSystem.zoneIp());
            NL.settings.manualSystem.t_system(NL.settings.manualSystem.systemCode());
            NL.settings.manualSystem.t_zone(NL.settings.manualSystem.zoneNumber());

            controls.settings.disconnect(null, {reason: 'Direct Connection'});                    
            self.targetSystemId('manual');
            NL.settings.selectedSystemId('manual'); 

            controls.diagnostics.testWebSocketLocal(NL.settings.selectedSystem());
            controls.settings.connect();        
        },
        discoverConnect : function(data, event) {
            console.log('discover Connect' , data);
            var self = controls.settings;
            if(self.bWaitInProgress()){
                return;            
            }
            self.suspendedConnect = false;
            self.bWaitInProgress(true); 
            self.lmShown(true); // Ebrahim M: Added to make direct connect similar to 

            NL.settings.manualSystem.zoneIp(data.zoneIp);
            NL.settings.manualSystem.systemCode(data.systemName);
            NL.settings.manualSystem.zoneNumber(data.zoneNo);

            NL.settings.manualSystem.t_address(data.zoneIp);
            NL.settings.manualSystem.t_system(data.systemName);
            NL.settings.manualSystem.t_zone(data.zoneNo);

            controls.settings.disconnect(null, {reason: 'Direct Connection'});                    
            self.targetSystemId('manual');
            NL.settings.selectedSystemId('manual');  
            controls.diagnostics.testWebSocketLocal(NL.settings.selectedSystem());
            controls.settings.connect();
        },
        selectSystemConnect: function(data, event) {
            var self = controls.settings;
            self.suspendedConnect = false;
            self.lmShown(true);
            self.bWaitInProgress(true); 
            self.nominatedZoneForConnect = data.id;                        

            controls.settings.disconnect(null, {reason: 'Change Music Zone'});         
            self.targetSystemId(data.id);            
            controls.settings.connect();
        },
        requireAuthentication: function() {  // HOLD
            if( ( typeof(NL) !== typeof(undefined) ) && 
                (NL.settings.selectedSystemId() !== 'manual') ) {
                this.showSystems(false);
                controls.navigation.navigateToHash('tab-signin');
                controls.navigation.navigateFromHash();
                $('#settings-authentication').addClass('panel-' + controls.notification.types.danger);
                controls.notification.show('Authentication Failure', controls.notification.types.danger);
                controls.navigation.disableNav();
                NL.settings.accountStatus('Authentication Failure');
                NL.settings.selectedSystem(undefined);
                controls.settings.logout();
            }
        },
        requireVerification: function() {
            controls.notification.show('This action requires the user to verify their presence on site', controls.notification.types.danger);
        },
        login: function(param) {
                this.signoutSelected = param;
                var self = controls.settings;
                if (self.username() && self.password()) {
                    $('#settings-authentication').removeClass('panel-' + controls.notification.types.danger);
                    NL.settings.utopia.username(self.username());
                    NL.settings.utopia.password(self.password());
                    NL.api.applySettings();
                    controllers.request_systems.listing();
                } else {
                    controls.notification.show('Username or Password missing', controls.notification.types.danger);
                }
        },
        resetPassword: function() {
            var self = controls.settings;
            if(self.username() === '' || self.username() === undefined) {

                $('#password-reset-confirm').toggle();
                $('#password-reset-error').toggle();
            } else {
                controllers.request_user.forgotten_password(self.username(), 
                    function() {                        
                        $('#password-reset-confirm').toggle();  
                        $('#password-reset-result').toggle(); 
                    }
                );
            }

        },
        showResetPassword: function() {
            controls.modal.show('reset-password', {}, '');
        },
        clearStorage: function() {
            var ok = confirm('Are you sure you want to do that?');
            if (ok) {
                localStorage.clear();
                location.reload();
            }
        },
        init: function() {
           //  controls.settings.logout(); // Ebrahim M : Added to navigate to 'Sign-in' page for the first time
            if( (NL.settings.accountStatus() === 'Logged out')  || (NL.settings.accountStatus() === 'Not signed in') ){
                controls.settings.lmShown(false);
                controls.navigation.enableNav();
                controls.navigation.navigateFromHash();
                controls.navigation.navigateToHash('tab-signin');  // tab-settings - >tab-musiczones                
            }
            
            this.targetSystemId(NL.settings.selectedSystemId());
            this.username(NL.settings.utopia.username());
        },
        disconnectClick: function() {
            controls.settings.disconnect(null, {reason: 'User Initiated'});
        },
        disconnect: function(event, reason) {
           // console.log('########### disconnect called');
            var navigateToPage = '';
            NL.api.close(reason);
              
            controls.navigation.navigateFromHash();
            controls.diagnostics.stopAutoPing();
            var message = 'Connection Error - Disconnected (' + reason + ')' ;
            var msgType = controls.notification.types.danger;

            if (reason && reason.error) {
                message = 'Disconnected - ' + reason.error;
                msgType = controls.notification.types.error;
            } else if (event && event.wasClean) {
                message = 'Disconnected';
                if (reason && reason.reason) {
                    message += ' - ' + reason.reason;
                }
                msgType = controls.notification.types.info;
            }

            if(reason && reason.reason ) {
                if( (reason.reason !== 'Change Music Zone') && (reason.reason !== 'Direct Connection') ) { 
                  //  alert('1');
                    navigateToPage = 'tab-signin';
                }                
                if(reason.reason === 'Change Music Zone' || reason.reason === 'Direct Connection' ){
                    message = 'Please Wait...';
                    // enableControls = true;
                }
                if(reason.reason !== 'User Initiated') {
                    controls.settings.startReconnect();
                }
            }
            else{
                if(NL.settings.selectedSystemId() === 'manual' && NL.settings.accountStatus() !== 'Logged out'){
                    navigateToPage = 'tab-playlist';
                }
                else{
                    navigateToPage = 'tab-signin';
                }
            }

            // console.log('########### navigateToPage = ' +navigateToPage + ' , message = ' , message );

            if( (navigateToPage !== '') && ( message.indexOf('Disconnected') < 0 )) {
                controls.navigation.enableNav();             
                controls.navigation.navigateToHash(navigateToPage);
                //controls.navigation.disableNav();
            }
            else {
                // It comes here if the connection fails because of network problem or HDMS rebooting.
                controls.settings.navigateIfDisconnected(navigateToPage);
            }
            
            controls.notification.show(message, controls.notification.types.info);
            var self = controls.settings;
            // self.bWaitInProgress(enableControls);
            NL.settings.systemStatus(message);
        },
        navigateIfDisconnected: function(page){
            var self = controls.settings;
            if( (page === 'tab-signin') &&  (NL.settings.systemStatus() === 'Connected') && !self.signOutIsPressed ){
                controls.modal.hide(); // Ebrahim M : Added to hide the modal window where page is changed.
                controls.navigation.enableNav();
                controls.navigation.navigateFromHash();
                controls.navigation.navigateToHash('tab-musiczones');
                controls.settings.refreshBtnPressed();                
            }
            self.signOutIsPressed = false;
        },
        connected: function(selectedSystem) {
            var self = controls.settings;
            //controls.navigation.menuShown(true); // Ebrahim M : Removed                    
            var message = 'Connecting to Music Zone...'; // 'Connected' - > new value : requested by Paul D
            var msgType = controls.notification.types.success;
            controls.settings.reconnectCount = 0;
            if(selectedSystem && selectedSystem.access === 'diplomat'){
                message = 'Wow! You have a diplomatic access level. Enjoy...';
                msgType = controls.notification.types.warning;
            } else if (selectedSystem && selectedSystem.access !== 'full_control') {
                message = 'Current Access Level is not Full Control, user permissions are restricted!';
                msgType = controls.notification.types.warning;
            }
            if(!controls.settings.suspendedConnect){
                // If we are in the 'log out' page we simply stay there with no message
                if(NL.settings.accountStatus() !== 'Logged out'){ 
                    controls.notification.show(message, msgType);
                }
                // controls.newNotification.initNotif(message, 3000);
            }

            NL.settings.systemStatus('Connected');
            controls.settings.reconnectCount = 0;

            NL.zone.version(NL.settings.selectedSystem().version);
            clearTimeout(self.reconnectTimer);
            controls.diagnostics.startAutoPing();
            if(NL.zone.version_num() < 3.92) {
                controls.navigation.disableNav($(controls.navigation.primaryLinks[0]));
                $(controls.navigation.primaryLinks[0]).attr('title','Coming Soon');
            } else {
                $(controls.navigation.primaryLinks[0]).attr('title','Advertising');
            }
            if(NL.settings.connectionMethod() === 'local')  {
                NL.settings.accessCode.verified(true);
            } else {
                NL.settings.accessCode.verified(false);
            }

            if(selectedSystem){
                var ipRangeArray  = NL.settings.selectedSystem().local_ip.split('.');
                var NoOfDots = ipRangeArray.length - 1;
                if(NoOfDots == 3) { // ip format is true
                    self.discoverBaseIp(ipRangeArray[0] + '.' + ipRangeArray[1] + '.' + ipRangeArray[2]) ;
                    discoverBaseIpTemp = ipRangeArray[0] + '.' + ipRangeArray[1] + '.' + ipRangeArray[2];
                }

                self.getFavoriteRemvedSongs();
            }
            setTimeout(function () {
                if(NL.settings.selectedSystem() !== null && NL.settings.selectedSystem() !== undefined){
                    if(self.nominatedZoneForConnect === NL.settings.selectedSystem().id) {
                        self.bWaitInProgress(false);
                    }
                }
            }, 1500);
        },
        signedIn: function() {

            if(controls.navigation.isInstagramFeedModalOpen()) {
                controls.modal.hide('advertising-instagram-modal');
                setTimeout(function () {
                    controls.navigation.isInstagramFeedModalOpen(true);
                }, 500);
            }
                           
            var self = controls.settings;
            if(NL.settings.prevUsernames.indexOf(self.username()) < 0) {
                NL.settings.prevUsernames.push(self.username());                
            }
            if( ( typeof(NL) !== typeof(undefined) ) && 
                ( NL.settings.utopia.username() !== 'guest') ){
                NL.settings.lastLoggedUser ({user:NL.settings.utopia.username(), 
                                                password: NL.settings.utopia.password()});
            }

            //self.showSystems(true);
            NL.settings.accountStatus('Signed in');

            if(NL.settings.autoConnect() && NL.settings.selectedSystem() && !this.signoutSelected) {
                self.suspendedConnect = false;
                self.bWaitInProgress(true);
                controls.settings.lmShown(true);

                controls.settings.connect();
                controls.navigation.navigateFromHash();
                controls.navigation.navigateToHash('tab-musiczones');
            }
            else{
                // Ebrahim M: The 'signout' item is selected by the user so after the login page we should navigate the music zone page!
                controls.navigation.enableNav();
                controls.navigation.navigateFromHash();
                controls.navigation.navigateToHash('tab-musiczones');  // tab-settings - >tab-musiczones 

                controls.settings.refreshSystems();
            }

            // Ebrahim M : Commented
            // $('#txtUptopiaPassword, #txtUptopiaUsername, #btnLogin').prop('disabled', true);
            //$('#btnLogin').hide();
            //$('#btnLogout').prop('disabled', false).show();
            //$('#settings-authentication-body').css('display','none');
            //this.showSystems(false);
        },
        logout: function() { 
            console.log('######### logout called.');

            var self = controls.settings;            
            self.signOutIsPressed = true;

            //controls.navigation.disableNav();
            NL.settings.accountStatus('Logged out');
            clearTimeout(self.reconnectTimer);

            if(NL.api !== undefined && NL.api !== null && NL.api.socketStatus !== undefined){
                if (NL.api.socketStatus() === SocketReadyStateType.Open) {
                    self.disconnect();
                }
            }

            self.showSystems(false);
            self.password('');
            NL.settings.utopia.password(self.password());
            self.targetSystemId(undefined); // Ebrahim M : Added to detect the back button 
            NL.settings.selectedSystemId(undefined);             
        },
        displayOnsiteAccessCode: function() {
            controllers.request_zone.display_onsite_access_code();
        },
        requireOnSiteAccess: function() {
            controls.modal.show('onsite-access', NL, undefined);
        },
        saveOnsiteAccessCode: function() {
            var self = controls.settings;
            controllers.request_zone.verify_onsite_access_code(self.accessCode() * 1); 
        },
        startReconnect: function() {            
            var self = controls.settings;
            clearTimeout(controls.settings.reconnectTimer);
            if(NL.settings.autoDisconnect()) {
                self.reconnectTimer = setInterval(function() { //connect every n ms
                   console.log('reconnectCount' + self.reconnectCount + ' , reconnectMax() = ' + NL.settings.reconnectMax() );
                   if(self.reconnectCount > NL.settings.reconnectMax()) {
                       clearInterval(self.reconnectTimer);

                       // We are in the 'log out' page so we simply stay here!
                       if(NL.settings.accountStatus() !== 'Logged out'){
                            controls.settings.backInStartupPressed();
                            controls.notification.show('Max reconnect attempts reached... ' +
                                    controls.settings.reconnectCount, controls.notification.types.error);
                       }
                       self.reconnectCount = 0;
                       
                       return;
                   } else {
                    self.connect();                  
                    $('#zone-status').text('Attemping to reconnect... ' + controls.settings.reconnectCount);                  
                    self.reconnectCount++;
                   }
                }, NL.settings.reconnectInterval());
            }
        },
        refreshSystems: function() {         
            //var callback = function(result) { console.log(result) }             
            // controls.notification.show('Systems refreshment started', controls.notification.types.info);                  
            var self = controls.settings;                                    
            if(NL.settings.connectionMethod() == 'local'){                            
                self.listOfLocalClient.removeAll(); // remove all the zones and start from teh beginning!.
                for(i=0 ; (NL.settings.systems) && i < (NL.settings.systems().length - 1) ; i++){                    
                    if (NL.settings.systems()[i].system){          
                        // controls.diagnostics.testWebSocketLocal(NL.settings.systems()[i], callback);                        
                        controls.diagnostics.testWebSocketLocal(NL.settings.systems()[i]);
                    }
                }                                            
            }
            else{
                controllers.request_systems.listing();
            }
        },
        refreshBtnPressed: function(){                        
            //if(NL.settings.autoListing() === true) {               
            controls.settings.refreshSystems();  
            // } 
        }, 
        sortBtnPressed: function(){
            var self = controls.settings;
            self.listOfLocalClient.sort(function(left, right){ return left.client_name == right.client_name ? 
                                        ( (left.zone == right.zone ) ? (left.zone_name > right.zone_name ? 1: -1 ) :  
                                          (left.zone > right.zone ? 1 : -1) ):
                                        ( left.client_name  >  right.client_name ? 1: -1 ); }                                        
                                    );
        },       
        discoverIp: function(ipParam, zoneParam){
            var self = controls.settings;

            setTimeout(function () {
                if( (ipParam <= 255) && !self.discoverCancelPressed ){
                    self.discoverBaseIp('Trying to connect to: ' + self.discoverBaseIpTemp + '.' + ipParam + ' - Zone'  + zoneParam);              
                    controls.diagnostics.discoverLocal(self.discoverBaseIpTemp + '.' + ipParam, zoneParam , null);
                    if(zoneParam < 3){
                        ++zoneParam;
                    }
                    else {
                        ++ipParam;
                        zoneParam = 1;
                    }
                    self.discoverIp(ipParam , zoneParam);                                                                     
                }                                
                if(self.discoverCancelPressed){
                    self.discoverCancelPressed = false;
                    self.bWaitInProgress(false);
                    self.discoverBaseIp(self.discoverBaseIpTemp);
                    self.sortDiscoverdZones();
                    self.discoverRefreshRunning (false);
                } 
            }, 350); 

            if(ipParam > 255){                
                self.bWaitInProgress(false);
                self.discoverCancelPressed = false;
                self.discoverBaseIp(self.discoverBaseIpTemp);
                self.sortDiscoverdZones();
                self.discoverRefreshRunning (false);
            }
        },
        sortDiscoverdZones : function(){
            var self = controls.settings;
            self.discoveredZones.sort(function(left, right){ return left.zoneIp == right.zoneIp ? 
                                        (left.zoneNo > right.zoneNo ? 1 : -1):
                                        ( parseInt(left.zoneIp.split('.')[3] , 10) > parseInt(right.zoneIp.split('.')[3] , 10) ? 1: -1 ); }
                                        // compare the last number of the IP as an ineger.                                    

                                    );            
        },
        filteredDiscoveredZones : function(){
            var listDiscoveredZones = [];
            var self = controls.settings;
            var criteria = controls.settings.searchCriteria().toUpperCase();                                        
            var i = 0;
            for( i = 0; i< self.discoveredZones().length ; i++ ){
                if( (self.discoveredZones()[i].zoneIp.indexOf(criteria) >= 0) || (self.discoveredZones()[i].systemName.indexOf(criteria) >= 0) ){
                    listDiscoveredZones.push(self.discoveredZones()[i]);
                }
            }            
            return listDiscoveredZones;
        },
        refreshDiscover: function(){               
            var self = controls.settings;            
            if(self.discoverRefreshRunning()){
                controls.notification.show('Dicover zones is already in progress!', controls.notification.types.error);
                return;
            }

            self.discoveredZones.removeAll(); // clear the array of the discovered zone!
            var NoOfDots = self.discoverBaseIp().split('.').length - 1;
            if(NoOfDots == 2){       
                self.discoverRefreshRunning (true);
                self.bWaitInProgress(true);
                self.discoverBaseIpTemp = self.discoverBaseIp();            
                self.discoverIp(0 , 1);
            }
            else{
                controls.notification.show('Incorrect submask format', controls.notification.types.error);
            }
        },
        cancelDiscover: function(){
            var self = controls.settings;
            self.discoverCancelPressed = true;             
        },
        toggleSettingsAccordian: function(data, event) {

          var collapsed = $(event.currentTarget.parentElement).find('.settings-accordian').css('display');
            //$('.settings-accordian').css('display','none'); //hide all, 
            if(collapsed === 'none' ) {                 
                $(event.currentTarget.parentElement).find('.settings-accordian').css('display','block'); //show child of clicked
               
                //////////////////////////////////////////////////////////////
                // Ebrahim M : Added to set the default value of the combo box
                var element = document.getElementById('txtServer');
                var connectedAddr = NL.settings.utopia.address();                
                //console.log('1 ### connectedAddr = ' , connectedAddr);
                //console.log('2 #### new addr = ' , NL.api.socketSettings.address);
                
                if(connectedAddr.indexOf('dev') >= 0 ){
                    element.value = 'Dev';
                }
                else{
                    element.value = 'Live';
                }
                //////////////////////////////////////////////////////////////
                

            } else {
                $(event.currentTarget.parentElement).find('.settings-accordian').css('display','none');
            }
        },
        switchServer : function(ref) {
            if(ref === 'Dev' ) {
                NL.settings.utopia.address('http://dev-hdmsvpn.nightlife.com.au/hl'); 
                NL.settings.utopia.apiAddress('http://dev-hdmsvpn.nightlife.com.au/api');
            } else {
                NL.settings.utopia.address('http://hdms-live.nightlife.com.au/hl');
                NL.settings.utopia.apiAddress('http://hdms-live.nightlife.com.au/api');
            }
        },
        backPressed : function(){                        
            var self = controls.settings;            
            if(self.targetSystemId() === undefined){                
                controls.settings.logout();

                controls.navigation.navigateToHash('tab-signin');
            }
            else{
                if(NL.settings.systemStatus() === 'Connected'){
                    controls.navigation.navigateToHash('tab-playlist');   
                }
                else{
                    controls.notification.show('No music zone is connected', controls.notification.types.error);
                }
            }
        },
        searchPressed : function(){            
            var self = controls.settings;
            if(NL.settings.connectionPolicy() !== 'manual'){
                self.searchBoxIsActive(!self.searchBoxIsActive());
            }
            else{
                controls.notification.show('Sorry, search is not functional in this page.', controls.notification.types.error);
            }
        },
        filterZones : function(){
            // Ebrahim M : Nothing to do here!
        },
        setDownFlagShowDownDlg : function(aIsRestart){            
            var self = controls.settings;
            self.bIsRestart(aIsRestart);            
            controls.modal.show('dlg-confirm-sys-restart', {}, '');

        },
        downMusicSystem: function(aIsRestart){
            var self = controls.settings;
            if(self.bIsRestart()){                
                controllers.request_system.restart();
            }
            else{                
                controllers.request_system.shutdown();
            }
            controls.modal.hide();
        },
        getZoneNames : function() {
            var zoneNames = '';
            for(i=0 ; (NL.settings.systems) && i < NL.settings.systems().length ; i++){
                if(NL.settings.systems()[i].system && NL.settings.selectedSystem().system) {
                    if(NL.settings.systems()[i].system === NL.settings.selectedSystem().system && 
                        NL.settings.systems()[i].zone_name !== ''){                        
                        zoneNames += ('\'' + NL.settings.systems()[i].zone_name + '\', ');                        
                    }
                }
            }            
            return zoneNames + ' at ' + NL.settings.selectedSystem().client_name + '.'; 
        },
        getPlaybackMode : function(){
            var playbackMode  = 'DJ Automix';

            if(NL.zone.primary() ===  'scheduler_on'){
                playbackMode = 'Scheduler';
            }
            if(NL.zone.primary() ===  'dj_manual_cue_points') {
                playbackMode = 'DJ Manual';   
            }   

            return playbackMode;
        },
        backInStartupPressed:function(){ // Ebrahim M: Added to cover back button in startup page.
            var self = controls.settings;
            self.bWaitInProgress(false);
            self.suspendedConnect = true;
            controls.navigation.navigateToHash('tab-musiczones');
            self.refreshSystems();
            self.hideLM();
        },
        signoutInStartupPressed:function(){
            // if(NL.settings.systemStatus() === 'Connected'){
            // console.log( 'user name = '  + controls.settings.username()  + 'accountStatus' + NL.settings.accountStatus() );

            var self = controls.settings;
            self.bWaitInProgress(false);
            self.suspendedConnect = true;
            controls.settings.logout();
            controls.navigation.navigateToHash('tab-signin');
            self.hideLM();
        },
        getStartInStartupPressed:function(){ // Ebrahim M: Added to hide the start up page and show the playList
            // alert('3');
            if(NL.settings.systemStatus() === 'Connected'){                
                var self = controls.settings;
                self.bDeliberateDelay(true);
                controls.navigation.businessAsUsual(); 
                setTimeout(function () {
                    if(NL.settings.selectedSystem() !== null && NL.settings.selectedSystem() !== undefined){
                        var strConnectedTo = NL.settings.selectedSystem().zone_name;
                        if (NL.settings.selectedSystemId() === 'manual'){
                            strConnectedTo = NL.settings.manualSystem.systemCode();
                        }                    
                        if(!self.suspendedConnect){
                            controls.notification.show('Connected to ' + strConnectedTo + ' - Please wait...', controls.notification.types.info);
                        }
                    }
                }, 2500);
                setTimeout(function () {
                    if(!self.suspendedConnect){
                        self.hideLM();
                        self.bDeliberateDelay(false);
                        if(NL.context.modal() === 'song'){
                            controls.modal.hide('song-modal');                     
                        }
                    }
                }, 7500);
            }
        },
        getFavoriteRemvedSongs: function(){
            // Ebrahim M : Grab the favorite songs when the music zone is connected.
            controllers.request_user.favorite_songs('like');
            controllers.request_user.favorite_songs('peeve');

        },
        testSpotifyPhp: function(){
            var songs = ['CR01T1' , 'CR01T2'];
            controllers.general.getSpotifyIds(songs);

          /*  var urlSpotifyInServer = 'http://ebrahim.new-devintranet.nightlife.com.au/apps/SpotifyMmnl/SpGetTokens.php';
            $.post(urlSpotifyInServer, {
                spotifyCode : NL.spotify.spotifyCode ,
                redirectUrl : NL.spotify.redirect_uri
            }).done(function(data) {
                console.log('$$$$$$$$$$$$$$$$$ response From Server', data);
            });
            */
            // alert('testSpotifyPhp');
           //  controls.spotify.refreshToken();
        }
    }
});

controls.settings.bWaitInProgress.subscribe(function(value) {    
    if(!value){
        controls.settings.getStartInStartupPressed();
   }
});

// Wait for Ready state before hooking everything up
$doc.one('nightlife-ready', function() {
    controls.settings.init();
});
