$.extend(true, controls, { 
  song: {
    newPlaylistSpotify : ko.observable(''),
    spFrameIsReady : ko.observable(false),
    spFrmPlaylistIsReady : ko.observable(false),

    spinBtnExportShow : ko.observable(false),
    spinBtnClearContentShow : ko.observable(false),
    spinLoadSpDlg : ko.observable(false),
    spinBtnImportShow : ko.observable(false),
    bIsExpanded : ko.observable(false), // Ebrahim M : This variable is added to show if sng detail is expanded.
    bIsPlaylistEmpty : ko.observable(false),
    bIsNoExportedPlaylist : ko.observable(false),
    click: function(song, e) {
      var context = ko.contextFor(e.currentTarget);  
      if (context /*&& context.$index*/ ) {
       // controls.song.getPreviewURL(song);
        //controls.song.getPreviewURL(song); 
                
        controls.songlist.click(song, e);      
      }

    },
    clickCurrent: function(){
      $('.song-info.current[data-distance=1]').click();
    },
    play: function(song) {
      if (song && song.filename) {
        controllers.request_zone.play(
            song.filename, 
            'now', 
           controls.notification.show('Playing Now')
        );
      }
    },
    playNext: function(song) {
      if (song && song.filename) {
        controllers.request_zone.play(
          song.filename, 
          'next',
          controls.notification.show('Playing Next')
        );
      }
    },
    queue: function(song) {
      if (song && song.filename) {
        controllers.request_zone.queue(
          song.filename, 
          false,
          controls.notification.show('Song Queued')
        );
      }
    },
    playLast: function(song) {
      if (song && song.filename) {
        controllers.request_zone.queue(
          song.filename, 
          true,
          controls.notification.show('Playing Last')
        );
      }
    },
    remove: function(song) {
      if (song && song.filename) {
        controllers.request_zone.remove(
          song.filename,
          controls.notification.show('Song Removed from Playlist'));        
        controls.modal.hide();
      }
    },    
    // Ebrahim M: Added this function because the removed song should go to the 'Removed Song' list. 
    // bug reported by Mark B and the logic is copied from MMNL mobile app!
    addToPeeve:function(filename, aRemovedComment){      
      var songArray = [];
      songArray.push({'filename':filename});
      songArray.push({'comment':aRemovedComment});
      songArray.push({'system': NL.settings.selectedSystem().system});
      songArray.push({'zone': NL.settings.selectedSystem().zone});

      if ( !controls.songlist.isSongInPeevedList() ) {
        controllers.request_user.add_favorite_songs(songArray, 'peeve');
        NL.userInfo.dislikeSongs.push(filename);
      }
    },
    /////////////////////////////////
    setRemoved: function(song) {
      if (song && song.filename) {
        if(!song.removed()) {     // confirm if removing
          // var message = 'Are you sure you want to permanently Remove ' + song.title + ' from ALL lists?';
          // if (confirm(message) ) {}
          controls.modal.show('dlg-confirm-remove-song', song, '');
        } else {
          controllers.request_zone.set_removed(
            song.filename, 
            !song.removed(),
            controls.notification.show('Song Restored'));
        }
      }
    },
    removeSong:function(song){
      var strRemovedComment = $('#txtRemoveComment')[0].value;
      controllers.request_zone.set_removed(
        song.filename, !song.removed(),
        function() {
          controls.song.addToPeeve(song.filename, strRemovedComment);
          controls.notification.show('Song Removed');
          controls.modal.hide();
        }
      );      
    },
    toLists: function(song) {           
      if (song) {
        NL.context.selectedSong(song);
        controls.modal.hide();
        controls.navigation.navigateToHash('#tab-lists');
        controls.lists.setTab('#lists-song');
      }
    },
    toSearch: function(song, param) {
      if(song) {
        NL.context.selectedSong(song);
        controls.modal.hide();
        controls.navigation.navigateToHash('#tab-search');
        controls.search.fromSong(song, param);
      }
    },
    removeImage: function(obj, event) {
      $(event.currentTarget).remove();
    },
    getPreviewURL: function(song) {
      if(song.id_spotify !== undefined && song.id_spotify !== null) {
        $.getJSON('https://api.spotify.com/v1/tracks/' + song.id_spotify, function(data) {
          song.preview_url(data.preview_url);      
          console.log(song.preview_url());
        });
      } else {
        song.preview_url('');
      }
    },
    toggleSpotify: function(song){
      var self = controls.song;
      self.spFrameIsReady(false);

      if(!NL.spotify.isSingedIn() || !controls.song.isSpotifyIdAvailable(song) ){
          return false;
      }

      var spFrame = $('#spotify_frame');
      spFrame.attr('src', 'https://embed.spotify.com/?uri=spotify:track:' + song.id_spotify);
      self.checkIfSpFrameLoaded(spFrame.context);     
      collapsed = $('#song-spotify-detail').toggle();
      self.bIsExpanded(false);      
      // console.log( 'style = ' , $('#song-spotify-detail').attr('style') );
      if($('#song-spotify-detail').attr('style') !== 'display: none;'){
        self.bIsExpanded(true);
      }

    },
    checkIfSpFrameLoaded: function(spFrameDoc){
      var self = controls.song;
      setTimeout(function() {         
        if (spFrameDoc.readyState  == 'complete' ) {            
          // console.log('complete recieved!');
          self.spFrameIsReady(true);
        }
        else{
          console.log('checkIfSpFrameLoaded called');
          self.checkIfSpFrameLoaded();
        }
      }, 1000);
    },
    addTrackToSpotify: function(song , event ,playlistName){
      if (NL.spotify.selectedExpPlaylist() === undefined || NL.spotify.selectedExpPlaylist() === ''){        
        controls.song.createPlaylistAddTrack(song);
      }

      var i = 0; 
      var playlistId = '';  
      if(playlistName === undefined || playlistName === '' ){
        for (i=0; i< NL.spotify.spotifyExpPlaylists().length; i++){
          if(NL.spotify.selectedExpPlaylist() === NL.spotify.spotifyExpPlaylists()[i].id){
            playlistName = NL.spotify.spotifyExpPlaylists()[i].name;
            playlistId = NL.spotify.spotifyExpPlaylists()[i].id;
            break;
          }
        }
      }
      if(playlistName === undefined || playlistName === '' || playlistName === '-'){
        if(controls.song.newPlaylistSpotify() === ''){
          controls.newNotification.initNotif('Please select a playlist from the dropdown list and try again.', 5000);
        }
        return false;
      }
      NL.spotify.isTrackDuplicated(playlistId, song.title , song.artist , function(param){

        if(param){
          controls.newNotification.initNotif('Duplicate error: The song already exists in the playlist.', 5000);   
        }
        else{
          var arSpotifyTracks = [];
          if(song.id_spotify !== undefined && song.id_spotify !== null && song.id_spotify !== '') {
              arSpotifyTracks.push(song.id_spotify);
              NL.spotify.addTracksToSpPlaylist( arSpotifyTracks, playlistName, function(param) {
                if(param === 'success'){
                  NL.spotify.moveLastTrackToTop(playlistId);
                  controls.newNotification.initNotif( '(' + song.title + ') added to (' + playlistName + ') playlist.', 5000);
                }
              });
          }          
        }
      });
    },
    createPlaylistAddTrack: function(song){
      // Hang on : Done
      var playlistName = controls.song.newPlaylistSpotify();
      if(playlistName === '' || playlistName === null || playlistName === undefined){
        controls.newNotification.initNotif('Please type a playlist name and try again.', 5000);
      }

      /*for(i = 0; i< NL.spotify.spotifyImpPlaylists().length ; i++){
        arSpotifyPlaylists.push( NL.spotify.spotifyImpPlaylists()[i].name.toUpperCase());
      } */
      var i=0;
      var arSpotifyPlaylists = [];

      for(i=0; i < NL.spotify.spotifyPlaylists().length ; i++){
        if(NL.spotify.spotifyPlaylists()[i].guestUser === NL.spotify.userName() ){
          break;
        }
      }

      if( i < NL.spotify.spotifyPlaylists().length){
        for(var j = 0; j < NL.spotify.spotifyPlaylists()[i].guestPlaylists.length ; j++){
            arSpotifyPlaylists.push( NL.spotify.spotifyPlaylists()[i].guestPlaylists[j].name.toUpperCase() );
        }
      }

      if( controls.song.existsInArray( arSpotifyPlaylists, playlistName.toUpperCase() ) ) {
        controls.newNotification.initNotif('Duplicate error: The playlist already exists in the Spotify account.', 5000);
        return false;
      }

      if(song.id_spotify !== undefined && song.id_spotify !== null && song.id_spotify !== '') {
          NL.spotify.createSpPlaylist( playlistName, function (param, data){
            if(param === 'success'){
              NL.spotify.selectedExpPlaylist(data.id);
              controls.song.addTrackToSpotify(song , {},  playlistName);
              controls.newNotification.initNotif('created (' + playlistName+') playlist successfully.', 5000);
              controls.song.newPlaylistSpotify('');
            }
          });
      }
    },
    existsInArray: function(arr, item){
      var i = 0;
      for(i = 0; i < arr.length ; i++ ){
        if(arr[i] === item){
          return true;
        }
      }
      return false;
    },
    snapShotHistory: function(song){
      controls.song.spinBtnExportShow(true);
      var arSpotifyTracks = [];
      var i =  0;
      var playlistName = NL.settings.selectedSystem().displayName();
      var playlistId = '';

      for (i = NL.playlist.history().length - 1; i >= 0  ; i-- ){
        //console.log('i = ' + i , NL.playlist.history()[i].id_spotify);
        if(NL.playlist.history()[i].id_spotify !== undefined && 
          NL.playlist.history()[i].id_spotify  !== null && 
          NL.playlist.history()[i].id_spotify  !== ''){
            if( !controls.song.existsInArray(arSpotifyTracks, NL.playlist.history()[i].id_spotify) ) {
              arSpotifyTracks.push( NL.playlist.history()[i].id_spotify );
            }
        }
        if(arSpotifyTracks.length >= 50){
          break;
        }
      }

      if(arSpotifyTracks.length > 0) {
        for (i = 0; i< NL.spotify.spotifyExpPlaylists().length; i++){
          if(playlistName === NL.spotify.spotifyExpPlaylists()[i].name){
            playlistId = NL.spotify.spotifyExpPlaylists()[i].id;
            break; // The playlist has already created !
          }
        }
        if( i < NL.spotify.spotifyExpPlaylists().length){ // The playlist has already created !      
          controls.song.clearPlaylist(playlistId, playlistName ,function(param) {
            if(param === 'success' || param === 'empty'){
              NL.spotify.addTracksToSpPlaylist( arSpotifyTracks, playlistName, function(param) {
                controls.song.spinBtnExportShow(false);
                if(param === 'success'){
                  controls.newNotification.initNotif(arSpotifyTracks.length + ' tracks added to (' + playlistName + ') playlist.', 5000);
                }
              }); 
            }
            else{
              controls.song.spinBtnExportShow(false);
            }
          });
        }
        else{ // It is a new playlist          
          NL.spotify.createSpPlaylist( playlistName, function (param, data){
            if(param === 'success'){
              controls.newNotification.initNotif('created (' + playlistName+') playlist successfully.', 5000);
              NL.spotify.addTracksToSpPlaylist( arSpotifyTracks, playlistName, function(param) {
                controls.song.spinBtnExportShow(false);
                if(param === 'success'){
                  controls.newNotification.initNotif(arSpotifyTracks.length + ' tracks added to (' + playlistName + ') playlist.', 5000);
                }
              }); 
            }
            else{
              controls.song.spinBtnExportShow(false);
            }
          });            
        }
      }       
    },

    importTrackFromSpotify: function (){
      if(NL.spotify.selectedImpPlaylist().userName === ''){
        console.log('Error: Spotify userName is null');
        return false;
      }
      controls.song.spinBtnImportShow(true);
      NL.spotify.getPlaylistTracks( NL.spotify.selectedImpPlaylist().userName , 
                                    NL.spotify.selectedImpPlaylist().playlistId , 
                                    function(items , aPlaylistId) {
        var i = 0; 
        var arSpotifyTracks = [];
        if((items !== undefined)  && (items !== null) ){
          for(i = 0; i < items.length ; i++ ){
            if(items[i] !== null){
              arSpotifyTracks.push(items[i].track.id);
            }
          }
        }
        if(arSpotifyTracks.length === 0 ){
          controls.newNotification.initNotif('Oops, No track read from (' + 
                                                      NL.spotify.selectedImpPlaylist().playlistName + ').', 4000);
          controls.song.spinBtnImportShow(false);
          return false;
        }                
        controls.newNotification.initNotif(arSpotifyTracks.length + ' tracks read from (' + 
                                                      NL.spotify.selectedImpPlaylist().playlistName + ').', 5000);
        if( controls.song.isItInsertedBefore(aPlaylistId) ){
          controls.song.removePlaylist(aPlaylistId);
        }
        controls.search.arImportedPlaylists.push({spPlaylistName: NL.spotify.selectedImpPlaylist().playlistName, 
                                                  spPlaylistId:aPlaylistId,
                                                  spOwner: NL.spotify.selectedImpPlaylist().userName,
                                                  fileNames:[]//,
                                                  //fileNamesAtSys:[],
                                                  //resultCntAtSys:0,
                                                  //AtSystem:NL.settings.selectedSystem().system                                                  
                                                  });
        controls.search.arImportedResultlists.push({ spPlaylistId:aPlaylistId,
                                                  spOwner: NL.spotify.selectedImpPlaylist().userName,                                                  
                                                  fileNamesAtSys:[],
                                                  resultCntAtSys:0,
                                                  AtSystem:NL.settings.selectedSystem().system                                                  
                                                  });
        controllers.request_system.getNlAssetForSp(arSpotifyTracks);
      });    
    },
    removePlaylist: function (aPlaylistIId){
      controls.search.arImportedPlaylists.remove(function(item){ 
        return item.spPlaylistId === aPlaylistIId;
      });

      /*controls.search.arImportedResultlists.remove(function(item){ 
        return item.spPlaylistId === aPlaylistIId;
      });*/
    },    
    isItInsertedBefore: function(aPlaylistId){
      for(var i = 0; i < controls.search.arImportedPlaylists.length; i++){
        if(controls.search.arImportedPlaylists()[i].spPlaylistId === aPlaylistId){
          return true;
        }
      }
      return false;
    },
    clearPlaylist: function(playlistId, playlistName , cb){
      NL.spotify.getPlaylistTracks( NL.spotify.userName(), playlistId ,  function(items, aPlaylistId) {
        var i = 0; 
        var arSpotifyTracks = [];
        for(i = 0; i < items.length ; i++ ){
          arSpotifyTracks.push(items[i].track.id);          
        }
        if(arSpotifyTracks.length > 0){
          NL.spotify.removeTracksFromSpPlaylist( playlistId , arSpotifyTracks , function(param) {          
            if(param === 'success'){
              controls.newNotification.initNotif('Tracks removed from the (' + playlistName+') playlist.', 5000);
            }
            else{
             console.log('### Error in removing tracks from playlist!' , param); 
            }
            cb(param);
          });
        }
        else{
          cb('empty'); // success -> empty
        }
      });
    },
    isSpotifyIdAvailable : function(song){
      if(song.id_spotify !== undefined && song.id_spotify !== null && song.id_spotify !== '') {        
        return true;
      }      
      return false;
    },
    toggleCommentExpand : function(data, event){        
      $('#remove-comment').toggle();
      collapsed = $('#confirm-remove').toggle();
      setTimeout(function() {
        $('#txtRemoveComment')[0].focus();
        //document.getElementById('txtRemoveComment').focus();
      }, 200);      
    },
    showSpotifyDlg : function(){
      controls.navigation.jumpToListsSpotify();
      /* var self = controls.song;
      controls.modal.show('dlg-spotify', {}, ''); */
      /*self.spinLoadSpDlg(true);
      NL.spotify.getUserPlaylists(function (){
        self.spinLoadSpDlg(false);
        self.spFrmPlaylistIsReady(false);
        if( !NL.spotify.isSingedIn() ){
            return false;
        }
        controls.modal.show('dlg-spotify', {}, '');
        // self.linkWidgetToPlaylist(NL.settings.selectedSystem().displayName());
        self.linkWidgetToMultipleTracks();
      });*/

    },
    linkWidgetToMultipleTracks: function (){
      var self = controls.song;
      var previewTracks = '';
      var arSpotifyTracks = [];
      for (i = NL.playlist.history().length - 1; i >= 0  ; i-- ){
        if(NL.playlist.history()[i].id_spotify !== undefined && 
          NL.playlist.history()[i].id_spotify  !== null && 
          NL.playlist.history()[i].id_spotify  !== ''){
            if( !controls.song.existsInArray(arSpotifyTracks, NL.playlist.history()[i].id_spotify) ) {
              arSpotifyTracks.push( NL.playlist.history()[i].id_spotify );
            }
        }
        if(arSpotifyTracks.length >= 50){
          break;
        }
      }

      if(arSpotifyTracks.length > 0) {
        previewTracks = arSpotifyTracks.toString();
        //console.log('>>>>>>>>>>>>>>>>>>' , previewTracks);
        var spFrame = $('#spotify_frame_playlist');
        spFrame.attr('src', 'https://embed.spotify.com/?uri=spotify:trackset:PREFEREDTITLE:' + previewTracks);
        // '5Z7ygHQo02SUrFmcgpwsKW,1x6ACsKV4UdWS2FMuPFUiT,4bi73jCM02fMpkI11Lqmfe');
        self.checkIfSpFramePlaylistLoaded(spFrame.context);
      }
    },
    checkIfSpFramePlaylistLoaded: function(spFrameDoc){
      var self = controls.song;
      setTimeout(function() {         
        if (spFrameDoc.readyState  == 'complete' ) {          
          self.spFrmPlaylistIsReady(true);
        }
        else{
          console.log('checkIfSpFramePlaylistLoaded called');
          self.checkIfSpFramePlaylistLoaded();
        }
      }, 1000);
    }, 
    undoCreatePlaylist: function(){
      if(NL.spotify.spotifyExpPlaylists().length > 0){
        NL.spotify.selectedExpPlaylist(NL.spotify.spotifyExpPlaylists()[0].id);
      }
      else{
        NL.spotify.selectedExpPlaylist('');
      }

      for (i=0; i< NL.spotify.spotifyExpPlaylists().length ; i++){
        if(NL.spotify.spotifyExpPlaylists()[i].name === 'My Nightlife Tracks'){
          NL.spotify.selectedExpPlaylist(NL.spotify.spotifyExpPlaylists()[i].id);
        }
      }
    }
  }
});
