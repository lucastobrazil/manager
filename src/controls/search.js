$.extend(true, controls, {
  search: {
    selectedUsername : ko.observable(''),
    arImportedPlaylists : ko.observableArray(),
    arImportedResultlists: ko.observableArray(),

    removeResultList: function (aPlaylistIId){
      controls.search.arImportedResultlists.remove(function(item){ 
        return item.spPlaylistId === aPlaylistIId;
      });
    }, 
    newSearch : function() {
      controls.search.clearSearch();
      controls.search.search();

    },

    clearSearch : function() {
      //NL.songSearchResults.lockScroll=true;
      NL.songSearchResults.songs([]);
      NL.songSearch.startIndex(0);
    //  NL.songSearch.offset('');
      controls.search.callback = function(){};
      controls.search.setJump('All'); //@TODO check
    },
    
    appendMore : function() { /* 75% more */         
        if(NL.songSearchResults.moreResults()) {
             NL.songSearch.startIndex(
              Math.min(
                (NL.songSearch.startIndex() + parseInt(NL.songSearch.limit()*0.75,10)),
                (NL.songSearchResults.count() - parseInt(NL.songSearch.limit()*0.75,10))
              )
            );     
             NL.songSearch.offset('');
              $('#search-results-body').css('overflow-y','hidden'); //disable scrollign 
            controls.search.callback = function(){ 
                var delta = $('#search-results-body').prop('scrollHeight')*0.25 - $('#search-results-body').height() + $('.search-spinner').height();
                if(delta < $('#search-results-spinner-top').height()) { // fix for last page, hide spinner
                  delta = $('#search-results-spinner-top').height();
                }
                  
                $('#search-results-body').scrollTop(delta);
                $('#search-results-body').css('overflow-y','scroll');               
                NL.songSearchResults.unlockScroll();
            };        
            controls.search.search();
      } 
    },
    prependMore : function() {
       if(NL.songSearchResults.lessResults()) {
        NL.songSearch.offset('');
         NL.songSearch.startIndex(Math.max(NL.songSearch.startIndex() - parseInt(NL.songSearch.limit(),10)*0.75),0);
        $('#search-results-body').css('overflow-y','hidden'); //disable scrollign 
         controls.search.callback = function(){     
            var delta =   $('#search-results-body').prop('scrollHeight')*0.75 - $('.search-spinner').height();
            $('#search-results-body').scrollTop(delta);
            $('#search-results-body').css('overflow-y','scroll');
            NL.songSearchResults.unlockScroll();
          };
          controls.search.search();
      }

    },
    jumpTo : function(d,e) {
        
        var index=$(e.currentTarget).index();

        if(d === '#' || d==='All') {
          d = ''; // change to no offset so we get weird chars first
        }
        if(['title','artist'].indexOf(NL.songSearch.primarySort()) >= 0) {
          if(d > NL.songSearchResults.maxAlpha()) {            
            d= NL.songSearchResults.maxAlpha();
          }
        }        
        NL.songSearch.offset(d); // set offset from calling object
        NL.songSearch.startIndex(0); //reset, we want first reuslt for offset
        controls.search.callback = controls.search.scrollToTop;//scroll to top after load        
        controls.search.search();
    },
   /* jumpToFirst : function() {
      NL.songSearch.offset('');
      NL.songSearch.startIndex(0);
    },
    jumpToLast : function() {
      NL.songSearch.offset('');
      NL.songSearch.startIndex(NL.songSearchResults.maxKey());
    },*/
    setJump : function(e) {
      var c = e[0].toLowerCase();      
      if(c.search(/[^A-Za-z\s]/) != -1) { //if non alpha-numeric
        if(c === '\'') { c = e[1].toLowerCase;}
        else {c = '#';}
      }

      $('#search-jumps').find('.active').removeClass('active'); // remove current active
      $($('#search-jumps').find('a').filter(function(index) { return $(this).text() === c;})).parent().addClass('active');

    },

    search: function() {      
        if(NL.songSearch === undefined){
          return;
        }

        var f = NL.songSearch.searchQuery();
        if(f.search_released) {
          if(f.search_released.start === '') {
            delete f.search_released; // replace blank year with all songs
          }
        }
        if(f.search_title) { // Clean up whitespace on
          f.search_title = f.search_title.replace(/\s{2,}/g, ' ').trim();
          f.search_title = f.search_title.trim();
        }
        if(f.search_artist) {
          f.search_artist = f.search_artist.replace(/\s{2,}/g, ' ').trim();
          f.search_artist = f.search_artist.trim();
        }

        f.limit = NL.songSearch.limitObj();
        controllers.request_zone.songs(f, controls.search.callback);
    },
    /*searchOnFilenames:function(){ // Ebrahim M: Added for searching based on the filename.
      var self = controls.search;
      //var model = {"filter_songs" : ["HD21T2"], "limit": {"length" : 20}}
      var model = {'filter_songs' : self.arFilenames(), 'limit': {'length' : self.arFilenames().length}};      
      controllers.request_zone.songs(model, controls.search.callback);           
    }, */
    toggleAdjacent: function() {
      NL.songSearch.adjacent(!NL.songSearch.adjacent());
    },
    allSongs: function() {
      NL.songSearch.searchYear('');
      controls.navigation.navigateToHash('#tab-search');      
      $('a[href=#search-year]').tab('show');
      controls.search.clearSearch();
    },
    fromSong: function(song, param) {
      controls.search.clearSearch();
      NL.songSearch.searchArtist(song.artist);
      NL.songSearch.searchTitle(song.title);
      NL.songSearch.searchCategory(song.category);

      var year = parseInt(song.releasedYear,10);
      if(year > 50) {
        year += 1900;
      } else {
        year += 2000;
      }
      year = parseInt(year / 10,10);
      year = year * 10;
      if( year < 1920){
        year = 'Pre 1920';
      }
      else{
        year = year + 's';
      }    

      NL.songSearch.searchYear(year);

      if( param === 'artist'){
        $('a[href=#search-artist]').tab('show');
      }
      if(param === 'year'){     
        $('a[href=#search-year]').tab('show');        
      }

      controls.search.search();      
    },
    fromList: function(list) {  
      controls.search.clearSearch();
      if(list.group.toUpperCase() === 'AMBIENT'){
        NL.songSearch.typeFilter(NL.songSearch.filterFieldTypes[2]);
      }
      else if(list.group.toUpperCase() === 'KARAOKE'){
        NL.songSearch.typeFilter(NL.songSearch.filterFieldTypes[1]);
      }
      else{
        NL.songSearch.typeFilter(NL.songSearch.filterFieldTypes[0]);
      }

      NL.songSearch.selectedListSlot(list.slot);
      $('a[href=#search-lists]').tab('show');
      controls.search.search();

      //setTimeout(function (){
      //}, 3000);
      return; 
    },
    scrollToTop: function() {
      if(NL.songSearchResults.lessResults()) {       
        $('#search-results-body').scrollTop($('#search-results-spinner-top').height());
         
      }
    },
    scrollToBottom: function() { // scroll to the bottom including offset
      var srb = $('#search-results-body');
     srb.scrollTop(srb[0].scrollHeight -srb.height() - $('#search-results-spinner-top').height());
    },
    navigateToImportSpotify: function(){
      controls.song.showSpotifyDlg();
    }, 
    refreshSpotifyPlaylist: function(aSelectSpList){
     // console.log('>>>> aSelectSpList = ' , aSelectSpList);
      if ( aSelectSpList !== undefined ) {       
        controls.lists.importSpPlaylist(aSelectSpList.id, aSelectSpList.name , aSelectSpList.owner);
      }    
    },
    removeSpotifyPlaylist:function(aSelectSpList){
      console.log('Remove this >>>> aSelectSpList = ' , aSelectSpList);
      var playlistId = aSelectSpList.id;
      
      controls.search.arImportedPlaylists.remove(function(item){      
        return item.spPlaylistId === playlistId;
      });
      controls.search.arImportedResultlists.remove(function(item){
        return item.spPlaylistId === playlistId;
      });
      // NL.songSearch.selectedSpotifyList(undefined);
      if(controls.search.arImportedPlaylists().length > 0 ){
        var i = 0;
        var selectedSpId ='';
        var selectedSpName ='';
        var selectedSpOwner ='';

        selectedSpId = controls.search.arImportedPlaylists()[0].spPlaylistId;
        selectedSpName = controls.search.arImportedPlaylists()[0].spPlaylistName;
        selectedSpOwner = controls.search.arImportedPlaylists()[0].spOwner;
        NL.songSearch.selectedSpotifyList({ id:selectedSpId,
                                        name:selectedSpName,
                                        owner: selectedSpOwner
                                      });        
      }
      else{
        NL.songSearch.selectedSpotifyList(undefined);
      }
    },
    getDropdownListOptions: function(){
      var arOptions = [];
      var i= 0;
      var j=0;
      for ( i =0 ; i < NL.spotify.spotifyPlaylists().length ; i++) {
        if(controls.search.selectedUsername() !== undefined && 
              controls.search.selectedUsername() === NL.spotify.spotifyPlaylists()[i].guestUser ){
          for ( j= 0 ; j < NL.spotify.spotifyPlaylists()[i].guestPlaylists.length ; j++) {          
            arOptions.push({id:NL.spotify.spotifyPlaylists()[i].guestPlaylists[j].id,
                              name:/* '\'' + NL.spotify.spotifyPlaylists()[i].guestUser + '/' +*/ 
                              NL.spotify.spotifyPlaylists()[i].guestPlaylists[j].owner + '\': '+
                              NL.spotify.spotifyPlaylists()[i].guestPlaylists[j].name,
                            owner:NL.spotify.spotifyPlaylists()[i].guestPlaylists[j].owner,
                            color: 'spotify-list-main' /*( (i % 2)? 'spotify-list-guest': 'spotify-list-guest2')*/
                            });
          }
        }
      }      
      return arOptions;
    },
    getDropListOption:function() {
      var self = controls.search;
      var arPlaylists = [];
      var i = 0;
      var j = 0;
      if((NL.spotify.spotifyOwners().length === 0) || (self.arImportedPlaylists().length === 0) ){
        return arPlaylists;
      }

      self.ownerSort(NL.spotify.spotifyOwners());
      for (i = 0; i < NL.spotify.spotifyOwners().length ; i++) {
        var arOneOwnerPL  = [];
        for ( j = 0 ; j < self.arImportedPlaylists().length; j++ ){
          if(self.arImportedPlaylists()[j].spOwner.toUpperCase() === 
                            NL.spotify.spotifyOwners()[i].ownerId.toUpperCase() ){
            var tempId    = self.arImportedPlaylists()[j].spPlaylistId;
            var tempOwner = self.arImportedPlaylists()[j].spOwner;
            var tempName  = self.arImportedPlaylists()[j].spPlaylistName + 
                                      ' ('  + self.getPlaylistDesc(tempId) + ')';            
            arOneOwnerPL.push({'id'   : tempId,
                               'name' : tempName,
                               'owner': tempOwner});
          }
        }
        if( arOneOwnerPL.length > 0){
          controls.search.sort(arOneOwnerPL);
          arPlaylists.push({
            'owner':'Owner:' + NL.spotify.spotifyOwners()[i].ownerId.toUpperCase(), 
            'ownerPlaylists':arOneOwnerPL });
        }
      }
      return arPlaylists;
    },    
    /*  Ebrahim M: I have just copied this structre as a back up !!!!
    getDropListOption:function() {    
      var arPlaylists = [];
      var i = 0;
      if(NL.spotify.spotifyPlaylists().length === 0){
        return arPlaylists;
      }

      for ( i = 0 ; i < NL.spotify.spotifyPlaylists().length ; i++) {
        if(controls.search.selectedUsername() !== undefined && 
              controls.search.selectedUsername() === NL.spotify.spotifyPlaylists()[i].guestUser ){
          break;
        }
      }
      if( i >= NL.spotify.spotifyPlaylists().length){
        return arPlaylists;
      }
      
      var self = controls.search;
      for ( var j=0 ; j < NL.spotify.spotifyPlaylists()[i].guestUserOwners.length ; j++) {
        var arOneOwnerPL = [];
        for(var k=0; k < NL.spotify.spotifyPlaylists()[i].guestPlaylists.length ;k++){
          if(NL.spotify.spotifyPlaylists()[i].guestUserOwners[j].toUpperCase() === 
                NL.spotify.spotifyPlaylists()[i].guestPlaylists[k].owner.toUpperCase()){
            var tempName = NL.spotify.spotifyPlaylists()[i].guestPlaylists[k].name + 
                ' ('  + self.getPlaylistDesc(NL.spotify.spotifyPlaylists()[i].guestPlaylists[k].id) + ')';
            var tempId = NL.spotify.spotifyPlaylists()[i].guestPlaylists[k].id;
            var tempOwner = NL.spotify.spotifyPlaylists()[i].guestPlaylists[k].owner;

            arOneOwnerPL.push({'id'   : tempId,
                               'name' : tempName,
                               'owner': tempOwner});
          }
        }
        controls.search.sort(arOneOwnerPL);
        arPlaylists.push({
          'owner':'Owner:' + NL.spotify.spotifyPlaylists()[i].guestUserOwners[j] , 
          'ownerPlaylists':arOneOwnerPL });
      }
      return arPlaylists;
    },*/
    ownerSort : function(arrInput) {
      arrInput.sort(function(left, right) {
        return left.ownerId.toUpperCase() < right.ownerId.toUpperCase() ? -1 : 1;
      });
    },
    sort : function(arrInput) {
      arrInput.sort(function(left, right) {
        return left.name.toUpperCase() < right.name.toUpperCase() ? -1 : 1;
      });
    },
    getPlaylistDesc : function (aPlaylist){      
      var strRetVal = '';
      var i = 0;
      var j = 0;
      if( (controls.search.arImportedPlaylists() === undefined) || 
            (controls.search.arImportedResultlists === undefined )) {
        return strRetVal;
      }
      for(i = 0; i < controls.search.arImportedPlaylists().length ; i++){
          if(controls.search.arImportedPlaylists()[i].spPlaylistId === aPlaylist ){
              break;
          }
      }

      for(j = 0; j < controls.search.arImportedResultlists().length ; j++){
          if(controls.search.arImportedResultlists()[j].spPlaylistId === aPlaylist ){
              break;
          }
      }

      if( (i < controls.search.arImportedPlaylists().length) && 
          (j < controls.search.arImportedResultlists().length) ){ 
            if(controls.search.arImportedResultlists()[j].AtSystem === NL.settings.selectedSystem().system){
              strRetVal  = controls.search.arImportedResultlists()[j].resultCntAtSys + ' / ';
              strRetVal += controls.search.arImportedPlaylists()[i].fileNames.length + ' available @';
              strRetVal += controls.search.arImportedResultlists()[j].AtSystem ;
            }
            else{
              strRetVal  = controls.search.arImportedPlaylists()[i].fileNames.length + ' matches';
            }
      }
      
      return strRetVal;
    },
    changePlaylist : function(param){
      var arrTemp = param.split(',');
      var selPlaylistId = arrTemp[0];
      var selPlaylistOwner = arrTemp[1];
      var selPlaylistName = '';

      var idx = 0;
      for(idx = 0 ; idx < controls.search.arImportedPlaylists().length ; idx++){
        if(controls.search.arImportedPlaylists()[idx].spPlaylistId === selPlaylistId){
          break;
        }
      }
      if(idx < controls.search.arImportedPlaylists().length){
        selPlaylistName = controls.search.arImportedPlaylists()[idx].spPlaylistName;
      }
                                   
      NL.songSearch.selectedSpotifyList({ id:selPlaylistId, 
                                          name:selPlaylistName, 
                                          owner:selPlaylistOwner });

      // console.log('>>>>>>>>>>>>>> selectedSpotifyList= ', NL.songSearch.selectedSpotifyList() );
    },
    changeUserList : function (selUsername){
      if( typeof(NL) !== typeof(undefined) ) {
        var self = controls.search;
        var i = 0;
        for( i = 0; i < NL.spotify.spotifyPlaylists().length ; i++){
          if(NL.spotify.spotifyPlaylists()[i].guestUser === selUsername){
            self.changePlaylist(NL.spotify.spotifyPlaylists()[i].guestPlaylists[0].id);
            break;
          }
        }
      }      
    },
    onLoadSpotifyTab: function(){
      alert('Yes I am here');
    }

  }  
});
