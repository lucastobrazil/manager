$.extend(true, controls, {
  notification: {
    wrapper: undefined,
    notice: undefined,
    content: undefined,
    ready: false,
    duration: 3000,
    init: function() {
      this.wrapper = $('#notice-wrapper');
      this.notice = $('.alert', this.wrapper);
      this.content = $('.notice-content', this.notice);

      this.notice.removeClass();
      this.content.html();

      this.ready = true;
    },
    types: {
      success: 'success',
      warning: 'warning',
      error: 'danger',
      danger: 'danger',
      info: 'info'
    },
    getWrapper: function() {
      if (!this.ready) {
        this.init();
      }

      if (!this.wrapper) {
        this.wrapper = $('#notice-wrapper');
      }

      return this.wrapper;
    },
    show: function(message, type, duration) {
      var self = controls.notification;

      type = type || self.types.info;
      self.getWrapper();
      self.notice.removeClass();
      self.notice.addClass('alert alert-' + (self.types[type] || self.types.info));
      self.content.html(message);
//      self.wrapper.slideDown();
         var thisbox = self.wrapper;
      //  thisbox.css('bottom', '0px').animate({bottom: parseInt(thisbox.css('bottom'),1) === 0 ? thisbox.outerHeight() :  50 }); //slide down the notification
      thisbox.fadeIn(500);
        if(self.timer){
        win.clearInterval(self.timer);
      }
      if(duration === undefined /*&& type != self.types.error*/ ){
        duration = self.duration;
        self.timer = win.setTimeout(self.hide, duration);
      }
    },
    hide: function() {
      var self = controls.notification;
      var thisbox = self.getWrapper();
//      alert(thisbox);
     //thisbox.animate({bottom: parseInt(thisbox.css('bottom'),10) === 0 ? -thisbox.outerHeight() :  0 }); //slide up the notification
     thisbox.fadeOut(500);
    }
  },
  newNotification:{
    initNotif: function(msg, timeout, tint) {
    var displayTime;
    
    // set up some defaults and fallbacks
    if(timeout === 999){
      // infinite
      displayTime = 9999999999999;
    }else if(timeout < 3000){
      // minimum
      displayTime = 3000;
    }else{
      displayTime = timeout;
    }
    var effectTint = tint ? tint : '';
    var svgshape = document.getElementById( 'notification-shape' );
    var notification = new NotificationFx({
        wrapper : svgshape,
        message : msg,
        layout : 'other',
        effect : 'loadingcircle',
        ttl : displayTime,
        type : 'notice', // notice, warning or error
        onClose : function() {
         // bttn.disabled = false;
        }
      });

      // show the notification
      notification.show();

      // Set vertical padding of msg depending on length
      if(msg.length <= 70){
        $('.ns-box').css('padding-top', '21px');
      }else if(msg.length <= 140){
        $('.ns-box').css('padding-top', '12px');
      }else if(msg.length > 140){
        $('.ns-box').css('padding-top', '4px');
      }
    }
  }
});