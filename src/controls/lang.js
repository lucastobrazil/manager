$.extend(true, controls, {
  lang: {
    culture: ko.observable(),
    cultures: ko.observableArray(),
    setCulture: function(culture){
      if(!culture){
        return false;
      }
      return Globalize.culture(culture);
    },
    getCulture: function(){
      return Globalize.culture();
    },
    format: function(obj, format){
      return Globalize.format(obj, format);
    },
    init: function(){
      if(Globalize){
        var keys = Object.keys(Globalize.cultures);
        for(var i = 0; i < keys.length; i++){
          if(keys[i] !== 'default' && keys[i] !== 'en'){
            this.cultures.push({name: Globalize.cultures[keys[i]].englishName, value: keys[i] });
          }
        }

        var current = NL.settings.culture();
        if(current && Globalize.cultures[current]){
          Globalize.culture(current);
          this.culture(current);
        }

        this.culture.subscribe(function(newValue){
          if(newValue && Globalize.cultures[newValue]){
            Globalize.culture(newValue);
            NL.settings.culture(newValue);
          }
        });
      }
    }
  }
});

// Wait for Ready state before hooking everything up
$doc.one('nightlife-ready', function() {
  $.call(controls.lang.init(), controls.lang);
});
