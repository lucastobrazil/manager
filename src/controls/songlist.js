$.extend(true, controls, {
  songlist: {    
    click: function(song, event) {
      var obj = $(event.currentTarget);
      var context = ko.contextFor(event.currentTarget);

      if (song && song.filename) {
        var list = obj.closest('.songlist');
       // console.log('YES I AM HERE!!!!!!!!', list);
        console.log('>>>>>>>>>>>>>>>>>>', obj);
        if (list.length > 0) {
          var index = 0 ;
          if(context.$index){
            index = context.$index();
          }
          else{
           index = NL.playlist.history().length;
          }
          this.info(list, index);          
          NL.context.selectedSong(song);
        }
      }
    },
    // Used to auto highlight the active song
    highlightIndex: function(obj, song) {
      var self = controls.songlist;
      var context = ko.contextFor(obj[1]);

      var $obj = $(obj);
      if ($obj.length) {
        var list = $obj.closest('.songlist');
        var activeIndex = list.data('active-index');
        var index = context.$index();

        if (activeIndex == index) {
          self.highlight($obj, list, index);
        }
      }
    },
    info: function(list, index, direction) {
      var context = ko.contextFor(list[0]);
      if(context === undefined){
        return false;
      }

      if (context.$data && context.$data.songs) {

        var song = context.$data.songs()[index];
        var model = new SongInfoViewModel(song, list, index);

        if (index !== undefined) {
          if (model.index > 0) {
            model.prev = model.index - 1;
          }

          var max = context.$data.songs().length - 1;
          if (model.index < max) {
            model.next = model.index + 1;
          }
        }

        model.showRemove   = model.distance !== null && model.distance > 1 && model.context === '#tab-playlist';
        model.showPlay     = model.distance !== null;
        model.showPlayNext = model.distance !== null;
        model.showPlayLast = model.distance !== null && model.distance >= 1 && model.context === '#tab-playlist';
        model.showQueue    = model.distance !== null && (model.distance < 1 || model.context === '#tab-search');
        model.showMix      = model.distance !== null && model.distance === 1;
        var obj = $($('.song-info', list)[index]);
        if (obj) {
          if(direction !== undefined) {
            this.scrollTo(obj,direction);
          }
          this.highlight(obj, list, index);
          
          controls.modal.show('song-modal', model, 'song');
          NL.context.selectedSong(model);
          //make sure the dropdown menu event for the remove action gets wired up
          $('.actions .dropdown-toggle').dropdown();
        } else {
          controls.modal.hide();
        }

        // console.log('modl = ' , model);
        if( controls.song.bIsExpanded() && (model.id_spotify !== '') && (model.id_spotify !== null) ) {
          controls.song.toggleSpotify(model);
        }
      }
    },
    scrollTo: function(obj, direction) { //distance is the item we're scrolling to
      var self = controls.songlist;
      
      if(obj){
        var pane = $(obj).closest('.tab-pane');
       // if (self.wrapper.is(':visible')) {
            if(! $(obj).inViewPlaylist()) { // if not in view, scroll to it
                if(direction === 'up') {
                    
                    pane.scrollTop(self.zeroTop(obj));
                } else {
                    
                    pane.scrollTop(self.zeroBottom(obj));
                }
            }
          self.showBAU = false;
        /*}else{
          self.showBAU = true;
        }*/
      }
      return true;
    },
    isSongInFavoriteList: function(){      
      var i = 0;
      for(i=0; i < NL.userInfo.likeSongs().length ; i++){
        if(NL.context.selectedSong().filename === NL.userInfo.likeSongs()[i]){
          return true;
        }
      }            
      return false;
    },
    isSongInPeevedList: function(){
      var i = 0;
      for(i=0; i < NL.userInfo.dislikeSongs().length ; i++){
        if(NL.context.selectedSong().filename === NL.userInfo.dislikeSongs()[i]){
          return true;
        }
      }            
      return false;
    },
    cancelFavDlg: function(){
      controls.modal.hide('song-fav-modal');
    },
    toggleLike: function(){
      var self = controls.songlist;
      var songArray = [];
      songArray.push(NL.context.selectedSong().filename);

      if( self.isSongInFavoriteList() ){      
        controllers.request_user.remove_favorite_songs(songArray, 'like');
        NL.userInfo.likeSongs.remove(NL.context.selectedSong().filename);
      }
      else{
        controllers.request_user.add_favorite_songs(songArray, 'like');
        NL.userInfo.likeSongs.push(NL.context.selectedSong().filename);    

        if(self.isSongInPeevedList() ) {
          controllers.request_user.remove_favorite_songs(songArray , 'peeve');
          NL.userInfo.dislikeSongs.remove(NL.context.selectedSong().filename);        
        }         
      }
    },
    toggleDislike: function(){
      var self = controls.songlist;
      var songArray = [];
      songArray.push(NL.context.selectedSong().filename);

      if( self.isSongInPeevedList() ){      
        controllers.request_user.remove_favorite_songs(songArray , 'peeve');
        NL.userInfo.dislikeSongs.remove(NL.context.selectedSong().filename);        
      }
      else{
        controllers.request_user.add_favorite_songs(songArray, 'peeve');
        NL.userInfo.dislikeSongs.push(NL.context.selectedSong().filename);  

        if(self.isSongInFavoriteList() ) {
          controllers.request_user.remove_favorite_songs(songArray, 'like');
          NL.userInfo.likeSongs.remove(NL.context.selectedSong().filename);          
        }     
      }
    },    
    toggleFavorite: function(){      
      /*controls.modal.show('song-fav-modal', {}, '');
      document.getElementById('txtComment').focus();
      */      
      var self = controls.songlist;
      var songArray = [];
      songArray.push(NL.context.selectedSong().filename);

      if( self.isSongInFavoriteList() ){      
        controllers.request_user.remove_favorite_songs(songArray);
        NL.userInfo.likeSongs.remove(NL.context.selectedSong().filename);
      }
      else{
        controllers.request_user.add_favorite_songs(songArray);
        NL.userInfo.likeSongs.push(NL.context.selectedSong().filename);        
      }      
    },
    changeInfo: function(model, event) {
      var self = controls.songlist;
      var obj = $(event.currentTarget);
      var newIndex = obj.data('index');      
      var direction = event.currentTarget.id == 'song-next' ? 'down' : ('song-prev' ? 'up' : undefined);      

      if (newIndex !== undefined) {
        self.info(model.list, newIndex, direction );
      }

    },
    removeHighlights: function(list) {
      $('.song-info.active', list).removeClass('active');

      if (list && list.length) {
        list.removeData('active-index');
      }
    },
    highlight: function(obj, list, index) {
      this.removeHighlights(list);
      obj.addClass('active');
      list.data('active-index', index);
    },
    zeroTop: function(obj) {
      var result = 0;      
      var wrapper = $(obj).closest('.tab-pane');
      if (wrapper) {
        var pos = $(obj, wrapper).position();
        if(pos){
          result = pos.top;
        }
      }
      return result;
    },
    zeroBottom: function(obj) {
      var result = 0;
      var wrapper = $(obj).closest('.tab-pane');
      if (wrapper) {
        var pos = $(obj, wrapper).position();
        if(pos){
          result = pos.top  - $('#body-wrapper').height() + $('footer').height() - $('.song-info').height();
        }
      }
      return result;
    }
  }
});