$.extend(true, controls, {
  modal: {
    reposition: function() {
      // Sets the modal's vertical position to the center
      var modal = $(this),
      dialog = modal.find('.modal-dialog');
      modal.css('display', 'block');
      dialog.css('margin-top', Math.max(0, ($(window).height() - dialog.height()) / 2));
    },
    show: function(templateName, model, cssClass, cb){
     
      var wrapper = $('#container-'+templateName+'.modal:first');
      cssClass = cssClass || model.cssClass || '';
      switch(templateName) {
          case 'song-modal' : 
            NL.context.modal('song'); 
            NL.playlist.active.distance(model.distance);
            NL.playlist.active.filename(model.filename);

          break;
          case 'zone-controls' : 
            NL.context.modal('zone-controls'); 
            break;
          case 'dlg-bingo':
            NL.context.modal('dlg-bingo');
            break; 
          case 'dlg-confirm-bingo-restart':
            NL.context.modal('dlg-bingo-restart'); 
            break;
          default:break;
      }
      
      ko.applyBindingsToNode(wrapper[0], {
        template: 'layout-modal'
      }, {
        templateName: templateName,
        model: model,
        cssClass: cssClass
      });
      
      // Some Special modals have their own positioning,
      // So don't use the reposition function on them
      $('.modal:not(".song-modal"):not(".controls-modal")').on('show.bs.modal', controls.modal.reposition);
      
      wrapper.modal('show'); 
      
      // Lucas A - for Advertising Modals, apply the 'darker' class to cover the whole interface.
      if (templateName.indexOf('advertising') !== -1 || templateName.indexOf('dlg') !== -1 ) {
        //if(templateName !== 'dlg-confirm-remove-song'){ // Lucas A and Ebi M Added this. 
        $('.modal-backdrop.in').first().addClass('darker');
        //}
      }
      wrapper.one('hidden.bs.modal', function () {              
        $(this).removeData('bs.modal');
        NL.context.modal(''); 
        $('.modal-backdrop.in').removeClass('darker'); // Lucas A and Ebi M Added this.
      });
      
      return(wrapper);
    },

    hide: function(templateName){
      var wrapper;
      if(templateName === undefined) {
        wrapper = $('.modal.in');
      } else {
        wrapper = $('#container-'+templateName+'.modal:first');
      }
      wrapper.modal('hide');
      NL.context.modal('');
    }
  }
});