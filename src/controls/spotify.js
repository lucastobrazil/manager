// Ebrahim M : Developed this file to get the access token from Spotify
$.extend(true, controls, {
    spotify: {
        newSpGuestUsername : ko.observable(''),
        spinAddGuestUser : ko.observable(false),

        // Ebrahim M: the following two vars are defined for the dropdown list in 'Lists/Spotify Users Lists' tab page.
        spAllGuestUsers : ko.observableArray(),
        spSelectedGuestUser : ko.observable(''),
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////

        removeToken: function() {
        },
        validateCode: function(code) {
            if( (code !== undefined ) && (code !== '') ){
                    // console.log('spotifyCode = ' , code);
                    NL.spotify.spotifyCode = code;
                    controls.spotify.requestTokens();
            }
        },
        validateToken: function(token, state) {
            if( (state !== undefined ) && (token !== undefined ) && (token !== '') ){
                if(state === '102938'){
                    NL.spotify.access_token(token);
                }
            }
        },
        queryString:function(txtContent, findThis){
            var query = txtContent;
            var vars = query.split('&');
            for (var i=0;i<vars.length;i++) {
                var pair = vars[i].split('=');
                if(pair[0] == findThis){
                    return pair[1];
                }
            }
            return undefined;
        },
        fetchCodeOfString: function (txtContent, findThis){
            var query = txtContent;
            var hRef =  query.split('?');
            var vars = '';

            if(hRef.length > 1){
                hRef =  hRef[1].split('&');
                vars = hRef;
            }
            for (var i=0;i<vars.length;i++) {
                var pair = vars[i].split('=');
                if(pair[0] == findThis){
                    return pair[1];
                }
            }
            return undefined;
        },
        requestTokens: function(){
            // console.log('##########' , NL.spotify.spotifyCode);
            $.post(NL.spotify.urlSpotifyInServerTokens, {
                spotifyCode : NL.spotify.spotifyCode ,
                redirectUrl : NL.spotify.redirect_uri
            }).done(function(data) {
                // console.log('##########$$$$$$$$$' , data);
                // var temp = data.split(',');
                NL.spotify.access_token(data[0]);
                NL.spotify.refresh_token(data[1]);
            });
        },
        refreshToken : function(){
            if(NL.spotify.refresh_token().trim() !== ''){
                $.post(NL.spotify.urlSpotifyInServerRefresh, {
                    refreshToken : NL.spotify.refresh_token(),
                    redirectUrl : NL.spotify.redirect_uri
                }).done(function(data) {
                    console.log('Token is refreshed. The new token is:', data);
                    if(data !== ''){
                        NL.spotify.access_token(data);
                        NL.spotify.getLoggedUser();

                        // Refresh the Spotify token every 45 minutes!
                        var spotifyRefreshDuration = 1000 * 60 * 45;
                        setTimeout(function () {
                            controls.spotify.refreshToken();
                        }, spotifyRefreshDuration);
                    }
                }).fail(function(){
                    console.error('- Can not refresh the Spotify token!');
                    NL.spotify.access_token('');
                    NL.spotify.userName('Not signed in');
                });
            }
            else{
                console.error('+ Can not refresh the Spotify token!');
            }
        },
        getUserPlaylists: function(){
            //console.log(' getUserPlaylists()  is called!');
            NL.spotify.getSpotifyPlaylists(NL.spotify.userName() , null , function(){});
        },
        addSpGuestPlaylists: function(){
            var self = controls.spotify;
            var newSpUserName = self.newSpGuestUsername();
            if(controls.spotify.spSelectedGuestUser() !== undefined){
                newSpUserName = controls.spotify.spSelectedGuestUser();
            }

            NL.spotify.isSpUserValid(newSpUserName, function (status, ownerId , displayName){
                if(status === 'valid'){
                    if( newSpUserName === NL.spotify.userName() ){
                        controls.newNotification.initNotif('This user is the Spotify signed in user.<BR> All the playlists have already been added.', 8000);
                        //controls.navigation.jumpToListsSpotify();
                        return false;
                    }
                    for(var i = 0; i< NL.spotify.spotifyPlaylists().length ; i++){
                        if( NL.spotify.spotifyPlaylists()[i].guestUser === newSpUserName ){
                            self.spinAddGuestUser(false);
                            controls.newNotification.initNotif('This user has already been added!', 7000);
                            //controls.navigation.jumpToListsSpotify();
                            return false;
                        }
                    }
                    NL.spotify.getSpotifyPlaylists(newSpUserName, displayName , function(aStatus , aPublicPlaylist){
                        self.spinAddGuestUser(false);
                        if(aStatus === 'success'){
                            /////////////////////////////////////////////////////////////////////////////////////
                            // Ebrahim M: pushed it for the dropdown list in 'Lists/Spotify Users Lists' tab page.
                            if( !self.isGuestUserAdded(newSpUserName ) ) {
                                var tempDisplayName = (displayName === undefined || displayName === null)?
                                                                '': ( ' (' + displayName + ')' );
                                controls.spotify.spAllGuestUsers.push({
                                                    id:newSpUserName,
                                                    name: ( newSpUserName + tempDisplayName )
                                                    });
                            }
                            if(controls.spotify.spSelectedGuestUser() === undefined) {
                                controls.spotify.spSelectedGuestUser(newSpUserName);
                            }
                            /////////////////////////////////////////////////////////////////////////////////////
                            controls.lists.expandLastSpList();
                            /*controls.newNotification.initNotif(aPublicPlaylist +
                                ' public playlists added to \'GUESTS SPOTIFY PLAYLISTS\'.<BR>User: \'' +
                                                                newSpUserName + '\'', 5000); */
                            //controls.navigation.jumpToListsSpotify();
                        }
                        else{
                            controls.newNotification.initNotif('Oops! Invalid Spotify Username.<BR> Please correct the spelling and try again!', 7000);
                        }
                    });
                }
                else{
                    self.spinAddGuestUser(false);                  
                    controls.newNotification.initNotif('Oops! Invalid Spotify Username.<BR> Please correct the spelling and try again!', 7000);
                }
            });
        },
        isGuestUserAdded : function(aGuestUser){
            for(var i=0; i < controls.spotify.spAllGuestUsers().length ; i++){
                if(aGuestUser === controls.spotify.spAllGuestUsers()[i].id){
                    return true;
                }
            }
            return false;
        },
        execSpListChange : function (param){            
            var self = controls.spotify;
            if(param.trim() !== ''){
                self.spinAddGuestUser(true);

                for(var i = 0; i < NL.spotify.spotifyPlaylists().length ; i++){
                    if(NL.spotify.spotifyPlaylists()[i].guestUser !== NL.spotify.userName() ){
                        controls.lists.removeSpotifyGuestList(NL.spotify.spotifyPlaylists()[i] );
                    }
                }
                controls.spotify.spSelectedGuestUser(param);
                self.addSpGuestPlaylists();
            }
        },
        excKeyPressed : function(data,event){
            var self = controls.spotify;
            if(event.keyCode === 13){
                self.spinAddGuestUser(true);

                for(var i = 0; i < NL.spotify.spotifyPlaylists().length ; i++){
                    if(NL.spotify.spotifyPlaylists()[i].guestUser !== NL.spotify.userName() ){
                        controls.lists.removeSpotifyGuestList(NL.spotify.spotifyPlaylists()[i] );
                    }
                }

                self.addSpGuestPlaylists();
                controls.spotify.newSpGuestUsername('');
            }
            return true;
        },
        undoAddSpUser: function(){
            if(controls.spotify.spAllGuestUsers().length > 0){
                controls.spotify.spSelectedGuestUser(controls.spotify.spAllGuestUsers()[0].id);
            }
            else{
                controls.spotify.spSelectedGuestUser('');
            }

            for (i=0; i< controls.spotify.spAllGuestUsers().length ; i++){
                if(controls.spotify.spAllGuestUsers()[i].name === 'My Nightlife Tracks'){
                  controls.spotify.spSelectedGuestUser(controls.spotify.spAllGuestUsers()[i].id);
                }
            }
        }
    }
});

$doc.on('nightlife-ready', function() {
    // This part must be uncommneted later!
    var spotifyCode = controls.spotify.fetchCodeOfString(window.location.href , 'code');
    if( (spotifyCode !== undefined ) && (spotifyCode !== '') ){
        // Spotify Code is available so Refresh and access tokens will be requested from Spotify.
        spotifyCode = spotifyCode.trim();
        controls.spotify.validateCode(spotifyCode);
    }
    else{
        // There is no code so the token will be refreshed if possible.
        controls.spotify.refreshToken();
    }

    // Ebrahim M: I 've added these two lines here for correcting redirect url may come back from Spotify
    setTimeout(function () {
        href = window.location.href.split ('?');
        window.location.href = href[0];
    }, 5000);
});

$doc.on('nightlife-pre-ready', function() {
   /*var spotifyToken = controls.spotify.queryString(window.location.hash , '#access_token');
   var spotifyState = controls.spotify.queryString(window.location.hash , 'state');
   controls.spotify.validateToken(spotifyToken , spotifyState);
   */
});
