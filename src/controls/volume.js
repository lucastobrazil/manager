$.extend(true, controls, {
  volume: {
    model: undefined, // This will container a reference to the VolumeViewModel

    settings: {
      animationTime: 500,
      autoCloseTime: 800000,
      control: '#slider-volume',     
      isInit : false
    },
    init: function() {
      var self = this;                      
      self.model = NL.volume; // Grab a reference to the volumeViewModel object
      self.control = $(self.settings.control);

      // Setup timer to auto-close
      self.startTimer();

      // On touch/click - restart the timer 
      self.control.on('mousedown mouseup touchstart touchend click', function(e) {
        self.startTimer();
      });

      // On drag, check if it's the right object then restart the timer
      self.control.on('mousemove touchmove', function(e) {
        if (e.which > 0) {
          self.startTimer();
        }
      });
      self.settings.isInit = true;
    },
    // change level update from HDMS 
    changeLevel: function(newValue) {
      var self = this;

      self.model.level(newValue);
    },
    // set level push to HDMS
    setLevel: function(newValue) {
      if(newValue <= 100 && newValue >= 0) {
        var self = this;
//        self.model.level(newValue);
        controllers.request_zone.set_volume(self.model);
      }
    },
    reset: function() {
      var self = controls.volume;

      self.control.data('state', 'closed');

      controls.modal.hide();
      $doc.off('mousedown.volume touchstart.volume');
    },
    hide: function() {
      var self = controls.volume;

      if (self) {
        self.control.stop(true, true); // Get the control but ensure that we finish any animations
        self.reset();
      }
    },
    show: function() {
      var self = controls.volume;
      controls.modal.show('zone-controls', { level: 0, initLevel: 0}, 'volume');
      if(! self.settings.isInit) {
        self.init();
      }
      self.drawSlider();
      
        
    },
    state: function() {
      /*var result = 'closed';
      var self = controls.volume;

      if (self) {
        result = self.control.data('state');
      }*/

      return ;
    },
    toggle: function() {
      var self = controls.volume;

      if (self) {
        if (self.state() === 'open') {
          self.hide();
        } else {
          self.show();
        }
      }

      /////////////////////////////////////////////////////////////////////////////////////////////////////
      // Ebrahim M : Added to adjust the height of the Volume popup window to follow the Mode Controls window.
      var modeHeight = ($('#modes').height() - 65);
      modeHeight += 'px';     
      $('#slider-volume')[0].style.height = modeHeight;
      /////////////////////////////////////////////////////////////////////////////////////////////////////
    },
    startTimer: function() {
      this.stopTimer();
      this.autoCloseTimer = setTimeout(function() {
        controls.volume.hide();
      }, this.settings.autoCloseTime);
    },
    stopTimer: function() {
      clearTimeout(this.autoCloseTimer);
    },
    drawSlider: function() {
      var disabled = NL.settings.utopia.canIUse('request_zone','set_volume',true)();
        $( '#slider-volume' ).slider({
            orientation: 'vertical',
            disabled: disabled,
            range: 'min',
            min: 0,
            max: 100,
            value: NL.volume.level(),
            slide: function( event, ui ) {
              //$( '#amount' ).val( ui.value );
              NL.volume.sliderLevel(ui.value);
            },
            stop: function(event, ui) {
              NL.volume.level(ui.value);
              controls.volume.setLevel(ui.value);
            }
          }); 
    }
  }
});

// Wait for Ready state before hooking everything up
$doc.one('nightlife-ready', function() {
  controls.volume.init();

  NL.volume.level.subscribe(function(newValue) {
    //controls.volume.dial.val(newValue).trigger('change');
    $('#slider-volume').slider('value', newValue);
  });
});