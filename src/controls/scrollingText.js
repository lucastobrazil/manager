// Mon 28 Sep 15 10:32 AM Ebrahim Mardani: Created
$.extend(true, controls, {
    scrollingText: {        
        toggleScrollingText: function(){
            NL.advertising.playersVisible(false);
            controls.advertising.deselectAllAssets();
            if( !NL.textScrolling.scrollingTextVisible() ){
                NL.advertising.activeTab('user');
            }
            NL.textScrolling.scrollingTextVisible(!NL.textScrolling.scrollingTextVisible());
        },
        showCreateMsgText: function(){
            NL.textScrolling.newMsgText.priority(NL.advertising.activeTab() === 'Priority' ? true : false);
            if(NL.textScrolling.user.texts() !== undefined && 
                                NL.textScrolling.user.texts().length > 0){
                var idx = (NL.textScrolling.user.texts().length - 1);
                var nextSlot = NL.textScrolling.user.texts()[idx].slot() + 1;
                NL.textScrolling.newMsgText.slot(nextSlot);
            }
            controls.modal.show('advertising-msg-modal', NL.textScrolling.newMsgText,'');
        },
        getObjScrollText:function(param){
            var startDate = param.start_date();
            var endDate = param.end_date();
            if(param.selDate() === 'allYear'){
                startDate = '01/01';
                endDate = '31/12';
            }
            else{
                startDate = ('0' + startDate.getDate()).slice(-2)+'/'+('0' + (startDate.getMonth()+1)).slice(-2);
                endDate   = ('0' + endDate.getDate()).slice(-2)+'/'+('0' + (endDate.getMonth()+1)).slice(-2);
            }

            var obj = {
                start_date: startDate,
                end_date: endDate,
                start_day: (param.start_day() === 'All Week') ? 'All' : param.start_day(),
                end_day: (param.end_day() === '') ? 'All' : param.end_day(),
                start_time: param.start_time() === 'All Day' ? '00:00' : param.start_time(),
                end_time: param.start_time() === 'All Day' ? '23:59' : param.end_time(),
                frequency: param.frequency(),
                slot: parseInt(param.slot(), 10),
                speed: param.speed(),
                text: param.text()
            };
            return obj;
        },
        isValidNewTextMsg: function(aNewMsg){
            var i=0;
            if(aNewMsg.text().trim() === ''){
                controls.newNotification.initNotif('Oops, \'Text Message\' field is empty!', 4500);
                return false;
            }
            if(NL.advertising.activeTab() === 'priority') {
                for(i=0; i < NL.textScrolling.priority.texts().length; i++) {
                    if(parseInt(NL.textScrolling.priority.texts()[i].slot(), 10) === parseInt(aNewMsg.slot() , 10) ) {
                        controls.newNotification.initNotif('Oops, This slot has been already used.', 4500);
                        return false;
                    }
                }
            }
            else {
                for(i=0; i < NL.textScrolling.user.texts().length; i++) {
                    if( parseInt(NL.textScrolling.user.texts()[i].slot() , 10) ===  parseInt(aNewMsg.slot() , 10) ) {
                        controls.newNotification.initNotif('Oops, This slot has been already used.', 4500);
                        return false;
                    }
                }
            }
            return true;
        },
        addTextMsg: function(){
            var self = controls.scrollingText;
            if ( self.isValidNewTextMsg(NL.textScrolling.newMsgText) ) {
                NL.textScrolling.uploadInProgress(true);
                var arPriorityText = [];
                var arUserText = [];
                for(var i=0; i < NL.textScrolling.priority.texts().length; i++){
                    var obj1 = self.getObjScrollText(NL.textScrolling.priority.texts()[i]);
                    arPriorityText.push(obj1);
                }
                for(i=0; i < NL.textScrolling.user.texts().length; i++){
                    var obj2 = self.getObjScrollText(NL.textScrolling.user.texts()[i]);
                    arUserText.push(obj2);
                }

                var newObj = self.getObjScrollText(NL.textScrolling.newMsgText);
                
                if(NL.advertising.activeTab() === 'priority'){
                   arPriorityText.push(newObj);
                }
                else{
                    arUserText.push(newObj);
                }
                var obj = {
                    priority:arPriorityText,
                    user: arUserText
                };
                // console.log('Written object >>> ', obj);
                if( NL.settings.utopia.canIUse('request_system','set_scrolling_text')() ){
                    controllers.request_system.set_scrolling_text(obj); 
                }
                else{
                    NL.textScrolling.uploadInProgress(false);
                    controls.newNotification.initNotif('Error: Access denied', 3500); 
                }
                controls.modal.hide();
            }
        },
        showMsgSidebar: function(model , aParent){
            aParent.currentModel(model); 
            var newModel = new TextModel(ko.mapping.toJS(model));
            aParent.curMsgEditModel(newModel);
            aParent.sidebarMsgVisible(true); 
        },
        selectTextMsg: function(model, aParent){
            // console.log('>>>><<<<<>>>>>> model = ' , model.selected());
            // console.log('>>>><<<<<>>>>>> aParent = ' , aParent);
            var isCurrentlySelected = model.selected();
            var numTextCurrentlySelected = aParent.selectedTextMsg().length;
            
            if(numTextCurrentlySelected > 1){
                aParent.lastTextMsgSelected(aParent.selectedTextMsg()[aParent.selectedTextMsg().length - 1].viewIndex);
            }
           
            // Single Click
            if(isCurrentlySelected && numTextCurrentlySelected > 1){
                // if the file clicked is currently selected
                // as part of a group selection, we want to
                // deselect all Text Messages, then reselect this one
                controls.scrollingText.deselectAllTextMsgs(); 
                aParent.lastTextMsgSelected(model);
                model.selected(true);
            }else if(isCurrentlySelected){
                // if the file clicked is already selected, deselect it
                aParent.sidebarMsgVisible(false);
                model.selected(false);  
                return true;                        
            }else{                            
                // if the file clicked is NOT currently selected,
                // in the current selection group,
                // deselect all then select this item. 
                controls.scrollingText.deselectAllTextMsgs(); 
                aParent.lastTextMsgSelected(model);
                model.selected(true);                
            }
            // show the sidebar for the single selected asset
            controls.scrollingText.showMsgSidebar(model , aParent);
        },
        deselectAllTextMsgs: function(){
            if(NL.advertising.activeTab() === 'user'){
                for(i=0; i<NL.textScrolling.user.texts().length; i++){
                     NL.textScrolling.user.texts()[i].selected(false);
                }
            }else{
                for(i=0; i<NL.textScrolling.priority.texts().length; i++){
                    NL.textScrolling.priority.texts()[i].selected(false);
                }
            }
            // Added this to ensure that sidebar is turned off if no Text Msg selected
            NL.textScrolling[NL.advertising.activeTab()].sidebarMsgVisible(false);
        },
        cancelEditing : function(){
            controls.scrollingText.deselectAllTextMsgs();
            if(NL.advertising.activeTab() === 'user'){
                NL.textScrolling.user.sidebarMsgVisible(false);
            }
            else{
                NL.textScrolling.priority.sidebarMsgVisible(false);
            }
        },
        editDelHelper : function(model, aNewObj){
            var self = controls.scrollingText;
            var arPriorityText = [];
            var arUserText = [];
            for(var i=0; i < NL.textScrolling.priority.texts().length; i++){
                if(NL.advertising.activeTab() === 'priority'){
                    if(NL.textScrolling.priority.texts()[i].slot() === model.slot() ){
                        if(aNewObj !== null){
                            arPriorityText.push(aNewObj);
                        }
                        continue;
                    }
                }
                var obj1 = self.getObjScrollText(NL.textScrolling.priority.texts()[i]);
                arPriorityText.push(obj1);
            }
            for(i=0; i < NL.textScrolling.user.texts().length; i++){
                if(NL.advertising.activeTab() === 'user'){
                    if(NL.textScrolling.user.texts()[i].slot() === model.slot() ){
                        if(aNewObj !== null){
                            arUserText.push(aNewObj);
                        }
                        continue;
                    }
                }
                var obj2 = self.getObjScrollText(NL.textScrolling.user.texts()[i]);
                arUserText.push(obj2);
            }
            var obj = {
                priority:arPriorityText,
                user: arUserText
            };
            // console.log('Written object >>> ', obj);
            controllers.request_system.set_scrolling_text(obj);
        },
        deleteTextMsg : function(model){
            var self = controls.scrollingText;
            NL.textScrolling.deleteInProgress(true);
            self.editDelHelper(model, null);
        },      
        editAsset : function(model, event) {
            var self = controls.scrollingText;
            if(model.text().trim() === ''){
                controls.newNotification.initNotif('Oops, \'Text Message\' field is empty!', 4500);
                return false;
            }
            NL.textScrolling.uploadInProgress(true);
            var newObj = self.getObjScrollText(model);
            self.editDelHelper(model, newObj);
        },
        changeSelection : function(direction) { // Added to activate up and down keys 
            self = controls.scrollingText;
            if(direction === 'up') {                
                self.prevAsset();
            }
            if(direction === 'down') {
                self.nextAsset();
            }
        },
        nextAsset : function(model, event){
            var lastTextMsg = NL.textScrolling[NL.advertising.activeTab()].lastTextMsgSelected();
            var lastIndex = lastTextMsg.viewIndex;
            var nextIndex = lastIndex + 1; //advance the selected index
            var next = NL.textScrolling[NL.advertising.activeTab()].texts()[nextIndex];

            // if user pressed down arrow without previously selecting
            // an asset, default it to the [0] asset;
            if(!lastTextMsg){ 
                lastTextMsg = NL.textScrolling[NL.advertising.activeTab()].texts()[0];
            }
            
            // if there is no next asset, it means we're at the bottom
            // of the list, so just select the last item. 
            if(!next){
                next = lastTextMsg;
            }
            controls.scrollingText.deselectAllTextMsgs(); // HOLD
            next.selected(true);
            NL.textScrolling[NL.advertising.activeTab()].lastTextMsgSelected(next);
            controls.scrollingText.showMsgSidebar( next, NL.textScrolling[NL.advertising.activeTab()] );
        },
        prevAsset : function(model, event) {             
            var lastTextMsg = NL.textScrolling[NL.advertising.activeTab()].lastTextMsgSelected();
            var lastIndex = lastTextMsg.viewIndex;
            var prevIndex = lastIndex - 1;
            var prev = NL.textScrolling[NL.advertising.activeTab()].texts()[prevIndex] ;
            
            // if user pressed down arrow without previously selecting
            // an asset, default it to the [0] asset;
            if(!lastTextMsg){
                lastTextMsg = NL.textScrolling[NL.advertising.activeTab()].texts()[0];
            }
            
            // if there is no next asset, it means we're at the top
            // of the list, so just select the last item. 
            if(!prev){
                prev = lastTextMsg;
            }
            controls.scrollingText.deselectAllTextMsgs();
            prev.selected(true);
            NL.textScrolling[NL.advertising.activeTab()].lastTextMsgSelected(prev);
            controls.scrollingText.showMsgSidebar(prev, NL.textScrolling[NL.advertising.activeTab()]);
        }
    }
});