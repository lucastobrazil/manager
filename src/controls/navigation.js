$.extend(true, controls, {
  navigation: {
    linkSelector: 'footer .nav-tabs a',
    primaryLinks: [],
    queryString: [],
    menuShown: ko.observable(true),
    isPinned: ko.observable(true),
    lastNavigatedTab: '',
    isInstagramFeedModalOpen: ko.observable(false),
    isBingoModalOpen: ko.observable(false),
    isRngModalOpen: ko.observable(false),
    isPlaylistRecieved : false,
    init: function() {
      var self = this;
      self.primaryLinks = $(self.linkSelector);
      // Get the default tab and show it
      var startTab = self.primaryLinks.filter('.default');
      console.log('ST', startTab);
      if(startTab){
        var title = startTab.prop('title');
//        doc.title = 'HDMS | ' + title;
        doc.title = 'Manage My Nightlife';
        if(startTab.attr('href')){
          $('body').removeClass().addClass(startTab.attr('href').substr(1));
        }
        startTab.tab('show');
      }

      $win.on('hashchange', function() {
        self.navigateFromHash();
      });

      $win.on('orientationchange resize', function() {
          if(NL.context.tab() === '#tab-playlist') {
            controls.playlist.setBottomSpaceHeight();
          }
      });

      self.primaryLinks.on('shown.bs.tab', function(e) {
        var title = $(this).attr('title');
        win.location.hash = e.target.hash;
        //doc.title = 'HDMS | ' + title;
        doc.title = 'Manage My Nightlife';

        $('body').removeClass().addClass(e.target.hash.substr(1));
      });

      $('#container-advertising-instagram-modal').on('shown.bs.modal', function () {
          // will only come inside after the modal is shown
          self.isInstagramFeedModalOpen(true);
      });

      $('#container-advertising-instagram-modal').on('hidden.bs.modal', function () {
          // will only come inside after the modal is hidden
          // alert('hidden container-advertising-instagram-modal');
          self.isInstagramFeedModalOpen(false);
      });

      $('#container-dlg-bingo').on('shown.bs.modal', function () {
          // will only come inside after the modal is shown
          self.isBingoModalOpen(true);
      });

      $('#container-dlg-bingo').on('hidden.bs.modal', function () {
          // will only come inside after the modal is hidden
          self.isBingoModalOpen(false);
      });

      $('#container-dlg-rng').on('shown.bs.modal', function () {
          // will only come inside after the modal is shown
          self.isRngModalOpen(true);
      });

      $('#container-dlg-rng').on('hidden.bs.modal', function () {
          // will only come inside after the modal is hidden
          self.isRngModalOpen(false);
      });

      /*
      $(document).keydown(function(evt){
          if (evt.keyCode==108 && (evt.ctrlKey) && (evt.shiftKey)){
              evt.preventDefault();
              $('#yourModal').modal('show');
          }
          console.log('keydown-> ' , evt);
      });
      */

      self.retrieveQueryString();
      self.disableNav();
    },
    navigateFromHash: function() {
      var hash = doc.location.hash;
      if(!controls.navigation.isPinned()){
        controls.navigation.navigateDrawerMenu();
      }
      if(hash.indexOf('access_token') < 0) {
        NL.context.tab(hash);
        if (hash) {
          var levels = hash.split('/');

          $('.nav-tabs a[href=' + levels[0] + ']').tab('show');
          if (levels.length === 2) {
            var content = $(levels[0]);
            $('a[href="' + hash + '"]', content).tab('show');
          }
        } else {
          $('.nav-tabs a[href^=#]:first()').tab('show');
        }
      }
    },
    navigateToHash: function(target) {
      var hash = target.indexOf('#') === 0 ? target : '#' + target;
      doc.location.hash = hash;
      //console.log('>>>>>>>>>>>> ' , doc.location.hash);

    },
    navigateDrawerMenu: function() {
      //if( (NL.context.tab() !== '#tab-hashtag') && (NL.context.tab() !== '#tab-uname') ){
      var self = this;
      if(!self.isInstagramFeedModalOpen()){
        controls.navigation.menuShown(!controls.navigation.menuShown());
      }
    },
    pinMenuToggle: function() {
      controls.navigation.isPinned(!controls.navigation.isPinned());
      if(!controls.navigation.isPinned()){
        controls.navigation.menuShown(false);
      }
    },
    retrieveQueryString: function() {
      var self = this;
      var search = doc.location.search;

      if (search && search !== '') {
        var parameters = search.substr(1).split('&');
        parameters.forEach(function(obj) {
          var hash = obj.split('=');
          var value = decodeURIComponent(hash[1]).replace('+', ' ');
          self.queryString.push(value);
          self.queryString[hash[0]] = value;
        });
      }
    },
    /*
         *  Disable the primary navigation (except for the current active pane)
         *  Note: This does not disable url hash based navigation
         */

    disableNav: function($nav) {
      if($nav === undefined) {
        this.primaryLinks.each(function(i, obj) {
          $obj = $(obj);
          var href = $obj.attr('href');
          var li = $obj.parent('li');

          if (!li.hasClass('active')) {

            if (href) {
              $obj.removeAttr('data-toggle href');
              $obj.data('href', href);
              li.addClass('disabled');
            }
          }
        });
      } else {
        $nav.removeAttr('data-toggle href');
        $nav.data('href', $nav.attr('href'));
        $nav.parent('li').addClass('disabled');
      }
    },
    // Re-enable the primary navigation
    enableNav: function($nav) {
      if($nav === undefined) {
        this.primaryLinks.each(function(i, obj) {
          $obj = $(obj);
          var href = $obj.data('href');

          if (href) {
            $obj.removeAttr('data-href');
            $obj.attr({
              'href': href,
              'data-toggle': 'tab'
            });
            $obj.parent('li').removeClass('disabled');
          }
        });
      } else {
        $nav.removeAttr('data-href');
        $nav.attr({
          'href': $obj.data('href'),
          'data-toggle': 'tab'
        });
        $nav.parent('li').removeClass('disabled');
      }
    },
    businessAsUsual: function(obj, event) {
      // Don't perform BAU if the user clicked on one of the player controls
      if(event && event.target){
        if($.contains($('#player .controls')[0], event.target)){
          return;
        }else if($.contains($('.nav-branding')[0], event.target)){
          // If user selected the top 'nav branding' logo area, just jump to playlist
          NL.context.selectedSong(NL.playlist.current());
          controls.navigation.navigateToHash('#tab-playlist');
          return;
        }
      }

      // Ebrahim M : added to cover Mark's suggestion to make it similar to MMNL mobile app!
      if( (NL.context.tab() !== '#tab-playlist') || !controls.playlist.isCurrntSongOnTop() ) {
        // Set Current as selected song
        NL.context.selectedSong(NL.playlist.current());
        controls.navigation.navigateToHash('#tab-playlist');
        controls.playlist.scrollToCurrent();
        controls.settings.bWaitInProgress(false);
        controls.notification.hide();
      }
      else { // Ebrahim M : Show / Hide song detail popup window.
        //  Note: We can also get open popup window by calling 'NL.context.modal()'
        NL.context.selectedSong(NL.playlist.current());
        controls.navigation.navigateToHash('#tab-playlist');
        controls.playlist.scrollToCurrent();

        if(!controls.settings.bDeliberateDelay()){
          $('.song-info.current[data-distance=1]').click();
        }
      }
    },
    jumpToAdvertising : function(obj, event) {
      controls.navigation.navigateToHash('#tab-advertising');
      setTimeout(function () {
        controls.instagram.showInstagramModal();
        controls.settings.hideLM();
      }, 2000);
    },
    jumpToBingo : function() {
      controls.navigation.navigateToHash('#tab-visuals');
      setTimeout(function () {
        controls.settings.hideLM();
        if( controls.video.newRngLook() ){
          controls.video.showBingoModal();
        }
      }, 2000);
    },
    jumpToRng: function() {
      controls.navigation.navigateToHash('#tab-visuals');
      setTimeout(function () {
        controls.settings.hideLM();
        if( controls.video.newRngLook() ){
          controls.video.showRngModal();
        }
      }, 2000);
    },
    jumpToVisuals : function(obj, event) {
      controls.navigation.navigateToHash('#tab-visuals');
    },
    jumpToListsSpotify : function (obj, event){
      controls.navigation.navigateToHash('#tab-lists');
      $('a[href=#lists-guests-panel]').tab('show');
      controls.modal.hide();
      // HOLD HERE
      // controls.spotify.spSelectedGuestUser.subscribe(controls.spotify.addSpGuestPlaylists);
    },
    jumpToSearchSpotify : function(obj ,event){
      controls.navigation.navigateToHash('#tab-search');
      $('a[href=#search-spotify]').tab('show'); // Ebrahim M : This will run the query.
      controls.modal.hide();
    }
  }
});
