$.extend(true, controls, {
    instagram: {
        showInstagramModal: function() {

            // Ebrahim M : Initialize the variables
            NL.instagram.uname('');
            NL.instagram.tag('');
            NL.instagram.displayTime(20);
            NL.instagram.limitNumber(10);
            NL.instagram.advancedSettingsShown(false);   
            $(location).attr('href' , '#tab-uname');

            controls.modal.show('advertising-instagram-modal',NL.instagram,'');
            /*function handleAuthResult(result){ 
                alert('res : ' + JSON.stringify(result));
            }
            try {
                window.opener.HandlePopupResult(sender.getAttribute('result'));
            }*/
            //controls.instagram.fetchByTag();
           /*if(! controls.advertising.isInit) {
                controls.advertising.init();
            }*/
        },
        validateToken: function() {            
            if(NL.instagram.access_token() !== '') {                
                $.getJSON(
                    'https://api.instagram.com/v1/users/self?callback=?&access_token='+NL.instagram.access_token(),
                    function(data) {
                        console.log(data);
                        if(data.meta.code === 400) {
                            NL.instagram.access_token(''); // token expired or invalid
                            NL.instagram.username('');
                        } else {
                            NL.instagram.username(data.data.username);
                        }
                    }
                );
            }            
        },
        removeToken: function() {
            NL.instagram.access_token('');
            NL.instagram.username('');
        }
    }

});


$doc.on('nightlife-ready', function() {    
    if(window.location.hash.indexOf('#access_token') >= 0) {
        NL.instagram.access_token(window.location.hash.substr(window.location.hash.indexOf('=')+1));
        window.location.href = '#tab-advertising';        
        // controls.instagram.showInstagramModal();
    }
    if(NL.instagram.access_token() !== '') {
        controls.instagram.validateToken();
    }
});

