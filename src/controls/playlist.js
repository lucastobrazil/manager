$.extend(true, controls, {
  playlist: {
    songH: 45,
    songs: undefined, // Reference to the songlist
    wrapper: undefined, // Element which will have the scrollbars
    list: undefined,
    showBAU: true,
    init: function() {
      var self = this; 
      this.wrapper = $('#playlist-wrapper');
      this.list = $('.songlist', this.wrapper);
      this.songs = NL.playlist.songs;
      // Just enough time to populate the dom
      setTimeout(function() {
        controls.playlist.setBottomSpaceHeight();
        self.scrollToCurrent();
        self.wrapper.animate({
          opacity: 1
        });
      }, 1000);
    },

    onshown: function() {      
      var self = controls.playlist;

      //if(self.showBAU){           // Ebrahim M : removed 'if' : always go to the current song
      self.scrollToCurrent();
      // }

 //   $('.tooltip-helper').tooltip();
    },    

    scrollToCurrent: function() {
      var self = controls.playlist;
      if (self.list) {
        controls.songlist.removeHighlights(self.list);
      }
      // Check if we can scroll now, or if we need to do it when the tab is next active
      if(self.wrapper){
        var pane = self.wrapper.closest('.tab-pane');
        if (self.wrapper.is(':visible')) {
          // reset the pane to make calculations more accurate
          pane.scrollTop(0);
          pane.scrollTop(self.zeroTop(1));
          self.selectCurrent();
          self.showBAU = false;
        }else{
          self.showBAU = true;
        }
      }
      return true;
    },
    scrollToDistance: function(distance, direction) { //distance is the item we're scrolling to
      var self = controls.playlist;
      if(self.wrapper){
        var pane = self.wrapper.closest('.tab-pane');
        if (self.wrapper.is(':visible')) {            
            if(! $('.song-info[data-distance='+distance+']').inViewPlaylist()) { // if not in view, scroll to it
                if(direction === 'up') {
                    pane.scrollTop(self.zeroTop(distance));
                } else {
                    pane.scrollTop(self.zeroBottom(distance));
                }
            }
          self.showBAU = false;
        }else{
          self.showBAU = true;
        }
      }
      return true;
    },
    zeroTop: function(dist) {
      var result = 0;
      if (this.wrapper) {
        var pos = $('.song-info[data-distance='+dist+']', this.wrapper).position();
        if(pos){
          result = pos.top;
        }
      }
      return result;
    },
    zeroBottom: function(dist) {
      var result = 0;
      if (this.wrapper) {
        var pos = $('.song-info[data-distance='+dist+']', this.wrapper).position();
        if(pos){
          result = pos.top  - $('#body-wrapper').height() + $('footer').height() - $('.song-info').height();
        }
      }
      return result;
    },
    refresh: function() {
      var playlist = NL.playlist;
      if (playlist) {
        playlist.history.valueHasMutated();
        playlist.current.valueHasMutated();
        playlist.queue.valueHasMutated();
      }
    },
    selectDistance : function(distance, direction) {
        var self = controls.playlist;
        $('.active','#playlist-wrapper').removeClass('active');
       $('.song-info[data-distance='+(distance)+']', this.wrapper).addClass('active');
       self.scrollToDistance(distance,direction);
    },
    changeSelection : function(direction) {
      var self = controls.playlist;
        var currentDistance = $('.active','#playlist-wrapper').attr('data-distance');
        if(currentDistance === undefined) {
            if(direction === 'up') { // if nothing selected, select top or bottom
                currentDistance = self.getVisible().first().attr('data-distance');
            } else {
                currentDistance = self.getVisible().last().attr('data-distance');
            }
            self.selectDistance(currentDistance,direction);
            return;
        }
        if(direction === 'down') {
            currentDistance++;
            currentDistance = currentDistance === 0 ? 1 : currentDistance;
            if(currentDistance > NL.playlist.queue().length) { return; } //stop if at bottom
            self.selectDistance(currentDistance,direction);
        } else {
            currentDistance--;
            currentDistance = currentDistance === 0 ? -1 : currentDistance;
            if(currentDistance < - NL.playlist.history().length) { return; } //stop if at t 
            self.selectDistance(currentDistance,direction);
        }
    },
    selectCurrent : function() {
        var self = controls.playlist;
        var pane = self.wrapper.closest('.tab-pane');
        $('.active','#playlist-wrapper').removeClass('active');
        $('.song-info[data-distance='+(1)+']', this.wrapper); //not the bug
        $('#tab-playlist').scrollTop(controls.playlist.zeroTop(1));
    },
    openSelection : function() {
        $('.active','#playlist-wrapper').click();
    },
    openDistance : function(distance) {
        var self = controls.playlist;
        self.selectDistance(distance);
        $('.active','#playlist-wrapper').click();
    },
    openFirst : function() {
      var t = $('#playlist-wrapper').find('.song-info').first();
      var d = t.attr('data-distance');
      $('#tab-playlist').scrollTop(this.zeroTop(d));
      t.click();
    },
    openLast : function() {
      var t = $('#playlist-wrapper').find('.song-info').last();
      var d = t.attr('data-distance');
      $('#tab-playlist').scrollTop(this.zeroTop(d));
      t.click();
    },
    getVisible : function() {
        return $('.song-info').filter(function() {return $(this).inViewPlaylist();});
    },
    setBottomSpaceHeight : function() {
        var self = controls.playlist;
        var ret = (self.getNumRowsView() - NL.playlist.queue().length - 1) * $('.song-info[data-distance=1]').outerHeight();
        $('#playlist-spacer-bottom').height(Math.round(ret) + 'px'); // add spacer if required
    },
    updateModal : function() {
      if(NL.context.modal() === 'song' && NL.context.tab() === '#tab-playlist') { // if playlist modal open
        var fn = NL.context.selectedSong().filename;
        var d = NL.context.selectedSong().distance;
        var targets = $('#playlist-wrapper').find('.song-info[data-filename='+fn+']');

        var ds = []; // store distance deltas
        for(var i=0;i<targets.length;i++) {
          ds.push(Math.abs(d - $(targets[i]).attr('data-distance')));          
        }

        var dNew = Math.min.apply( Math, ds ); // new distance we want 
        if($('.song-info[data-distance='+(dNew+d-0)+']').attr('data-filename') === fn) {
          dNew = dNew+d-0;                  
        } else if($('.song-info[data-distance='+(d-dNew)+']').attr('data-filename') === fn) {
          //controls.modal.hide();
          dNew = d-dNew-0;                    
        } else {          
          return;
        }

        controls.modal.hide();        
        if($('.song-info[data-distance='+(dNew)+']') === undefined) {
          return;
        }
        setTimeout(function(){$('#playlist-wrapper .song-info[data-distance='+(dNew)+']').click();});          
        
      }
    },
    getNumRowsView : function() {
        var viewTop   = $(window).scrollTop() + $('header').height();
        var viewBottom   = viewTop + $('#body-wrapper').height(); // - 36
        var delta = viewBottom - viewTop;
        var nRows = delta / $('.song-info[data-distance=1]').outerHeight();
        return nRows;
    },
    enableSorting : function() {
        setTimeout(function() {                   
          $('#playlist-wrapper').children().sortable({ 
              axis: 'y', 
              items:'.song-info.future',             
              handle: '.distance',
              zIndex: 9999 ,
              update: function( event, ui ) {
                moved = ui.item;
                moved.removeClass('history').addClass('future');
                controls.playlist.setPlaylist(moved.attr('data-distance'));
              }
            }); //re-enable dragNdrop           
          $('#playlist-wrapper').find('.distance').disableSelection();
        }, 0);

    },
    setPlaylist : function(newPosition) {

      var songs = $('#playlist-wrapper').find('.song-info.future').map(function(index, e){
        return {
          filename: $(e).attr('data-filename'),
          added_by: NL.settings.utopia.username(),
          load_method: newPosition === $(e).attr('data-distance') ? 'UM' 
            : NL.playlist.queue()[$(e).attr('data-distance')-2].load_method //-2 because 1 indexing distance
        };
      }).get();
      controllers.request_zone.set_playlist(songs);
    },
    isCurrntSongOnTop : function(){
      if($('.song-info[data-distance=1]').offset() !== undefined){
        var pos = $('.song-info[data-distance=1]').offset().top - ($('.navbar-header').outerHeight() + 
                    $('#player').outerHeight() + $('#zone-status').outerHeight());
        if(pos >= 0 && pos < 1){
          return true;        
        }
      }
      
      return false;
    }
  }
});