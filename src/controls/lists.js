$.extend(true, controls, {
  lists: {
    show: function() {
      $('#lists-loaded').show();
    },
    setTab: function(tab) {
		  $('#list-tabs a[href='+tab+'-panel]').tab('show');
    },
    toSearch: function(list) {
		  controls.navigation.navigateToHash('#tab-search');
		  controls.search.fromList(list);
    },
    loadLists: function() {

      var selected = NL.musicList.selectedLists();
      var list_names = [];
      if(selected.length === 0) {
        controls.notification.show('No Lists Selected');
        return;
      }
      
      selected.forEach(function(list, index) {
        list_names.push(list.nameOriginal); // list.name => list.nameOriginal : because of only numbers lists.
      });
      controllers.request_zone.set_music_list(list_names, 
          function() {
//            controls.navigation.businessAsUsual();
            controls.notification.show('Music Lists Updated');
          });
      
    },
    clearLists: function() {
      controls.navigation.businessAsUsual();
      controllers.request_zone.clear_music_list(controls.notification.show('Music Lists Updated'));
    },
    importSpPlaylist: function(spPlaylistId, spPlaylistName , spUserName){
      if((spPlaylistId === '') || (spPlaylistId === undefined) ){
        controls.newNotification.initNotif('Improper Spotify playlist name.', 5000);
        return;
      }

      if(spUserName === undefined){
        spUserName = NL.spotify.userName();
        var i=0;
        for(i=0; i < NL.spotify.spotifyPlaylists().length ; i++){
          if(NL.spotify.spotifyPlaylists()[i].guestUser === spUserName){
            break;
          }
        }
        if( i < NL.spotify.spotifyPlaylists().length){
          for(var j = 0; j < NL.spotify.spotifyPlaylists()[i].guestPlaylists.length ; j++){
            if(NL.spotify.spotifyPlaylists()[i].guestPlaylists[j].id === spPlaylistId){
              spPlaylistName = NL.spotify.spotifyPlaylists()[i].guestPlaylists[j].name; 
            }
          }
        }
      }
      /////////////////////////////////////////////////////////////////////////////////////        
      NL.spotify.selectedImpPlaylist({playlistId: spPlaylistId, 
                                      playlistName: spPlaylistName, 
                                      userName: spUserName });
      controls.song.importTrackFromSpotify();
    },
    getMatchedSongsNo : function(aPlaylistId){
      var i = 0;
      for (i=0; i < controls.search.arImportedPlaylists().length ; ++i){
        if(controls.search.arImportedPlaylists()[i].spPlaylistId === aPlaylistId){
          break;
        }
      }
      if(i < controls.search.arImportedPlaylists().length){
        return controls.search.arImportedPlaylists()[i].fileNames.length;
      }
      return '?';
    },
    getMatchedSongsNoOnSystem : function(aPlaylistId){
      var i = 0;
      for (i=0; i < controls.search.arImportedResultlists().length ; ++i){
        if(controls.search.arImportedResultlists()[i].spPlaylistId === aPlaylistId){
          break;
        }
      }
      if(i < controls.search.arImportedResultlists().length){
       // $('#AvlTitle').attr('data-original-title', '@' + controls.search.arImportedPlaylists()[i].AtSystem);
        if(NL.settings.selectedSystem().system === controls.search.arImportedResultlists()[i].AtSystem){
          //Ebrahim M: It means the the connected syatem is the same system that the matched tracks have been grabbed.
          //return controls.search.arImportedResultlists()[i].fileNamesAtSys.length;
          return controls.search.arImportedResultlists()[i].resultCntAtSys;
        }
      }
      return '?';
    },
    getBadgeTooltip : function(aPlaylistId){
      var i = 0;
      for (i=0; i < controls.search.arImportedResultlists().length ; ++i){
        if(controls.search.arImportedResultlists()[i].spPlaylistId === aPlaylistId){
          break;
        }
      }
      if(i < controls.search.arImportedResultlists().length ){        
        if(NL.settings.selectedSystem().system === controls.search.arImportedResultlists()[i].AtSystem){
          //Ebrahim M: It means the the connected syatem is the same system that the matched tracks have been grabbed.
          return (controls.search.arImportedResultlists()[i].fileNamesAtSys.length + 
                      ' songs available @' + 
                       controls.search.arImportedResultlists()[i].AtSystem);
        }
      }
      return 'Oops! Not matched yet.';
    },    
    removeSpotifyGuestList : function(guestItem){    
      NL.spotify.spotifyPlaylists.remove(function(item){ 
        return  item.guestUser === guestItem.guestUser;
      }); 

      NL.spotify.spotifyUsers.remove(function(item){ 
        return  item === guestItem.guestUser;
      }); 
    },
    getDisplayName: function ($data) {
      if( typeof(NL) !== typeof(undefined) ) {
        for(var i= 0 ; i< NL.spotify.spotifyOwners().length ; i++) {
          if ( ($data === NL.spotify.spotifyOwners()[i].ownerId.toUpperCase() ) && 
               ( NL.spotify.spotifyOwners()[i].displayName !== '' ) && 
               (NL.spotify.spotifyOwners()[i].displayName !== null) ) {
            return (' (' + NL.spotify.spotifyOwners()[i].displayName + ')');
          }
        }
      }
      return ('');      
    },
    toggleSpListsCollapse: function(param){
      for(var i = 0; i< NL.spotify.spotifyPlaylists().length ; i++){
        var iconId = '#sp-icon-chevron_' + i;
        var groupId = '#_spotify-guests-lists_' + i;

        if(! $(groupId).hasClass('collapse') ){
          $(iconId).removeClass('icon-chevron-up');
          $(iconId).addClass('icon-chevron-down');
        }
        else{
          $(iconId).removeClass('icon-chevron-down');
          $(iconId).addClass('icon-chevron-up');          
        }

        // Collapse all Spotify lists except the clicked one which is already expanded.        
        if( i !== param){ 
          $(groupId).addClass('collapse');
          $(groupId).removeClass('in');
          $(iconId).removeClass('icon-chevron-up');
          $(iconId).addClass('icon-chevron-down');          
        }

      }
    },
    expandLastSpList : function(){
      for(var i = 0; i < NL.spotify.spotifyPlaylists().length ; i++){
        var groupId = '#_spotify-guests-lists_' + i;          
        var iconId = '#sp-icon-chevron_' + i;

        if( i === (NL.spotify.spotifyPlaylists().length - 1) ){ // Expand the last one
          $(groupId).removeClass('collapse');
          $(groupId).addClass('in');

          $(iconId).removeClass('icon-chevron-down');
          $(iconId).addClass('icon-chevron-up');
        }
        else{
          $(groupId).addClass('collapse');
          $(groupId).removeClass('in');
          $(iconId).removeClass('icon-chevron-up');
          $(iconId).addClass('icon-chevron-down');
        }
      }
    }

  }
});
