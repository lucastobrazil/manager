$.extend(true, controls, {
    drawermenu: {
        selectMenuAdvertising: function(href){
            controls.modal.hide(); // Ebrahim M : Added to hide the modal window where page is changed.
            controls.modal.hide('drawer-menu', {}, '');
            
            // Do not let open 'Advertising' page if user is connected directly.
            if(NL.settings.selectedSystemId() === 'manual'){
                controls.newNotification.initNotif('Oops, Guests Access to Advertising is NOT Permitted.', 5000);
            }
            else{
                controls.navigation.navigateToHash('tab-advertising');
            }
        },
        selectMenuVisuals: function(href){
            controls.modal.hide(); // Ebrahim M : Added to hide the modal window where page is changed.
            controls.modal.hide('drawer-menu', {}, '');
            controls.navigation.navigateToHash('tab-visuals');           
        },
        selectMenuPlaylist: function(href){
            controls.modal.hide(); // Ebrahim M : Added to hide the modal window where page is changed.
            controls.modal.hide('drawer-menu', {}, '');
            controls.navigation.navigateToHash('tab-playlist');           
        },
        selectMenuLists: function(href){
            controls.modal.hide(); // Ebrahim M : Added to hide the modal window where page is changed.
            controls.modal.hide('drawer-menu', {}, '');
            controls.navigation.navigateToHash('tab-lists');           
        },
        selectMenuSearch: function(href){          
            controls.modal.hide(); // Ebrahim M : Added to hide the modal window where page is changed.
            controls.modal.hide('drawer-menu', {}, '');
            controls.navigation.navigateToHash('tab-search');           
        },
        selectMenuSetting: function(href){
            controls.modal.hide(); // Ebrahim M : Added to hide the modal window where page is changed.
            controls.modal.hide('drawer-menu', {}, '');
            controls.navigation.navigateToHash('tab-settings');           
        },
        selectMenuMusicZone: function(href){
           // controls.song.click();
            controls.modal.hide(); // Ebrahim M : Added to hide the modal window where page is changed.
            controls.modal.hide('drawer-menu', {}, '');  
            controls.navigation.navigateToHash('tab-musiczones');           
            controls.settings.refreshSystems();
        },
        selectMenuSignIn: function(href){
            if( NL.advertising[NL.advertising.activeTab()].sidebarVisible() ){
                NL.advertising[NL.advertising.activeTab()].sidebarVisible(false);                
            }
            controls.modal.hide(); // Ebrahim M : Added to hide the modal window where page is changed.
            controls.modal.hide('drawer-menu', {}, '');
            controls.drawermenu.logoutSpotify();
            controls.instagram.removeToken();
            controls.settings.logout();
            controls.navigation.navigateToHash('tab-signin');           
        },
        loginSpotify: function(){
            controls.modal.hide(); // Ebrahim M : Added to hide the modal window where page is changed.
            window.open( NL.spotify.authURL ,'_self');
        },
        logoutSpotify: function(){
            NL.spotify.userName('Not signed in');
            NL.spotify.access_token('');
            NL.spotify.refresh_token('');
        }
    }
});
