$.extend(true, controls, {
    advertising: {        
        isInit : false,
        pollInterval : 1000,
        progressPercent: ko.observable(0), // Ebrahim M : added for progress bar (in advertising-file-modal popup window )

        pollJob : function(job_id, cb) {
            NL.advertising.jobs[job_id] = setInterval(function() {
                controllers.request_system.job_info(job_id, function(resp) {
                    if(resp.object.state === 'completed' || 
                        resp.object.state ===  'error') 
                    {                             
                        controls.advertising.deleteJob(job_id);
                        controllers.request_system.da_environment();                         
                        cb(resp);
                    } 
                });
            },this.pollInterval); // check on job status every second            
        },           
        deleteAsset: function(model,event) {            
            //event.preventDefault();
            //$(event.target).html('<img src="assets/img/spinner.gif" />');

            NL.advertising.deleteInProgress(true);
            var self=this;
            var priority = NL.advertising.activeTab() === 'priority' ? true : false;
            if(confirm('Are you sure you want to permanently delete ' + model.filename()) ) {
                controllers.request_system.delete_da_slide(model.filename(), priority, function(msg) {    
                    //controls.notification.show('Deleting : ' + model.filename());
                    //console.log('########### model = ' , model);
                   controls.newNotification.initNotif('Deleted : ' + model.filename() + ' from the channel.' /* model.parent.name */, 5000);
                   NL.advertising.deleteInProgress(false);
                   // model.parent.sidebarVisible(false);
                    //controls.modal.hide();
                    if(msg.object.job_id) { 
                        var job = msg.object.job_id;                    
                        var d1 = new Date();
                        /* legacy: need to poll job status */
                        if(NL.settings.connectionMethod() == 'utopia' &&
                                NL.settings.selectedSystem().version.substr(0,4) === '3.91'
                            ) {                                           
                            NL.advertising.jobs[job] = setInterval(function() {
                                controllers.request_system.job_info(job, function(msg) {                                                                 
                                    var d2 = new Date();                            
                                    if(msg.object.state === 'completed') {                                
                                        controls.advertising.deleteJob(job);
                                        controllers.request_system.da_environment(); 
                                        controls.newNotification.initNotif('DONE!', 2500); // Lucas A - not sure if this works yet
                                    } else if(msg.object.state === 'error') {
                                        controls.advertising.deleteJob(job);
                                        controllers.request_system.da_environment(); 
                                        controls.notification.show('Error deleting slide');
                                        controls.newNotification.initNotif('Error deleting slide', 2500); // Lucas A - not sure if this works yet
                                        console.log('done!');
                                    } else if(d2 - d1 > 5000) {
                                        controls.advertising.deleteJob(job);
                                        controls.notification.show('Time out deleting slide');
                                        controls.newNotification.initNotif('Time out deleting slide', 2500); // Lucas A - not sure if this works yet
                                    }
                                }); 
                            },1000); // check on job status every second
                        }
                    }
                    return; 
                });
            }else{
                //console.log('clicked cancel');
                NL.advertising.deleteInProgress(false);
            }
        },
        validateFile : function(file) {            
            //var file = document.getElementById('advertising-file').files[0];
            var img = new Image();
            img.onload = function() {
                // alert(this.width + ' ' + this.height);
                NL.advertising.newFileAsset.width(this.width);
                NL.advertising.newFileAsset.height(this.height);

                if( file !== undefined ){
                    var fileType = file.type.split('/');
                    NL.advertising.newFileAsset.isItJpgFile(fileType[1] === 'jpeg');
                }
            };
            var _URL = window.URL || window.webkitURL;
            img.src=_URL.createObjectURL(file);
        },
        isGalleryUploadable : function (files){
            for(var i = 0; i < files.length; i++) {
                //console.log(files[i].file.name);
                if( !controls.advertising.isFileUploadable(files[i].file , true) ){
                    controls.newNotification.initNotif('Gallery is not created because of ' + 
                        '\'' + files[i].file.name + 
                        '\' which is not uploaded! Remove it and try again.'+
                        '<BR>Note: Just *.jpeg and *.jpg files are uploadable.', 5500 ); 

                    return false;
                }
            }
            return true;
        },
        // Ebrahim M: Added because of Paul D request.
        isFileUploadable : function(file, isItGallery){            
            if( file !== undefined ){        
                var fileName = file.name;
                if(isItGallery){
                   if(fileName.substr(-3).toLowerCase() === 'jpg') {
                        return true;
                    }    
                }
                else{
                   if(fileName.substr(-3).toLowerCase() === 'jpg' || fileName.substr(-3).toLowerCase() === 'mpg' || 
                      fileName.substr(-3).toLowerCase() === 'mp4' || fileName.substr(-3).toLowerCase() === 'swf') {
                        return true;
                    }    
                } 

                // console.log('#############' + fileType[1].substr(-3).toLowerCase()  ,file );
                /*var fileType = file.type.split('/');
                console.log('#############' + fileType[1].substr(-3).toLowerCase()  ,file );
                if(isItGallery){
                   if(fileType[1].substr(-3).toLowerCase() === 'jpg' || fileType[1].substr(-4).toLowerCase() === 'jpeg') {
                        return true;
                    }    
                }
                else{
                   if(fileType[1].substr(-3).toLowerCase() === 'jpg' || fileType[1].substr(-4).toLowerCase() === 'jpeg' ||
                        fileType[1].substr(-4).toLowerCase() === 'mpeg' || fileType[1].substr(-3).toLowerCase() === 'mp4'||
                        fileType[1].substr(-5).toLowerCase() === 'flash') {
                        return true;
                    }    
                }*/           
            }
            return false;
        },
        addAsset: function(model, event) {
            NL.advertising.uploadInProgress(true); 
            var dest = NL.advertising.activeTab() === 'priority' ? 'priority' : 'user';

            var self=this;
            var startDate = NL.advertising.newFileAsset.start_date();
            var endDate = NL.advertising.newFileAsset.end_date();
            var file = document.getElementById('advertising-file').files[0];

            if(controls.advertising.isFileUploadable(file ,false)){
                NL.advertising.newFileAsset.file(file);

                if(NL.advertising.newFileAsset.selDate() === 'allYear'){
                    startDate = '01/01';
                    endDate = '31/12';
                }
                else{
                    startDate = ('0' + startDate.getDate()).slice(-2)+'/'+('0' + (startDate.getMonth()+1)).slice(-2);
                    endDate   = ('0' + endDate.getDate()).slice(-2)+'/'+('0' + (endDate.getMonth()+1)).slice(-2);
                }
                var obj = {
                    start_date: startDate,
                    end_date : endDate,
                    start_day : ( NL.advertising.newFileAsset.start_day() === 'All Week') ? 'All' : NL.advertising.newFileAsset.start_day(),
                    end_day : ( NL.advertising.newFileAsset.end_day() === '' )? 'All' : NL.advertising.newFileAsset.end_day(),
                    start_time : NL.advertising.newFileAsset.start_time() === 'All Day' ? '00:00' : NL.advertising.newFileAsset.start_time(),
                    end_time : NL.advertising.newFileAsset.start_time() === 'All Day' ? '23:59' : NL.advertising.newFileAsset.end_time(),
                    filename: file.name.toUpperCase(),
                    num_secs : NL.advertising.newFileAsset.num_secs(),
                    plays_per_hour : NL.advertising.newFileAsset.plays_per_hour(),
                    squish : NL.advertising.newFileAsset.squish(),
                    priority : NL.advertising.activeTab() === 'priority' ? true : false
                };            
                var msg = new SocketMessageModel(NL.settings.utopia, 'request_system', 'da_upload', obj); 
                msg.object = obj;
                msg.target = {system : NL.settings.selectedSystem().system, zone : NL.settings.selectedSystem().zone };
                NL.api.uploadClient.sendFile(file, msg,
                    function progress(filename , e) { // Ebrahim M: Added due to the progress bar
                        var self = controls.advertising;
                        var percent  = e.loaded * 100 / e.totalSize;
                        // percent = Math.round(percent * 10) / 10;
                        percent = Math.round(percent) ;
                        self.progressPercent( percent );                    
                    }, 
                    {
                        success : function(resp) {                                                                          
                            resp = typeof(resp) === 'string' ? JSON.parse(resp) : resp; 
                            if(resp.verb.event_category === 'response_error') {
                                controls.newNotification.initNotif('Error: ' + resp.object.error, 2500); // 999 -> 2500
                                NL.advertising.uploadInProgress(false);
                                controls.modal.hide();
                                return;
                            }
                            controls.newNotification.initNotif('Success! Uploaded: ' + file.name + ' to ' + dest + ' channel.', 8000);
                            NL.advertising.uploadInProgress(false); 
                            controls.modal.hide();

                            if(resp.object.job_id) {  // legacy: need to poll
                                 var job = resp.object.job_id;
                                 controls.advertising.pollJob(job, function(){
                                    controls.advertising.closeFileModal();  
                                 });
                             }
                        },
                        error : function(xhr, status, err) {
                             controls.newNotification.initNotif('Error: '+xhr.responseJSON.object.error, 2500 ); // 999 -> 2500
                             NL.advertising.uploadInProgress(false);
                             controls.modal.hide();
                        }                        
                    }
                );
            }
            else
            {
                NL.advertising.uploadInProgress(false);
                controls.newNotification.initNotif('\'' + file.name + 
                        '\' is not uploaded!<BR>Just *.jpg, *.mpg , *.swf and *.mp4 files are uploadable.', 4500 ); 
            }
        },
        addAndBuildTOI: function(){
            // This function invokes the build_toi.php script on the LIVE server
            // the build script bascially receieves all the parameters of the TOI slide
            // And the image, and then generates a ZIP file on the server
            // The script returns a URL (data.url) and then we need to
            // Proceed with addTOItoSystem that treats it like a regular DA slide
            // the only difference is that we do an API call with a url (data.urL) 
            // instead of an actual file to upload

            // First, we convert the newTOIAsset object into valid FormData for the builder
            var fd = new FormData();
            var file = NL.advertising.newTOIAsset.bgImage();

            if(file){
                var count = NL.advertising.newTOIAsset.tbArray().length;
                
                // Iterate through the text boxes
                for (var i = 0; i < count; i++) {
                    var t = escape(NL.advertising.newTOIAsset.tbArray()[i].content);
                    console.log(t);
                    fd.append('tl' + i, t);
                }
                
                fd.append('file', file);
                fd.append('textSize', NL.advertising.newTOIAsset.textSize()); 
                fd.append('textColour', NL.advertising.newTOIAsset.textColour()); 
                fd.append('fontFamily', NL.advertising.newTOIAsset.fontFamily()); 
                fd.append('bold', NL.advertising.newTOIAsset.bold()); 
                fd.append('shadowed', NL.advertising.newTOIAsset.shadowed()); 
                fd.append('verticalOffsetPercent', NL.advertising.newTOIAsset.verticalOffsetPercent()); 
                fd.append('verticalSpacingPercent', NL.advertising.newTOIAsset.verticalSpacingPercent()); 
                fd.append('bgColor', NL.advertising.newTOIAsset.bgColor()); 
                fd.append('bgOpacity', NL.advertising.newTOIAsset.bgOpacity()); 
                fd.append('bgImage', NL.advertising.newTOIAsset.bgImage()); 
                fd.append('copy_bg_img', NL.advertising.newTOIAsset.copy_bg_img()); 
                fd.append('upload_img', NL.advertising.newTOIAsset.upload_img()); 
                fd.append('filename', NL.advertising.newTOIAsset.filename());


                var urlOfBuildOnServer = 'http://localhost/nightlife/nl_toi_builder/build_toi.php';

                $.ajax({
                    type: 'POST',
                    // Url of PHP build program on HDMS-LIVE server
                    url: urlOfBuildOnServer,
                    data: fd,
                    contentType: false,
                    // crossDomain: true,
                    headers: {
                    // Set any custom headers here.
                    // If you set any non-simple headers, your server must include these
                    // headers in the 'Access-Control-Allow-Headers' response header.
                    },
                    // dataType: 'json',
                    processData: false,
                    success: function(data) {                        
                        if ( console && console.log ) {
                            console.log('completed!');
                        }

                        // Get the TOI builder's response
                        // this contains the URL to the generated ZIP file to send
                        // to the HDMS
                        // console.log(data);
                        controls.advertising.addTOItoSystem(JSON.parse(data));
                        controls.newNotification.initNotif('Build completed, sending file...', 2500);
                    },
                    error: function(jqXHR, status, err) {
                        controls.newNotification.initNotif('Ajax Error', 2500);
                        // lm(1, JSON.stringify(JSON.parse(jqXHR.responseText)), true)
                        // console.log('jq',jqXHR);
                        // console.log(status);
                        // console.log(err);
                    }
                });
            }
        },
        addTOItoSystem: function(response){
            console.log('TOI built, now ready to deploy...');
            console.log('URL to Zip: ' + response.zip_url);
            console.log('URL to HTML: ' + response.html_url);

            NL.advertising.uploadInProgress(true); 
            
            var dest = NL.advertising.activeTab() === 'priority' ? 'priority' : 'user';

            var self=this;
            var startDate = NL.advertising.newTOIAsset.start_date();
            var endDate = NL.advertising.newTOIAsset.end_date();

            // The URL of the actual TOI zip file on the server
            // We need a new API call to tell the HDMS to download this
            // And Unzip it
            var file = response.zip_url;

            // Lawrence O suggested in the meantime we just display the HTML file
            // Just keep in mind, if someone else goes and makes a DA slide with the same name,
            // The server will overwrite it
            var linkToGeneratedHTMLFile = response.html_url;
        
            var obj = {
                start_date: ('0' + startDate.getDate()).slice(-2)+'/'+('0' + (startDate.getMonth()+1)).slice(-2),
                end_date : ('0' + endDate.getDate()).slice(-2)+'/'+('0' + (endDate.getMonth()+1)).slice(-2),
                start_day : NL.advertising.newTOIAsset.start_day(),
                end_day : NL.advertising.newTOIAsset.end_day(),
                start_time : NL.advertising.newTOIAsset.start_time() === 'All Day' ? '00:00' : NL.advertising.newFileAsset.start_time(),
                end_time : NL.advertising.newTOIAsset.start_time() === 'All Day' ? '23:59' : NL.advertising.newFileAsset.end_time(),
                num_secs : NL.advertising.newTOIAsset.num_secs(),
                plays_per_hour : NL.advertising.newTOIAsset.plays_per_hour(),
                squish : NL.advertising.newTOIAsset.squish(),
                priority : NL.advertising.activeTab() === 'priority' ? true : false,

                // Hack so the API accepts it, add http:// in front of it
                filename: 'http://' + NL.advertising.newTOIAsset.filename() + '.TOI'
            };            
                
            if( (NL.settings.utopia.canIUse('request_system','add_da_slide')() && 
                   ( (NL.settings.selectedSystem().da_access === 'priority') || 
                        ((NL.advertising.activeTab() === 'user') &&
                        (NL.settings.selectedSystem().da_access === 'user')) 
                    ) ) ) 
            {            
                var msg = new SocketMessageModel(NL.settings.utopia, 'request_system', 'add_da_slide', obj); 
                controllers.request_system.add_da_slide(obj, function() {                       
                    controls.newNotification.initNotif('Success! Created: '+ obj.filename + ' to ' + dest + ' channel.', 2500);                             
                    controls.modal.hide();
                    NL.advertising.uploadInProgress(false);
                });
            }
            else{ 
                controls.newNotification.initNotif('Error: Access denied', 2500); 
                NL.advertising.uploadInProgress(false);
                controls.modal.hide();
            }
        },
        addWebPage : function(model, event) {    
            //$(event.target).parent().html('<img src="assets/img/spinner.gif" />');
            NL.advertising.uploadInProgress(true);
            var dest = NL.advertising.activeTab() === 'priority' ? 'priority' : 'user';

            var startDate = NL.advertising.newFileAsset.start_date();
            var endDate = NL.advertising.newFileAsset.end_date();
            if(model.selDate() === 'allYear'){
                startDate = '01/01';
                endDate = '31/12';
            }
            else{
                startDate = ('0' + startDate.getDate()).slice(-2)+'/'+('0' + (startDate.getMonth()+1)).slice(-2);
                endDate   = ('0' + endDate.getDate()).slice(-2)+'/'+('0' + (endDate.getMonth()+1)).slice(-2);
            }

            var obj = {
                start_date: startDate,
                end_date : endDate,
                start_day : (NL.advertising.newWebPageAsset.start_day()  === 'All Week') ? 'All' : NL.advertising.newWebPageAsset.start_day(),
                end_day : (NL.advertising.newWebPageAsset.end_day() === '') ? 'All' : NL.advertising.newWebPageAsset.end_day(),
                start_time : NL.advertising.newWebPageAsset.start_time() === 'All Day' ? '00:00' : NL.advertising.newWebPageAsset.start_time(),
                end_time : NL.advertising.newWebPageAsset.start_time() === 'All Day' ? '23:59' : NL.advertising.newWebPageAsset.end_time(),
                num_secs : NL.advertising.newWebPageAsset.num_secs(),
                plays_per_hour : NL.advertising.newWebPageAsset.plays_per_hour(),
                squish : NL.advertising.newWebPageAsset.squish(),
                priority : NL.advertising.activeTab() === 'priority' ? true : false,
                sources : [],
                filename : 'http://'+NL.advertising.newWebPageAsset.url()
            }; 

            if( (NL.settings.utopia.canIUse('request_system','add_da_slide')() && 
                   ( (NL.settings.selectedSystem().da_access === 'priority') || 
                        ((NL.advertising.activeTab() === 'user') &&
                        (NL.settings.selectedSystem().da_access === 'user')) 
                    ) ) ) 
            {            
                var msg = new SocketMessageModel(NL.settings.utopia, 'request_system', 'add_da_slide', obj); 
                controllers.request_system.add_da_slide(obj, function() {                       
                    controls.newNotification.initNotif('Success! Created: '+ obj.filename + ' to ' + dest + ' channel.', 2500);                             
                    controls.modal.hide();
                    NL.advertising.uploadInProgress(false);
                });
            }
            else{ 
                controls.newNotification.initNotif('Error: Access denied', 2500); 
                NL.advertising.uploadInProgress(false);
                controls.modal.hide();
            }
        },
        addInstagramPage : function(model, event) {
            NL.advertising.uploadInProgress(true);

            var dest = NL.advertising.activeTab() === 'priority' ? 'priority' : 'user';
            var startDate = model.start_date();
            var endDate = model.end_date();
            if(model.selDate() === 'allYear'){
                startDate = '01/01';
                endDate = '31/12';
            }
            else{
                startDate = ('0' + startDate.getDate()).slice(-2)+'/'+('0' + (startDate.getMonth()+1)).slice(-2);
                endDate   = ('0' + endDate.getDate()).slice(-2)+'/'+('0' + (endDate.getMonth()+1)).slice(-2);
            }
            // console.log('model >>>>>>>>>>>>>>>>>>>> model url= ', model.tag());
            // console.log('model >>>>>>>>>>>>>>>>>>>> model url= ', model.uname());
            // console.log('model >>>>>>>>>>>>>>>>>>>> model url= ', model._url());
            var obj = {
                start_date: startDate,
                end_date : endDate,
                start_day : (model.start_day()  === 'All Week') ? 'All' : model.start_day(),
                end_day : (model.end_day() === '') ? 'All' : model.end_day(),
                start_time : model.start_time() === 'All Day' ? '00:00' : model.start_time(),
                end_time : model.start_time() === 'All Day' ? '23:59' : model.end_time(),
                num_secs : model.num_secs(),
                plays_per_hour : model.plays_per_hour(),
                squish : model.squish(),
                priority : NL.advertising.activeTab() === 'priority' ? true : false,
                sources : [],
                filename : model._url()
            };  

            
            if( (NL.settings.utopia.canIUse('request_system','add_da_slide')() && 
                   ( (NL.settings.selectedSystem().da_access === 'priority') || 
                        ((NL.advertising.activeTab() === 'user') &&
                        (NL.settings.selectedSystem().da_access === 'user')) 
                    ) ) ) 
            {  
                var msg = new SocketMessageModel(NL.settings.utopia, 'request_system', 'add_da_slide', obj); 
                controllers.request_system.add_da_slide(obj, function() {
                controls.newNotification.initNotif('Success! Created: '+ obj.filename+ ' to ' + dest + ' channel.', 2500);
                NL.advertising.uploadInProgress(false);
                controls.modal.hide();
                });           
            }
            else{ 
                controls.newNotification.initNotif('Error: Access denied', 2500); 
                NL.advertising.uploadInProgress(false);
                controls.modal.hide();
            }

        },
        editAsset : function(model, event) {
            if( !(NL.settings.utopia.canIUse('request_system','edit_da_slide')() && 
                    ( (NL.settings.selectedSystem().da_access === 'priority') || 
                        ((NL.advertising.activeTab() === 'user') &&
                        (NL.settings.selectedSystem().da_access === 'user')) 
                    ) ) ) {
                return false;
            }

            NL.advertising.uploadInProgress(true);
            var dest = NL.advertising.activeTab() === 'priority' ? 'priority' : 'user';
            var self = this;
            var startDate = model.start_date();
            var endDate = model.end_date();

            if(model.selDate() === 'allYear'){
                startDate = '01/01';
                endDate = '31/12';
            }
            else{
                startDate = ('0' + startDate.getDate()).slice(-2)+'/'+('0' + (startDate.getMonth()+1)).slice(-2);
                endDate   = ('0' + endDate.getDate()).slice(-2)+'/'+('0' + (endDate.getMonth()+1)).slice(-2);
            }

            var obj = {
                filename: model.filename(),
                start_date: startDate,
                end_date : endDate,
                start_day : (model.start_day() === 'All Week') ? 'All' : model.start_day(),
                end_day : (model.end_day() === '') ? 'All' : model.end_day(),
                start_time : model.start_time() === 'All Day' ? '00:00' : model.start_time(),
                end_time : model.start_time() === 'All Day' ? '23:59' : model.end_time(),
                num_secs : model.num_secs(),
                plays_per_hour : model.plays_per_hour(),
                squish : model.squish(),
                sources : [],
                priority : NL.advertising.activeTab() === 'priority' ? true : false
            };

            console.log('#### obj = ' , obj);
            controllers.request_system.edit_da_slide(obj, function() {
                controls.newNotification.initNotif('Success! Updated: '+ obj.filename+ ' in ' + dest + ' channel.', 2500); // 999 -> 2500
                NL.advertising.uploadInProgress(false);
                // model.parent.sidebarVisible(false);
                //controls.modal.hide();
            });
        },
        editGallery : function(model, event) {
            NL.advertising.uploadInProgress(true);
            var dest = NL.advertising.activeTab() === 'priority' ? 'priority' : 'user';     
            var self = this;
            var startDate = model.start_date();
            var endDate = model.end_date();
            if(model.selDate() === 'allYear'){
                startDate = '01/01';
                endDate = '31/12';
            }
            else{
                startDate = ('0' + startDate.getDate()).slice(-2)+'/'+('0' + (startDate.getMonth()+1)).slice(-2);
                endDate   = ('0' + endDate.getDate()).slice(-2)+'/'+('0' + (endDate.getMonth()+1)).slice(-2);
            }

            var obj = {
                filename: model.filename(),
                start_date: startDate,
                end_date : end_date,
                start_day : (model.start_day() === 'All Week') ? 'All' : model.start_day(),
                end_day : (model.end_day() === '') ? 'All' : model.end_day(),
                start_time : model.start_time() === 'All Day' ? '00:00' : model.start_time(),
                end_time : model.start_time() === 'All Day' ? '23:59' : model.end_time(),
                num_secs : model.num_secs(),
                plays_per_hour : model.plays_per_hour(),
                squish : model.squish(),
                sources : [],
                priority : NL.advertising.activeTab() === 'priority' ? true : false
            };
            controllers.request_system.edit_da_slide(obj, function() {
                controls.advertising.galleryAddPhoto(model,function() {
                    NL.advertising.uploadInProgress(false);
                    controls.newNotification.initNotif('Saved '+ model.filename() +' in ' + dest + ' channel.'  , 2500); // 999 -> 2500 
                    controls.modal.hide();
                });         
            });
        },
        createGallery : function() {

            var files = NL.advertising.newFileList();
            if(!controls.advertising.isGalleryUploadable(files)){
                return false;
            }


            var self = this;          
            NL.advertising.uploadInProgress(true);
            var dest = NL.advertising.activeTab() === 'priority' ? 'priority' : 'user';
            var startDate = NL.advertising.newGallery.start_date();
            var endDate = NL.advertising.newGallery.end_date();
            if(NL.advertising.newGallery.selDate() === 'allYear'){
                startDate = '01/01';
                endDate = '31/12';
            }
            else{
                startDate = ('0' + startDate.getDate()).slice(-2)+'/'+('0' + (startDate.getMonth()+1)).slice(-2);
                endDate   = ('0' + endDate.getDate()).slice(-2)+'/'+('0' + (endDate.getMonth()+1)).slice(-2);
            }

            var obj = {
                start_date: startDate,
                filename: NL.advertising.newGallery.filename()+'.GAL',
                end_date : endDate,
                start_day : (NL.advertising.newGallery.start_day() === 'All Week') ? 'All' : NL.advertising.newGallery.start_day(),
                end_day : (NL.advertising.newGallery.end_day() === '') ? 'All' : NL.advertising.newGallery.end_day(),
                start_time : NL.advertising.newGallery.start_time() === 'All Day' ? '00:00' : NL.advertising.newGallery.start_time(),
                end_time : NL.advertising.newGallery.start_time() === 'All Day' ? '23:59' : NL.advertising.newGallery.end_time(),
                num_secs : NL.advertising.newGallery.num_secs(),
                plays_per_hour : NL.advertising.newGallery.plays_per_hour(),              
                sources : [],
                priority : false,//NL.advertising.newGallery.priority(),//NL.advertising.activeTab() === 'priority' ? true : false,
                squish: true
            };                   
            controllers.request_system.gallery_create(obj, function(resp) {                    
                controls.advertising.pollJob(resp.object.job_id,function(){
                    if(NL.advertising.newFileList().length > 0) {
                        controls.advertising.galleryAddPhoto(NL.advertising.newGallery,function() {
                             //controls.notification.show('Created: '+NL.advertising.newGallery.filename()+'.GAL' );
                            NL.advertising.uploadInProgress(false);
                            var msg = ('Created  ' + NL.advertising.newGallery.filename() +'.GAL in ' + dest + ' channel.');
                            controls.newNotification.initNotif(msg, 2500); // 999 -> 2500   
                            NL.advertising[NL.advertising.activeTab()].sidebarVisible(false);                 
                            controls.modal.hide();
                        });    
                    }else{
                        var msg = ('Created  ' + NL.advertising.newGallery.filename() +'.GAL in ' + dest + ' channel with no content.');
                        NL.advertising.uploadInProgress(false);
                        controls.newNotification.initNotif(msg, 2500);   // 999 -> 2500
                        NL.advertising[NL.advertising.activeTab()].sidebarVisible(false);                 
                        controls.modal.hide();
                    } 
                });
            }); 
        },
        galleryAddPhoto : function(model, cb) {                
            var fname = model.filename().slice(-4)=='.GAL' ? model.filename() : model.filename()+'.GAL';
            obj = {
                filename : fname
            };                                    
            var msg = new SocketMessageModel(NL.settings.utopia, 'request_system', 'gallery_add_photo', obj);
            msg.object = obj;
            msg.target = {system : NL.settings.selectedSystem().system, zone : NL.settings.selectedSystem().zone }; 
            var count = 0;      
            var handleSuccess = function(i,resp) {
                //controls.notification.show('Added: ' + files[i].filename);  
                controls.newNotification.initNotif('Added: ' + files[i].filename + ' Uploading...'  , 2500);  
                NL.advertising[NL.advertising.activeTab()].sidebarVisible(false);                                    
                NL.advertising.newFileList()[i].uploaded(true);                     
                resp = typeof(resp) === 'string' ? JSON.parse(resp) : resp;    
                count += 1;                            
                $('#gallery-progress').css('width', 100*(count/files.length)+'%');
                $('#gallery-progress').text('Uploaded: '+count+ ' of ' + files.length);  
                if(count === files.length && typeof(cb) === 'function') {                    
                    cb();
                }
            };     
            var handleError = function(index, xhr, status, err){                  
               // controls.notification.show(
                //    'Error uploading "'+ NL.advertising.newFileList()[index].filename+'" : '+xhr.responseJSON.object.error
              // );
                NL.advertising.uploadInProgress(false);
                controls.newNotification.initNotif(
                        'Error uploading "'+ NL.advertising.newFileList()[index].filename+'" : '+xhr.responseJSON.object.error + '.', 
                2500);    // 999 -> 2500
            };

            var progressFunc = function(filename , e){
                var self = controls.advertising;
                var percent  = e.loaded * 100 / e.totalSize;
                percent = Math.round(percent * 10) / 10;

                for(i = 0; i < NL.advertising.newFileList().length ;i++){
                    if(NL.advertising.newFileList()[i].filename.toUpperCase() === filename){
                        NL.advertising.newFileList()[i].uploadingProgress(percent);                                                 
                    }
                }                 
            };

            var files = NL.advertising.newFileList();
            for(var i = 0; i < files.length; i++) {
                NL.api.uploadClient.sendFile(files[i].file, msg, progressFunc,
                {
                    success : handleSuccess.bind(undefined, i),                                           
                    error : handleError.bind(undefined, i)
                });
            }
        },
        cancelEditing : function(){
            // Lucas A - when editing is cancelled, we want to deselect the asset and also disable the sidebar
            controls.advertising.deselectAllAssets();
        },
        showFileModal : function() { 
            controls.advertising.progressPercent(0);           
            NL.advertising.newFileAsset.priority(NL.advertising.activeTab() === 'Priority' ? true : false);
            controls.modal.show('advertising-file-modal',NL.advertising.newFileAsset,'');
            if(! controls.advertising.isInit) {
                controls.advertising.init();
            }
        },
        showWebPageModal : function() {
            // Ebrahim M : Initialize the variables
            NL.advertising.newWebPageAsset._url('');

            NL.advertising.newWebPageAsset.priority(NL.advertising.activeTab() === 'Priority' ? true : false);
            controls.modal.show('advertising-web-page-modal',NL.advertising.newWebPageAsset,'');
        },
        showTOIModal : function() {
            NL.advertising.newTOIAsset.priority(NL.advertising.activeTab() === 'Priority' ? true : false);
            
            // Reset
            NL.advertising[NL.advertising.activeTab()].TOIwizardpage(0);
            NL.advertising.newTOIAsset.filename('');
            NL.advertising.newTOIAsset.bgColor('000000');
            
            // Clear out the Text Box array
            // for(var i = 0; i <= NL.advertising.newTOIAsset.tbArray().length; i++){
            //     // NL.advertising.newTOIAsset.tbArray()[i].content = '';
            //     NL.advertising.newTOIAsset.tbArray().pop();
            // }
            NL.advertising.newTOIAsset.tbArray().length = 1;

            NL.advertising.toiUploadDialogVisible(true);
            controls.modal.show('advertising-toi-modal', NL.advertising.newTOIAsset,'');
            
            if(! controls.advertising.isInit) {
                controls.advertising.initTOIImageChosen();
            }
        }, 
        cleanFileName: function(t){
            console.log(t);
            output = t.filename().trim().split(' ').join('_');
            NL.advertising.newTOIAsset.filename(output);
            // return output;
        },
        drawSlider: function() {
          // var disabled = NL.settings.utopia.canIUse('request_zone','set_volume',true)();
            $( '#slider-font-size' ).slider({
                orientation: 'horizontal',
                disabled: false,
                range: 'min',
                min: 1,
                max: 100,
                value: NL.advertising.newTOIAsset.textSize(),
                // value: 43,
                slide: function( event, ui ) {
                  //$( '#amount' ).val( ui.value );
                  // NL.volume.sliderLevel(ui.value);
                  console.log(ui.value);
                  console.log('slide');
                },
                stop: function(event, ui) {
                  // NL.volume.level(ui.value);
                  // controls.volume.setLevel(ui.value);
                  console.log('stop');
                  console.log(ui.value);
                }
              }); 
        }, 
        toggleTOIBold: function(){
            NL.advertising.newTOIAsset.bold(!NL.advertising.newTOIAsset.bold());
        },
        toggleTOIShadowed: function(){
            NL.advertising.newTOIAsset.shadowed(!NL.advertising.newTOIAsset.shadowed());
        },      
        textBoxAddRemove: function(direction){
            if(direction === 'add' && NL.advertising.newTOIAsset.tbArray().length < 6){
                // NL.advertising.newTOIAsset.textBoxCount(NL.advertising.newTOIAsset.textBoxCount() + 1);
                NL.advertising.newTOIAsset.tbArray.push({content:''});

            }else if (direction === 'remove' && NL.advertising.newTOIAsset.tbArray().length > 1){
                // NL.advertising.newTOIAsset.textBoxCount(NL.advertising.newTOIAsset.textBoxCount() - 1);
                NL.advertising.newTOIAsset.tbArray.pop();
            }
        },
        expandFontListDropDown: function(){
            NL.advertising.fontListDropDownOpen(!NL.advertising.fontListDropDownOpen());
        },
        toggleColorPickerVisible: function(property){
            if(property === 'bgColorPicker'){
                NL.advertising.bgColorPickerVisible(!NL.advertising.bgColorPickerVisible());
            }else if(property === 'textColorPicker'){
                NL.advertising.textColorPickerVisible(!NL.advertising.textColorPickerVisible());
            }else if(property === 'closeAll'){
                // Collapse all
                NL.advertising.textColorPickerVisible(false);
                NL.advertising.bgColorPickerVisible(false);
            }
        },
        chooseTOIFontFamily: function(data, event){
            // Get context of clicked font name to get its index number
            var context = ko.contextFor(event.target);
            var fontIndex = context.$index();
            
            NL.advertising.newTOIAsset.fontFamily(NL.advertising.fontFamilies()[fontIndex].name);
            // Close dropdown
            controls.advertising.expandFontListDropDown();
            
            // console.log('name is ' + NL.advertising.fontFamilies()[fontIndex].name);
        },
        showEditModal : function(model, event) {
            $('.asset.row').removeClass('selected');
            $(event.currentTarget).addClass('selected');
            /*cb = function(mWrapper) {
                console.log('modalCB');
                mWrapper.one('hidden.bs.modal', function() {
                    $('.asset.row').removeClass('selected');
                });
            };*/
            var w;
            var newModel = new AdvertisementModel(ko.mapping.toJS(model));
            if(model.gallery) {                
                w = controls.modal.show('advertising-edit-gallery-modal',newModel,'');   
                NL.advertising.newFileList([]); // clear file buffer
               // if(! controls.advertising.isInit) {
                    controls.advertising.initGallery();
                    
              //  }                         
            } else { 
                w = controls.modal.show('advertising-edit-asset-modal',newModel,'');       
                if(! controls.advertising.isInit) {
                    controls.advertising.init();
                }         
            }
            w.one('hidden.bs.modal', function () {
                $('.asset.row').removeClass('selected');
            });
        },
        showCreateGalleryModal : function(model) {
            NL.advertising.newFileList([]);
            controls.modal.show('advertising-create-gallery-modal',NL.advertising.newGallery,'');        
            controls.advertising.initGallery();    
        },
        closeEditModal : function() {
           // $('.asset.row').removeClass('selected');
            controls.modal.hide();
        },
        closeFileModal : function() {
            controls.modal.hide();
        },
        deleteJob : function(job) {
          clearInterval(NL.advertising.jobs[job]);
          delete(NL.advertising.jobs[job]);
        },
        init : function() { 
            // Ebrahim M : Initialize the observable variable as the popup wnd opens  
            NL.advertising.newFileAsset.width('');
            NL.advertising.newFileAsset.height('');
            NL.advertising.newFileAsset.isItJpgFile(false);
            ///////////////////////////////////////////////////////////////////////////

            $('#advertising-file').on('change', function() {                
                var input = $('#advertising-file'),
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', label);                
            });
            $('#advertising-file').on('fileselect', function(event, label) {        
                var input = $('#advertising-file-text');
                if( input.length && label ) {
                    input.val(label); // display the filename 
                                       
                    // Ebrahim M : divided the file to two parts 'knwon Extensions' and 'Unknown Extensions'
                    if(label.substr(-3).toLowerCase() === 'jpg' || label.substr(-4).toLowerCase() === 'jpeg' ||
                        label.substr(-3).toLowerCase() === 'png' || label.substr(-3).toLowerCase() === 'bmp' ||
                        label.substr(-3).toLowerCase() === 'gif') { // Ebrahim M: known file extensions
                        var reader = new FileReader();
                        reader.onload = function(e) {                
                            $('#advertising-preview').attr('src', e.target.result);                                            
                            $('#advertising-preview').css('visibility','visible');
                            controls.advertising.validateFile($('#advertising-file')[0].files[0]);
                        };
                        reader.readAsDataURL($('#advertising-file')[0].files[0]);
                    }
                    else{ // Ebrahim M: unknown file extensions
                        $('#advertising-preview').attr('src', 'unknown');                                            
                        $('#advertising-preview').css('visibility','visible');
                        controls.advertising.validateFile($('#advertising-file')[0].files[0]);
                    }
                }
            });
        },
        loadAndRenderImage: function(){
            function rendered() {
                //Render complete
                NL.advertising.toiImageProcessing(false);
                NL.advertising.toiUploadDialogVisible(false);
            }

            function startRender() {
                //Rendering start
                requestAnimationFrame(rendered);
            }

            function loaded()  { requestAnimationFrame(startRender); }

            loaded();
        },

        initTOIImageChosen: function(){
            NL.advertising.newTOIAsset.width('');
            NL.advertising.newTOIAsset.height('');
            NL.advertising.newTOIAsset.isItJpgFile(false);

            $('#toi-image-file').on('change', function() {                
                var input = $('#toi-image-file'),
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                    input.trigger('fileselect', label);                
            });            
            $('#toi-image-file').on('fileselect', function(event, label) {        
                NL.advertising[NL.advertising.activeTab()].TOIwizardpage(2); 
                NL.advertising.toiImageProcessing(true); 

                if(label) {
                    NL.advertising.newTOIAsset.bgImageFilename(label);
                    
                    //if user hasn't set a filename, prepopulate it with the image file name
                    if(NL.advertising.newTOIAsset.filename() === ''){
                        NL.advertising.newTOIAsset.filename(label); 
                    }
                                       
                    // Ebrahim M : divided the file to two parts 'knwon Extensions' and 'Unknown Extensions'
                    if(label.substr(-3).toLowerCase() === 'jpg' || label.substr(-4).toLowerCase() === 'jpeg' ||
                        label.substr(-3).toLowerCase() === 'png' || label.substr(-3).toLowerCase() === 'bmp' ||
                        label.substr(-3).toLowerCase() === 'gif') { // Ebrahim M: known file extensions
                        var reader = new FileReader();
                        reader.onload = function(e) {         
                            $('.img-container').css('background-image', 'url(' + e.target.result + ')');  

                            // Dismiss the image selector dialog after confirmation of rendered image       
                            controls.advertising.loadAndRenderImage();            
                            
                            NL.advertising.newTOIAsset.bgImage($('#toi-image-file')[0].files[0]);
                        };
                        reader.onloadend = function(){
                            console.log('ended');
                        };
                        reader.readAsDataURL($('#toi-image-file')[0].files[0]);
                    }
                    else{ // Ebrahim M: unknown file extensions
                        // $('.img-container').attr('src', 'unknown');                                            
                        NL.advertising.toiImageProcessing(false);    
                        controls.advertising.validateFile($('#toi-image-file')[0].files[0]);
                    }
                }else{
                    alert('no file?');
                    NL.advertising.toiImageProcessing(false);    
                }
            });
        },

        initGallery : function() {            
            $('#advertising-gallery-file, #advertising-gallery-folder').on('change', function(e) {                     
                var input = $(e.target),
                    files = input.get(0).files,
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = numFiles > 1 ? numFiles + ' Files Selected' : 
                        input.val().replace(/\\/g, '/').replace(/.*\//, '');               
                NL.advertising.fileSrc = e.target;
                input.trigger('fileselect', label);
                 var textField = $('#advertising-gallery-file-text');                    
                 NL.advertising.fileSrc = e.target;
                 NL.advertising.newFileList([]);                 
                if( textField.length && label ) { 
                    textField.val(label); // display the filename
                    var reader = new FileReader();
                    reader.onload = controls.advertising.previewFile;                    
                    reader.readAsDataURL(input[0].files[0]);
                }                

                var fileConstructorCb = function(fileObj) {
                   // NL.advertising.newFileList.push(fileObj);                    
                };
                for(var i = 0; i < files.length; i++) {
                    var f = new AdvertisingFileModel(files[i], fileConstructorCb);      
                     NL.advertising.newFileList.push(f);                    
                }        
                 
            });          
        },
        appSetIntro : function() {
            var file = $('#advertising-app-logo').get(0).files[0];
            controllers.request_venue.set_intro( 
                file, 
                {
                    header : NL.advertising.app.header(),
                    sub_header : NL.advertising.app.sub_header(),
                    body_text : NL.advertising.app.body_text(),
                    bg_color : NL.advertising.app.bg_color().substr(1)
                }, function(success) {
                    if(success) {
                        controllers.request_venue.intro();
                    } else {
                        controls.newNotification.initNotif('Upload Failed', 2500); 
                    }
                }
            );
        },
        nextAsset : function(model, event){                     
            var lastAsset = NL.advertising[NL.advertising.activeTab()].lastAssetSelected();
            var lastIndex = lastAsset.viewIndex;
            var nextIndex = lastIndex + 1; //advance the selected index
            var next = NL.advertising[NL.advertising.activeTab()].assets()[nextIndex];

            // if user pressed down arrow without previously selecting
            // an asset, default it to the [0] asset;
            if(!lastAsset){ 
                lastAsset = NL.advertising[NL.advertising.activeTab()].assets()[0];
            }
            
            // if there is no next asset, it means we're at the bottom
            // of the list, so just select the last item. 
            if(!next){
                next = lastAsset;
            }
                controls.advertising.deselectAllAssets();
                next.selected(true);
                NL.advertising[NL.advertising.activeTab()].lastAssetSelected(next);
                NL.advertising[NL.advertising.activeTab()].firstAssetSelected(next.viewIndex);
                controls.advertising.showAssetSidebar(next);
        },
        prevAsset : function(model, event) {             
            var lastAsset = NL.advertising[NL.advertising.activeTab()].lastAssetSelected();
            var lastIndex = lastAsset.viewIndex;
            var prevIndex = lastIndex - 1;
            var prev = NL.advertising[NL.advertising.activeTab()].assets()[prevIndex] ;
            
            // if user pressed down arrow without previously selecting
            // an asset, default it to the [0] asset;
            if(!lastAsset){
                lastAsset = NL.advertising[NL.advertising.activeTab()].assets()[0];
            }
            
            // if there is no next asset, it means we're at the top
            // of the list, so just select the last item. 
            if(!prev){
                prev = lastAsset;
            }
                controls.advertising.deselectAllAssets();
                prev.selected(true);
                NL.advertising[NL.advertising.activeTab()].lastAssetSelected(prev);
                NL.advertising[NL.advertising.activeTab()].firstAssetSelected(prev.viewIndex);
                controls.advertising.showAssetSidebar(prev);
        },
        scrollTo: function(e, direction) { //distance is the item we're scrolling to
            self = controls.advertising;
            var pane = $(e.scrollParent());
            if (pane.is(':visible')) {
                if(! e.inViewAdvertising()) { // if not in view, scroll to it
                    if(direction === 'up') {
                        pane.scrollTop(self.zeroTop(e));
                    } else {
                        pane.scrollTop(self.zeroBottom(e));
                    }
                }                
            }
            return true;
        },
        zeroTop: function(e) {
            var result = 0;
            var pane = $(e.scrollParent());            
              if (pane) {
                var pos = $(e, pane).position();
                if(pos){
                  result = pos.top + pane.scrollTop();
                }
              }             
              return result;
        },
        zeroBottom: function(e) {
            var result = 0;
            var pane = $(e.scrollParent());
            if (pane) {
                var pos = $(e, pane).position();
                if(pos){                     
                    result = pos.top - pane.height() + e.height() + pane.scrollTop();
                }
            }            
            return result;
        },
        activePanel: ko.observable('user'),
        switchActivePanel: function(panelName) {    
          controls.advertising.activePanel(panelName);
          NL.advertising.activeTab(panelName);
          controls.advertising.deselectAllAssets();
          NL.advertising[NL.advertising.activeTab()].lastAssetSelected(NL.advertising[NL.advertising.activeTab()].assets()[0]); // Lucas A - 2nd April 2015
          NL.advertising.user.sidebarVisible(false);
          NL.advertising.priority.sidebarVisible(false);
        },
        togglePlayers: function(){
            NL.advertising.playersVisible(!NL.advertising.playersVisible());
            console.log(NL.advertising.playersVisible());
            NL.advertising[NL.advertising.activeTab()].sidebarVisible(false);
        },
        toggleEditMode: function(){
            console.log('toggleEditMode called');
             NL.advertising[NL.advertising.activeTab()].DAListEditMode(!NL.advertising[NL.advertising.activeTab()].DAListEditMode());
             NL.advertising[NL.advertising.activeTab()].sidebarVisible(false);
            console.log(NL.advertising[NL.advertising.activeTab()].DAListEditMode());
        },
        toggleSidebar: function(model){
        // Lucas A - I think this function is no longer used. Ebrahim, perhaps remove it
           NL.advertising[NL.advertising.activeTab()].sidebarVisible(false);        
        },
        selectAsset: function(model, event){
            //var thisElement = $(event.currentTarget);
            // console.log('SELASSET', model);
            var isCurrentlySelected = model.selected();
            var numAssetsCurrentlySelected = model.parent.selectedAssets().length;
            if(numAssetsCurrentlySelected > 1){
                model.parent.lastAssetSelected(model.parent.selectedAssets()[model.parent.selectedAssets().length - 1].viewIndex);
                console.log('last asset was' + model.parent.lastAssetSelected());
            }
            //var channelName = model.priority ? 'priority' : 'user';
            
            //if we're in edit-in-place mode, you can't select a row
            //if(!NL.advertising.DAListEditMode()){
            if(!model.parent.DAListEditMode()){

                // Multi Select
                // (and is allowed to)
                if(event.ctrlKey && model.parent.assetListConfig.canMultiSelect()){
                    controls.advertising.ctrlSelectAsset(model, event);
                    return true;
                }else if(event.shiftKey && model.parent.assetListConfig.canMultiSelect()){
                     controls.advertising.shiftSelectAsset(model, event);
                    return true;
                }else{
                    // Single Click
                    if(isCurrentlySelected && numAssetsCurrentlySelected > 1){
                        // if the file clicked is currently selected
                        // as part of a group selection, we want to
                        // deselect all assets, then reselect this one
                        controls.advertising.deselectAllAssets();
                        model.parent.lastAssetSelected(model);
                        model.selected(true);
                    }else if(isCurrentlySelected){
                        // if the file clicked is already selected,
                        // deselect it

                        model.parent.sidebarVisible(false);
                        model.selected(false);  
                        return true;                        
                    }else{                            
                        // if the file clicked is NOT currently selected,
                        // in the current selection group,
                        // deselect all then select this item. 
                        controls.advertising.deselectAllAssets();
                        model.parent.lastAssetSelected(model);
                        model.selected(true);
                        
                        // this is the first item selected,
                        // keep track of its index for when
                        // we shift select
                        model.parent.firstAssetSelected(model.viewIndex);
                        model.parent.lastSelectedWasCtrl(false);
                        //console.log('lastSelectedWasCtrl = ' + NL.advertising.lastSelectedWasCtrl());
                       // console.log('First asset is index = ' + NL.advertising.firstAssetSelected());
                    }
                    // show the sidebar for the single selected asset
                    controls.advertising.showAssetSidebar(model);

                }
            }


        },
        ctrlSelectAsset: function(model, event){
            
            //toggle the selected state 
            model.selected(!model.selected());
            
            // set the selection anchor to this element
            model.parent.firstAssetSelected(model.viewIndex);
            model.parent.lastAssetSelected(model);
            //console.log('Ctrl Clicked, last asset seelected = ' +  model.parent.lastAssetSelected().viewIndex);

            // if user has previously selected multiple ads
            // but their selection has gone down to one again
            // make sure the 'current' asset selected is now
            // the remaining one
            if( model.parent.selectedAssets().length == 1){
                // only one selected asset remains, 
                // show the asset sidebar with it's details
                controls.advertising.showAssetSidebar( model.parent.selectedAssets()[0], event);
            }
              model.parent.lastSelectedWasCtrl(true);
             console.log('lastSelectedWasCtrl = ' +  model.parent.lastSelectedWasCtrl());
        },        
        shiftSelectAsset: function(model, event){
            var i =  model.parent.firstAssetSelected();
            var clickedIndex = model.viewIndex;
            console.log('clicked Index = ' + clickedIndex);
            console.log('i = ' + i);
            //console.log('lastSelectedWasCtrl = ' +  model.parent.lastSelectedWasCtrl());

           // console.log('i = ' + i);
            

            if( model.parent.lastSelectedWasCtrl === false){
                console.log('false, deselecting all...');
                controls.advertising.deselectAllAssets();
            }

            // if we've clicked underneath the anchor asset
            if(clickedIndex > i){
                while (i <= model.viewIndex){
                     model.parent.assets()[i].selected(true);
                    i++;
                }
            // if we've clicked above the anchor asset
            }else{
                while (i >= model.viewIndex){
                    model.parent.assets()[i].selected(true);
                    i--;
                }
            }
             model.parent.lastSelectedWasCtrl(false);
             model.parent.lastAssetSelected(model);
        },
        deselectAllAssets: function(){
            if(NL.advertising.activeTab() === 'user'){
                for(i=0; i<NL.advertising.user.assets().length; i++){
                     NL.advertising.user.assets()[i].selected(false);
                }
            }else{
                for(i=0; i<NL.advertising.priority.assets().length; i++){
                    NL.advertising.priority.assets()[i].selected(false);
                }
            }
            // Lucas A - added this to ensure that sidebar is turned off if no assets selected
            NL.advertising[NL.advertising.activeTab()].sidebarVisible(false);
        },
        showAssetSidebar: function(model){    
            // console.log('@@@@@@@ mode  = ' , model);
            model.parent.currentModel(model); 

            ///////////////////////////////////////////////////////
            // Added by Ebrahim M
            var newModel = new AdvertisementModel(ko.mapping.toJS(model));            
            model.parent.currentEdittingModel(newModel);
            //////////////////////////////////////////////////////
            if(NL.advertising.playersVisible(true)){
                controls.advertising.togglePlayers();
            }
            model.parent.sidebarVisible(true);  
            $('#numDisplayTime').focus();
        },
        showPreviewFileModal: function(model, event) {       
            //var w;
           var newModel = new AdvertisementModel(ko.mapping.toJS(model));
           controls.modal.show('advertising-preview-file-modal',newModel,'');           
        },
        fan: function(event){
            $('.stack-leaflet').toggleClass('active');
        },
        clearSearchFilter: function(){            
            NL.advertising[NL.advertising.activeTab()].searchStr('');
            NL.advertising[NL.advertising.activeTab()].sidebarVisible(false);
            controls.advertising.deselectAllAssets();
        },
        changeSelection : function(direction) { // Ebrahim M: Added to activate up and down keys 
            self = controls.advertising;
            if(direction === 'up') {                
                self.prevAsset();
            }
            if(direction === 'down') {
                self.nextAsset();
            }
        },
        getActiveBtnText : function(){
            var strRet = 'User Advertising';
            if(NL.advertising.activeTab() !== 'user'){
                strRet = 'Priority Advertising';
            }            
            if( NL.textScrolling.scrollingTextVisible() ){
                strRet = 'Scrolling Text';
            }
            return strRet;
        },
        toiCalculatePercentages: function(){
            self = NL.advertising.newTOIAsset;
            var h = self.previewContainerHeight();
            var w = self.previewContainerWidth();
            
            self.verticalOffsetPercent(self.verticalOffset() / h);
            self.verticalSpacingPercent(self.verticalSpacing() / h);
            console.log('vertical offset percent = ' + self.verticalOffsetPercent() + '%' );
            console.log('vertical spacing percent = ' + self.verticalSpacingPercent() + '%');
        },
        getActiveBtnIcon : function(){
            var strRet = 'icon-user';
            if(NL.advertising.activeTab() !== 'user'){
                strRet = 'nm-icons-film';
            }            
            if( NL.textScrolling.scrollingTextVisible() ){
                strRet = 'icon-text-width';
            }
            return strRet;
        },
        navTOIWizard: function(direction){
            page = NL.advertising[NL.advertising.activeTab()].TOIwizardpage();
            if(direction === 'next'){
                page++;
            }else if(direction === 'previous'){
                page--;
            }
            
            NL.advertising[NL.advertising.activeTab()].TOIwizardpage(page);

            if(page === 3){
                controls.advertising.toiCalculatePercentages();
            }
        },
        toggleTOIUploadDialog: function(){
            NL.advertising.toiUploadDialogVisible(!NL.advertising.toiUploadDialogVisible());
        },
        getBtnText : function(menuItem){
            var strRet = 'User Advertising';
            if( NL.textScrolling.scrollingTextVisible() ){
                if(menuItem === 1){
                    strRet = 'User Advertising';
                }
                if(menuItem === 2){
                    strRet = 'Prioprity Advertising';
                }
            }
            else{
                if(menuItem === 1){
                    if(NL.advertising.activeTab() === 'user'){
                        strRet = 'Priority Advertising';
                    }
                    else{
                      strRet = 'User Advertising';
                    }
                }
                if(menuItem === 2){
                  strRet = 'Scrolling Text';
                }
            }
            return strRet;
        },
        getBtnIcon : function(menuItem){
            var strRet = 'icon-user';
            if( NL.textScrolling.scrollingTextVisible() ){
                if(menuItem === 1){
                    strRet = 'icon-user';
                }
                if(menuItem === 2){
                    strRet = 'nm-icons-film';
                }
            }
            else{
                if(menuItem === 1){
                    if(NL.advertising.activeTab() === 'user'){
                        strRet = 'nm-icons-film';
                    }
                    else{
                      strRet = 'icon-user';
                    }
                }
                if(menuItem === 2){
                  strRet = 'icon-text-width';
                }
            }
            return strRet;            
        }
    }
});

ko.bindingHandlers.progress = {
    init: function(element, valueAccessor) {
        $(element).progressbar({
            value: 0
        });
    },
    update: function(element, valueAccessor) {
        var val = ko.utils.unwrapObservable(valueAccessor());
       $(element).progressbar('value', parseFloat(val));
    }
};

$doc.on('nightlife-ready', function() {    
    $('a[data-toggle="tab"].advertising-tab').on('shown.bs.tab', function(e) {
        NL.advertising.activeTab($(e.target).data().ref);
    });
});
