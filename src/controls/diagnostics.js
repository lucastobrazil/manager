$.extend(true, controls, {
    diagnostics: {
        // pingState: ko.observable(),
        //  resumeState: ko.observable(),
        //   HDMSLiveState: ko.observable(),
        //  googleState: ko.observable(),
        pingTimer: undefined,
        autoTimer: undefined,
        timeoutCount : 0,
        manualIP: ko.observable(),
        bTestWS : true,
        bTestHDMSLive: true,
        bTestPingHDMS: true,
        bTestParrotHDMS: true,
        bTestManualIP: false,
        HDMSLiveResult: ko.observable(),
        WSResult: ko.observable(),
        PingHDMSResult: ko.observable(),
        ParrotHDMSResult: ko.observable(),
        ManualIPResult: ko.observable(),
        startParrotTest: function() {
            var self = controls.diagnostics;
            self.manualPing = true;
            self.ParrotHDMSResult('<p> Testing HDMS (WS) Parrot connection </p>');
            controllers.request_system.parrot();
        },
        endParrotTest: function(time) {
            var self = controls.diagnostics;
                if (time) {
                    self.ParrotHDMSResult(self.ParrotHDMSResult() + '<p class="text-success">Response time:' + time + 'ms</p>');
                } else {
                    self.ParrotHDMSResult(self.ParrotHDMSResult() + '<p class="text-danger">Request timed out</p>');
                }
        },
        startHDMSLiveTest: function() {
            var self = controls.diagnostics;
            var tStr = '<p> Testing HDMSLive (HTTP) connection  </p>';
            self.HDMSLiveResult(tStr);
            this.ip = 'hdms-live.nightlife.com.au/albums/JR01T13.JPG';
            var _that = this;
            this.h_img = new Image();
            this.h_img.onload = function() {
                _that.pingingUtopia = false;
                clearTimeout(_that.utopiaTimer);
                self.HDMSLiveResult(tStr + ('<p class="text-success">Response time: ' + (new Date().getTime() - _that.start) + 'ms</p>'));
            };
            this.h_img.onerror = function() {
                self.HDMSLiveResult(self.HDMSLiveResult() + '<p class="text-danger"> Response error</p>');
                _that.pingingUtopia = false;
                clearTimeout(_that.utopiaTimer);
            };
            this.start = new Date().getTime();
            this.h_img.src = 'http://' + this.ip + '?cachebreaker=' + new Date().getTime();
            this.utopiaTimer = setTimeout(function() {
                self.HDMSLiveResult(self.HDMSLiveResult() + '<p class="text-danger"> Response timed out</p>');
            }, 5000);
        },
        startWebSocketTest: function() {
            var self = controls.diagnostics;
            var _that = {};
            var tStr = '<p> Testing HDMSLive (WS) connection  </p>';
            self.WSResult(tStr);
            // Ebrahim M: Removed 9090 from the address
            // var ws = new SockJS(NL.settings.utopia.address()); // PENDING            
            var ws = new SockJS(NL.api.socketSettings.address);
              ws.onopen = function(e) {
                self.WSResult('Connected..');
                _that.start = new Date().getTime();
                ws.send('PING ' + _that.start);
              };
              ws.onerror = function(e) {
                self.WSResult(tStr + '<p class="text-danger">Error Connecting</p>');
              };
              ws.onclose = function(e) {
               return;
              };
              ws.onmessage = function(e) {
                self.WSResult(tStr + '<p class="text-success">Response time: ' + (new Date().getTime() - _that.start) + 'ms</p>');
                ws.close();
              };
        },
        testWebSocketLocal : function(systemCandidate, cb) { // Ebrahim M : Added for testing the local systems around 
            // console.log( 'Web Socket Request Sent' , systemCandidate);
            var self = controls.diagnostics;
            var _that = {};
            var tStr = '<p> Testing HDMS connction  directly </p>';
            self.WSResult(tStr);
            systemCandidate.isDetecting(true);
            systemCandidate.localActive = systemCandidate.active;

            if(controls.settings.showOfflineZone()){
                controls.settings.listOfLocalClient.push(systemCandidate);                                 
            }

            var ws = new SockJS('http://' + systemCandidate.local_ip + ':9090/hl');
                ws.onopen = function(e) {
                    console.log( 'who_are_you message Sent' , systemCandidate.local_ip);
                    var msg = new SocketMessageModel(NL.settings.utopia, 'control', 'who_are_you', {});
                    ws.send(JSON.stringify(msg));
                };
                ws.onerror = function(e) {
                    self.WSResult(tStr + '<p class="text-danger">Error Connecting</p>');
                    console.log(tStr + '<p class="text-danger">Error Connecting</p>');
                    //cb(true, null)
                };
                ws.onclose = function(e) {                    
                    if(JSON.stringify(e.code) == 1002) { // Can not connect to the server
                        // console.log('Closed : ' + e.reason + ': ' + e.code, systemCandidate);
                        // Note the order of the following two instruction is important!
                        
                        systemCandidate.localActive = false;
                        systemCandidate.isDetecting(false);
                     }
                    return;
                };
                ws.onmessage = function(message) {                 
                    var obj = JSON.parse(message.data);
                    if(obj.verb.event == 'who_i_am'){
                        // Note the order of the following two instruction is important!
                        console.log('who_i_am recived' , obj);
                        systemCandidate.localActive = true;
                        systemCandidate.isDetecting(false);                       
                        if (!controls.settings.showOfflineZone()){
                           controls.settings.listOfLocalClient.push(systemCandidate);
                        }

                        systemCandidate.version = obj.actor.version.substring(0,4);
                    }
                    else{
                        // Note the order of the following two instruction is important!
                        systemCandidate.localActive = false;
                        systemCandidate.isDetecting(false);
                    }
                    //cb(null, {})
                    ws.close();                
                };
        },
        discoverLocal : function(ipAddr , zoneNo , cbFunc){           
            var self = controls.diagnostics;
            var _that = {};
            var tStr = '<p> Discovering HDMS connction  directly </p>';
            self.WSResult(tStr);
            var ws = new SockJS('http://' + ipAddr + ':9090/hl');
                ws.onopen = function(e) {
                    var msg = new SocketMessageModel(NL.settings.utopia, 'control', 'who_are_you', {});
                    ws.send(JSON.stringify(msg));
                };
                ws.onerror = function(e) {
                    self.WSResult(tStr + '<p class="text-danger">Error Connecting</p>');
                    console.log(tStr + '<p class="text-danger">Error Connecting</p>');
                    ws.close();
                };
                ws.onclose = function(e) {
                   // console.log('Closed : ' + e.reason + ': ' + e.code + ':' + zoneNo , e);
                    return;
                };
                ws.onmessage = function(message) {                 
                    var obj = JSON.parse(message.data);
                    if(obj.verb.event == 'who_i_am'){                        
                        console.log('who_i_am recived : ' +  ipAddr + ':' + zoneNo, obj);                        
                        
                         var msg = new SocketMessageModel(NL.settings.utopia, 'request_zone', 'selection_set', {});
                            msg.target = {system:obj.actor.system , zone:zoneNo};
                         ws.send(JSON.stringify(msg));
                         //console.log('selection_set sent : Zone ' + zoneNo ,  msg);    
                    }
                    else{
                        if(obj.verb.event == 'selection_set'){
                            console.log('selection_set recived : ' +  ipAddr + ':' + obj.actor.zone, obj);                            
                            controls.settings.discoveredZones.push({zoneIp:ipAddr, 
                                                                    systemName: obj.actor.system, 
                                                                    zoneNo:obj.actor.zone });
                        }

                        console.log('Closed for : ' +  ipAddr + ':' + zoneNo, obj);
                        ws.close();             
                    }
                };
        },
        startManualIPTest: function() {
            var self = controls.diagnostics;
            this.ip = self.manualIP();
            var tStr = '<p> Testing ' + this.ip + ' (HTTP) connection </p>';
            self.ManualIPResult(tStr);
            var _that = this;
            this.m_img = new Image();
            this.m_img.onload = function() {
                self.ManualIPResult(tStr + ('<p class="text-success">Response time: ' + new Date().getTime() - _that.start + 'ms'));
                clearTimeout(_that.manualTimer);
            };
            this.m_img.onerror = function() {
                //   alert(_that.start);
                self.ManualIPResult(tStr + ('<p class="text-success"> Response time: ' + '' + (new Date().getTime() - _that.start) + 'ms'));
                clearTimeout(_that.manualTimer);
            };
            this.start = new Date().getTime();
            this.m_img.src = 'http://' + this.ip + '/?cachebreaker=' + new Date().getTime();
            this.manualTimer = setTimeout(function() {
                self.GoogleResult(tStr + '<p class="text-danger"> Response timed out</p>');
            },
                    NL.settings.respTimeout());
        },
        startPingTest: function() {
            var self = controls.diagnostics;
            self.manualPing = true;
            self.PingHDMSResult('<p> Testing HDMS (WS) connection </p>');
            controllers.websocket.ping();
        },
        endPingTest: function(time) {
            var self = controls.diagnostics;
            clearTimeout(self.pingTimer);
            timeoutCount = 0;
            self.pingTimer = undefined;
            if(self.manualPing){
                if (time) {
                    self.PingHDMSResult(self.PingHDMSResult() + '<p class="text-success">Response time:' + time + 'ms</p>');
                } else {
                    self.PingHDMSResult(self.PingHDMSResult() + '<p class="text-danger">Request timed out</p>');
                }
                self.manualPing = false;
            } else {
                  log.event('auto ping: ', time);
            }
        },
        startAutoPing: function() {
            var self = controls.diagnostics;        
            if (NL.settings.pingInterval() > 0) {
                clearInterval(self.autoTimer);
                self.autoTimer = setInterval(function() {                    
                    log.event('auto ping: ');
                    clearTimeout(self.pingTimer);
                    controllers.websocket.ping();
                    self.pingTimer = setTimeout(function() {

                        self.timeoutCount += 1;

                        if (NL.settings.autoDisconnect() && self.timeoutCount > NL.settings.maxPings() ) {
                            clearInterval(self.autoTimer);
                            controls.settings.disconnect(null, {reason: 'Ping Timeout', error: 'Ping Timeout'});
                        }
                    }, NL.settings.respTimeout());
                }, NL.settings.pingInterval());
            }
        },
        stopAutoPing: function() {
            var self = controls.diagnostics;
            self.timeoutCount = 0;
            clearInterval(self.autoTimer);
            clearInterval(self.pingTimer);
        },
        runTests: function() {
            var self = controls.diagnostics;
            self.HDMSLiveResult('');
            self.WSResult('');
            self.PingHDMSResult('');
            self.ParrotHDMSResult('');
            if (self.bTestHDMSLive) {
                self.startHDMSLiveTest();
            }
            if (self.bTestWS) {
                self.startWebSocketTest();
            }
            if (self.bTestPingHDMS && NL.settings.systemStatus() === 'Connected') {
                self.startPingTest();
            }
            if (self.bTestParrotHDMS && NL.settings.systemStatus() === 'Connected') {
                self.startParrotTest();
            }
            if (self.bTestManualIP) {
                self.startManualIPTest();
            }
        },
        toggleDiagnosticsPanel: function() {
             if ($('#diagnostics-body').css('display') === 'none' ) {
                $('#diagnostics-body').css('display','block');
            } else {
                $('#diagnostics-body').css('display','none');
            }
        }
    }
});