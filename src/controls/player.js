$.extend(true, controls, {
  player: {
    playNext: function(){      
      if( NL.settings.utopia.canIUse('request_zone','mix',true)() ){
        controls.newNotification.initNotif('Oops, Not enough access control level for \'Mix\' button', 5000);
      }
      else{    
        controllers.request_zone.mix();
      }
    },
    animate: function() {
      var model = NL.nowPlaying();

      if (win.tickInterval) {
        win.clearInterval(win.tickInterval);
      }

      // initialise the width so paused songs get an initial width
      var progressBar = $('.now-playing .progress .progress-bar');
      progressBar.stop(true, true);
      progressBar.css('width', model.progress() + '%');
      
      if (model.playing) {
        if(NL.settings.animationEnabled()) {
            win.tickInterval = setInterval(function() {
              
              model.tick();
            }, NL.settings.tickInterval());
            progressBar.animate({
              'width': '100%'
            }, model.remaining(), 'linear');
        } else {
            win.tickInterval = setInterval(function() {
              model.tick();
              progressBar.css('width', model.progress() + '%');
            }, NL.settings.tickInterval());
         }
      }
    },
    pause: function(){
      if( NL.settings.utopia.canIUse('request_zone','halt',true)() ){
        if (NL.settings.utopia.canIUse('request_zone','halt',true , true)() ){
          controls.newNotification.initNotif('Oops, Not enough access control level for \'Pause\' button.', 5000);          
        }
        else{
          controls.newNotification.initNotif('You need onsite access code for executing \'Pause\' button.' + 
                                              '<BR> Please open \'Settings/ On Site Access\' ', 8000);
        }
      }
      else{
        controllers.request_zone.haltNow();
        $('.now-playing .progress .progress-bar').stop(true, false);
        win.clearInterval(win.tickInterval);
      }
    },
    pauseNext: function(){      
      if( NL.settings.utopia.canIUse('request_zone','halt',true)() ){
        if (NL.settings.utopia.canIUse('request_zone','halt',true , true)() ){
          controls.newNotification.initNotif('Oops, Not enough access control level for \'Pause After Song\' button.', 5000);
        }
        else{
          controls.newNotification.initNotif('You need onsite access code for executing \'Pause After Song\' button.' + 
                                              '<BR> Please open \'Settings/ On Site Access\' ', 8000);
        }
      }
      else{ 
        controllers.request_zone.haltEnd();
      }
    }, 
   resume: function(){
      // Ebrahim M : Modified the function due to the bug in 3.92 for resuming function.
      // I just followed MMNL mobile app source code (hdms_manager\fragment\PlayerFragment.java - Resume button)
      if( NL.settings.utopia.canIUse('request_zone','resume',true)() ){
        controls.newNotification.initNotif('Oops, Not enough access control level for \'Resume\' button.', 5000);
      }
      else{      
        if( ( !NL.zone.system_mode.djManual() && !NL.zone.system_mode.karaoke() ) || ( NL.zone.version_num() >= 3.93 ) ){
          if(NL.zone.pause() !== '') {
            controllers.request_zone.resume();
            controls.player.animate();
          }
        }
        else{
          controllers.request_zone.play(NL.nowPlaying().filename , 'now');
        }
      }
    },
    recue: function(){
      if( NL.settings.utopia.canIUse('request_zone','recue',true)() ){
        controls.newNotification.initNotif('Oops, Not enough access control level for \'Recue\' button.', 5000);
      }
      else{
        controllers.request_zone.recue();
      }
    },
    stop: function(){
      
      if( NL.settings.utopia.canIUse('request_zone','stop',true)() ){
        controls.newNotification.initNotif('Oops, Not enough access control level for \'Stop\' button.', 5000);
      }
      else{    
        controllers.request_zone.stop();
      }
    }
  }
});