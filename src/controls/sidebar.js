$.extend(true, controls, {
  sidebar: {
    show: function(templateName, model, cssClass, cb){
     
      var wrapper = $('.sidebar-' + templateName);
      cssClass = cssClass || model.cssClass || '';
      
      ko.applyBindingsToNode(wrapper[0], {
        template: 'layout-sidebar'
      }, {
        templateName: templateName,
        model: model,
        cssClass: cssClass
      });

      NL.advertising.sidebarVisible(true);
      
      return(wrapper);
      
    },

    hide: function(templateName){
      var wrapper;
      if(templateName === undefined) {
        wrapper = $('.modal.in');
      } else {
        wrapper = $('#container-'+templateName);
      }
      wrapper.modal('hide');
      NL.context.modal('');
    }
  }
});