$.extend(true, controls, {
	modes : {
		init : function(e) {
		},
		toggleScheduler : function(v, e) {			
			NL.zone.system_mode.scheduler(!NL.zone.system_mode.scheduler());
		},
		toggleKaraoke : function(v, e) {
			NL.zone.system_mode.karaoke(!NL.zone.system_mode.karaoke());
		},
		toggleDjManual : function(v, e) {
			NL.zone.system_mode.djManual(!NL.zone.system_mode.djManual());
		},
		toggleRandom : function(v, e) {
			if( !NL.zone.system_mode.random() ){
				controls.navigation.jumpToVisuals();
			}			
			NL.zone.system_mode.random(!NL.zone.system_mode.random());	
		},
		toggleBingo : function(v, e) {
			if( !NL.zone.system_mode.bingo() ){
				controls.navigation.jumpToVisuals();
			}
			NL.zone.system_mode.bingo(!NL.zone.system_mode.bingo());
		},
		toggleJuke : function(v,e) {
			NL.zone.system_mode.juke(!NL.zone.system_mode.juke());	
		},
		callback : function() {
			return;
		},
		isAvail : function(mode) {
			switch(mode) {
				case  'scheduler' :
					return NL.zone.availModes.primary.indexOf('scheduler_on') >= 0 && 
							NL.zone.availModes.primary.indexOf('scheduler_off') >= 0;
				case 'karaoke' :
					return NL.zone.availModes.primary.indexOf('karaoke_mc') >= 0;
				case 'random_number' :
					return NL.zone.availModes.additive.indexOf('random_number') >= 0;
				case 'bingo' :
					return NL.zone.availModes.additive.indexOf('bingo') >= 0;
				case 'jukebox' :
					return NL.zone.availModes.juke.indexOf('juke_on') >= 0;
				default :
					return false; 
			}
		},
		toggleJukeFreePlay : function() {
			var juke = NL.zone.juke();
			juke.free_play = !juke.free_play;
			controllers.request_system.set_jukebox_freeplay(juke.free_play);					
		},
		addJukeCredit : function() {
			if( NL.zone.system_mode.juke() ){
				controllers.request_system.add_jukebox_credit(1);
				controls.notification.show('Jukebox Credit Sent');				
			}
		},
		toggleCrowdDJ:function(){
			var appModeTemp = NL.zone.appMode();

			if( appModeTemp == 'credits'){
				appModeTemp = 'off';
			}
			else{
				appModeTemp = 'credits';
			}					
			controllers.request_zone.set_app_mode(appModeTemp);								
		},
		togglecrowdDJFreePlay : function(){			
			var creditsMaxTemp = NL.zone.creditsMax();
			var creditRefillTemp = NL.zone.creditRefill();
			//var freePlayModeTemp = NL.zone.freePlayMode();

			if(creditRefillTemp === 0){			
				controllers.request_zone.set_credit_limits(creditsMaxTemp , 30 * 60); // 60 Mins
			}
			else{				
				controllers.request_zone.set_credit_limits(creditsMaxTemp , 0);
			}
			
		},
		
		// Added by Ebrahim M 
		sendCrowdDJCredit : function () {			
			controllers.request_zone.reset_credits();					
			controls.notification.show('crowdDJ Credit Sent');
		},
		saveCrowdDJCredits : function() {
			var creditRefillTemp = NL.zone.creditRefill();
			var creditsMaxTemp = NL.zone.creditsMax();
			controllers.request_zone.set_credit_limits(creditsMaxTemp , creditRefillTemp);

		},
		togglecrowdDJExpand : function(data, event){				
			collapsed = $('#crowddj-credits-detail').toggle();			
			var modeHeight = ($('#modes').height() - 65);
			modeHeight += 'px';			
			$('#slider-volume')[0].style.height = modeHeight;
			
			/* collapsed = $(event.currentTarget.parentElement).find('#crowddj-credits-detail').css('display');
			collapsed = $('#myModal').removeClass('hide');
			collapsed = $(event.currentTarget.parentElement).find('#myModal').css('display');
			alert(collapsed);
			if(collapsed === 'none' ) { 
			    $(event.currentTarget.parentElement).find('.settings-accordian').css('display','block'); //show child of clicked
			} else {
			    $(event.currentTarget.parentElement).find('.settings-accordian').css('display','none');
			}*/
		}
	}
});

$doc.one('nightlife-ready', function() {  
  //controls.modes.init();  
}); 