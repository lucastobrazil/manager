

KeyboardJS.on('esc', function() {
   controls.modal.hide();
});

KeyboardJS.on('ctrl + 0', function(){

});

KeyboardJS.on('alt + left', function() {
    var index = null;

    var prev = $('.tabs-container').find('li.active').prev().find('a');
    if(prev) {
        prev.click();
    }
    return false;
});

KeyboardJS.on('alt + right', function() {    
    var next = $('.tabs-container').find('li.active').next().find('a');
    if(next != []) {
        next.click();
    }
    return false;
});


KeyboardJS.on('alt + v', function() {
    controls.volume.toggle();
    return false;
});

KeyboardJS.on('alt + up', function() {
        controls.volume.setLevel(NL.volume.level()+1);
        return false;
});

KeyboardJS.on('alt + down', function() {
        controls.volume.setLevel(NL.volume.level()-1);
        return false;
});


/** Song functions **/
KeyboardJS.on('enter', function() {
    if( NL.context.tab() === '#tab-playlist' ) {
        controls.playlist.openSelection();
        return false;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    // Ebrahim M : Added to handle 'Enter' Event in advertising page
    if( (NL.context.tab() === '#tab-advertising') && NL.advertising.user.sidebarVisible() ) {
        if(NL.advertising.user.selectedAssets().length > 0){
            controls.advertising.editAsset(NL.advertising.user.selectedAssets()[0] , {});            
            return false;
        }
    }

    if( (NL.context.tab() === '#tab-advertising') && NL.advertising.priority.sidebarVisible() ) {
        if(NL.advertising.priority.selectedAssets().length > 0){
            controls.advertising.editAsset(NL.advertising.priority.selectedAssets()[0] , {});            
            return false;
        }
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////

    return true;
});

KeyboardJS.on('up', function() {
    if(NL.context.modal() === ''){
        switch (NL.context.tab()){
            case '#tab-playlist':
                controls.playlist.changeSelection('up');
                break;
            case '#tab-advertising':
                if( NL.textScrolling.scrollingTextVisible() ){
                    controls.scrollingText.changeSelection('up');
                }
                else{
                    controls.advertising.changeSelection('up');
                }
                break;
        }
    }
    else{
        switch(NL.context.modal()) {
            case 'song': 
                controls.playlist.changeSelection('up'); $('#song-prev').click(); 
                break;
            case 'volume': 
                controls.volume.setLevel(NL.volume.level()+1); 
                break;
        }
    }
    return false;
});

KeyboardJS.on('down', function() {
    if(NL.context.modal() === ''){
        switch (NL.context.tab()){
            case '#tab-playlist':
                controls.playlist.changeSelection('down');
                break;
            case '#tab-advertising':
                if( NL.textScrolling.scrollingTextVisible() ){
                    controls.scrollingText.changeSelection('down');
                }
                else{
                    controls.advertising.changeSelection('down');                    
                }
                break;
        }
    }
    else{
        switch(NL.context.modal()) {
            case 'song' : 
                controls.playlist.changeSelection('down'); $('#song-next').click(); 
                break;
            case 'volume' : 
                controls.volume.setLevel(NL.volume.level()-1); 
                break;
        }
    }
    return false;
});

KeyboardJS.on('f3', function() {
    $('#song-play-now').click();
    return false;
});

KeyboardJS.on('f4', function() {
    $('#song-play-next').click();
    return false;
});

KeyboardJS.on('del', function() {
    if(NL.context.modal() === 'song' && NL.context.tab() === '#tab-playlist') {
        $('#song-remove-playlist').click();
        return false;
    }
});