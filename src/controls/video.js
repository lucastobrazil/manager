$.extend(true, controls, {
  video: {
    bingoCols : ko.observable(90),
    newBingoLook : ko.observable(false),
    newRngLook : ko.observable(false),
    displayDrawnNum : ko.observable(false),
    excludeNum : ko.observable(0),
    excludeNumsSel : ko.observableArray(),
    excludeRange : ko.observable({From:0, To: 0}),
    arExcludeRange :ko.observableArray(),
    excludeRangesSel : ko.observableArray(),
    randomBox : ko.observableArray(),
    isRngDemoPattenRunning : ko.observable(false),

    pause: function(output) {
      controllers.request_system.change_advertising_state({
        output: output,
        pause: true
      });

    },
    play: function(output) {
      controllers.request_system.change_advertising_state({
        output: output,
        pause: false
      });

    },
    next: function(output) {
      controllers.request_system.change_video_asset({
        output: output,
        navigate: 'next'
      });
    },
    previous: function(output) {
      controllers.request_system.change_video_asset({
        output: output,
        navigate: 'previous'
      });
    },
    changeMode: function(output, mode) {      
      console.log('########### Mode = ' , mode);
      console.log('########### output = ' , output);
      controllers.request_system.change_video_mode({
        output: output,
        mode: mode
      });
    },
    unlock: function(output) {
      controls.video.changeMode(output, 'AUTO');
      controls.video.removeAdditive();      
    },
    generateRandom: function() {
      var self = controls.video;      
      controllers.request_system.generate_random_number();      
      self.runRngDemoPattern();
    },
    runRngDemoPattern: function(){
      var self = controls.video;
      if( !self.isRngDemoPattenRunning() ){
        self.isRngDemoPattenRunning(true);
        self.randomBox.removeAll();
        NL.videoStates.lastDrawnRngNo(0);
        self.fillRandomBoxes();
      }
    },
    fillRandomBoxes : function(){
      var self = controls.video;
      setTimeout(function(){
        var randNum = Math.floor( (Math.random() * 40) );
        if( self.randomBox().indexOf(randNum) < 0 ){ // If it is not inserted before!
          self.randomBox.push(randNum) ;
        }
        else{
          for(var i=0; i < 40 ; i++){
            randNum = i;
            if(self.randomBox().indexOf(randNum) < 0){
              self.randomBox.push(randNum) ;
              break;
            }
          }
        }
        if(self.randomBox().length < 40){
          self.fillRandomBoxes();
        }
        else{
          NL.videoStates.lastDrawnRngNo(NL.videoStates.lastDrawnRngNoTemp);          
          setTimeout(function () {
            self.isRngDemoPattenRunning(false);
            NL.videoStates.lastDrawnRngNoTemp = 0;
            NL.videoStates.lastDrawnRngNo(-1);
          }, 3000);          
        }
      }, 100);
    },
    isRandomBoxHidden : function(rngBox){
      for(var i = 0; i< controls.video.randomBox().length ; i++){
        if(rngBox === controls.video.randomBox()[i]){
          return true;
        }
      }
      return false;
    },
    /*popupRandom: function() {
      // Ebrahim M: For popping up the RNG window without drawing a randowm number in the screen,
      // we call requestzone\set_system_mode ({additive:'random_number'}).
      // for doing this I have set 'system_mode' to true so it calls the following function
      // 'models\zoneViewModel.js\random()' to call the API.
      NL.zone.system_mode.random(true);
    },*/
    clearDrawnNumbers: function() {
      var self = controls.video;
      controllers.request_system.clear_random_numbers();
      /*setTimeout(function(){
          self.getDrawnRng();
        }, 2000);
      */
    },
    removeAdditive: function() {
      NL.zone.additive('none');
      controllers.request_zone.set_system_mode(NL.zone);
      controls.modal.hide();
      setTimeout(function(){
          controllers.request_system.videostate();
        }, 1000);       
    },
    generateBingo: function() {      
      controllers.request_system.generate_bingo_number();
    },
    getGeneratedBingo: function(){
      controllers.request_system.generated_bingo_number();
    },
    restartBingo: function() {      
      controls.modal.show('dlg-confirm-bingo-restart', {}, '');
    },
    startNewBingoGame: function(){
      controls.modal.hide();
      controllers.request_system.start_bingo_game();

      if( controls.video.newBingoLook() ){
        controls.modal.show('dlg-bingo', {}, '');
      }      
    },
    showBingoModal : function(){
      if( controls.video.newBingoLook() ){
        controls.video.getGeneratedBingo();
        controls.modal.show('dlg-bingo', {}, '');
      }
    },
    isNumDrawn: function(drawnNum){
      var i = 0;
      for(i = 0; i< NL.videoStates.states()[0].generatedBngNumbers().length ; i++){
        if(drawnNum === NL.videoStates.states()[0].generatedBngNumbers()[i] ){
          return true;
        }
      }
      return false;
    },
    getDrawnRng : function(){
      var self = controls.video;

      if( self.newRngLook() && NL.settings.accessCode.verified() ) {
        controllers.request_system.generated_random_numbers();
        controllers.request_system.random_number_config();
      }
    },
    showRngModal : function(){
      var self = controls.video;
      if( self.newRngLook() ) {
        self.getDrawnRng();
        controls.modal.show('dlg-rng', NL.videoStates , '');
      }
    },
    getExcRanges : function(excRanges){      
      var self = controls.video;
      self.arExcludeRange.removeAll();
      if(excRanges !== undefined){
        for(var i = 0; i < excRanges().length ; i++){
          self.arExcludeRange.push('From: ' + excRanges()[i].from + 
                              ' - To: ' + excRanges()[i].to);
        }  
      }      
    },
    addNumber : function(){
      var self = controls.video;      
      if(parseInt(self.excludeNum() , 10) < parseInt(NL.videoStates.objRng().incRangeFrom() , 10) || 
          parseInt(self.excludeNum() , 10) > parseInt(NL.videoStates.objRng().incRangeTo() , 10) ) {
        controls.newNotification.initNotif('Oops, Excluded number must be between (Include Range \'From\' and \'To\').<BR>' + 
                                           'Please fix the input data and try again.', 6000);
        return false;
      }
      NL.videoStates.objRng().excNumbers().push( parseInt(self.excludeNum() , 10) );
      NL.videoStates.objRng().sendRngSettings();
    },
    addRange : function(){
      var self = controls.video;
      if(parseInt(self.excludeRange().From, 10) >= parseInt(self.excludeRange().To, 10)) {
        controls.newNotification.initNotif('Oops, Error in data! (From >= To)<BR>' + 
                                           'Please fix the input data and try again.', 6000);
        return false;
      }
      if( parseInt(self.excludeRange().From, 10) < parseInt(NL.videoStates.objRng().incRangeFrom(), 10) || 
         parseInt(self.excludeRange().From, 10) > parseInt(NL.videoStates.objRng().incRangeTo() , 10) || 
         parseInt(self.excludeRange().To, 10) < parseInt(NL.videoStates.objRng().incRangeFrom() , 10) || 
         parseInt(self.excludeRange().To, 10) > parseInt(NL.videoStates.objRng().incRangeTo(), 10) ){
        controls.newNotification.initNotif('Oops, Excluded range must be between (Include Range \'From\' and \'To\').<BR>' + 
                                           'Please fix the input data and try again.', 6000);
        return false;
      }
      NL.videoStates.objRng().excRanges().push({ from: parseInt(self.excludeRange().From, 10),
                                                   to: parseInt(self.excludeRange().To, 10) });
      NL.videoStates.objRng().sendRngSettings();
    },
    removeExcNum: function (aExcNums, aIndex){
      var self = controls.video;
      if(NL.videoStates.objRng() !== undefined){        
          NL.videoStates.objRng().excNumbers.remove(function(item){
            return item === aExcNums[aIndex];
          });          
          if(aIndex > 0 ){
            self.removeExcNum(aExcNums, (aIndex  - 1));
          }
      }
    },
    excNumKeyPressed : function(data,event){
      var self = controls.video;
      if(event.keyCode === 46){ // Delete button is pressed
        self.removeExcNum( self.excludeNumsSel() , (self.excludeNumsSel().length - 1) );
      }
    },
    removeExcRange: function (aExcRanges, aIndex){
      var self = controls.video;
      if(NL.videoStates.objRng() !== undefined){
          var FromStartIdx = aExcRanges[aIndex].indexOf(':',0) + 1;          
          var FromEndIdx = aExcRanges[aIndex].indexOf(' -',0) ;
          var ToStartIdx = aExcRanges[aIndex].indexOf(':', FromEndIdx) + 1;
          var ToEndIdx = aExcRanges[aIndex].length ;

          var From  = aExcRanges[aIndex].substr(FromStartIdx, (FromEndIdx - FromStartIdx));
          var To  = aExcRanges[aIndex].substr(ToStartIdx, ( ToEndIdx - ToStartIdx) ) ;
          NL.videoStates.objRng().excRanges.remove(function(item){
            return  ( (item.from === parseInt(From , 10)) && (item.to === parseInt(To , 10)) );
          });          
          if(aIndex > 0 ){
            self.removeExcRange(aExcRanges, (aIndex  - 1));
          }          
      }
    },
    excRangeKeyPressed : function(data,event){
      var self = controls.video;
      if(event.keyCode === 46){ // Delete button is pressed
        self.removeExcRange(  self.excludeRangesSel() , (self.excludeRangesSel().length - 1) );
      }
    }
  }
});